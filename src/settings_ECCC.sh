# Compilers and tools
. r.load.dot /fs/ssm/eccc/mrd/rpn/code-tools/ENV/cdt-1.6.5/ECCC/inteloneapi-2022.1.2

# RPN (needs to be defined after compilers)
. r.load.dot mrd/rpn/libs/20230906
. r.load.dot mrd/rpn/utils/20230906

# CMOE
. ssmuse-sh -d eccc/cmo/cmoe/eer/20230925

# CMDS
. ssmuse-sh -d eccc/cmd/cmds/apps/SPI/20230922

# Env variables
export GEO_PATH=/home/sdfo000/sitestore7/hnas/geo
