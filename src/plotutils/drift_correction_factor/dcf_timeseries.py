"""
Drift correction factor time series plot
=========================================
:Author: Jennifer Holden
:Created: 2022-05-20
"""

import cartopy.crs as ccrs
import matplotlib
import matplotlib.pyplot as plt
import os
import sys
from cartopy.mpl.ticker import (LongitudeFormatter, LatitudeFormatter)

from driftutils import cli
from driftutils import utils
from plotutils import map_functions as mapfuncs

matplotlib.use('Agg')
logger = utils.logger


def plot_uv(ds, ax, var):
    """
    plot ueast and vnorth velocities from drift correction factor outputs

    Parameters
    ----------
    ds : xr.Dataset
        Dataset containing drift correction factor output. The variables
        time, <var>_ocean, <var>_drifter must be present, where <var>
        is either ueast or vnorth. If <var>_atmos is present, that data
        is also used when plotting.
    ax : Axes
        Matplotlib axes where the plot will be added.
    var : str
        String specifying which variables will be plotted. This must be
        either 'ueast' or 'vnorth'.
    """

    ax.plot(ds.time.values, ds[var + '_drifter'].values, label='drifter')
    ax.plot(ds.time.values, ds[var + '_ocean'].values, label='ocean')

    if var == 'ueast':
        ax.set_ylabel('Eastward \n velocity [m/s]')
    elif var == 'vnorth':
        ax.set_ylabel('Northward \n velocity [m/s]')
    else:
        logger.info('Unrecognized variable (must be either ueast or vnorth)')

    ax.grid(axis='x')
    ax.tick_params(axis='x', rotation=45)

    if 'ueast_atmos' in ds.keys():

        ax2 = ax.twinx()
        ax2.plot(ds.time.values,
                 ds[var + '_atmos'].values,
                 'C2',
                 label='atmos')

        if var == 'ueast':
            ax2.set_ylabel('Eastward atmos \n velocity [m/s]')
        else:
            ax2.set_ylabel('Northward atmos \n velocity [m/s]')

        ax2.grid(axis='x')
        ax2.tick_params(axis='x', rotation=45)

    return ax


def plot_speeds(ds, ax):
    """
    plot speeds calculated from drift correction factor outputs

    Parameters
    ----------
    ds : xr.Dataset
        Dataset containing drift correction factor output. The variables
        time, ueast_ocean, vnorth_ocean, ueast_drifter, and vnorth_drifter
        must be present in the dataset. If ueast_atmos and vnorth_atmos
        is present, atmospheric speed is also plotted.
    ax : Axes
        Matplotlib axes where the plot will be added.
    """

    ax.plot(ds.time.values, ds.speed_drifter.values, label='drifter speed')
    ax.plot(ds.time.values, ds.speed_ocean.values, label='ocean speed')

    ax.set_ylabel('Speed [m/s]')
    ax.grid(axis='x')
    ax.tick_params(axis='x', rotation=45)
    handles, labels = ax.get_legend_handles_labels()

    if 'ueast_atmos' in ds.keys():

        ax2 = ax.twinx()
        ax2.plot(ds.time.values,
                 ds.speed_atmos.values,
                 'C2',
                 label='atmos speed')

        handles2, labels2 = ax2.get_legend_handles_labels()
        handles = handles + handles2
        labels = labels + labels2

        ax2.set_ylabel('Atmos speed [m/s]')
        ax2.grid(axis='x')
        ax2.tick_params(axis='x', rotation=45)

    return (ax, handles, labels)


def plot_track(ds, ax, dataproj=ccrs.PlateCarree()):
    """
    plot drifter track from drift correction factor outputs

    Parameters
    ----------
    ds : xr.Dataset
        Dataset containing drift correction factor output. The variables
        time, lat and lon must be present as well as the global variable
        obs_buoyid.
    ax : Axes
        Matplotlib axes where the plot will be added.
    dataproj : cartopy.crs.Projection
        Cartopy projection to use for plotting and mapped data
        transformations. Default is set to Plate Carree.
    """

    rcParams = matplotlib.rcParams

    # add a green start dot
    ax.scatter(ds.lon.values[0],
               ds.lat.values[0],
               color='lime',
               edgecolor='green',
               transform=dataproj,
               zorder=3)

    # add the tracks
    ax.plot(ds.lon.values,
            ds.lat.values,
            transform=dataproj,
            zorder=1)

    # set the aspect ratio to produce a square map
    ax.set_aspect('equal', adjustable='datalim', anchor='C')
    plt.draw()

    # set the extent to the same as the one used when
    # setting the aspect ratio to square. This ensures
    # that any islands not included in the data extent
    # will still appear in the plot extent.
    bbox_aspect = list(ax.get_xlim()) + list(ax.get_ylim())
    ax.set_extent(bbox_aspect, crs=ccrs.PlateCarree())

    # add the land
    mapfuncs.plot_land_mask(ax, bbox_aspect)

    # draw parallels and meridians
    ax.gridlines(
        crs=ccrs.PlateCarree(),
        draw_labels=['bottom', 'left'],
        xformatter=LongitudeFormatter(),
        xlabel_style={'rotation': 45, 'ha': 'center',
                      'size': rcParams['axes.labelsize']},
        yformatter=LatitudeFormatter(),
        ylabel_style={'rotation': 45, 'ha': 'center',
                      'size': rcParams['axes.labelsize']}
    )


def plot_timeseries(*, ds, plot_dir, style='default'):
    """
    Creates a timeseries plot for each drifter. The plot will show the
    drifter track, as well as east and north velocities, and speeds for
    ocean, drifter and atmos (if present).

    Parameters
    ----------
    ds : xr.Dataset
        xarray dataset containing output from a drift correction factor
        experiment (required)
    plot_dir : str
        full path to folder where plots will be generated (required)
    style : str
        string representing the plot style that will be used when creating
        the plot. By default, this parameter is set to use Matplotlibs
        built in default parameters. Alternately, users can provide one of
        Matplotlibs other available styles or else provide a path to a
        custom mplstyle style sheet created by the user.
    """

    # Assume that the user is ok with the data in plot_dir being overwritten.
    if plot_dir:
        if not os.path.exists(plot_dir):
            os.mkdir(plot_dir)
    else:
        sys.exit('...failed to create timeseries plot - the user must '
                 + 'provide plot_dir (full path to the directory where '
                 + 'the plots will be generated)')

    # do not attempt to create the timeseries plot for data with only
    # one measurement.
    if len(ds.alpha_real.values) < 2:
        logger.info('...failed to create timeseries plot - drift correction '
                    + 'factor output only contains one measurement.')
        return

    logger.info('...creating timeseries plot')

    # apply a style for the upcoming plot
    with plt.style.context(style):
        rcParams = matplotlib.rcParams
        # begin to set up the figure - aspect ratio 0.75
        figsize = rcParams['figure.figsize']
        figsize[-1] = 0.75*figsize[0]
        fig = plt.figure(figsize=figsize)
        gs = fig.add_gridspec(3, 2)
        buoyid = ds.obs_buoyid

        # First add the bottom subplot (ie, the speeds). This will provide
        # an axis to use for sharing the x axis with the other subplots as
        # well as the handles for the common legend.
        ax3 = fig.add_subplot(gs[2, 1])
        ax_leg, handles, labels = plot_speeds(ds, ax3)

        # Add the subplots for ueast and vnorth. Remove the x axis tick
        # labels since they will be shared with ax3
        ax1 = fig.add_subplot(gs[0, 1], sharex=ax3)
        plot_uv(ds, ax1, 'ueast')
        plt.setp(ax1.get_xticklabels(), visible=False)
        ax2 = fig.add_subplot(gs[1, 1], sharex=ax3)
        plot_uv(ds, ax2, 'vnorth')
        plt.setp(ax2.get_xticklabels(), visible=False)

        # Tidy up the plot
        fig.tight_layout(w_pad=2, h_pad=0.5)
        fig.align_ylabels()

        # add a legend to the unused subplot. Only include the data from
        # one of the subplots since the legend should contain at most 3 lines.
        labels = ['Drifter', 'Ocean', 'Atmosphere']
        ax4 = plt.subplot(gs[0, 0])
        ax4.legend(
            handles,
            labels,
            loc="center",
            borderaxespad=0.1,
            title=(str(buoyid) + '\n'),
            title_fontsize=rcParams['axes.labelsize']
        )
        ax4.patch.set_visible(False)
        ax4.axis('off')

        # add the drifter track
        ax0 = fig.add_subplot(gs[1:, 0], projection=ccrs.PlateCarree())
        plot_track(ds, ax0)

        # save the figure
        figname = 'dcf_timeseries_{}.png'.format(buoyid)
        fig.savefig(os.path.join(plot_dir, figname), bbox_inches='tight')

        plt.close('all')


def main():

    cli.run(plot_timeseries)

    utils.shutdown_logging()


if __name__ == '__main__':

    main()
