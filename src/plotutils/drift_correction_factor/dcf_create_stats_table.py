"""
Calculate statistics for drift correction factor output and write the
resulting data to csv and netcdf files
==============================================
:Author: Jennifer Holden
:Created: 2022-08, 2022
"""

from scipy.stats import pearsonr
import pandas as pd
import xarray as xr
import numpy as np
import fileinput
import os

from driftutils import utils
from driftutils import cli

logger = utils.logger

# The following metadata is used to add attributes to the netcdf output
# as well as to later provide more verbose column headers for the csv output
VARIABLE_METADATA = {
    # ratios
    'VDR': {
        'standard_name': 'velocity_difference_ratio',
        'long_name': 'Velocity Difference Ratio',
        'description': ('the velocity difference ratio (VDR) defined as the '
                        + 'ratio of the sum of the squared magnitudes of the '
                        + 'vector velocity differences to the sum of the '
                        + 'squared magnitudes of the observed velocities. '
                        + 'VDR = sum(|Vm - Vo|^2) / sum(|Vo|^2)')
    },
    'SDR': {
        'standard_name': 'speed_difference_ratio',
        'long_name': 'Speed Difference Ratio',
        'description': ('the speed difference ratio (SDR) defined as the '
                        + 'ratio of the sum of the squared speed differences '
                        + 'to the sum of the squared magnitudes of the '
                        + 'observed velocities. '
                        + 'SDR = sum(|Vm|-|Vo|)^2 / sum(|Vo|^2)')
    },
    # mean, std, and R of speeds
    'mean_drifter_speed': {
        'standard_name': 'mean_speed_drifter',
        'long_name': 'Average Drifter Speed',
        'units': 'm/s',
        'description': 'mean of the observed current speed'
    },
    'drifter_speed_std': {
        'standard_name': 'std_speed_drifter',
        'long_name': 'Standard Deviation of the Drifter Speed',
        'units': 'm/s',
        'description': 'standard deviation of the observed current speed'
    },
    'mean_ocean_speed': {
        'standard_name': 'mean_speed_ocean',
        'long_name': 'Average Ocean Model Speed',
        'units': 'm/s',
        'description': 'mean of the ocean model current speed'
    },
    'ocean_speed_std': {
        'standard_name': 'std_speed_ocean',
        'long_name': 'Standard Deviation of the Ocean Model Speed',
        'units': 'm/s',
        'description': 'standard deviation of the ocean model current speed'
    },
    'R_speed': {
        'standard_name': 'correlation_coefficient_of_speed',
        'long_name': ('Pearson correlation coefficient between the model '
                      + 'and observation speeds'),
        'description': ('Pearson correlation coefficient between the model '
                        + 'and observation speeds')
    },
    # vector velocity difference
    'mean_VVD': {
        'standard_name': 'mean_vector_velocity_difference',
        'long_name': 'Average vector velocity difference',
        'units': 'm/s',
        'description': ('mean of the magnitude of the vector velocity '
                        + 'difference between the observed and model '
                        + 'velocities')
    },
    'VVD_std': {
        'standard_name': 'std_vector_velocity_difference',
        'long_name': ('Standard deviation of the magnitude of the vector '
                      + 'velocity difference'),
        'units': 'm/s',
        'description': ('standard deviation of the magnitude of the vector '
                        + 'velocity difference between the observed and '
                        + 'model velocities')
    },
    # difference angle
    'mean_difference_angle': {
        'standard_name': 'mean_difference_angle',
        'long_name': 'Average difference angle',
        'units': 'degrees',
        'description': ('mean of the difference angle between the observed '
                        + 'and model velocities')
    },
    'difference_angle_std': {
        'standard_name': 'std_difference_angle',
        'long_name': 'Standard deviation of the difference angle',
        'units': 'degrees',
        'description': ('standard deviation of the difference angle between '
                        + 'the observed and model velocities')
    },
    'R_bearings': {
        'standard_name': 'correlation_coefficient_of_bearings',
        'long_name': ('Pearson correlation coefficient between the model and '
                      + 'observation bearings'),
        'description': ('Pearson correlation coefficient between the model '
                        + 'and observation bearings')
    },
    # correlation coefficient of velocity components
    'R': {
        'standard_name': 'correlation_coefficient_of_combined_vel_comp',
        'long_name': ('Pearson correlation coefficient between the model and '
                      + 'observational velocity components'),
        'description': ('Pearson correlation coefficient between the model and'
                        + ' observational combined (northward plus eastward) '
                        + 'velocity components')
    },
    'R_vnorth': {
        'standard_name': 'correlation_coefficient_of_mod_obs_vel_vnorth_comp',
        'long_name': ('Pearson correlation coefficient between the model and '
                      + 'observational northward velocity components'),
        'description': ('Pearson correlation coefficient (R) between the model'
                        + ' and observational northward velocity components')
    },
    'R_ueast': {
        'standard_name': 'correlation_coefficient_of_model_obs_vel_ueast_comp',
        'long_name': ('Pearson correlation coefficient between the model and '
                      + 'observational eastward velocity components'),
        'description': ('Pearson correlation coefficient (R) between the model'
                        + ' and observational eastward velocity components')
    },
    # data set description
    'num_records': {
        'standard_name': 'num_records',
        'long_name': 'Number of measurements in sample',
        'description': 'number of measurements recorded per drifter'
    },
}

########################################################################
# functions used to calculate the various stats
########################################################################


def magnitude(x, y):
    """Calculate the magnitude of a vector"""
    return np.sqrt(x**2 + y**2)


def calculate_bearing(east, north):
    """Calculate the bearing of a vector"""
    radians = np.arctan2(east, north)
    degrees = radians * 180. / np.pi
    degrees[degrees < 0] = degrees[degrees < 0] + 360
    return degrees


def vector_velocity_difference(ds):
    """the magnitude of the difference vector between observed and modeled
    velocities calculated as magnitude(ds.ueast_ocean - ds.ueast_drifter,
    ds.vnorth_ocean - ds.vnorth_drifter)"""
    return ds.speed_ocean_vdiff.values


def difference_angle(ds):
    """The difference angle (DA) is the magnitude of the difference in
    direction of the observed and modeled velocities. It is calculated as
    abs(ds.bearing_ocean_error), where ds.bearing_ocean_error =
    (ds.bearing_ocean - ds.bearing_drifter). In addition,
    ds.bearing_ocean_error has been normalized by subtracting
    360 from values greater than 180 and adding 360 for values less
    than -180. """
    return abs(ds.bearing_ocean_error.values)


def calculate_VDR(ds):
    """
    Calculates the velocity difference ratio (VDR) defined as the ratio of
    the sum of the squared magnitudes of the vector velocity differences
    to the sum of the squared magnitudes of the observed velocities.
    VDR = sum(|Vm - Vo|^2) / sum(|Vo|^2)

    Parameters
    ----------
    ds : xr.Dataset
        Xarray dataset containing drift correction factor output
    """
    # the sum of the squared magnitudes of the vector velocity differences
    # np.sum(magnitude(ds.ueast_ocean - ds.ueast_drifter,
    # ds.vnorth_ocean - ds.vnorth_drifter) ** 2)
    top = np.nansum(vector_velocity_difference(ds) ** 2)

    # the sum of the squared magnitudes of the observed velocities
    # np.sum(ds.speed_drifter ** 2)
    bot = np.nansum(ds.speed_drifter.values ** 2)

    # return the ratio
    return top / bot


def calculate_SDR(ds):
    """
    Calculates the speed difference ratio (SDR) defined as the ratio of
    the sum of the squared speed differences to the sum of the squared
    magnitudes of the observed velocities.
    SDR = sum(|Vm|-|Vo|)^2 / sum(|Vo|^2)

    Parameters
    ----------
    ds : xr.Dataset
        Xarray dataset containing drift correction factor output
    """
    # the sum of the squared speed differences
    # np.sum((ds.speed_ocean - ds.speed_drifter) ** 2)
    top = np.nansum((ds.speed_ocean_error.values) ** 2)

    # the sum of the squared magnitudes of the observed velocities
    # np.sum(ds.speed_drifter**2)
    bot = np.nansum(ds.speed_drifter.values ** 2)

    # return the ratio
    return top / bot


def calculate_correlation_coefficient(d1, d2):
    """
    Pearson correlation coefficient calculated as the covariance of two
    variables divided by the product of the standard deviation of each
    data sample.  R = covariance(X, Y) / (stdv(X) * stdv(Y))

    Parameters
    ----------
    d1 :
        Data sample with a Gaussian or Gaussian-like distribution.
    d2 :
        Data sample with a Gaussian or Gaussian-like distribution.
        Must have the same length as data1.
    Returns
    -------
    R : float
       Pearson correlation coefficient. The coefficient is a
       value between -1 and 1 with a value of 0 indicating
       no correlation. Values below -0.5 or above 0.5 indicate
       notable correlations.
    """

    if len(d1) != len(d2):
        logger.info('correlation coefficient can only be calculated '
                    + 'using data samples of equal length')
    try:
        R, _ = pearsonr(d1, d2)
    except ValueError:
        logger.warn("WARNING: Correlation correlation coefficent "
                    "could not be calculated. Setting to NaN.")
        R = np.nan
    return R


def R_bearings(ds):
    """ Calculates the correlation coefficient between the model and
    observed bearings """
    Rbearings = calculate_correlation_coefficient(
        ds.bearing_ocean.values,
        ds.bearing_drifter.values
    )
    return Rbearings


def R_speed(ds):
    """ Calculates the correlation coefficient between the model and
    observed speeds """
    Rspeed = calculate_correlation_coefficient(
        ds.speed_ocean.values,
        ds.speed_drifter.values
    )
    return Rspeed


def R_separate_vel_components(ds, direction):
    """ Calculates the correlation coefficient for the ueast and vnorth
    velocity components """
    R_separate = calculate_correlation_coefficient(
        ds['{}_ocean'.format(direction)].values,
        ds['{}_drifter'.format(direction)].values
    )
    return R_separate


def R_combined_vel_components(ds):
    """ Calculates the correlation coefficient for the combined ueast
    and vnorth velocity components """
    R_combined = calculate_correlation_coefficient(
        list(ds.ueast_ocean.values) + list(ds.vnorth_ocean.values),
        list(ds.ueast_drifter.values) + list(ds.vnorth_drifter.values)
    )
    return R_combined


########################################################################
# functions used to create the data table and write out the csv and
# netcdf files.
########################################################################

def reorder_columns(df):
    """reorders the columns in a dcf output stats table so that the
    columns are more nicely presented"""

    return df[['buoyid', 'num_records', 'mean_drifter_speed',
               'drifter_speed_std', 'mean_ocean_speed', 'ocean_speed_std',
               'R_speed', 'SDR', 'VDR', 'R', 'R_vnorth', 'R_ueast', 'mean_VVD',
               'VVD_std', 'mean_difference_angle', 'difference_angle_std',
               'R_bearings']]


def new_header_line(metadata_dict):
    """Helper script that creates a one line pandas dataframe to add
    to the data before writing it out to the csv file

    Parameters
    ----------
    metadata_dict : dict
        Dictionary containing metadata related to the the dcf output.
        It is expected that at least some entries in the dictionary
        will contain 'units' and 'long_name' information
    """

    # Create a more descriptive name by combining the long_name and units
    # information from the metadata dictionary:
    cols = [vname for vname in metadata_dict.keys()]
    units = [(' (' + str(metadata_dict[col]['units']) + ')')
             if 'units' in metadata_dict[col] else ''
             for col in cols]
    lname = [str(metadata_dict[col]['long_name'])
             if 'long_name' in metadata_dict[col] else ''
             for col in cols]
    desc_name = [''.join([p[0], p[1]]) for p in zip(lname, units)]

    # create a one line pandas dataframe that can be concatenated
    # with the original data to provide some extra information in the csv
    linedict = {'buoyid': 'unique drifter identifier'}
    for col in cols:
        linedict[col] = desc_name[cols.index(col)]
    linedf = pd.DataFrame(linedict, index=[0])

    # Adjust the column order so that the resulting table is nicely ordered
    linedf = reorder_columns(linedf)

    return linedf


def flatten_dataset(ds):
    """Since we are most often using a concatenated dataset as input,
    flatten the data so that the stats functions will work regardless
    of whether the input data contains a single drifter or multiple"""
    dims = ds.dims
    flat = ds.stack(record=dims.keys())
    flat = flat.dropna(dim='record')
    return flat


def create_datadict(ds):

    # flatten the data variables to handle the case where
    # the dataset contains data for all the drifters
    ds = flatten_dataset(ds)
    # add the various stats to a dictionary using the
    # appropriate functions:
    datadict = {
        'num_records': [len(ds.lat.values)],
        'mean_drifter_speed': [np.nanmean(ds.speed_drifter.values)],
        'drifter_speed_std': [np.nanstd(ds.speed_drifter.values)],
        'mean_ocean_speed': [np.nanmean(ds.speed_ocean.values)],
        'ocean_speed_std': [np.nanstd(ds.speed_ocean.values)],
        'R_speed': [R_speed(ds)],
        'SDR': [calculate_SDR(ds)],
        'VDR': [calculate_VDR(ds)],
        'R': [R_combined_vel_components(ds)],
        'R_vnorth': [R_separate_vel_components(ds, 'vnorth')],
        'R_ueast': [R_separate_vel_components(ds, 'ueast')],
        'mean_VVD': [np.nanmean(vector_velocity_difference(ds))],
        'VVD_std': [np.nanstd(vector_velocity_difference(ds))],
        'mean_difference_angle': [np.nanmean(difference_angle(ds))],
        'difference_angle_std': [np.nanstd(difference_angle(ds))],
        'R_bearings': [R_bearings(ds)],
    }

    return datadict


def create_stats_dataframe(ds):
    """Create a dataframe containing the data defined in create_datadict()"""

    if 'buoyid' not in ds.dims:
        if 'buoyid' not in list(ds.dims.keys()):
            ds = ds.assign_coords(buoyid=ds.obs_buoyid)
            ds = ds.expand_dims('buoyid')
        ind = str(ds.buoyid.values)
        df = pd.DataFrame(data=create_datadict(ds), index=[ind])
    else:
        # add the global data from the concatenated dataset
        df = pd.DataFrame(data=create_datadict(ds), index=['entire_dataset'])

        # then add the data for each individual dataframe
        for buoy in list(ds.buoyid.values):
            dsindv = ds.sel(buoyid=buoy)
            indvdf = pd.DataFrame(data=create_datadict(dsindv), index=[buoy])
            df = pd.concat([df, indvdf])

    return df


def prepend_text(filename, text):
    """ helper function that prepends a line of text to a previously
    generated text file """
    with fileinput.input(filename, inplace=True) as file:
        for line in file:
            if file.isfirstline():
                print(text)
            print(line, end="")


def stats_to_netcdf(df, ncname):
    """ create a netcdf file from the stats data """
    # create a dataset from the pandas dataframe
    df.insert(loc=0, column='buoyid', value=df.index)
    df = df.reset_index(drop=True)
    df = df.set_index('buoyid')
    ds = df.to_xarray()

    # Then actually add the new variable attributes:
    for var in VARIABLE_METADATA.keys():
        if var in ds.keys():
            for attr in VARIABLE_METADATA[var].keys():
                ds[var].attrs[attr] = VARIABLE_METADATA[var][attr]

    # add some global attributes
    desc_str = ('This file contains statistics calculated using output from '
                + 'the drift correction factor module of Fisheries and Oceans '
                + 'Canada\'s Drift Workflow Tool (https://gitlab.com/dfo-drift'
                + '-projection/drift-workflow-tool).')

    citation_str = ('For more details on specific calculations see: Han, G., '
                    + 'Z. Lu, Z. Wang, J. Helbig, N. Chen, and B. de Young '
                    + '(2008), Seasonal variability of the Labrador Current '
                    + 'and shelf circulation off Newfoundland, J. Geophys. '
                    + 'Res., 113, C10013, doi:10.1029/2007JC004376')

    ds.attrs = {'description': desc_str,
                'further_information': citation_str,
                'number_of_drifters_in_set': str(len(ds.buoyid.values))}

    # Then, can also write the data back to a netcdf file
    ds.to_netcdf(ncname, mode='w')


def stats_to_csv(df, csvname):
    """ create a csv file from the stats data for easy viewing """
    # Add a second header line that is more descriptive
    # than the original variable names
    linedf = new_header_line(VARIABLE_METADATA)
    df = pd.concat([linedf, df.iloc[0:]]).reset_index(drop=True)

    # Write the data out to the csv file
    df.to_csv(csvname, index=False)

    # Add some extra header text to the csv output file
    citation_str = ('# For more details on specific calculations see: Seasonal'
                    + ' variability of the Labrador Current and shelf '
                    + 'circulation off Newfoundland '
                    + '(doi:10.1029/2007JC004376)')
    desc_str = ('# This file contains statistics calculated using output from '
                + 'the drift correction factor module of Fisheries and Oceans '
                + 'Canada\'s Drift Workflow Tool (https://gitlab.com/dfo-drift'
                + '-projection/drift-workflow-tool).')
    prepend_text(csvname, '#')
    prepend_text(csvname, citation_str)
    prepend_text(csvname, desc_str)


def create_dcf_stats_tables(*, ds, dir):
    """ Workflow script that compiles stats for drift correction factor
    output and writes the resulting information to csv and netcdf files

    Parameters
    ----------
    ds : xr.Dataset
        Xarray dataset containing drift correction factor output for one
        or more drifters.
    dir : str
        full path to the directory where the csv and netcdf files will be
        written. The output files will be named velocity_error_stats.nc
        and velocity_error_stats.csv
    """
    csvname = os.path.join(dir, 'dcf_velocity_error_stats.csv')
    ncname = os.path.join(dir, 'dcf_velocity_error_stats.nc')

    # calculate the stats
    statsdf = create_stats_dataframe(ds)

    # write to an additional netcdf file for more complete metadata
    stats_to_netcdf(statsdf, ncname)

    # write to a csv file for easy viewing.
    stats_to_csv(statsdf, csvname)

    logger.info('\nsaving csv output to ' + str(csvname))
    logger.info('saving netcdf output to ' + str(ncname))


def main():

    cli.run(create_dcf_stats_tables)
    utils.shutdown_logging()


if __name__ == '__main__':
    main()
