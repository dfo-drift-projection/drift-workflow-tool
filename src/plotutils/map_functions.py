"""
Map plotting functions
================
:Author: Jennifer Holden
:Contributors: Nancy Soontiens
:Created: 2020-07-02
"""

import glob
import os
import sys

import cartopy
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from cartopy.mpl.ticker import (LongitudeFormatter, LatitudeFormatter)
import cmocean
from geopy.distance import distance
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmap
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches
from matplotlib.pylab import cm as cmlab
import numpy as np
import pandas as pd
import xarray as xr

from driftutils import utils
from plotutils import plotting_utils as putils
from plotutils import scalebar as sbar

logger = utils.logger

matplotlib.use('Agg')

# The following code sets a default path for shapefiles used by the plotting.
# If this is unset, the Cartopy package will attempt to download the data
# from http://www.naturalearthdata.com/downloads/ instead, which is not
# possible to do as part of a submitted job.
cartopy.config['pre_existing_data_dir'] = cartopy.config['data_dir']
gshhs_data_path = os.path.join(cartopy.config['pre_existing_data_dir'],
                               'shapefiles/gshhs/')

LatLonBoundingBox = putils.LatLonBoundingBox


def return_map_extremes(drifterdict, include_persistence=False):
    """Return the minimum and maximum latitudes and longitudes for a
    set of drifter tracks

    Parameters
    ----------
    drifterdf : pd.DataFrame
        pandas dataframe containing data for a single drifter.
    include_persistence : boolean
        boolean determining if persistence tracks will be included in
        the map extremes values (persistence tracks are not taken into
        account if the default value of False is set).

    Returns
    -------
    return_dict : dict
        dictionary containing tight spatial extremes as well as the full
        set of latitudes and longitudes.
        tight_extremes : list
            list of floats in the form [minlon, maxlon, minlat, maxlat]
        all_lats : list of floats containing all latitudes included
            in the data
        all_lons : list of floats containing all longitudes included
            in the data
    """

    # In the case where the model start points are plotted without the
    # drifter tracks, it is necessary to use the 'initial_lat' and
    # 'initial_lon' instead
    try:
        lats_all_sets = drifterdict['obs_lat'].values.tolist()
        lats_all_sets.extend(drifterdict['mod_lat'].values.tolist())
        lons_all_sets = drifterdict['obs_lon'].values.tolist()
        lons_all_sets.extend(drifterdict['mod_lon'].values.tolist())
        if include_persistence:
            lats_all_sets.extend(drifterdict['pers_lat'].values.tolist())
            lons_all_sets.extend(drifterdict['pers_lon'].values.tolist())
    except KeyError:
        lats_all_sets = drifterdict['initial_lat'].values.tolist()
        lons_all_sets = drifterdict['initial_lon'].values.tolist()

    # find the min and max of the entire set
    minlon = np.nanmin(lons_all_sets)
    maxlon = np.nanmax(lons_all_sets)
    minlat = np.nanmin(lats_all_sets)
    maxlat = np.nanmax(lats_all_sets)

    return_dict = {
        'tight_extremes': [minlon, maxlon, minlat, maxlat],
        'all_lats': lats_all_sets,
        'all_lons': lons_all_sets
    }

    return return_dict


def pad_aspect_ratio(
    map_extremes,
    proj,
    dataproj,
    min_aspect_ratio=None
):
    """
    Fixes the aspect ratio of the plot so that it is more visually pleasing.

    Parameters
    ----------
    map_extremes : list
        list of min and max latitude and longitude values
        ([minlon, maxlon, minlat, maxlat]). This will often represent the
        output from either return_map_extremes() or add_map_extent_buffer()
    proj : cartopy.crs.Projection
        Cartopy projection to use when plotting the data.
    dataproj : cartopy.crs.Projection
        Cartopy projection to use for transforming the data when plotting.
    min_aspect_ratio : float
        value representing the minimum aspect ratio as defined by
        latitude/longitude. A value of 1 means the plot will be square, 0.5
        means the plot will be twice as wide as it is tall, 2 means that
        the plot will be twice as tall as it is wide, etc

    Returns
    -------
    return_dict : list
        list containing spatial extremes to use with mapped plots, The
        returned list has the form [min_lon, max_lon, min_lat, max_lat]
    """

    if min_aspect_ratio is None:
        # don't adjust the padding at all.
        return map_extremes

    if min_aspect_ratio == 'None':
        # don't adjust the padding at all.
        return map_extremes

    # Note: This currently doesn't work with the polar projections
    c1 = proj.transform_point(
        map_extremes[0], map_extremes[2], src_crs=dataproj
    )
    c2 = proj.transform_point(
        map_extremes[1], map_extremes[2], src_crs=dataproj
    )
    c3 = proj.transform_point(
        map_extremes[0], map_extremes[3], src_crs=dataproj
    )
    c4 = proj.transform_point(
        map_extremes[0], map_extremes[3], src_crs=dataproj
    )

    extremes = [
        min(c1[0], c2[0], c3[0], c4[0]),
        max(c1[0], c2[0], c3[0], c4[0]),
        min(c1[1], c2[1], c3[1], c4[1]),
        max(c1[1], c2[1], c3[1], c4[1])
    ]

    aspect = (extremes[3] - extremes[2]) / (extremes[1] - extremes[0])

    # we only need to add padding if the aspect ratio is not already set
    # to the min_aspect_ratio.

    if aspect < min_aspect_ratio:
        # pad latitude (y)
        mid_lat = (extremes[3] + extremes[2]) / 2
        lon_d = (extremes[1] - extremes[0])
        adj_lon_d = lon_d * min_aspect_ratio
        extremes[2] = mid_lat - (adj_lon_d / 2)
        extremes[3] = mid_lat + (adj_lon_d / 2)

    elif aspect > min_aspect_ratio:
        # pad longitude (x)
        mid_lon = (extremes[1] + extremes[0]) / 2
        lat_d = (extremes[3] - extremes[2])
        adj_lat_d = lat_d * min_aspect_ratio
        extremes[0] = mid_lon - (adj_lat_d / 2)
        extremes[1] = mid_lon + (adj_lat_d / 2)

    return extremes


def add_map_extent_buffer(
    minlon, maxlon, minlat, maxlat,
    lon_buff_percent=None,
    lat_buff_percent=None,
):
    """Return the minimum and maximum latitudes and longitudes for a
    given set of drifter tracks with added lat and lon buffer space
    for plotting

    Parameters
    ----------
    minlon : float
        minimum longitude value in all dataframes
    maxlon : float
        maximum longitude value in all dataframes
    minlat : float
        minimum latitude value in all dataframes
    maxlat : float
        maximum latitude value in all dataframes
    lon_buff_percent : float
        multiplier to act as a longitude buffer to
        add white space to plot extents.
    lat_buff_percent :
        multiplier to act as a longitude buffer to
        add white space to plot extents.

    Returns
    -------
    return_dict : dict
        dictionary containing spatial extremes to use with mapped plots
        and a ratio of the latitude and longitude buffer to use when
        determing the aspect ratio for plotting. Spatial extremes have
        latitude and longitude buffers individually applied to add
        physical space around tracks in plots.
        buffered_extremes : list
            list of floats representing the buffered plot extremes
            [plotminlon, plotmaxlon, plotminlat, plotmaxlat]
        buffratio : float
            latitude buffer value divided by longitude buffer value
    """

    if not lat_buff_percent:
        lat_buff_percent = 0.1
    if not lon_buff_percent:
        lon_buff_percent = 0.1

    if maxlat == minlat:
        latbuff = 1 * lat_buff_percent
    else:
        latbuff = (maxlat - minlat) * lat_buff_percent

    if maxlon == minlon:
        lonbuff = 1 * lon_buff_percent
    else:
        lonbuff = (maxlon - minlon) * lon_buff_percent

    # determine a buffratio. Used to decide the aspect ratio of a plot.
    buffratio = latbuff / lonbuff

    # define the buffered extremes
    plotminlon = minlon - lonbuff
    plotmaxlon = maxlon + lonbuff
    plotminlat = minlat - latbuff
    plotmaxlat = maxlat + latbuff

    return_dict = {
        'buffered_extremes': [plotminlon, plotmaxlon, plotminlat, plotmaxlat],
        'buffratio': buffratio,
    }

    return return_dict


def finalize_plot_extremes(
    data=None,
    proj=ccrs.PlateCarree(),
    dataproj=ccrs.PlateCarree(),
    min_aspect_ratio=None,
    lon_buff_percent=None,
    lat_buff_percent=None,
    polygons=None,
    zone='all',
    include_persistence=False
):
    """ Helper script to determine the plot extremes

    Parameters
    ----------
    data : dict
        dictionary containing one or more pandas dataframes for a
        single drifter. Keys are experiment setnames and values are
        dataframes.
    proj : cartopy.crs.Projection
        Cartopy projection to use when plotting the data.
    dataproj : cartopy.crs.Projection
        Cartopy projection to use for transforming the data when plotting.
    min_aspect_ratio : float
        minimum value to use for the aspect ratio (latitude/longitude)
    lon_buff_percent : float
        multiplier to act as a longitude buffer to
        add white space to plot extents.
    lat_buff_percent :
        multiplier to act as a longitude buffer to
        add white space to plot extents.
    polygons : dict
        dictionary containing zone names as keys and polygon area
        coordinates as values
    zone : str
        identifying label for the zone in question. This should represent
        one of the keys present in the polygon definitions file
        defined in the user config file or 'all' if no file is given.
    include_persistence : boolean
        boolean determining if persistence tracks will be included in
        the map extremes values (persistence tracks are not taken into
        account if the default value of False is set).

    Returns
    -------
    return_dict : dict
        dictionary containing spatial extremes to use with mapped plots,
        as well as a ratio of the latitude and longitude buffer to use when
        determing the aspect ratio for plotting and a calculation of the
        diagonal distance between two corners of the plot extent. Spatial
        extremes have latitude and longitude buffers individually applied
        to add physical space around tracks in plots.
        tight_extremes : dict
            map extremes using only the supplied data with no buffer or
            padding added.
            tight_extremes : list
                list of the extreme values [min_lon, max_lon, min_lat, max_lat]
            all_lats : list
                list of the latitude values used to calculate the extremes
            all_lons : list
                list of the longitude values used to calculate the extremes
        buffered_extremes : dict
            map extremes determined by adding a buffer around the tight
            extremes.
            buffered_extremes : list
                list of floats representing the buffered plot extremes
                [minlon, maxlon, minlat, maxlat]
            buffratio : float
                latitude buffer value divided by longitude buffer value
        padded_extremes : list
            list of extremes that have been padded out to achieve the
            min_aspect_ratio if one has been supplied.
        diagonal_dist : float
            diagional distance between two corners of the plot extent.
    """
    returndict = {}

    if isinstance(data, dict):
        data = pd.concat(data.values())
    elif isinstance(data, pd.DataFrame):
        pass
    else:
        return returndict

    # find the geographical extremes across all the experiments
    if polygons:
        tight_extremes = plot_extremes_with_polygons(polygons, data, zone=zone)
    else:
        tight_extremes = return_map_extremes(
            data,
            include_persistence=include_persistence
        )

    returndict['tight_extremes'] = tight_extremes
    minlon, maxlon, minlat, maxlat = tight_extremes['tight_extremes']

    # add a buffer if requested
    returndict['buffered_extremes'] = add_map_extent_buffer(
        minlon, maxlon, minlat, maxlat,
        lon_buff_percent=lon_buff_percent,
        lat_buff_percent=lat_buff_percent
    )

    # pad out the aspect ratio if necessary
    padded_extremes = pad_aspect_ratio(
        returndict['buffered_extremes']['buffered_extremes'],
        proj, dataproj,
        min_aspect_ratio=min_aspect_ratio
    )
    returndict['padded_extremes'] = padded_extremes

    # calculate the diagonal distance
    lr = (padded_extremes[2], padded_extremes[1])
    ul = (padded_extremes[3], padded_extremes[0])
    returndict['diagonal_dist'] = distance(ul, lr).m

    return returndict


def plot_extremes_with_polygons(
    polygons,
    drifterdf,
    zone='all',
):
    """Return the minimum and maximum latitudes and longitudes for a
    given set of drifter tracks with added lat and lon buffer space
    for plotting

    Parameters
    ----------
    polygons : dict
        dictionary containing zone names as keys and polygon area
        coordinates as values
    drifterdf : pd.dataFrame
        pandas dataframe containing drifter track information for a single
        buoyid.
    zone : str
        identifying label for the zone in question. This should represent
        one of the keys present in the polygon definitions file
        defined in the user config file or 'all' if no file is given.

    Returns
    -------
    coord_dict : dict
        dictionary containing spatial extremes to use with mapped plots.
        map_extremes : list
            list of floats representing the buffered plot extremes
            [plotminlon, plotmaxlon, plotminlat, plotmaxlat]
        all_lats : list
            list of all the latitudes in the given dataframe
        all_lons : list
            list of all the latitudes in the given dataframe
    """

    # find the coordinates if using 'all'
    if zone == 'all':
        coords_pairs = []
        if polygons:
            for k in polygons['polygon_coords'].keys():
                coords_pairs.extend(polygons['polygon_coords'][k])
    else:
        coords_pairs = polygons['polygon_coords'][zone]

    plats = []
    plons = []
    if polygons:
        for pair in coords_pairs:
            plats.append(pair[0])
            plons.append(pair[1])

    if 'initial_lat' in drifterdf.keys():
        all_lats = list(drifterdf['initial_lat'].values) + list(plats)
        all_lons = list(drifterdf['initial_lon'].values) + list(plons)
    else:
        all_lats = list(drifterdf['mod_start_lat'].values) + list(plats)
        all_lons = list(drifterdf['mod_start_lon'].values) + list(plons)

    minlon = np.nanmin(all_lons)
    maxlon = np.nanmax(all_lons)
    minlat = np.nanmin(all_lats)
    maxlat = np.nanmax(all_lats)

    returndict = {
        'tight_extremes': [minlon, maxlon, minlat, maxlat],
        'all_lats': all_lats,
        'all_lons': all_lons,
    }

    return returndict


def shift_legend(ax, labels, handles, leg_fontsize):
    """ helper function to move the legend to the outside of the plot area

    Parameters
    ----------
    ax : Axes
        Axis to use for plotting
    labels : list
        list of strings to use as the legend labels
    handles : list
        list of plot handles to assign to each label
    """

    # Put a legend to the right of the current axis
    ax.legend(
        handles,
        labels,
        loc='center left',
        bbox_to_anchor=(1, 0.5),
        fontsize=leg_fontsize
    )


def create_tracks_dataframe(
    datatype='dir',
    datadir=None,
    given_dslist=None,
    drifterid=None
):
    """Return a dataframe representation of all drifter track data for
    a given set of drifter data.

    Parameters
    ----------
    datatype : str
        string representing the type of input data that will be
        included ('dir' or 'dataset').
    datadir : str
        full path to directory where input files are stored.
    given_dslist : list
        list of datasets containing output from a drift tool experiment.
    drifterid : str
        optional argument representing a unique buoy id number.

    Returns
    -------
    alltracksdict : dict
        dictionary containing all information for the tracks to be plotted.
        key values are experiment setnames and values are pandas dataframe.
    """

    alltracksdict = {}

    if datatype not in ['dir', 'dataset']:
        sys.exit('unrecognized data type in create_tracks_dataframe')

    # figure out which drifters to add to the tracks list
    if drifterid is not None:
        buoyids = [drifterid]
    else:
        if datatype == 'dataset':
            buoyids = []
            for elem in given_dslist:
                buoyids.append(elem.obs_buoyid)
        else:
            buoyids = utils.grab_buoyids_from_attrs(datadir)

    # decide on a list of datasets to use
    if datatype == 'dataset':
        dslist = filter(lambda e: e.obs_buoyid in buoyids, given_dslist)

    elif datatype == 'dir':
        dslist = []
        for buoyid in buoyids:
            # this assumes only one file with that filename.
            namestr = ('*' + buoyid + '*.nc')
            for fname in glob.glob(os.path.join(datadir, namestr)):
                with xr.open_dataset(fname) as ds:
                    dslist.append(ds)

    for ds in dslist:
        buoyid = ds.obs_buoyid
        vars_to_keep = [
            var for var in ds.data_vars if 'DEPTH' not in ds[var].dims
        ]

        if 'setname' in ds.dims:

            buoydf = ds[vars_to_keep].to_dataframe()

            for setname in ds.setname.values:

                df = buoydf.iloc[
                    buoydf.index.get_level_values('setname') == setname
                ].copy()

                df.reset_index(level='setname', drop=True, inplace=True)

                df.insert(0, 'buoyid', buoyid, True)

                initial_lats = []
                initial_lons = []
                initial_starts = []
                for m in ds.model_run.values:
                    listlen = len(ds.timestep.values)
                    dsel = ds.sel(model_run=m, setname=setname)
                    initial_lats.extend(
                        [dsel.mod_start_lat.values] * listlen
                    )
                    initial_lons.extend(
                        [dsel.mod_start_lon.values] * listlen
                    )
                    initial_starts.extend(
                        [dsel.mod_start_date.values] * listlen
                    )
                df.insert(3, 'initial_lat', initial_lats, True)
                df.insert(4, 'initial_lon', initial_lons, True)
                df.insert(5, 'initial_date', initial_starts, True)

                # add the dataframe to a list of dataframes
                if setname in alltracksdict.keys():
                    alltracksdict[setname].append(df)
                else:
                    alltracksdict[setname] = [df]

        else:
            # use ds.ocean_model as the setname
            setname = ds.ocean_model
            df = ds[vars_to_keep].to_dataframe()
            df.insert(0, 'buoyid', buoyid, True)
            initial_lats = []
            initial_lons = []
            initial_starts = []

            for m in ds.model_run.values:
                listlen = len(ds.timestep.values)
                initial_lats.extend([ds.mod_start_lat.values[m]] * listlen)
                initial_lons.extend([ds.mod_start_lon.values[m]] * listlen)
                initial_starts.extend([ds.mod_start_date.values[m]] * listlen)
            df.insert(3, 'initial_lat', initial_lats, True)
            df.insert(4, 'initial_lon', initial_lons, True)
            df.insert(5, 'initial_date', initial_starts, True)

            # add the dataframe to a list of dataframes
            if setname in alltracksdict.keys():
                alltracksdict[setname].append(df)
            else:
                alltracksdict[setname] = [df]

    # combine all the lists of dataframes into one dataframe so that the
    # output is one large dataframe
    for key, value in alltracksdict.items():
        alltracksdict[key] = pd.concat(value)

    return alltracksdict


def compute_score_per_leadtime(df_all, skill, plotdur):
    """ Gather data at a particular plot leadtime

    Parameters
    ----------
    df_all : pandas.DataFrame
        Pandas DataFrame containing data for all drifters in an
        experiment. Data should be binned by hour.
    skill : str
        skill score values that should be returned
    plotdur : int
        value representing the time at which to make the plot

    Returns
    -------
    df_lead : pandas.DataFrame
        pandas dataframe with data at the given lead time only
    lat : list
        list of mean values of latitude grouped by buoyid and
        mod_start_date.
    lon : list
        list of mean values of longitude grouped by buoyid and
        mod_start_date.
    score : list
        list of mean values of skill score grouped by buoyid and
        mod_start_date.
    """

    df_lead = df_all.loc[df_all['hours since start'] == plotdur]
    lat = df_lead.groupby(
        ['buoyid', 'mod_start_date']
    )['initial_lat'].mean().values
    lon = df_lead.groupby(
        ['buoyid', 'mod_start_date']
    )['initial_lon'].mean().values
    score = df_lead.groupby(
        ['buoyid', 'mod_start_date']
    )[skill].mean().values

    # Pad out the score when there aren't enough data points
    score_pad = np.zeros_like(lat)
    score_pad[:] = np.nan
    score_pad[:score.shape[0]] = score
    score = score_pad

    return (df_lead, lat, lon, score)


def plot_polygons(
    polygons,
    ax=None,
    dataproj=ccrs.PlateCarree(),
    show_legend=False,
    zones=None,
    zcolors=None,
    polygon_border_only=False,
    lwidth=0.5,
    lstyle='-'
):
    """ plot model start positions and overlay region polygons

    Parameters
    ----------
    polygons : dict
        python dictionary containing keys 'depth' and 'polygon_coords'.
        The value of polygon['polygon_coords'] is itself a dictionary
        with keys representing the labels for each zone. The values of
        this dictionary are lists containing pairs of coordinates
        representing vertices of the polygon area.
    ax : Axes
        Axis to use for plotting
    dataproj : cartopy.crs.Projection
        Cartopy projection to use for transforming the data when plotting.
    show_legend : boolean
        True value adds a plot legend
    zones : list
        list of strings representing the analysis zones
    zcolors : dict
        dictionary containing zone names as keys and colors as values.
    polygon_border_only : boolean
        value allowing plotting of filled polygon zones (default, True)
        or border only (False)
    lwidth : float
        width of polygon border
    lstyle : str
        line style for polygon border
    """

    # if there are any polygons to plot
    if polygons:
        lat_coor = []
        lon_coor = []
        for zone in polygons['polygon_coords'].keys():
            plist = polygons['polygon_coords'][zone]
            for pair in plist:
                lat_coor.append(pair[0])
                lon_coor.append(pair[1])

        all_regions_map = {
            name: [(poly[i][1], poly[i][0]) for i in range(len(poly))]
            for name, poly in polygons['polygon_coords'].items()
        }

        if isinstance(zones, str):
            zones = [zones]

        if zones[0] == 'all':
            regions_map = all_regions_map
            polygon_labels = list(polygons['polygon_coords'].keys())
            if len(regions_map.keys()) != 1:
                if 'Combined_Polygons_Boundary' in regions_map.keys():
                    del regions_map['Combined_Polygons_Boundary']
                    polygon_labels.remove('Combined_Polygons_Boundary')
        else:
            regions_map = {zone: all_regions_map[zone] for zone in zones}
            polygon_labels = [zone]

        if not zcolors:
            zcolors = putils.assign_colors(list(regions_map.keys()), None)

        edge_color = 'black'
        alpha_value = 0.25
        fillval = True

        for name in regions_map.keys():

            if polygon_border_only:
                edge_color = zcolors[name]
                lwidth = 1
                lstyle = '--'
                alpha_value = 1.0
                fillval = False

            poly = patches.Polygon(
                regions_map[name],
                closed=True,
                facecolor=zcolors[name],
                edgecolor=edge_color,
                alpha=alpha_value,
                label=str(zone),
                fill=fillval,
                linewidth=lwidth,
                zorder=100,
                linestyle=lstyle,
                transform=dataproj,
            )
            ax.add_patch(poly)

        if show_legend:
            # Put a legend to the right of the current axis
            ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))


def plot_drifter_tracks(
    df_all,
    plot_parameters,
    dataproj=ccrs.PlateCarree(),
    obs_tracks=False,
    mod_tracks=False,
    pers_tracks=False,
    start_dot=False,
    mod_start_dots=False,
    mod_initial_positions=False,
    drifterid=None,
    landmask_type=None,
    landmask_files=None,
    etopo_file=None,
    ax=None,
    map_extremes=None,
    obs_color=None,
    mod_color=None,
    dot_color=None,
    mod_dot_color=None,
    pers_color=None,
    show_legend=False,
    fig=None,
    add_landmask=False,
    add_gridlines=False,
    restrict_extent=False,
):
    """
    Parameters
    ----------
    df_all : pandas.DataFrame
        pandas dataframe with all the tracks to be plotted
    plot_parameters : dict
        dictionary containing parameters to use when saving plots
    dataproj : cartopy.crs.Projection
        Cartopy projection to use for transforming the data when plotting.
    obs_tracks : boolean
        True value adds observed drifter tracks
    mod_tracks : boolean
        True value adds modelled drifter tracks
    pers_tracks : boolean
        True value adds calculated persistence drifter tracks
    start_dot : boolean
        True value adds dots representing start points for obs tracks
    mod_start_dots : boolean
        True value adds dots representing the first calculated modelled
        position for the modelled drifter track (ex, mod_lat and mod_lon)
    mod_initial_positions : boolean
        True value adds dots representing the initial starting points
        that are used to begin the model projection (mod_start_lat and
        mod_start_lon). They represent the initial starting point for
        the model projection.
    drifterid : str
        optional argument used if plotting single drifter while passing
        in dataframe with more than one buoyid
    landmask_type : str
        optional. If None, default GSHHS landmask is plotted.
    landmask_files : dict
        dictionary containing full paths to landmask files. If None or
        missing, default GSHHS landmask is used. If variable names other
        than standard NEMO conventions are used, landmask_files is a
        nested dictionary instead.
    etopo_file : str
        full path to etopo file. If None, no bathymetry is plotted.
    ax : Axes
        Axis to use for plotting
    map_extremes : list
        minimum and maximum latitudes and longitudes for a set of
        drifter data with an added lat and lon buffer for use with
        plotting
    obs_color : str
        string representing color selection if other than default
    mod_color : str
        string representing color selection if other than default
    dot_color : str
        string representing color selection if other than default
    mod_dot_color : str
        string representing color selection if other than default
    pers_color : str
        string representing color selection if other than default
    show_legend : boolean
        True value adds a plot legend
    fig : figure
        current matplotlib figure
    add_landmask : boolean
        boolean value defining whether or not to plot a landmask
    add_gridlines : boolean
        boolean value defining whether or not to add gridlines.
    restrict_extent : boolean
        bolean value defining whether or not to set the plot extent inside the
        function

    Returns
    -------
    savestr : str
        a unique string representing the chosen plot options.
    """

    if drifterid is not None:
        buoyids = [drifterid]
    else:
        buoyids = np.unique(df_all.buoyid.values)

    trackcounter = 0

    if drifterid is None:
        coordf = df_all.copy()
    else:
        coordf = df_all.loc[df_all['buoyid'] == drifterid]

    if map_extremes:
        bbox = LatLonBoundingBox(
            lon_min=map_extremes[0],
            lat_min=map_extremes[2],
            lon_max=map_extremes[1],
            lat_max=map_extremes[3]
        )
    else:
        lat_coor = []
        lon_coor = []
        if obs_tracks:
            lat_coor.extend(coordf['obs_lat'].tolist())
            lon_coor.extend(coordf['obs_lon'].tolist())
        if mod_tracks:
            lat_coor.extend(coordf['mod_lat'].tolist())
            lon_coor.extend(coordf['mod_lon'].tolist())
        if start_dot:
            lat_coor.extend(coordf['obs_lat'].tolist())
            lon_coor.extend(coordf['obs_lon'].tolist())
        if mod_start_dots:
            lat_coor.extend(coordf['mod_lat'].tolist())
            lon_coor.extend(coordf['mod_lon'].tolist())
        if mod_initial_positions:
            lat_coor.extend(coordf['mod_start_lat'].tolist())
            lon_coor.extend(coordf['mod_start_lon'].tolist())
        if pers_tracks:
            lat_coor.extend(coordf['pers_lat'].tolist())
            lon_coor.extend(coordf['pers_lon'].tolist())

        bbox = LatLonBoundingBox(
            lon_min=np.nanmin(lon_coor),
            lat_min=np.nanmin(lat_coor),
            lon_max=np.nanmax(lon_coor),
            lat_max=np.nanmax(lat_coor)
        )

    # add bathymetery if specified
    if etopo_file is not None:
        plot_bathymetry(
            ax, bbox,
            plot_parameters=plot_parameters,
            bathymetry_file=etopo_file,
            dataproj=dataproj,
            add_contourf=True,
            colorbar_visible=True,
            add_contours=False,
            fig=fig
        )

    if add_landmask:
        plot_land_mask(
            ax,
            bbox,
            dataproj=dataproj,
            landmask_type=landmask_type,
            landmask_files=landmask_files
        )

    if restrict_extent:
        ax.set_extent(map_extremes)

    savestr = ''
    if obs_tracks:
        savestr = savestr + '_obs'
    if mod_tracks:
        savestr = savestr + '_mod'
    if pers_tracks:
        savestr = savestr + '_pers'
    if start_dot:
        savestr = savestr + '_stdot'
    if mod_start_dots:
        savestr = savestr + '_modstdot'
    if mod_initial_positions:
        savestr = savestr + '_origmodstdot'
    savestr = savestr.replace('_', '-')
    savestr = savestr.replace('-', '_', 1)

    if obs_color is None:
        obs_color = 'black'
    if mod_color is None:
        mod_color = 'dimgray'
    if pers_color is None:
        pers_color = 'teal'
    if dot_color is None:
        dot_color = 'green'
    if mod_dot_color is None:
        mod_dot_color = 'blue'

    # to split the tracks if the lon values wrap.
    # from https://stackoverflow.com/questions/2652368
    #   /how-to-detect-a-sign-change-for-elements-in-a-numpy-array
    def get_split_indices(list_to_split):
        asign = np.sign(list_to_split)
        sz = asign == 0
        while sz.any():
            asign[sz] = np.roll(asign, 1)[sz]
            sz = asign == 0
        signchange = ((np.roll(asign, 1) - asign) != 0).astype(int)
        idx = [i for i, x in enumerate(signchange) if x == 1]
        if len(idx) == 0:
            idx = [0]
        return idx

    # from https://stackoverflow.com/questions/1198512
    #   /split-a-list-into-parts-based-on-a-set-of-indexes-in-python
    def partition(alist, indices):
        return [alist[i:j] for i, j in zip([0] + indices, indices + [None])]

    # for each obs drifter
    for buoyid in buoyids:
        trackcounter += 1

        if trackcounter == 1:
            obs_label = 'observed tracks'
            mod_label = 'modelled tracks'
            pers_label = 'persistence'
            st_label = 'obs deployment points'
            mod_st_label = 'first modelled positions'
            mod_init_pos_label = 'mod initial positions'
        else:
            obs_label = '_nolabel_'
            mod_label = '_nolabel_'
            pers_label = '_nolabel_'
            st_label = '_nolabel_'
            mod_st_label = '_nolabel_'
            mod_init_pos_label = '_nolabel_'

        drifterdf = df_all.loc[df_all['buoyid'] == buoyid]

        # create a function that will plot the drifter tracks
        def plot_drifter(
                drifterdf=drifterdf,
                plot_color=None,
                plot_label=None,
                dataproj=ccrs.PlateCarree(),
                lat_label=None,
                lon_label=None):

            count = 0
            for m in np.unique(drifterdf.index.get_level_values('model_run')):
                count += 1
                if count > 1:
                    plot_label = '_nolabel_'
                plotdf = drifterdf[[lat_label, lon_label]].loc[
                    drifterdf.index.get_level_values('model_run') == m
                ].copy()
                plotdf = plotdf.dropna()

                full_lons = plotdf[lon_label].values
                full_lats = plotdf[lat_label].values

                # split the track into parts if necessary.
                idx = get_split_indices(full_lons)
                lon_list = partition(full_lons, idx)
                lat_list = partition(full_lats, idx)

                # plot the individual segments
                for le in range(1, (len(idx) + 1)):
                    lats = lat_list[le]
                    lons = lon_list[le]
                    if lat_label == 'obs_lat':
                        ax.plot(
                            lons,
                            lats,
                            color=plot_color,
                            linewidth=0.5,
                            transform=dataproj,
                            label=plot_label,
                            zorder=3
                        )
                    else:
                        if len(lats) == 0 and len(lons) == 0:
                            # if mlats and mlons are empty, skip the segment.
                            continue

                        if np.nanstd(lons) > 10:
                            logger.info('..strange values found in model '
                                        'track' + str(m) + 'segment number'
                                        + str(le))
                            pass
                        else:
                            ax.plot(
                                lons,
                                lats,
                                color=plot_color,
                                linewidth=0.3,
                                transform=dataproj,
                                label=plot_label,
                                zorder=2
                            )

        # decide which tracks to plot:
        if obs_tracks:
            plot_drifter(
                drifterdf=drifterdf,
                plot_color=obs_color,
                plot_label=obs_label,
                dataproj=dataproj,
                lat_label='obs_lat',
                lon_label='obs_lon')

        if mod_tracks:
            plot_drifter(
                drifterdf=drifterdf,
                plot_color=mod_color,
                plot_label=mod_label,
                dataproj=dataproj,
                lat_label='mod_lat',
                lon_label='mod_lon')

        if pers_tracks:
            plot_drifter(
                drifterdf=drifterdf,
                plot_color=pers_color,
                plot_label=pers_label,
                dataproj=dataproj,
                lat_label='pers_lat',
                lon_label='pers_lon')

        # Add a function for plotting the starting dots
        def plot_drifter_dots(
                drifterdf=drifterdf,
                plot_dot_color=None,
                plot_label=None,
                dataproj=ccrs.PlateCarree(),
                lat_label=None,
                lon_label=None,
                markersize=2.2):

            count = 0
            for m in np.unique(drifterdf.index.get_level_values('model_run')):
                count += 1
                zord = 10
                if count > 1:
                    plot_label = '_nolabel_'

                if lat_label == 'obs_lat':
                    sdlat = drifterdf['obs_lat'].iloc[[0, 0]].values
                    sdlon = drifterdf['obs_lon'].iloc[[0, 0]].values
                    zord = 15
                elif lat_label == 'mod_lat':
                    sdlat = drifterdf[lat_label].loc[
                        drifterdf.index.get_level_values('model_run') == m
                    ].values[0][0]
                    sdlon = drifterdf[lon_label].loc[
                        drifterdf.index.get_level_values('model_run') == m
                    ].values[0][0]
                else:
                    sdlat = drifterdf[lat_label].loc[
                        drifterdf.index.get_level_values('model_run') == m
                    ].values[0]
                    sdlon = drifterdf[lon_label].loc[
                        drifterdf.index.get_level_values('model_run') == m
                    ].values[0]
                ax.plot(
                    sdlon,
                    sdlat,
                    ms=markersize,
                    ls='',
                    marker='o',
                    color=plot_dot_color,
                    linewidth=1,
                    transform=dataproj,
                    label=plot_label,
                    zorder=zord
                )

        # Plot the first calculated modelled position for the
        # modelled drifter track
        if mod_start_dots:
            plot_drifter_dots(
                drifterdf=drifterdf,
                plot_dot_color=mod_dot_color,
                plot_label=mod_st_label,
                dataproj=dataproj,
                lat_label='mod_lat',
                lon_label='mod_lon'
            )

        # Plot the initial starting points that are used to begin
        # the model projection
        if mod_initial_positions:
            plot_drifter_dots(
                drifterdf=drifterdf,
                plot_dot_color=mod_dot_color,
                plot_label=mod_init_pos_label,
                dataproj=dataproj,
                lat_label='mod_start_lat',
                lon_label='mod_start_lon'
            )

        # Plot the first values in the observed track
        if start_dot:
            plot_drifter_dots(
                drifterdf=drifterdf,
                plot_dot_color=dot_color,
                plot_label=st_label,
                dataproj=dataproj,
                lat_label='obs_lat',
                lon_label='obs_lon',
                markersize=3
            )

    # if adding gridlines inside the function instead of separately
    if add_gridlines:
        ax.gridlines(
            crs=dataproj,
            draw_labels=['bottom', 'left'],
            xformatter=LongitudeFormatter(),
            xlabel_style={'rotation': 45, 'ha': 'center'},
            yformatter=LatitudeFormatter(),
            ylabel_style={'rotation': 45, 'ha': 'center'}
        )

    if show_legend:
        # Put a legend to the right of the current axis
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

    if drifterid is not None:
        savestr = drifterid + '_drifter-tracks' + savestr
    else:
        savestr = 'drifter-tracks' + savestr

    if landmask_type is not None:
        savestr = savestr + '_' + landmask_type

    return savestr


def plot_hexbins(
    skill,
    plotdur,
    setname,
    df_all,
    plot_parameters,
    grid_spacing,
    edge_color='black',
    opacity_level=1.0,
    startdate=None,
    enddate=None,
    landmask_type=None,
    landmask_files=None,
    etopo_file=None,
    ax=None,
    map_extremes=None,
    mapstr=None,
    cbar_label=None,
    dataproj=ccrs.PlateCarree(),
    fig=None,
    add_landmask=False,
    add_scale=False,
):
    """
    Create hexbin plots of skill scores

    Parameters
    ----------
    skill : str
        string representing the skill score to be plotted.
    plotdur : int
        integer representing the time at which to gather the data. For
        example, a value of 48 will plot the skill score values at 48h.
    setname : str
        unique identifier for the experiment being plotted. For example,
        'RIOPS'.
    df_all : pandas.DataFrame
        pandas dataframe containing all the data from all the observed
        drifters for a single experiment.
    plot_parameters : dict
        dictionary containing parameters to use when saving plots
    grid_spacing : list
        integer or list of two integers used to determine the hexbin
        size. If no values is given, 80 is used by default. Can be given
        as a single value (ex, 80) or as a list (ex, ['100','100']).
    edge_color : str
        string representing the edge color of the hexbins. If no value
        is given, 'black' is set by default. If no edge color is desired,
        use 'face' instead.
    opacity_level : float
        float representing the opacity of the hexbins (1.0 by default)
    startdate : str
        string representing the start date for the plotted data. If
        given, this is used as part of the default plot title.
    enddate : str
        string representing the end date for the plotted data. If given
        this is used as part of the default plot title.
    landmask_type : str
        optional. If None, default GSHHS landmask is plotted.
    landmask_files : dict
        dictionary containing full paths to landmask files. If None or
        missing, default GSHHS landmask is used. If variable names other
        than standard NEMO conventions are used, landmask_files is a
        nested dictionary instead.
    etopo_file : str
        full path to etopo file. If None, no bathymetry is plotted.
    ax : Axes
        Axis to use for plotting
    map_extremes : list
        minimum and maximum latitudes and longitudes for a set of
        drifter data with an added lat and lon buffer for use with
        plotting
    mapstr : str
        a unique string representing the chosen plot options.
    cbar_label : str
        label to use if adding description to the colorbar
    dataproj : cartopy.crs.Projection
        Cartopy projection to use for transforming the data when plotting.
    fig : figure
        current matplotlib figure
    add_landmask : boolean
        boolean value defining whether or not to plot a landmask
    add_scale : boolean
        boolean value defining whether or not to plot a scalebar

    Returns
    -------
    mapstr : str
        a unique string representing the chosen plot options.

    """

    logger.info('\ncreating hexbin plot of ' + skill + ' for ' + setname
                + ' at ' + str(plotdur) + 'h')

    # only create plot if there are more than one starting point.
    # otherwise the program crashes.
    checklats = df_all['initial_lat'].values.tolist()
    checklons = df_all['initial_lon'].values.tolist()
    starts = zip(checklats, checklons)

    if len(set(starts)) > 1:

        # grab the skill scores at the necessary time
        df_lead, lat_coor, lon_coor, score =\
            compute_score_per_leadtime(df_all, skill, plotdur)

        if map_extremes:
            bbox = LatLonBoundingBox(
                lon_min=map_extremes[0],
                lat_min=map_extremes[2],
                lon_max=map_extremes[1],
                lat_max=map_extremes[3]
            )
        else:
            bbox = LatLonBoundingBox(
                lon_min=np.nanmin(lon_coor),
                lat_min=np.nanmin(lat_coor),
                lon_max=np.nanmax(lon_coor),
                lat_max=np.nanmax(lat_coor)
            )
            map_extremes = [
                bbox.lon_min, bbox.lon_max, bbox.lat_min, bbox.lat_max
            ]

        # add bathymetery if specified
        if etopo_file is not None:
            plot_bathymetry(
                ax, bbox,
                plot_parameters=plot_parameters,
                bathymetry_file=etopo_file,
                dataproj=dataproj,
                add_contourf=True,
                colorbar_visible=True,
                add_contours=False,
                fig=fig
            )

        if add_landmask:
            plot_land_mask(
                ax, bbox,
                dataproj=dataproj,
                landmask_type=landmask_type,
                landmask_files=landmask_files,
                lon_var='nav_lon',
                lat_var='nam_lat',
                tmask_var='tmask'
            )

        if skill in ['sep']:
            vmin_value = np.nanmin(score) * 1e-3
            vmax_value = np.nanmax(score) * 1e-3
            plot_limits_type = 'calculated_limits'
            scorevalues = score * 1e-3
            cbarval_list = np.linspace(
                vmin_value,
                vmax_value,
                6,
                endpoint=True
            )
            colorbarstr = [f"{np.round(x):0.0f} km" for x in cbarval_list]
            skilllabel = 'Average separation distance'
            cmap_value = cmap.get_cmap('cmo.dense', 5)
            if cbar_label:
                colorbarlabel = cbar_label
            else:
                colorbarlabel = skilllabel

        elif skill in ['obsratio', 'modratio']:
            scorevalues = score
            vmin_value = np.nanmin(score)
            vmax_value = np.nanmax(score)
            plot_limits_type = 'default_limits'
            # To calculate the plot limits in a way that removes some
            # outliers use the following instead:
            # v_std = np.nanmean(score) - 2*np.nanstd(score)
            # vmax_value = np.nanmean(score) + 2*np.nanstd(score)
            # vmin_value = np.nanmax([0, v_std])
            # cbarval_list=np.linspace(vmin_value,vmax_value, 6, endpoint=True)
            # colorbarstr = [f"{x:.3g}" for x in cbarval_list]
            if skill == 'obsratio':
                skilllabel = 'Obs disp from origin / Total obs track length'
            if skill == 'modratio':
                skilllabel = 'Mod disp from origin / Total mod track length'
            cmap_value = cmap.get_cmap('cmo.dense')
            if cbar_label:
                colorbarlabel = cbar_label
            else:
                colorbarlabel = ' '.join([skill, 'skill'])

        else:
            vmin_value = 0
            vmax_value = 1.0
            plot_limits_type = 'calculated_limits'
            if (skill == 'molcard') or (skill == 'liu'):
                skillcap = skill.capitalize()
            else:
                skillcap = skill
            if cbar_label:
                colorbarlabel = cbar_label
            else:
                colorbarlabel = ' '.join([skillcap, 'skill'])
            cbarval_list = np.linspace(
                vmin_value,
                vmax_value,
                6,
                endpoint=True
            )
            colorbarstr = ['0', '0.2', '0.4', '0.6', '0.8', '1.0']
            skilllabel = ' '.join([skillcap, 'skill'])
            scorevalues = score
            cmap_value = cmap.get_cmap('cmo.dense', 5)

        if plot_limits_type == 'calculated_limits':

            # To set the plot limits specifically
            sd_bin = ax.hexbin(
                lon_coor, lat_coor,
                C=scorevalues,
                vmin=vmin_value,
                vmax=vmax_value,
                reduce_C_function=np.nanmean,
                linewidths=0.2,
                edgecolor=edge_color,
                gridsize=grid_spacing,
                zorder=100,
                alpha=opacity_level,
                cmap=cmap_value,
                transform=ccrs.PlateCarree(),
                extent=(map_extremes)
            )

            divider = make_axes_locatable(ax)
            cbar_ax = divider.new_horizontal(
                size="5%",
                pad=0.1,
                axes_class=plt.Axes
            )
            fig.add_axes(cbar_ax)
            cbar = plt.colorbar(sd_bin, cax=cbar_ax)
            cbar.set_label(
                colorbarlabel,
                fontsize=plot_parameters['colorbar_label_fontsize']
            )
            cbar.set_ticks(cbarval_list)
            cbar.ax.set_yticklabels(
                colorbarstr,
                fontsize=plot_parameters['colorbar_ticklabel_fontsize']
            )
        else:
            # Using the default limits
            sd_bin = ax.hexbin(
                lon_coor, lat_coor,
                C=scorevalues,
                reduce_C_function=np.nanmean,
                linewidths=0.2,
                edgecolor=edge_color,
                gridsize=grid_spacing,
                zorder=100,
                alpha=opacity_level,
                cmap=cmap_value,
                transform=ccrs.PlateCarree(),
                extent=(map_extremes)
            )
            divider = make_axes_locatable(ax)
            cbar_ax = divider.new_horizontal(
                size="5%",
                pad=0.1,
                axes_class=plt.Axes
            )
            fig.add_axes(cbar_ax)
            cbar = plt.colorbar(sd_bin, cax=cbar_ax)
            cbar.set_label(
                colorbarlabel,
                fontsize=plot_parameters['colorbar_label_fontsize']
            )
            cbar.set_ticks(cbarval_list)
            cbar.ax.set_yticklabels(
                colorbarstr,
                fontsize=plot_parameters['colorbar_ticklabel_fontsize']
            )

        # add a scale bar to the plot. This feature is currently missing
        # from Cartopy, so the most likely candidate for the funtion that
        # will be included has been added to the drift-tool repo as a
        # temporary measure (scalebar.py, which is found in the carotpy
        # pull request: https://github.com/SciTools/cartopy/pull/1728).
        # Once a scalebar feature is included in Cartopy, we should switch
        # to that version instead.
        if add_scale:

            # first, define the place where the bar will appear on the plot
            lat_pos = bbox.lat_min+(bbox.lat_max-bbox.lat_min)/20

            # determine the width of the plot and use it to calculate the
            # length of the scale. This way the scale bar will be
            # approximately 1/10 of the width of the plot area.
            lftpt = (lat_pos, bbox.lon_min)
            rgtpt = (lat_pos, bbox.lon_max)
            plot_width = distance(lftpt, rgtpt).m
            scale_width = plot_width/10

            sbar_len = scale_width
            if sbar_len > 10000:
                sbar_len = round(sbar_len, -4)

            # scale location choices:
            # sbar_loc_center = (0.43, 0.1)
            # sbar_loc_right = (0.85, 0.1)
            sbar_loc_left = (0.0005, 0.1)

            # add the scalebar
            fontsize = plot_parameters['colorbar_ticklabel_fontsize']
            sbar.add_scalebar(ax,
                              bbox_to_anchor=sbar_loc_left,
                              length=sbar_len,
                              ruler_unit='km',
                              max_stripes=1,
                              fontsize=fontsize,
                              frameon=True,
                              ruler_unit_fontsize=fontsize,
                              ruler_fontweight='normal',
                              tick_fontweight='normal',
                              dy=0.005)


        mapstr = '_PlateCarree'
        return mapstr, skilllabel

    else:
        logger.info('WARNING! cannot create hexbin plot because there'
                    ' is only one starting point. Skipping ')

        return None, None


def compute_subtracted_score_per_leadtime(df_all, skill, plotdur):
    """ compile the subtracted skill scores for a given leadtime

    Parameters
    ----------
    df_all : pandas.DataFrame
        dataframe containing the entire dataset (ie, all observed and
        modelled drifters that will be included in the analysis) for all
        times.
    skill : str
        string representing the skill score to be compiled.
    plotdur : int
        integer value representing the time at which to compile the data.
        For example, plotdur=48 will return skill values at 48 hours.

    Returns
    -------
    df_lead : pandas.Dataframe
        Pandas DataFrame containing all data at a given leadtime only.
    """

    df_all.rename(
        columns={
            'initial_lon': 'mod_start_lon',
            'initial_lat': 'mod_start_lat'
        },
        inplace=True
    )

    df_all['buoyid_modcoords'] = (
        df_all['buoyid'] + 'LON' + df_all['mod_start_lon'].astype(str)
        + 'LAT' + df_all['mod_start_lat'].astype(str)
    )
    df_lead = df_all.loc[
        df_all['hours since start'] == plotdur
    ]
    df_lead = df_lead.dropna(subset=[skill])

    # Calculate average scores
    df_all_grouped_tracks = df_lead.groupby(
        ['buoyid_modcoords', 'hours since start']
    )[skill].mean()

    # Determine initial lat and lons = mean is used
    # because all mod_start_lon/mod_start_lat
    # are identical for a buoyid
    lat = df_lead.groupby(
        ['buoyid_modcoords', 'hours since start']
    )['mod_start_lat'].mean().values
    lon = df_lead.groupby(
        ['buoyid_modcoords', 'hours since start']
    )['mod_start_lon'].mean().values

    # Reorganize the data frame
    df_all_grouped_tracks_new = df_all_grouped_tracks.reset_index()
    df_all_grouped_tracks_new['mod_start_lon'] = lon
    df_all_grouped_tracks_new['mod_start_lat'] = lat

    return df_all_grouped_tracks_new


def plot_subtracted_hexbins(
    fig,
    skill,
    plotdur,
    df_all,
    df_all2,
    dataset_labels,
    zone='all',
    grid_spacing=None,
    edge_color='black',
    opacity_level=1.0,
    landmask_type=None,
    landmask_files=None,
    etopo_file=None,
    ax=None,
    logplot=None,
    map_extremes=None,
    mapstr=None,
    plot_parameters=None,
    cbar_label=None,
    dataproj=None,
    add_gridlines=True,
    plot_pairs_label=None,
    add_scale=False,
):
    """ create hexbin plot of subtracted skill scores

    Parameters
    ----------
    fig : figure
        current matplotlib figure
    skill : str
        string representing the skill score to be plotted.
    plotdur : int
        integer representing the time at which to gather the data. For
        example, a value of 48 will plot the skill score values at 48h.
    df_all : pandas.DataFrame
        pandas dataframe containing all the data from all the observed
        drifters for a single experiment.
    df_all2 : pandas.DataFrame
        pandas dataframe containing all the data from all the observed
        drifters for a single experiment.
    dataset_labels : list
        list of setnames for the two experiments being subtracted.
    zone : str
        string representing the label of the user defined polygon area
        in which to plot data. If no sub zones are defined, zone label
        'all' is used and the entire dataset is plotted.
    grid_spacing : list
        integer or list of two integers used to determine the hexbin
        size. If no values is given, 80 is used by default. Can be given
        as a single value (ex, 80) or as a list (ex, ['100','100']).
    edge_color : str
        string representing the edge color of the hexbins. If no value
        is given, 'black' is set by default. If no edge color is desired,
        use 'face' instead.
    opacity_level : float
        float representing the opacity of the hexbins (1.0 by default)
    landmask_type : str
        optional. If None, default GSHHS landmask is plotted
    landmask_files : dict
        dictionary containing full paths to landmask files. If None or
        missing default GSHHS is used. If variable names other than
        standard NEMO conventions are use, landmask_files is a nested
        dictionary instead.
    etopo_file : str
        full path to etopo file. If None, no bathymetry is plotted.
    ax : Axes
        Axis to use for plotting
    logplot : boolean
        boolean value used to determine if the resulting plot will be
        on a log (np.e) or linear scale.
    map_extremes : list
        minimum and maximum latitudes and longitudes for a set of
        drifter data with an added lat and lon buffer for use with
        plotting
    mapstr : str
        unique string representing the plotting parameters used to create
        the plot. This string is useful for defining the output filename.
        Plotting choices in this function append to the mapstr to create
        the subsavestr variable.
    plot_parameters : dict
        dictionary of plotting parameters used when creating the figure.
        Contains keys related to fontsizes, plot aspect ratio, etc
    cbar_label : str
        label to use if adding description to the colorbar
    dataproj : cartopy.crs.Projection
        Cartopy projection to use for transforming the data when plotting.
    add_gridlines : boolean
        boolean value specifying whether or not to add gridlines to the plot.
    plot_pairs_label : str
        identifier for the experiments used in the plot
    add_scale : boolean
        boolean value defining whether or not to plot a scalebar

    Returns
    -------
    subsavestr : str
        unique string representing the plotting parameters used to create
        the plot. This string is useful for defining the output filename.
    """

    if skill != 'sep':
        logger.info('\nComparison hexbin plot for subtracted values '
                    'not realistic for molcard, liu, obsratio and modratio '
                    'skill scores. Skipping plot.')
        return None

    # only create plot if there are more than one starting point.
    # otherwise the program crashes.

    checklats = df_all['initial_lat'].values.tolist()
    checklons = df_all['initial_lon'].values.tolist()
    starts = zip(checklats, checklons)

    checklats2 = df_all['initial_lat'].values.tolist()
    checklons2 = df_all['initial_lon'].values.tolist()
    starts2 = zip(checklats2, checklons2)

    if (len(set(starts)) > 1) and (len(set(starts2)) > 1):

        # the dataframes at this point are full concatonated dataframes with
        # data binned by drifter and by plot dur. This gives a dataframe that
        # still has multiple values for each buoyid, to account for all the
        # times that fall into the hour before plotdur.

        # bin by mod start coordinates/buoyid and hours since start,
        # then subtract.
        df_lead = compute_subtracted_score_per_leadtime(
            df_all,
            skill,
            plotdur
        )
        df_lead2 = compute_subtracted_score_per_leadtime(
            df_all2,
            skill,
            plotdur
        )

        unique_buoymodruns1 = np.unique(df_lead['buoyid_modcoords'].values)
        unique_buoymodruns2 = np.unique(df_lead2['buoyid_modcoords'].values)

        commonids = [
            v for v in unique_buoymodruns1 if v in unique_buoymodruns2
        ]
        uncommonids1 = [
            v for v in unique_buoymodruns1 if v not in unique_buoymodruns2
        ]
        uncommonids2 = [
            v for v in unique_buoymodruns2 if v not in unique_buoymodruns1
        ]
        uncommon_check1 = df_lead.loc[
            df_lead['buoyid_modcoords'].isin(uncommonids1)
        ]
        uncommon_check2 = df_lead.loc[
            df_lead['buoyid_modcoords'].isin(uncommonids2)
        ]
        if uncommon_check1.empty is False:
            sys.exit(
                'All model runs not in the common set. They should be!'
            )
        if uncommon_check2.empty is False:
            sys.exit(
                'All model runs not in the common set. They should be!'
            )

        df_all_common = df_lead.loc[
            df_lead['buoyid_modcoords'].isin(commonids)
        ]
        df_all_common.reset_index(inplace=True)
        df_all2_common = df_lead2.loc[
            df_lead2['buoyid_modcoords'].isin(commonids)
        ]
        df_all2_common.reset_index(inplace=True)

        ulat_check1 = ''.join(
            df_all_common['mod_start_lat'].astype(str).values
        )
        ulat_check2 = ''.join(
            df_all2_common['mod_start_lat'].astype(str).values
        )
        ulon_check1 = ''.join(
            df_all_common['mod_start_lon'].astype(str).values
        )
        ulon_check2 = ''.join(
            df_all2_common['mod_start_lon'].astype(str).values
        )
        buoymodcoords_check1 = ''.join(
            df_all_common['buoyid_modcoords'].astype(str).values
        )
        buoymodcoords_check2 = ''.join(
            df_all2_common['buoyid_modcoords'].astype(str).values
        )

        if (ulat_check1 not in ulat_check2):
            sys.exit('common lat columns are not the same length? why?')
        if (ulon_check1 not in ulon_check2):
            sys.exit('common lon columns are not the same length? why?')
        if (buoymodcoords_check1 not in buoymodcoords_check2):
            sys.exit('common lon columns are not the same length? why?')

        subdf = pd.DataFrame()
        subdf['lat'] = df_all_common['mod_start_lat'].values
        subdf['lon'] = df_all_common['mod_start_lon'].values
        subdf['score1'] = (df_all_common[skill]).astype(float)
        subdf['score2'] = (df_all2_common[skill]).astype(float)
        subdf.dropna(subset=['score1', 'score2'], inplace=True)
        subdf['score'] = subdf['score1'] - subdf['score2']

        sub_lat = subdf['lat'].values
        sub_lon = subdf['lon'].values
        sub_score = subdf['score'].values

        # if the user has not provided a hexbin size, calculate it.
        if not grid_spacing:
            grid_spacing = calculate_hexbin_size(fig, ax, map_extremes)

        subsavestr = do_subtracted_hexbin_plot(
            fig,
            sub_lat,
            sub_lon,
            sub_score,
            plotdur,
            edge_color,
            opacity_level,
            grid_spacing=grid_spacing,
            zone=zone,
            landmask_type=landmask_type,
            landmask_files=landmask_files,
            etopo_file=etopo_file,
            ax=ax,
            logplot=logplot,
            map_extremes=map_extremes,
            plot_parameters=plot_parameters,
            mapstr=mapstr,
            cbar_label=cbar_label,
            dataproj=dataproj,
            add_gridlines=add_gridlines,
            plot_pairs_label=plot_pairs_label,
            add_scale=add_scale
        )

        return subsavestr

    else:
        logger.info('WARNING! cannot create hexbin plot because there '
                    'is only one starting point. Skipping ')


def do_subtracted_hexbin_plot(
    fig,
    lat,
    lon,
    score,
    plotdur,
    edge_color,
    opacity_level,
    grid_spacing=None,
    zone='all',
    landmask_type=None,
    landmask_files=None,
    etopo_file=None,
    ax=None,
    logplot=None,
    map_extremes=None,
    mapstr=None,
    cbar_label=None,
    plot_parameters=None,
    dataproj=None,
    add_gridlines=True,
    plot_pairs_label=None,
    add_scale=False,
):

    """ create hexbin plot of subtracted skill scores

    Parameters
    ----------
    fig : figure
        current matplotlib figure
    lat : list
        list of latitude values at which to plot the hexbin areas
    lon : list
        list of longitude values at which to plot the hexbin areas.
    score : list
        list of skill scores at the appropriate leadtime for each hexbin.
    plotdur : int
        integer representing the time at which to gather the data. For
        example, a value of 48 will plot the skill score values at 48h.
    grid_spacing : list
        integer or list of two integers used to determine the hexbin
        size. If no values is given, 80 is used by default. Can be given
        as a single value (ex, 80) or as a list (ex, ['100','100']).
    edge_color : str
        string representing the edge color of the hexbins. If no value
        is given, 'black' is set by default. If no edge color is desired,
        use 'face' instead.
    opacity_level : float
        float representing the opacity of the hexbins (1.0 by default)
    zone : str
        string representing the label of the user defined polygon area
        in which to plot data. If no sub zones are defined, zone label
        'all' is used and the entire dataset is plotted.
    landmask_type : str
        optional. If None, default GSHHS landmask is plotted
    landmask_files : dict
        dictionary containing full paths to landmask files. If None or
        missing default GSHHS is used. If variable names other than
        standard NEMO conventions are use, landmask_files is a nested
        dictionary instead.
    etopo_file : str
        full path to etopo file. If None, no bathymetry is plotted.
    ax : Axes
        Axis to use for plotting
    logplot : boolean
        boolean value used to determine if the resulting plot will be
        on a log (np.e) or linear scale.
    map_extremes : list
        minimum and maximum latitudes and longitudes for a set of
        drifter data with an added lat and lon buffer for use with
        plotting
    cbar_label : str
        label to use if adding description to the colorbar
    mapstr : str
        unique string representing the plotting parameters used to create
        the plot. This string is useful for defining the output filename.
        Plotting choices in this function append to the mapstr to create
        the subsavestr variable.
    plot_parameters : dict
        dictionary of plotting parameters used when creating the figure.
        Contains keys related to fontsizes, plot aspect ratio, etc
    dataproj : cartopy.crs.Projection
        Cartopy projection to use for transforming the data when plotting.
    add_gridlines : boolean
        boolean value specifying whether or not to add gridlines to the plot
    plot_pairs_label : string
        identifier for the sets used in the plot
    add_scale : boolean
        boolean value defining whether or not to plot a scalebar

    Returns
    -------
    subsavestr : str
        unique string representing the plotting parameters used to create
        the plot. This string is useful for defining the output filename.
    """

    if not dataproj:
        dataproj = ccrs.PlateCarree()

    if not map_extremes:
        map_extremes = [np.nanmin(lon), np.nanmax(lon),
                        np.nanmin(lat), np.nanmax(lat)]

    # if the user has not provided a hexbin size, calculate it
    if not grid_spacing:
        grid_spacing = calculate_hexbin_size(fig, ax, map_extremes)

    ################################
    # plot the bathymetry
    ################################

    # add bathymetery if specified
    if etopo_file is not None:
        plot_bathymetry(
            ax, map_extremes,
            plot_parameters=plot_parameters,
            bathymetry_file=etopo_file,
            dataproj=dataproj,
            add_contourf=True,
            colorbar_visible=True,
            add_contours=False,
            fig=fig
        )
    ################################
    # plot the landmask
    ################################
    plot_land_mask(
        ax, map_extremes,
        dataproj=dataproj,
        landmask_type=landmask_type,
        landmask_files=landmask_files
    )
    ################################
    # set up the values for the cmap
    ################################

    cmap_value = cmocean.cm.balance
    cbar_extreme = max(abs(max(score)), abs(min(score)))
    vmin_value = cbar_extreme * -1e-3
    vmax_value = cbar_extreme * 1e-3
    if cbar_label:
        colorbarlabel = cbar_label
    else:
        colorbarlabel = ''.join([
            'Average separation distance difference (km) \n',
            plot_pairs_label,
        ])
    scorevalues = score * 1e-3

    ################################
    # plot the actual hexbins
    ################################

    # if making a log plot version
    if logplot:
        norm_value = colors.SymLogNorm(
            linthresh=1,
            linscale=1,
            vmin=vmin_value,
            vmax=vmax_value,
            base=np.e
        )

        logs = np.logspace(0, np.log(vmax_value), num=3, base=np.e)
        cbarval_list = list(-1 * logs) + [0] + list(logs)
        cbarval_list = [np.round(x) for x in cbarval_list]
        colorbarstr = [f"{x:0.0f} km" for x in cbarval_list]

        sd_bin = ax.hexbin(
            lon, lat,
            C=scorevalues,
            reduce_C_function=np.nanmean,
            linewidths=0.2,
            edgecolor=edge_color,
            gridsize=grid_spacing,
            zorder=100,
            alpha=opacity_level,
            cmap=cmap_value,
            norm=norm_value,
            transform=ccrs.PlateCarree(),
            extent=(map_extremes)
        )

    else:
        # to make the non-log version:
        cbarval_list = np.linspace(vmin_value, vmax_value, 6, endpoint=True)
        cbarval_list = [np.round(x) for x in cbarval_list]
        colorbarstr = [f"{x:0.0f} km" for x in cbarval_list]

        sd_bin = ax.hexbin(
            lon, lat,
            C=scorevalues,
            vmin=vmin_value,
            vmax=vmax_value,
            reduce_C_function=np.nanmean,
            linewidths=0.2,
            edgecolor=edge_color,
            gridsize=grid_spacing,
            zorder=100,
            alpha=opacity_level,
            cmap=cmap_value,
            transform=ccrs.PlateCarree(),
            extent=(map_extremes)
        )

    #########################
    # add the color bar
    #########################

    divider = make_axes_locatable(ax)
    cbar_ax = divider.new_horizontal(size="5%", pad=0.1, axes_class=plt.Axes)
    fig.add_axes(cbar_ax)
    cbar = fig.colorbar(sd_bin, cax=cbar_ax)
    cbar.set_ticks(ticks=cbarval_list,
                   labels=colorbarstr,
                   fontsize=plot_parameters['colorbar_ticklabel_fontsize'])
    cbar.set_label(
        colorbarlabel,
        fontsize=plot_parameters['colorbar_label_fontsize']
    )
    cbar.ax.minorticks_off()

    #######################
    # if adding gridlines
    #######################

    if add_gridlines:
        ax.gridlines(
            crs=dataproj,
            draw_labels=['bottom', 'left'],
            xformatter=LongitudeFormatter(),
            xlabel_style={'rotation': 45, 'ha': 'center'},
            yformatter=LatitudeFormatter(),
            ylabel_style={'rotation': 45, 'ha': 'center'}
        )

    ######################
    # add scalebar
    ######################

    # add a scale bar to the plot. This feature is currently missing
    # from Cartopy, so the most likely candidate for the funtion that
    # will be included has been added to the drift-tool repo as a
    # temporary measure (scalebar.py, which is found in the carotpy
    # pull request: https://github.com/SciTools/cartopy/pull/1728).
    # Once a scalebar feature is included in Cartopy, we should switch
    # to that version instead.
    if add_scale:

        # first, define the place where the bar will appear on the plot
        lat_pos = map_extremes[2]+(map_extremes[3]-map_extremes[2])/20

        # determine the width of the plot and use it to calculate the
        # length of the scale. This way the scale bar will be
        # approximately 1/10 of the width of the plot area.
        lftpt = (lat_pos, map_extremes[0])
        rgtpt = (lat_pos, map_extremes[1])
        plot_width = distance(lftpt, rgtpt).m
        scale_width = plot_width/10

        sbar_len = scale_width
        if sbar_len > 10000:
            sbar_len = round(sbar_len, -4)

        # scale location choices:
        # sbar_loc_center = (0.43, 0.1)
        # sbar_loc_right = (0.85, 0.1)
        sbar_loc_left = (0.0005, 0.1)

        # add the scalebar
        fontsize = plot_parameters['colorbar_ticklabel_fontsize']
        sbar.add_scalebar(ax,
                          bbox_to_anchor=sbar_loc_left,
                          length=sbar_len,
                          ruler_unit='km',
                          max_stripes=1,
                          fontsize=fontsize,
                          frameon=True,
                          ruler_unit_fontsize=fontsize,
                          ruler_fontweight='normal',
                          tick_fontweight='normal',
                          dy=0.005)


    ###################################################
    # define an info string to use when saving the plot
    ###################################################

    if logplot is True:
        extralogstr = 'log_'
    else:
        extralogstr = 'linear_'

    if mapstr is None:
        mapstr = ''
    subsavestr = ('diff-hexbins_' + extralogstr + str(plotdur) + 'h' + mapstr)

    return subsavestr


def draw_driftmap_parallels_and_meridians(bmap, bbox):
    """
    Helper function for drawing parallels and meridians when plotting
    driftmap data. This should eventually be replaced by calling the
    same map setup functions used with DriftEval

    Parameters
    ----------
    bmap : Basemap
        Basemap object pre-defined in the DriftMap plotting functions.
    bbox : list
        list of coordinates defining the bounding box.
        [minlon, maxlon, minlat, maxlat]
    """

    minlon = bbox[0]
    maxlon = bbox[1]
    minlat = bbox[2]
    maxlat = bbox[3]

    if maxlon - minlon < 6:
        merfmt = '%.1f'
        merrotamt = 0
        rangestep = (maxlon - minlon) / 4
        shiftstep = rangestep / 2
        merrange = np.arange(
            minlon + shiftstep, maxlon + shiftstep, rangestep)
    else:
        merfmt = '%.0f'
        merrotamt = 0
        rangestep = (np.floor(maxlon) - np.ceil(minlon)) / 6
        shiftstep = rangestep / 2
        fmerrange = np.arange(
            np.ceil(minlon - shiftstep),
            np.floor(maxlon - shiftstep),
            rangestep
        )
        merrange = np.append(fmerrange, [fmerrange[-1] + rangestep])

    if maxlat - minlat < 6:
        parfmt = '%.1f'
        parrotamt = 90
        rangestep = (maxlat - minlat) / 4
        shiftstep = rangestep / 2
        parrange = np.arange(
            minlat + shiftstep,
            maxlat + shiftstep,
            rangestep
        )
    else:
        parfmt = '%.0f'
        parrotamt = 90
        rangestep = (np.floor(maxlat) - np.ceil(minlat)) / 6
        shiftstep = rangestep / 2
        fparrange = np.arange(
            np.ceil(minlat - shiftstep),
            np.floor(maxlat - shiftstep),
            rangestep
        )
        parrange = np.append(fparrange, [fparrange[-1] + rangestep])

    bmap.drawmeridians(
        merrange,
        dashes=[1, 1],
        linewidth=0.1,
        fontsize=12,
        fmt=merfmt,
        labels=[0, 0, 0, 1],
        rotation=merrotamt
    )
    bmap.drawparallels(
        parrange,
        dashes=[2, 2],
        linewidth=0.1,
        fontsize=12,
        fmt=parfmt,
        labels=[1, 0, 0, 0],
        rotation=parrotamt
    )


def plot_bathymetry(
    ax,
    bbox,
    plot_parameters=None,
    bathymetry_file=None,
    dataproj=ccrs.PlateCarree(),
    add_contourf=True,
    colorbar_visible=False,
    add_contours=False,
    fig=None
):
    """Plot topography/bathymetry on the given axes.

    Parameters
    ----------
    ax : Axes
        Axes on which to plot the bathymetry contours.
    bbox : LatLonBoundingBox
        Bounding box for plot in terms of lat/lon.
    plot_parameters : dict
        dictionary containing parameters to use when creating and saving plots.
        Includes values such as 'dpi', 'fontsize', etc.
    bathymetry_file : str
        Name of netCDF containing bathymetry data.
    dataproj : cartopy.crs.Projection
        Cartopy projection to use for transforming the data when plotting.
    add_contourf : boolean
        boolean value defining whether the contourf version
        of the bathymetry will be plotted. True by default
    colorbar_visible : boolean
        boolean value defining whether or not to add a
        colorbar for the bathymetry values.
    add_contours : boolean
        boolean value definitely whether the contour lines
        will be plotted. This is False by default.
    fig : figure
        current Matplotlib figure
    """

    if not bathymetry_file:
        return

    if isinstance(bbox, list):
        bbox = LatLonBoundingBox(lon_min=bbox[0], lat_min=bbox[2],
                                 lon_max=bbox[1], lat_max=bbox[3])

    # Read in topography/bathymetry. An example of a useful dataset is:
    # http://www.ngdc.noaa.gov/mgg/global/relief/ETOPO1/data/ice_surface \
    #    /grid_registered/netcdf/ETOPO1_Ice_g_gmt4.grd.gz
    etopo = xr.open_dataset(bathymetry_file)
    lons = etopo['x'].values[:]
    lats = etopo['y'].values[:]

    res = utils.find_subset_indices(
        bbox.lat_min - 5,
        bbox.lat_max + 5,
        bbox.lon_min - 20,
        bbox.lon_max + 10,
        lats, lons
    )

    lon, lat = np.meshgrid(lons[res.lon_min: res.lon_max],
                           lats[res.lat_min: res.lat_max])
    etopo_reduced = etopo['z'][res.lat_min: res.lat_max,
                               res.lon_min: res.lon_max]

    bathy = etopo_reduced.where(etopo_reduced <= 0)

    # This part adds the colored bathymetry
    if add_contourf:
        levels = [-5000, -4000, -3000, -2000, -1500, -1000,
                  -500, -400, -300, -250,
                  -200, -150, -100, -75, -65, -50,
                  -35, -25, -15, -10, -5, 0]

        # Adjust colormap to avoid darkest blues
        cmap = cmlab.Blues_r(np.linspace(0, 1, len(levels)+5))
        cmap = colors.ListedColormap(cmap[5:, :-1])
        contour_set = ax.contourf(
            lon, lat,
            bathy, levels,
            cmap=putils.LevelColormap(levels, cmap=cmap),
            transform=dataproj,
            extend='min',
            alpha=1.0,
            origin='lower',
            zorder=0
        )
        contour_set.axis = 'tight'

        # optionally add the colorbar
        if colorbar_visible:
            box = ax.get_position()
            cax = fig.add_axes([box.x0,
                                box.y0-0.18,
                                box.width,
                                0.05])
            colorbar = plt.colorbar(contour_set, cax=cax,
                                    orientation='horizontal')

            colorbar.ax.invert_xaxis()
            if plot_parameters:
                colorbar_label_fontsize = plot_parameters[
                                                'colorbar_label_fontsize'
                                            ]
                colorbar_ticklabel_fontsize = plot_parameters[
                                                'colorbar_ticklabel_fontsize'
                                            ]
            else:
                colorbar_label_fontsize = 11
                colorbar_ticklabel_fontsize = 9

            colorbar.set_label(
                'Depth (m)'
                )
            for t in colorbar.ax.get_yticklabels():
                t.set_fontsize(colorbar_ticklabel_fontsize)

    # This part just adds the contour lines
    if add_contours:
        ax.contour(
            lon, lat, bathy,
            levels=[-500, -250, -200, -150, -100, -75],
            transform=dataproj,
            extend='neither',
            linewidths=0.1,
            linestyles='solid',
            colors='k',
            zorder=1
        )
    etopo.close()


def calculate_hexbin_size(fig, ax, map_extremes,
                          hex_ratio_adjustment=0.6,
                          hex_size_px=9

):
    """ helper function used to determine a visually pleasing hexbin size
    based on the data provided and the size of the figure. In the past,
    this was handled by matplotlib.pyplot.hexbin() directly when a user
    provided a single int for grid_spacing. When plotting with Cartopy,
    this no longer seems to work as it did previously. Providing a tuple
    value tailored to the plot for grid_spacing gives a more consistent
    result. Currently, this has only been tested using the PlateCaree()
    projection, but will potentially work for other projections as well.

    Parameters
    ---------
    fig : figure
        current matplotlib figure
    ax : Axes
        current axis for the plot
    map_extremes : list
        list of the bounding box values of the plot in the form
        [minlon, maxlon, minlat, maxlat]
    hex_ratio_adjustment : float
         Ratio to adjust the height of the hexbin.
         A value less than one makes a taller hexbin.
    hex_size_px : int
        Default horizontal hexsize in pixels.

    Returns
    -------
    hexbin_size : tuple
        tuple with gridsize information to be used when plotting the hexbins
    """

    # The hexbin size needs to be based both on the size of the figure
    # window and also on the extent of the data. This helps avoid hexbins
    # that are too small / too large or oddly stretched.

    # calculate the distance across the center of the plot
    minlon, maxlon, minlat, maxlat = map_extremes
    midlat = (maxlat - minlat)
    midlon = (maxlon - minlon)
    horzdist = distance((midlat, minlon), (midlat, maxlon)).m
    vertdist = distance((minlat, midlon), (maxlat, midlon)).m

    # data aspect ratio
    aspratio = vertdist / horzdist

    # get the figure aspect ratio
    ax_pos = ax.get_position()
    fig_aspect = ax_pos.height / ax_pos.width

    # get the figure dimensions in pixels
    fig_width, fig_height = fig.get_size_inches() * fig.dpi

    if fig_aspect > 1:
        # tall figure
        vsize = fig_height / hex_size_px
        startsize = int(round(vsize / fig_aspect))
    else:
        # wide (or square) figure
        startsize = int(round(fig_width / hex_size_px))

    adjsize = int(round(startsize * aspratio, 0))

    # define the gridsize tuple to be passed to ax.hexbin()
    hexsize = (startsize, int(adjsize * hex_ratio_adjustment))

    return hexsize


def plot_land_mask(
    ax, bbox,
    dataproj=ccrs.PlateCarree(),
    landmask_type=None,
    landmask_files=None,
    lon_var=None,
    lat_var=None,
    tmask_var=None
):
    """Plot the landmask on a given axes.

    Parameters
    ----------
    ax : Axes
        Axes on which to plot the bathymetry contours.
    bbox : LatLonBoundingBox
        Bounding box for plot in terms of lat/lon.
    dataproj : projection
        ccrs.projection to use for plotting the data.
    landmask_type : str
        string representing which landmask to plot. If None, default
        GSHHS landmask is plotted
    landmask_files : dict
        dictionary containing full paths to landmask files. If None or
        missing, default GSHHS landmask is used. If variable names other
        than standard NEMO conventions are used, landmask_files is a nested
        dictionary instead.
    lon_var : str
        name of longitude variable in landmask_file if other than NEMO
        default (nav_lon)
    lat_var : str
        name of latitude variable in landmask_file if other than NEMO
        default (nav_lat)
    tmask_var : str
        name of mask variable in landmask_file if other than NEMO
        default (tmask)
    """

    # This function will be used as the default plotting method. The
    # shpfiles were downloaded from the Global Self-consistent,
    # Hierarchical High-resolution Geography Database (GSHHG).
    # downloaded from: https://www.ngdc.noaa.gov/mgg/shorelines/
    def plot_gshhs_landmask(bbox, ax):

        # if the bbox has been passed in as a list, adjust it.
        if isinstance(bbox, list):
            bbox = LatLonBoundingBox(lon_min=bbox[0], lat_min=bbox[2],
                                     lon_max=bbox[1], lat_max=bbox[3])

        # Decide how high a resolution to use based on the size of the
        # geographical area. Calculate the distance along a diagonal, then
        # select the resolution based on that value. The resolution levels
        # were arbitrarily chosen based on a plot of CIOPS-W data and may
        # still need to be tweaked to get the nicest plots possible.
        lr = (bbox.lat_min, bbox.lon_max)
        ul = (bbox.lat_max, bbox.lon_min)
        dist_calc = distance(ul, lr).m
        if dist_calc < 1000000:
            res = 'full'
        elif dist_calc < 3000000:
            res = 'high'
        else:
            res = 'intermediate'

        # Actually add the land. The scale represents the dataset scale
        # (either ‘auto’, ‘coarse’, ‘low’, ‘intermediate’, ‘high, or
        # ‘full’ with a default of ‘auto’). levels is a list of integers
        # from 1-4 corresponding to the desired GSHHS feature levels to
        # draw (default is [1] which corresponds to coastlines only).
        ax.add_feature(
            cfeature.GSHHSFeature(
                scale=res,
                levels=[1],
                edgecolor='black',
                facecolor='lightgray',
                linewidth=0.1,
                alpha=1.0,
                zorder=1
                )
            )

    # An alternate means of plotting a cartopy landmask. This provides a
    # landmask with a lower, but still acceptable resolution. It is
    # currently unused.
    def plot_NaturalEarthFeature_landmask(ax):
        ax.add_feature(
            cfeature.LAND.with_scale('10m'),
            facecolor="lightgrey",
            edgecolor='black',
            linewidth=0.2,
            alpha=1.0,
            zorder=0
        )
        ax.add_feature(
            cfeature.COASTLINE.with_scale('10m'),
            edgecolor='black',
            linewidth=0.1,
            alpha=1.0,
            zorder=1
        )
        ax.add_feature(cfeature.LAKES, facecolor='lightgrey', zorder=1)

    # This function handles the case where the user provides model landmasks
    # to use instead of the default plotting.
    def plot_model_landmask(
        ax, bbox,
        dataproj,
        landmask_type,
        landmask_files,
        lon_var='nav_lon',
        lat_var='nav_lat',
        tmask_var='tmask'
    ):

        # format the landmask_type and landmask_files keys to account
        # for some naming variations.
        landmask_type = landmask_type.lower().replace('-', '').replace('_', '')
        for key in list(landmask_files.keys()):
            landmask_files[
                key.lower().replace('-', '').replace('_', '')
            ] = landmask_files.pop(key)

        if landmask_type == 'riops':
            loggerstr = ("RIOPS landmask currently produces odd results "
                         + "below 46N latitude. If this is an issue, a "
                         + "better alternative would be to set landmask_type "
                         + "equal to None\n")
            logger.info(loggerstr)

        # if the landmask_type is in the landmask_files dict
        if landmask_type in list(landmask_files.keys()):

            # if landmask_files[landmask_type] is a nested dictionary
            if isinstance(landmask_files[landmask_type], dict):
                ocean_mesh_file = landmask_files[landmask_type]['path']
                # if there are non-standard var names
                if any(varname in list(landmask_files[landmask_type].keys())
                       for varname in ['lon_var', 'lat_var', 'tmask_var']):
                    lat_var = landmask_files[landmask_type]['lat_var']
                    lon_var = landmask_files[landmask_type]['lon_var']
                    tmask_var = landmask_files[landmask_type]['tmask_var']
            else:
                # if there is no nested dictionary given
                ocean_mesh_file = landmask_files[landmask_type]

            # assuming a landmask was given:
            ds = xr.open_dataset(ocean_mesh_file)
            lons = ds[lon_var].values
            lats = ds[lat_var].values

            def wrap_to_180(x):
                """Wrap values in degrees into the interval [-180, 180]."""
                x_wrap = np.remainder(x, 360)
                x_wrap[x_wrap > 180] -= 360
                return x_wrap
            lons = wrap_to_180(lons)

            import warnings
            with warnings.catch_warnings():
                warnings.simplefilter('ignore', UserWarning)

                mask = np.squeeze(ds[tmask_var].values)
                while mask.ndim > 2:
                    mask = mask[0, ...]
                mask_masked = np.ma.masked_values(mask, 1)

                cmap = colors.LinearSegmentedColormap.from_list(
                    "", [land_color, water_color]
                )

                # turn on/off for filled land
                # WARNING: CIOPS land isn't filled entirely because of
                # domain boundaries
                contour_set = ax.contourf(
                    lons, lats,
                    mask_masked,
                    levels=[0, 1],
                    cmap=cmap,
                    transform=dataproj,
                    alpha=1.0,
                    origin='lower',
                    zorder=0
                )
                contour_set.axis = 'tight'

                # turn on/off contour below to show landmask edges
                # WARNING: RIOPS mask is strange in Pacific Ocean so edges
                # appear below 46N
                ax.contour(
                    lons, lats, mask,
                    levels=[0, ],
                    transform=dataproj,
                    linewidths=0.5,
                    linestyles='solid',
                    colors='k',
                    zorder=1
                )
        else:
            logger.info(
                "landmask_type given in function call does not "
                + "match any of the keys given in the user config file. "
                + "Using default GSHHS landmask to draw land instead."
            )
            plot_gshhs_landmask(bbox, ax)

    ###########################################################
    # set some parameters and decide which landmask to plot
    ###########################################################

    land_color = "gray"  # "silver" also works well
    water_color = "white"

    # set the landmask if there are files given.
    landmask_type = get_landmask_type(landmask_files, landmask_type)

    # call the appropriate function based on whether the landmask will be
    # plotted from model data or from the GSHHS default.
    if landmask_type is None:
        plot_gshhs_landmask(bbox, ax)

    else:
        plot_model_landmask(
            ax, bbox,
            dataproj,
            landmask_type,
            landmask_files,
        )


def get_landmask_type(landmask_files=None,
                      expid=None):
    """ helper function used to determine the appropriate landmask_type """

    if landmask_files:

        # Account for the common case where there are underscores / dashes
        # in the landmask_type, but not in the landmask_files. This should
        # ultimately be removed, but the dashes/underscores are being removed
        # from the landmask_files keys somewhere in the processing, so they
        # need to be removed from the landmask_type for now too.
        expid_short = (str(expid).replace('_', '').replace('-', ''))

        # if the requested landmask_type matches a key from the landmask_files
        # directly, keep it as it is. If it matches with the underscores/dashes
        # removed, reset it to the new version. Otherwise, there is no match,
        # so set landmask_type to None.
        if str(expid) in list(landmask_files.keys()):
            landmask_type = str(expid)
        elif expid_short in list(landmask_files.keys()):
            landmask_type = expid_short
        else:
            landmask_type = None

    else:
        # if no landmask files are given, there is no need for a landmask_type
        landmask_type = None

    return landmask_type
