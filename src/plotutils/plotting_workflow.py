"""
Plot workflow script
================
:Author: Jennifer Holden
:Created: 2020-07-07
"""

import os
import sys
import argparse

import numpy as np

from driftutils import utils
from driftutils.utils import logger
from plotutils import regional_analysis_functions as rafuncs
from plotutils import plotting_utils as putils
from plotutils import map_functions as mapfuncs

from plotutils.plotting_workflows import line_plots_workflow as lpworkflow
from plotutils.plotting_workflows import subtracted_plots_workflow as spworkflow
from plotutils.plotting_workflows import hexbin_plots_workflow as hpworkflow
from plotutils.plotting_workflows import qc_plots_workflow as qcworkflow
from plotutils.plotting_workflows import drifter_track_plots_workflow as dpworkflow

########################################################################
# call the functions that create skill score lineplots
########################################################################

def lineplots_workflow(
    datasets_dict,
    skill_label_params,
    savedir,
    label_list,
    plotcolors,
    plot_selection,
    skills_list,
    filled_range_type,
    buoyids,
    overwrite_savedir,
    zone='all'
):

    """Plot skill score related line plots

    Parameters
    ----------
    datasets_dict : dict
        a dictionary with setnames as keys and associated datasets as
        values. ie {CIOPSW: [drifter1, drifter2, etc]}
    skill_label_params : dict
        dictionary containing label information for common variables
        included in the drift tool output
    savedir : str
        Path to the main directory where plots will be saved.
        Subdirectories are created within the code for organization.
    label_list : list
        list of the setnames dimensions included in the input files. If
        the setname dimension is not present in the input file, the
        ocean_model attribute is used instead.
    plotcolors : dict
        dictionary containing setnames as keys and a color names as
        values.
    plot_selection : dict
        Dictionary containing plot_type as keys and a boolean values as
        the dictionary value. These are default False and are set to
        True if desired via the user_plotting_config.yaml file.
    skills_list : list
        list of skill scores to calculate. If None or missing, default
        ['sep','molcard'] is used, but this will be overwritten by the
        user_plotting_config.yaml.
    filled_range_type : list
        optional argument specifying a method(s) for calculating the
        filled range for use with average skillscore plots.
        If None or missing, Interquartile Range (IQR) is used.
        Can be list with multiple options, Ex ['IQR','boot95, 'extremes','1std']
    buoyids : list
        list of strings representing the unique buoyids to be included
        in the plots.
    overwrite_savedir : bool
        boolean value set in the user config yaml. A value of True will
        allow a savedir to be cleared out and then overwritten with the
        new data. False will cause an error message to be logged and the
        program exits.
    zone : str
        string representing the label of the user defined polygon area
        in which to plot data. If no sub zones are defined, zone label
        'all' is used and the entire dataset is plotted.

    Returns
    -------
    creates plots specified by plot_selection in the
    user_plotting_config.yaml and saves them in savedir
    """

    ####################################################################
    # plot the drifter skillscores for each individual drifter
    ####################################################################

    if plot_selection['plot_drifter_skillscores']:
        lpworkflow.plot_drifter_skillscores_workflow(
            datasets_dict,
            skill_label_params,
            savedir,
            label_list,
            plotcolors,
            buoyids,
            overwrite_savedir,
            zone=zone
        )

    ####################################################################
    # plot average of means for each model
    ####################################################################

    if plot_selection['plot_average_of_mean_skills']:
        lpworkflow.plot_average_of_mean_skills_workflow(
            datasets_dict,
            skill_label_params,
            savedir,
            plotcolors,
            skills_list,
            zone=zone
        )

    ####################################################################
    # plot average of means for each model WITH filled
    ####################################################################

    if plot_selection['plot_skills_subplots_with_filled']:
        lpworkflow.plot_skills_subplots_with_filled_workflow(
            datasets_dict,
            skill_label_params,
            savedir,
            plotcolors,
            skills_list,
            filled_range_type,
            zone=zone
        )

    ####################################################################
    # plot average of means for each model WITH individual means
    ####################################################################

    if plot_selection['plot_skills_subplots_with_indv']:
        lpworkflow.plot_skills_subplots_with_indv_workflow(
            datasets_dict,
            skill_label_params,
            savedir,
            plotcolors,
            skills_list,
            buoyids,
            zone=zone
        )


########################################################################
# call the mapping functions
########################################################################
def track_plots_workflow(
    all_drifter_tracks_dict,
    etopo_file,
    landmask_files,
    savedir,
    label_list,
    plotcolors,
    zonecolors,
    plot_parameters,
    plot_selection,
    buoyids,
    overwrite_savedir,
    polygons,
    landmask_type=None,
    zone='all'
):

    """Create drifter track related plots

    Parameters
    ----------
    all_drifter_tracks_dict : dict
        dictionary containing all information for the tracks to be
        plotted. Key values are experiment setnames and values are
        pandas dataframes.
    etopo_file : str
        Path to the etopo file to use when plotting bathymetry.
        Can be None
    landmask_files : dict
        dictionary containing full paths to landmask files. If None or
        missing, default GSHHS landmask is used. If variable names other
        than standard NEMO conventions are use, landmask_files is a nested
        dictionary instead.
    landmask_type : str
        value determines which land mask will be used for plotting. If
        landmask_files is not None, the code will look at the landmask_files 
        dict for a path to an ocean model file to use. Otherwise, the code
        will plot the default GSHHS landmask.
    savedir : str
        Path to the main directory where plots will be saved.
        Subdirectories are created within the code for organization.
    label_list : list
        list of the setnames dimensions included in the input files. If
        the setname dimension is not present in the input file, the
        ocean_model attribute is used instead.
    plotcolors : dict
        dictionary containing setnames as keys and colors as values.
    zonecolors : dict
        dictionary containing zone names as keys and colors as values.
    plot_parameters : dict
        dictionary containing parameters to use when saving plots
        (resolution, filetype, figure size, etc)
    plot_selection : dict
        Dictionary containing plot_type as keys and a boolean values as
        the dictionary value. These are default False and are set to
        True if desired via the user_plotting_config.yaml file.
    buoyids : list
        list of strings representing the unique buoyids to be included
        in the plots.
    overwrite_savedir : bool
        boolean value set in the user config yaml. A value of True will
        allow a savedir to be cleared out and then overwritten with the
        new data. False will cause an error message to be logged and the
        program exits.
    polygons : dict
        dictionary containing zone names as keys and polygon area
        coordinates as values
    zone : str
        string representing the label of the user defined polygon area
        in which to plot data. If no sub zones are defined, zone label
        'all' is used and the entire dataset is plotted.

    Returns
    -------
    creates plots specified by plot_selection in the
    user_plotting_config.yaml and saves them in savedir
    """

    ####################################################################
    # Plot all observed drifter tracks in set. This is for plotting the
    # observed data only. If the model tracks are required, use
    # 'plot_all_drifter_tracks_with_mod' option instead.
    ####################################################################
    if plot_selection['plot_all_drifter_tracks']:
        dpworkflow.plot_all_drifter_tracks_workflow(
            all_drifter_tracks_dict,
            etopo_file,
            landmask_files,
            landmask_type,
            savedir,
            plotcolors,
            plot_parameters,
            zone
        )

    ####################################################################
    # Plot all observed drifter tracks plus all modelled tracks from all
    # experiments on the same axis. If only the observed tracks are
    # required, use 'plot_all_drifter_tracks' option instead.
    ####################################################################
    if plot_selection['plot_all_drifter_tracks_with_mod']:
        dpworkflow.plot_all_drifter_tracks_with_mod_workflow(
            all_drifter_tracks_dict,
            etopo_file,
            landmask_files,
            landmask_type,
            savedir,
            plotcolors,
            plot_parameters,
            zone
        )

    ####################################################################
    # Plot the model drifter start points plus polygon areas
    ####################################################################
    if plot_selection['plot_positions_with_polygons']:
        dpworkflow.plot_positions_with_polygons_workflow(
            all_drifter_tracks_dict,
            etopo_file,
            landmask_files,
            landmask_type,
            savedir,
            plotcolors,
            zonecolors,
            plot_parameters,
            polygons,
            zone
        )

    ####################################################################
    # Plot the drifter tracks (one obs track and the modelled tracks
    # from one experiment per plot)
    ####################################################################
    if plot_selection['plot_track_per_drifter']:
        dpworkflow.plot_track_per_drifter_workflow(
            all_drifter_tracks_dict,
            etopo_file,
            landmask_files,
            landmask_type,
            savedir,
            plotcolors,
            plot_parameters,
            overwrite_savedir,
            zone=zone
        )

    ####################################################################
    # Plot the comparison drifter tracks (one obs track, modelled tracks
    # from all sets)
    ####################################################################
    if plot_selection['plot_track_comparison']:
        dpworkflow.plot_track_comparison_workflow(
            all_drifter_tracks_dict,
            etopo_file,
            landmask_files,
            landmask_type,
            savedir,
            plotcolors,
            plot_parameters,
            buoyids,
            overwrite_savedir,
            zone=zone
        )

    ####################################################################
    # plots side by side versions of each dataset for each drifter
    ####################################################################
    if plot_selection['plot_track_side_by_side']:
        dpworkflow.plot_track_side_by_side_workflow(
            all_drifter_tracks_dict,
            plot_parameters,
            etopo_file,
            landmask_files,
            savedir,
            plotcolors,
            buoyids,
            overwrite_savedir,
            zone=zone
        )


########################################################################
# Create hexbin plots
########################################################################
def hexbin_plot_workflow(
    datasets_dict,
    plot_durations,
    skills_list,
    etopo_file,
    landmask_files,
    savedir,
    plot_parameters,
    plot_selection,
    polygons,
    zonecolors,
    landmask_type=None,
    zone='all'
):
    """Create hexbin plots of skill scores

    Parameters
    ----------
    datasets_dict : dict
        dictionary with setnames as values and lists of xarray DataSets
        as keys. Each Dataset represents a single observed drifter.
    plot_durations: list
        list of integers representing the plotting durations (ie, [48]
        would produce a plot representing skill score at 48h, etc)
    skills_list : list
        list of strings representing the skill scores to be plotted
    etopo_file : str
        Path to the etopo file to use when plotting bathymetry.
        Can be None
    landmask_files : dict
        dictionary containing full paths to landmask files. If None or
        missing, default GSHHS landmask is used. If variable names other
        than standard NEMO conventions are use, landmask_files is a nested
        dictionary instead.
    landmask_type : str
        value determines which land mask will be used for plotting. If
        landmask_files is not None, the code will look at the landmask_files
        dict for a path to an ocean model file to use. Otherwise, the code
        will plot the default GSHHS landmask.
    savedir : str
        Path to the main directory where plots will be saved.
        Subdirectories are created within the code for organization.
    plot_parameters : dict
        dictionary containing parameters to use when saving plots
        (resolution, filetype, figure size, etc)
    plot_selection : dict
        Dictionary containing plot_type as keys and a boolean values as
        the dictionary value. These are default False and are set to
        True if desired via the user_plotting_config.yaml file.
    polygons : dict
        dictionary containing zone names as keys and polygon area
        coordinates as values
    zonecolors : dict
        dictionary of color values to use with regional analysis.
    zone : str
        string representing the label of the user defined polygon area
        in which to plot data. If no sub zones are defined, zone label
        'all' is used and the entire dataset is plotted.


    Returns
    -------
    creates plots specified by plot_selection in the
    user_plotting_config.yaml and saves them in savedir
    """

    if plot_selection['plot_hexbin_skillscores']:

        hpworkflow.plot_hexbin_skillscores_workflow(
            datasets_dict,
            plot_durations,
            skills_list,
            etopo_file,
            landmask_files,
            landmask_type,
            savedir,
            plot_parameters,
            polygons,
            zonecolors,
            zone=zone
        )


########################################################################
# Create subtracted plots
########################################################################

def subtracted_plots_workflow(
    datasets_dict,
    datasets_list,
    dataset_labels,
    plot_durations,
    skills_list,
    etopo_file,
    landmask_files,
    savedir,
    plot_selection,
    plot_parameters,
    buoyids,
    reference_set,
    skill_label_params,
    overwrite_savedir,
    polygons,
    zonecolors,
    landmask_type=None,
    zone='all'
):

    """Create subtracted type plots of skill scores

    Parameters
    ----------
    datasets_dict : dict
        dictionary with setnames as values and lists of xarray DataSets
        as keys. Each Dataset represents a single observed drifter.
    datasets_list : list
        a list of datasets, one element for each file in the datadir,
        usually corresponds to one element per each drifter.
        ie, [drifter1, drifter2, etc]
    plot_durations: list
        list of integers representing the plotting durations (ie, [48]
        would produce a plot representing skill score at 48h, etc)
    skills_list : list
        list of strings representing the skill scores to be plotted
    etopo_file : str
        Path to the etopo file to use when plotting bathymetry.
        Can be None
    landmask_files : dict
        dictionary containing full paths to landmask files. If None or
        missing default GSHHS is used. If variable names other than
        standard NEMO conventions are use, landmask_files is a nested
        dictionary instead.
    landmask_type : str
        value determines which land mask will be used for plotting. If
        landmask_files is not None, the code will look at the landmask_files
        dict for a path to an ocean model file to use. Otherwise, the code
        will plot the default GSHHS landmask.
    savedir : str
        Path to the main directory where plots will be saved.
        Subdirectories are created within the code for organization.
    plot_selection : dict
        Dictionary containing plot_type as keys and a boolean values as
        the dictionary value. These are default False and are set to
        True if desired via the user_plotting_config.yaml file.
    plot_parameters : dict
        dictionary of values to use for plotting. Keys include values that
        modify such parameters as fontsizes and plot aspect ratio.
    buoyids : list
        list of strings representing the unique buoyids to be included
        in the plots.
    reference_set : str
        unique identifier for the experiment being analysed. This
        identifier must match one of the setname dimension values
        in the given comparison set output files.
    skill_label_params : dict
        dictionary containing label information for common variables
        included in the drift tool output
    overwrite_savedir : bool
        boolean value set in the user config yaml. A value of True will
        allow a savedir to be cleared out and then overwritten with the
        new data. False will cause an error message to be logged and the
        program exits.
    polygons : dict
        dictionary containing zone names as keys and polygon area
        coordinates as values
    zonecolors : dict
        dictionary of color values to use with regional analysis.
    zone : str
        string representing the label of the user defined polygon area
        in which to plot data. If no sub zones are defined, zone label
        'all' is used and the entire dataset is plotted.

    Returns
    -------
    creates plots specified by plot_selection in the
    user_plotting_config.yaml and saves them in savedir
    """

    if len(list(datasets_dict.keys())) < 2:
        logger.info(
            'Not able to create subtracted plots using only one experiment '
            'so skipping. The current set of data only contains '
            + str(list(datasets_dict.keys()))
        )
        return

    ####################################################################
    # plot subtracted hexbins
    ####################################################################

    if plot_selection['plot_subtracted_hexbins']:
        spworkflow.plot_subtracted_hexbins(
            datasets_dict,
            dataset_labels,
            plot_durations,
            skills_list,
            etopo_file,
            landmask_files,
            landmask_type,
            savedir,
            plot_parameters,
            zonecolors,
            polygons,
            reference_set,
            zone=zone
        )

    ####################################################################
    # plot subtracted of means for each drifter
    ####################################################################

    if plot_selection['plot_subtracted_skillscores']:
        spworkflow.plot_subtracted_skillscores_workflow(
            datasets_dict,
            datasets_list,
            skills_list,
            savedir,
            buoyids,
            skill_label_params,
            reference_set,
            zone=zone
        )

########################################################################
# Create QC type plots
########################################################################

def qc_plots_workflow(
    all_drifter_tracks_dict,
    datasets_dict,
    savedir,
    plotcolors,
    skill_label_params,
    etopo_file,
    landmask_files,
    plot_selection,
    buoyids,
    overwrite_savedir,
    plot_parameters,
    zone='all'
):
    """ workflow for creating the quality control type plots that do not
    fit in the other workflows

    Parameters
    ----------
    all_drifter_tracks_dict : dict
        dictionary containing all information for the tracks to be
        plotted. Key values are experiment setnames and values are
        pandas dataframes.
    datasets_dict : dict
        dictionary with setnames as values and lists of xarray DataSets
        as keys. Each Dataset represents a single observed drifter.
    savedir : str
        Path to the main directory where plots will be saved.
        Subdirectories are created within the code for organization.
    plotcolors : dict
        dictionary containing setnames as keys and a color names as
        values.
    skill_label_params : dict
        dictionary containing label information for common variables
        included in the drift tool output
    etopo_file : str
        Path to the etopo file to use when plotting bathymetry.
        Can be None
    landmask_files : dict
        dictionary containing full paths to landmask files. If None or
        missing, default GSHHS landmask is used. If variable names other
        than standard NEMO conventions are use, landmask_files is a nested
        dictionary instead.
    plot_selection : dict
        Dictionary containing plot_type as keys and a boolean values as
        the dictionary value. These are default False and are set to
        True if desired via the user_plotting_config.yaml file.
    buoyids : list
        list of strings representing the unique buoyids to be included
        in the plots.
    overwrite_savedir : bool
        boolean value set in the user config yaml. A value of True will
        allow a savedir to be cleared out and then overwritten with the
        new data. False will cause an error message to be logged and the
        program exits.
    plot_parameters : dict
        dictionary containing parameters to use when saving plots
        (resolution, filetype, figure size, etc)
    zone : str
        string representing the label of the user defined polygon area
        in which to plot data. If no sub zones are defined, zone label
        'all' is used and the entire dataset is plotted.

    Returns
    -------
    creates plots specified by plot_selection in the
    user_plotting_config.yaml and saves them in savedir.

    """

    ####################################################################
    # create plot of skills plus map per drifter per experiment
    ####################################################################

    if plot_selection['plot_skills_plus_map']:
        qcworkflow.plot_skills_plus_map_workflow(
            all_drifter_tracks_dict,
            datasets_dict,
            savedir,
            buoyids,
            overwrite_savedir,
            plotcolors,
            skill_label_params,
            landmask_files,
            etopo_file,
            plot_parameters,
        )

    ####################################################################
    # create plot of ratios plus map per drifter per experiment
    ####################################################################

    if plot_selection['plot_ratios_plus_map']:
        qcworkflow.plot_ratios_plus_map_workflow(
            all_drifter_tracks_dict,
            datasets_dict,
            savedir,
            buoyids,
            overwrite_savedir,
            plotcolors,
            skill_label_params,
            plot_parameters,
        )

    ####################################################################
    # Plot the persistence drifter tracks
    ####################################################################

    if plot_selection['plot_persistence']:
        qcworkflow.plot_persistence_workflow(
            all_drifter_tracks_dict,
            datasets_dict,
            savedir,
            plotcolors,
            etopo_file,
            buoyids,
            overwrite_savedir,
            plot_parameters,
            zone
        )

########################################################################
# Read in the data and run the workflow
########################################################################

def plotting_workflow(user_config_filename):

    # Define some default skill score labels to be used with upcoming
    # legends and plot labels
    skill_label_params = {
        'sep':
            {'ylab': 'average separation distance (km)',
             'yleg': 'average sep'},
        'obs_disp':
            {'ylab': 'average displacement of observed drifter (km)',
             'yleg': 'avg disp of obs'},
        'obs_dist':
            {'ylab': 'average distance travelled by observed drifter (km)',
             'yleg': 'avg dist obs'},
        'mod_disp':
            {'ylab': 'average displacement of modelled drifter (km)',
             'yleg': 'avg disp mod'},
        'mod_dist':
            {'ylab': 'average distance travelled by modelled drifter (km)',
             'yleg': 'avg dist mod'},
        'molcard':
            {'ylab': 'average molcard',
             'yleg': 'avg molcard'},
        'liu':
            {'ylab': 'average liu',
             'yleg': 'avg liu'},
        'sutherland':
            {'ylab': 'average sutherland',
             'yleg': 'avg sutherland'},
        'obsratio':
            {'ylab': 'Disp from origin/Total track length',
             'yleg': 'obsratio'},
        'modratio':
            {'ylab': 'Disp from origin/Total track length',
             'yleg': 'modratio'}
    }

    # list of default plot parameters to use if none are specified in
    # the config. 'fontsize' refers to what will be used for the axis
    # labels. 'output_filetype' should be 'jpg', 'png' or 'svg' (or any
    # option available through matplotlib. 'plot_aspect_ratio' is the
    # ratio of lat/lon where None=from data, 1=square, 2=twice as tall
    # as it is wide, etc.
    plot_parameters = {
        'figure_width': 8,
        'figure_height': 8,
        'fontsize': 11,
        'dpi': 300,
        'output_filetype': 'jpg',
        'plot_aspect_ratio': None,
        'dpi': 300,
    }

    # load the user defined configuation file
    usercfg = utils.load_yaml_config(user_config_filename)
    datadir = usercfg['datadir']
    rawsavedir = usercfg['savedir']
    logger.info('\nreading data from: ' + datadir)

    # If the user has decided to choose plot parameters other than the
    # defaults, the values from the config file will overwrite the defaults.
    if 'plot_parameters' in usercfg.keys():
        pparams = usercfg['plot_parameters']
        for key, val in pparams.items():
            plot_parameters[key] = val

    # Calculate some default font sizes for other labelling (ticklabels, etc).
    # Only add them to the plot_parameters dictionary if they have not been
    # given in the config file.
    extra_plot_parameters = {
        'ticklabel_fontsize': int(plot_parameters['fontsize'] - 2),
        'colorbar_ticklabel_fontsize': int(plot_parameters['fontsize'] - 2),
        'colorbar_label_fontsize': int(plot_parameters['fontsize']),
        'title_fontsize': int(plot_parameters['fontsize'] + 2),
        'legend_label_fontsize': int(plot_parameters['fontsize'] - 2)
    }

    for key in extra_plot_parameters.keys():
        if key not in plot_parameters.keys():
            plot_parameters[key] = extra_plot_parameters[key]

    # define the polygon areas if they exist and make a list of analysis
    # zones to use. If there are no analysis zones given, use the keys
    # keys from the polygons_file. If there is no polygon file, default
    # back to one zone ('all'), which includes all the data.
    analysis_zones = ['all']
    polygons = None
    polygons_file = None
    if 'polygons_file' in usercfg and usercfg['polygons_file'] != 'None':
        if usercfg['polygons_file']:
            polygons_file = usercfg['polygons_file']
            polygons = rafuncs.load_polygons(polygons_file)
            analysis_zones = list(polygons['polygon_coords'].keys())

    if 'analysis_zones' in usercfg and usercfg['analysis_zones']:
        if usercfg['analysis_zones'] != 'None':
            analysis_zones = usercfg['analysis_zones']

    # check to be sure that the given analysis zones are included in the
    # polygon file keys
    if polygons:
        polykeys = list(polygons['polygon_coords'].keys())
        polykeys.append('Combined_Polygons_Boundary')
        overlap = [z for z in analysis_zones if z in polykeys]
        skipzones = [z for z in analysis_zones if z not in polykeys]
        if len(overlap) > 0:
            analysis_zones = overlap
        else:
            analysis_zones = ['all']
        if skipzones:
            logstr1 = ('There is at least one zone label provided that does '
                       'not appear in the zone definitions file')
            logger.info(logstr1)
            for sz in skipzones:
                logstr = ('skipping zone ' + sz + ' because it does not '
                          'exist in the polygons file')
                logger.info(logstr)
    else:
        logger.info('No polygons file found in config file. Analyzing '
                    'data over the full spatial domain')
        analysis_zones = ['all']

    # only keep polygon zone definitions that are included in
    # analysis_zones to help with plotting the polygon areas.
    if polygons:
        for zone in list(polygons['polygon_coords'].keys()):
            if zone not in analysis_zones:
                del polygons['polygon_coords'][zone]

    # if 'Combined_Polygons_Boundary' has been chosen in the config file,
    # calculate the polygon that contains all the other polygons.
    if 'Combined_Polygons_Boundary' in analysis_zones:
        if len(analysis_zones) > 2:
            polygons['coords'] = rafuncs.append_combined_polygons_boundary(
                polygons['polygon_coords']
            )
        elif (len(analysis_zones) <= 2):
            if (analysis_zones[0] == 'Combined_Polygons_Boundary'):
                st = ('No user defined polygon areas - skipping '
                      'Combined_Polygons_Boundary')
                logger.info(st)
                analysis_zones.remove('Combined_Polygons_Boundary')
            else:
                st = ('Only one user defined polygon area - skipping '
                      'Combined_Polygons_Boundary')
                logger.info(st)
                analysis_zones.remove('Combined_Polygons_Boundary')

    if len(analysis_zones) == 1 and analysis_zones[0] == 'all':
        zonestr = 'full spatial domain'
    else:
        zonestr = ", ".join(analysis_zones) + " and full spatial domain"
    logstr2 = '\nperforming analysis over ' + zonestr
    logger.info(logstr2)

    # grab a list of the setnames
    label_list = putils.get_label_list(datadir)
    abbr_label_list = [
        lr.replace('-', '').replace('_', '') for lr in label_list
    ]

    logger.info('\nplotting data from the following experiments: '
                + ', '.join(label_list))

    # grab the colors from usercfg if they exist, otherwise they'll be
    # assigned randomly
    lcolors = None
    if 'lcolors' in usercfg and usercfg['lcolors'] != 'None':
        lcolors = usercfg['lcolors']

    # assign the colors for plotting from lcolors or randomly
    plotcolors = putils.assign_colors(label_list, lcolors)

    # grab the zonal colors from usercfg if they exist, otherwise they'll
    # be assigned randomly later
    zonecolors = None
    if 'zonecolors' in usercfg and usercfg['zonecolors'] != 'None':
        zonecolors = usercfg['zonecolors']

    # set a default None for the etopo_file, but read it from
    # the user_config if present.
    etopo_file = None
    if 'etopo_file' in usercfg and usercfg['etopo_file'] != 'None':
        etopo_file = usercfg['etopo_file']

    # set a default None for the landmask files dictionary, but read it
    # from the user_config if present.
    landmask_files = None
    if ('landmask_files' in usercfg and usercfg['landmask_files'] != 'None'):
        landmask_files = usercfg['landmask_files']

    # Next, handle the landmask files for plotting. If multiple landmask_files
    # are given, only use the files for plot types that show a single
    # experiment (ex, plot_hexbin_skillscores) and only when the
    # landmask_files key matches the setname for the experiment being
    # displayed. When plotting the output from a single experiment, this
    # means that if one of the landmask files provided matches the setname,
    # this landmask file will be used to plot the land data for all the
    # plot types. If a single landmask_files is given when plotting a
    # comparison output, assume that the user has chosen to use that
    # landmask file even in cases where multiple experiments are shown on
    # the same plot (ex: plot_all_drifter_tracks_with_mod).  In this case,
    # the landmask file is still not used for plots showing a single
    # dataset unless the setname matches the landmask_files key (ex:
    # plot_hexbin_skillscores).

    # if landmask_files have been given in the config
    if landmask_files:
        landmask_file_short = (
            str(list(landmask_files.keys())[0])
            .replace('_', '')
            .replace('-', '')
        )
        # if there is only one landmask_file given
        if len(list(landmask_files.keys())) == 1:
            # if its key matches the experiment identifier, use it.
            #This may involve removing underscores/dashes first.
            if list(landmask_files.keys())[0] in label_list:
                landmask_type = list(landmask_files.keys())[0]
            elif landmask_file_short in label_list:
                landmask_type = landmask_file_short
            else:
                # otherwise use the default GSHHS
                landmask_type = None
                logger.info(
                    'The keys of the landmask_files given in the config '
                    + 'do not match the experiment setnames. Land will be '
                    + 'plotted using the default GSHHS option.'
                )
        else:
            # if there is more than one landmask_file given and there are
            # multiple experiments being processed, use the default GSHHS
            # for comparison plots since no specific landmask file is being
            # defined:
            if len(label_list) > 1:
                landmask_type = None
            else:
                # if multiple landmask files but only experiment, let the
                # rest of the code handle it later.
                pass
    else:
        # there are no landmask_files
        landmask_type = None

    # including a check that will exit if the drift-tool output folder
    # is not present this could be due to an error or inadequate
    # ocean/drifter data to run the tool.
    if os.path.isdir(datadir) is False:
        sys.exit('The input folder ' + str(datadir) + ' does not exist!')

    ####################################################################
    # read which plots to create from the user defined yaml file
    ####################################################################

    if 'plot_selection' in usercfg and usercfg['plot_selection'] != 'None':
        plot_selection = usercfg['plot_selection']
        plotliststr = [
            p for p in plot_selection.keys() if plot_selection[p]
        ]
        lstr = ('\n' + 'plotting: ' + ', '.join(plotliststr))
        logger.info(lstr)
    else:
        sys.exit('no plotting options have been chosen!')

    skillscore_plot_list = ['plot_drifter_skillscores',
                            'plot_average_of_mean_skills',
                            'plot_skills_subplots_with_filled',
                            'plot_skills_subplots_with_indv']

    drifter_tracks_plot_list = ['plot_track_per_drifter',
                                'plot_track_comparison',
                                'plot_track_side_by_side',
                                'plot_all_drifter_tracks',
                                'plot_all_drifter_tracks_with_mod',
                                'plot_positions_with_polygons']

    hexbin_plot_list = ['plot_hexbin_skillscores']

    subtracted_plots_list = ['plot_subtracted_skillscores',
                             'plot_indv_subtracted_skillscores',
                             'plot_subtracted_hexbins']

    qc_plots_list = ['plot_skills_plus_map', 'plot_ratios_plus_map',
                     'plot_persistence']

    # add default False for any plot options that are not specified in
    # the config file
    plot_options_list = (skillscore_plot_list + drifter_tracks_plot_list
                         + hexbin_plot_list + subtracted_plots_list
                         + qc_plots_list)

    for val in plot_options_list:
        if val not in plot_selection.keys():
            plot_selection[val] = False

    # create a list of the plot_selections that are True
    plot_list = [psel for psel, pbool in plot_selection.items() if pbool]

    ####################################################################
    # organize the rest of the information from the config file
    ####################################################################

    # Check for a reference set if one will be needed, otherwise set to
    # None. Choosing None performs comparisons across all sets, which
    # can create many more plots than necessary.
    reference_set = None
    if bool(set(subtracted_plots_list) & set(plot_list)):
        if ('reference_set' in usercfg and usercfg['reference_set'] != 'None'):
            reference_set = usercfg['reference_set']

    # set a list of plot durations if one is given. This is required for
    # some hexbin plotting options.
    plot_durations = None
    if 'plot_durations' in usercfg and usercfg['plot_durations'] != 'None':
        plot_durations = usercfg['plot_durations']

    # define the skill scores to calculate. can be overwritten by the
    # userconfig file. currently available: ['sep', 'molcard', 'liu',
    # 'sutherland', 'obsratio', 'modratio', 'logobsratio', 'logmodratio']
    skills_list = ['sep', 'molcard']
    if 'skills_list' in usercfg and usercfg['skills_list'] != 'None':
        skills_list = usercfg['skills_list']

    # the default method for calculating the filled range is
    # Interquartile range.
    filled_range_type = 'IQR'
    # reset the filled_range_type if the user selects something
    # different or chooses extra options. This can be a list with
    # multiple options, ex: ['IQR','extremes','1std']
    if ('filled_calculation_method' in usercfg
            and usercfg['filled_calculation_method'] != 'None'):
        filled_range_type = usercfg['filled_calculation_method']

    # specify whether to overwrite output savedir
    overwrite_savedir = False
    if 'overwrite_savedir' in usercfg:
        overwrite_savedir = usercfg['overwrite_savedir']

    # specify whether to allow the zonal data to be written to and pulled
    # from pickle files. This defaults to False, since without caution it
    # is possible to load old data rather than updating pickle when new
    # data is added.
    use_pickles = False
    if 'use_pickles' in usercfg:
        use_pickles = usercfg['use_pickles']

    ####################################################################
    # make sure the savedir exists. Prep for making subfolders below if
    # user chooses to make plots for individual drifters
    ####################################################################

    # Set up the output directory. If the user has not chosen to
    # overwrite the savedir and it exists, provide an error message and
    # exit the program. Otherwise create a new folder or remove files
    # from an already exisiting folder.
    savedir = utils.setup_savedir(
        rawsavedir,
        overwrite_savedir=overwrite_savedir
    )

    lstr = ('\n' + 'plots will be saved in: ' + savedir + '\n')
    logger.info(lstr)

    ####################################################################
    # Prepare the data for any upcoming plots as required. For example,
    # if only skill score line plots will be created, do not gather the
    # data for drifter tracks. This step is intended to save computation
    # time, since calculations for large datasets can be time intensive.
    ####################################################################

    # grab a list of the buoyids
    buoyids = utils.grab_buoyids_from_attrs(datadir)

    if len(buoyids) < 1:
        exitstr = ('\n\nThe input directory does not contain any data: '
                   + datadir + '\n\n')
        sys.exit(exitstr)

    skillscore_type_plots = (skillscore_plot_list + hexbin_plot_list
                             + subtracted_plots_list + qc_plots_list)

    tracks_type_plots = (drifter_tracks_plot_list + hexbin_plot_list
                         + subtracted_plots_list + qc_plots_list)

    # gather the skillscore data
    logger.info('assembling data\n')
    dataset_labels, datasets_list, datasets_dict, buoyids =\
        putils.prep_files(datadir, buoyids)

    # Organize the data by region. If no polygon file is given, default
    # to using one zone called 'all' that contains all the data. If a
    # polygon file is given, sort the data using analysis_zones provided
    # by the user (if they exist) or by keys from the polygon_file (if
    # no zones are specified). The output from this function will create
    # dictionaries with zone names as keys and values representing
    # the previous non-regional datasets_list and datasets_dict
    zdata = rafuncs.get_zonal_data(
        analysis_zones,
        polygons,
        datasets_dict,
        datasets_list,
        savedir,
        overwrite_savedir,
        plot_durations,
        skills_list,
        use_pickles=use_pickles
    )

    zonal_dict = zdata['zonal_dict']
    zonal_list_dict = zdata['zonal_list_dict']
    analysis_zones = zdata['analysis_zones']
    polygons = zdata['polygons']
    zonal_metadata = zdata['zonal_metadata']

    # assign the colors for plotting from lcolors or randomly
    zonecolors = putils.assign_colors(analysis_zones, zonecolors)

    # cycle though each zone to make the plots
    for zone in zonal_dict.keys():
        zonelabel = zone
        if zone == 'all':
            zonelabel = 'the entire spatial domain'
        lstr = '\ncreating plots for ' + zonelabel
        logger.info('\ncreating plots for ' + zonelabel + '\n'
                    + '-' * (len(lstr)) + '\n')

        # gather the track data if needed
        if bool(set(tracks_type_plots) & set(plot_list)):
            logger.info('assembling drifter track data')
            all_drifter_tracks_dict = mapfuncs.create_tracks_dataframe(
                datatype='dataset',
                given_dslist=zonal_list_dict[zone]
            )

        # define the zone specific parameters
        datasets_dict = zonal_dict[zone]
        datasets_list = zonal_list_dict[zone]
        label_list = zonal_metadata[zone]['list_of_labels_in_zone']
        buoyids = zonal_metadata[zone]['buoyids_in_zone']
        savedir = zonal_metadata[zone]['zonal_savedir']
        psavedir = os.path.join(savedir, 'pickled_data')

        ################################################################
        # Run the various plotting workflows
        ################################################################

        # creates plots:
        # 'plot_drifter_skillscores'
        # 'plot_average_of_mean_skills'
        # 'plot_skills_subplots_with_filled'
        # 'plot_skills_subplots_with_indv'

        if bool(set(skillscore_plot_list) & set(plot_list)):

            lineplots_workflow(
                datasets_dict,
                skill_label_params,
                savedir,
                label_list,
                plotcolors,
                plot_selection,
                skills_list,
                filled_range_type,
                buoyids,
                overwrite_savedir,
                zone=zone
            )

        # creates plots:
        # 'plot_track_indiv'
        # 'plot_track_comparison'
        # 'plot_track_side_by_side'
        # 'plot_all_drifter_tracks'
        # 'plot_all_drifter_tracks_with_mod'
        # 'plot_positions_with_polygons'

        if bool(set(drifter_tracks_plot_list) & set(plot_list)):

            track_plots_workflow(
                all_drifter_tracks_dict,
                etopo_file,
                landmask_files,
                savedir,
                label_list,
                plotcolors,
                zonecolors,
                plot_parameters,
                plot_selection,
                buoyids,
                overwrite_savedir,
                polygons,
                landmask_type=landmask_type,
                zone=zone
            )

        # creates plots:
        # 'plot_hexbin_skillscores'

        if bool(set(hexbin_plot_list) & set(plot_list)):

            hexbin_plot_workflow(
                datasets_dict,
                plot_durations,
                skills_list,
                etopo_file,
                landmask_files,
                savedir,
                plot_parameters,
                plot_selection,
                polygons,
                zonecolors,
                landmask_type=landmask_type,
                zone=zone
            )

        # creates plots:
        # 'plot_subtracted_hexbins'
        # 'plot_subtraced_skillscores'

        if bool(set(subtracted_plots_list) & set(plot_list)):

            subtracted_plots_workflow(
                datasets_dict,
                datasets_list,
                dataset_labels,
                plot_durations,
                skills_list,
                etopo_file,
                landmask_files,
                savedir,
                plot_selection,
                plot_parameters,
                buoyids,
                reference_set,
                skill_label_params,
                overwrite_savedir,
                polygons,
                zonecolors,
                landmask_type=landmask_type,
                zone=zone
            )

        # creates plots:
        # 'plot_skills_plus_map'
        # 'plot_ratios_plus_map'
        # 'plot_persistence'

        if bool(set(qc_plots_list) & set(plot_list)):

            qc_plots_workflow(
                all_drifter_tracks_dict,
                datasets_dict,
                savedir,
                plotcolors,
                skill_label_params,
                etopo_file,
                landmask_files,
                plot_selection,
                buoyids,
                overwrite_savedir,
                plot_parameters,
                zone=zone
            )

        returndict = {
            zone: {
                'dataset_labels': dataset_labels,
                'datasets_list': datasets_list,
                'datasets_dict': datasets_dict,
                'buoyids': buoyids,
                'zonal_dict': zonal_dict,
                'zonal_list_dict': zonal_list_dict,
                'zonal_metadata': zonal_metadata,
                'label_list': label_list,
                'psavedir': psavedir,
                'savedir': savedir,
                'overwrite_savedir': overwrite_savedir,
                'filled_range_type': filled_range_type,
                'skills_list': skills_list,
                'plot_durations': plot_durations,
                'reference_set': reference_set,
                'plot_list': plot_list,
                'landmask_files': landmask_files,
                'etopo_file': etopo_file,
                'plotcolors': plotcolors,
                'zonecolors': zonecolors,
                'polygons': polygons,
                'polygons_file': polygons_file,
                'analysis_zones': analysis_zones,
                'skill_label_params': skill_label_params
            }
        }

    return returndict


def main(args=sys.argv[1:]):

    # load the command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--user_config',
        type=str,
        help='path to the user plotting config file (ex: ../../examples'
        '/plotting/user_plotting_config.yaml)'
    )
    parser.add_argument(
        '--log_level', default='info',
        choices=utils.log_level.keys(),
        help='Set level for log messages')

    args = (parser.parse_args())
    user_config_filename = args.user_config

    utils.initialize_logging(level=utils.log_level[args.log_level])

    with np.errstate(invalid='ignore'):
        returndict = plotting_workflow(user_config_filename)

    logger.info('\nAnalysis complete!')

    utils.shutdown_logging()


if __name__ == '__main__':
    main()
