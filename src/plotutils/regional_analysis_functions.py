"""
Regional Analysis functions
================
:Author: Nancy Soontiens
:Contributors: Jennifer Holden
:Created: 2020-09-17
"""

import yaml
import shapely
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import os
import numpy as np
import pickle
import pandas as pd
import matplotlib.pyplot as plt
import sys

from driftutils import utils
from plotutils import plotting_utils as putils
logger = utils.logger


class InvalidPlotDuration(Exception):
    pass


def load_polygons(polygons_file):
    """ load the yaml file containing polygon definitions"""

    # load the polygon files from the config file
    with open(polygons_file, 'r') as f:
        polygons = yaml.load(f, Loader=yaml.FullLoader)

    return polygons


def refine_regions(
                datasets,
                zone,
                regions,
                analysis_zones,
                polygons,
                include=True
                ):
    """Refine data to a set of regions based on initial model positions.

    Parameters
    ----------
    datasets : list
        list of datasets to be refined.
    regions : list
         list of shapely polygons that define regions for analysis.
    include : bool
        boolean value representing whether a given zone(s) will be
        included in the analysis. If True, the regions will be included
        in the output. If False, the region will be excluded.

    Returns
    -------
    datasets_refined : list
        A list of all the datasets with data in the zone containing only
        the model runs that have model initial positions inside the zone.
    """

    datasets_refined = []

    for d in datasets:
        model_runs = d.model_run.values
        model_runs_in_region = []

        for m in model_runs:

            # Check that the drift-tool output file contains the correct
            # number of dimensions. The output should either contain 3
            # dims for a comparison set (timestep, model_run, setname) or
            # 2 dims for a single exp output (timestep, model_run).
            if len(d.dims) == 3:
                start_lat = d.mod_start_lat.values[0, m]
                start_lon = d.mod_start_lon.values[0, m]
            elif len(d.dims) == 2:
                start_lat = d.mod_start_lat.values[m]
                start_lon = d.mod_start_lon.values[m]
            else:
                exitstr = ('The drift-tool output provided does not contain '
                           'the correct number of dimensions. Expected 2 '
                           '(single exp) or 3 (comparison exp), but recieved '
                           + str(len(d.dims)))
                sys.exit(exitstr)

            point = Point(start_lat, start_lon)

            for r in regions:

                if r.contains(point):
                    model_runs_in_region.append(m)
                    continue

        if include:
            model_runs_include = model_runs_in_region

        else:
            model_runs_include = [
                m for m in model_runs if m not in model_runs_in_region
                ]

        # load the data into memory. This is necessary to prevent an
        # error later when selecting out data by setname to plot.
        d.load()

        # grab the data for the model runs included in the area.
        dnew = d.sel(model_run=model_runs_include)

        if dnew.model_run.size > 0:
            datasets_refined.append(dnew)

    if not datasets_refined:
        print(zone, 'has no data - dropping from futher analysis')
        analysis_zones.remove(zone)
        del polygons['polygon_coords'][zone]

    return datasets_refined, analysis_zones, polygons


def compile_data_by_region(
    analysis_zones,
    polygons,
    datasets_dict,
    datasets_list
):

    """ compile the data by user defined regions

    Parameters
    ----------
    analysis_zones : list
        list of strings representing the zones to be included in the
        analysis
    polygons : dict
        dictionary containing zone names as keys and polygon area
        coordinates as values
    datasets_dict : dict
        a dictionary with setnames as keys and associated datasets as
        values. ie {CIOPSW: [drifter1, drifter2, etc]}
    datasets_list : list
        a list of datasets, one element for each file in the datadir,
        usually corresponds to one element per each drifter.
        ie, [drifter1, drifter2, etc]

    Returns
    -------
    zonal_dict : dict
        dictionary with zone labels as keys and dictionaries as values.
        The sub-dictionary has setnames as keys and associated datasets
        containing only data for a given region as values. The values for
        the sub-dictionaries are similar to the previous datasets_dict
        variable, but with data representing a sub region instead of the
        full domain.
    zonal_list_dict : dict
        dictionary containing zone labels as keys and lists as values.
        The lists represent the previous datasets_list variable, with
        each list containing only data within the given zone.
    """

    # start a dictionary with zone as keys and dictionary as values
    # (dict has setname as keys and list of datasets as values)
    zonal_dict = {}
    zonal_list_dict = {}
    zonal_dict['all'] = datasets_dict
    zonal_list_dict['all'] = datasets_list

    if analysis_zones != ['all']:
        logger.info('sorting data by region if necessary')
        # first, define any regions that will be used
        # this is a dictionary with zone name as key and polygon areas
        # as values
        regions = {zone: Polygon(polygons['polygon_coords'][zone])
                   for zone in analysis_zones}

        for zone in regions.keys():
            if zone != 'all':
                logger.info('compiling data in ' + zone)
                # a list of all the datasets with data in the zone
                zonal_datasets, analysis_zones, polygons = refine_regions(
                        datasets_list,
                        zone,
                        [regions[zone]],
                        analysis_zones,
                        polygons,
                        include=True,
                        )

                # split the datasets up by setname and add to a dictionary
                sdict = {}

                for setname in datasets_dict.keys():
                    dlist = []
                    for zds in zonal_datasets:

                        # Check that the drift-tool output file contains
                        # the correct number of dimensions. The output
                        # should either contain 3 dims for a comparison set
                        # (timestep, model_run, setname) or 2 dims for a
                        # single exp output (timestep, model_run).
                        if len(zds.dims) == 3:
                            dlist.append(zds.sel(setname=setname))
                        elif len(zds.dims) == 2:
                            dlist.append(zds)
                        else:
                            exitstr = ('The drift-tool output provided does '
                                       'not contain the correct number of '
                                       'dimensions. Expected 2 (single exp) '
                                       'or 3 (comparison exp), but recieved '
                                       + str(len(zds.dims)))
                            sys.exit(exitstr)

                    sdict[setname] = dlist

                # this is a dictionary with setnames as keys and lists
                # of datasets as values. each element in zonal_dict should
                # be the same format as the original datasets_dict but
                # with per zone data instead.
                if [] not in sdict.values():
                    zonal_dict[zone] = sdict
                    zonal_list_dict[zone] = zonal_datasets

    return zonal_dict, zonal_list_dict, analysis_zones, polygons


def count_points_in_region(
        modrunds, lats, lons, regions, zone
        ):

    """ count the number of points present in a given region

    Parameters
    ----------
    modrunds : xarray.dataset
        xarray dataset with data that has been selected such that it only
        contains data for a single model run.
    lats : str
        variable name for the latitude coordinate (ex, mod_lat or obs_lat)
    lons : str
        variable name for the longitude coordinate (ex, mod_lon or obs_lat)
    regions : list
        list of shapely polygons that define regions for analysis.
    zone : str
        identifying label for the zone in question. This should represent
        one of the keys present in the polygon definitions file
        defined in the user config file or 'all' if no file is given.

    Returns
    -------
    ptsin : int
        number of points included in the region
    ptsout : int
        number of points not included in the region
    hexout : int
        number of points not included in the region during the last hour
        of the experiment duration
    expdur : int
        the experiment duration in hours

    """

    # count the observed points
    clist = list(zip(
            modrunds.time.values.astype(
                        'datetime64[s]'
                        ).tolist(),
            modrunds[lats].values.tolist(),
            modrunds[lons].values.tolist()
            ))

    coordlist = [p for p in clist
                 if p[0] is not None
                 and not np.isnan(p[1])
                 and not np.isnan(p[2])]

    timelist = [timel[0] for timel in coordlist]
    latlist = [latl[1] for latl in coordlist]
    lonlist = [lonl[2] for lonl in coordlist]
    coords = list(zip(latlist, lonlist))
    time_since_start = [round((t - timelist[0]).total_seconds()/3600, 1)
                        for t in timelist]

    # if there is an empty model track, it is not possible to find the
    # last hour and the last_hour and expdur should be set to zero. This
    # situation arises because combine_track_segments.py adds a track of 
    # NaNs if there is no model track at an expected time.
    try:
        last_hour = float(time_since_start[-1]-1)
        expdur = time_since_start[-1]
    except IndexError:
        last_hour = 0
        expdur = 0

    ptsin = 0
    ptsout = 0
    hexout = 0
    paircount = 0
    for pair in coords:
        if zone == 'all':
            ptsin += 1
        else:
            if regions[zone].contains(Point(pair)):
                ptsin += 1
                paircount += 1
            else:
                ptsout += 1
                if time_since_start[paircount] > last_hour:
                    hexout += 1
                paircount += 1

    return ptsin, ptsout, hexout, expdur


def create_stats_table(statsdict, skill, time, savedir):
    """ helper function to write out a table of stats to a png

    Parameters
    ----------
    statsdict : dict
        nested dictionary containing the pre-calculated average values
        that will be included in the table (ex, average sep at 48h).
        The format of the nexted dictionary should be:
        {{zone:
            {setname: {skill: {time: value}, skill2: {time: value}}
            {setname2: {skill: {time: value}, skill2: {time: value}}}
        {zone2:
            {setname: {skill: {time: value}, skill2: {time: value}}
            {setname2: {skill: {time: value}, skill2: {time: value}}}}
    skill : str
        string representing the skill to include in the plot. For example,
        'sep' or 'molcard'. The value must represent the keys found in
        statsdict
    time : str
        string representation of the time at which to calculate the stats.
        for example, '48' would give stats for skill at 48h. This value
        must match the keys found in statsdict.
    savedir : str
        path to the folder where the table will be saved

    """
    from pandas.plotting import table

    # rearrange the dictionary so that it will output nicely, then convert
    # it to a dataframe
    newdict = {}
    for z in statsdict.keys():
        entry = {}
        for m in statsdict[z].keys():
            entry[m] = statsdict[z][m][skill][time]
        newdict[z] = entry
    tabledf = pd.DataFrame.from_dict(newdict, orient='index')

    # define the filenames:
    if skill == 'sep':
        skillstr = 'Separation Distance'
    else:
        skillstr = skill.capitalize()
    titlestr = "Average " + skillstr + " at " + str(time) + "h"
    savestr = titlestr.replace(' ', '_')

    # write the data to a csv file.
    tabledf.to_csv(os.path.join(savedir, savestr+'.csv'))

    # format the data so that it displays nicely
    if skill == 'sep':
        for name in tabledf.keys():
            tabledf[name] = tabledf[name]/1000
            tabledf[name] = [("{:.1f}".format(v) + ' km')
                             for v in tabledf[name]]
    else:
        for name in tabledf.keys():
            tabledf[name] = ["{:.3f}".format(v) for v in tabledf[name]]

    # start the table
    fig = plt.figure(figsize=(8, 2))
    ax = fig.add_subplot(111)

    # to add fill colors to the cells, you need to add the following to
    # the ax.table() command:
    # rowColours=rcolors
    # colColours=ccolors
    the_table = ax.table(cellText=tabledf.values,
                         rowLabels=tabledf.index,
                         colLabels=tabledf.columns,
                         loc='center',
                         rowLoc='center',
                         fontsize=15,
                         cellLoc='center',
                         colLoc="center")

    the_table.scale(1.0, 1.5)
    ax.axis("off")

    plt.savefig(os.path.join(savedir, savestr + '.png'),
                bbox_inches='tight',
                dpi=150)
    plt.close()


def create_regions_metadata(zonal_dict, polygons, savedir, overwrite_savedir):

    """ create a dictionary of metadata organized by zone

    Parameters
    ----------
    zonal_dict : dict
        dictionary with zone labels as keys and dictionaries as values.
        The sub-dictionary has setnames as keys and associated datasets
        containing only data for a given region as values. The values for
        the sub-dictionaries are similar to the previous datasets_dict
        variable, but with data representing a sub region instead of the
        full domain.
    polygons : dict
        dictionary containing zone names as keys and polygon area
        coordinates as values
    savedir : str
        path to the directory where the plot will be saved
    overwrite_savedir : bool
        boolean value set in the user config yaml. A value of True will
        allow a savedir to be cleared out and then overwritten with the
        new data. False will cause an error message to be logged and the
        program exits.

    Returns
    -------
    zonal_metadata : dict
        dictionary containing zone labels as keys and dictionaries of
        metadata as values.
    """

    logstr = '\n' + 'compiling metadata for zones'
    logger.info(logstr)

    # for each zone
    countdict = {}
    zonal_metadata = {}

    # first, define any regions that will be used
    # this is a dictionary with zone name as key and polygon areas as values
    if polygons:
        regions = {zone: Polygon(polygons['polygon_coords'][zone])
                   for zone in zonal_dict.keys()
                   if zone in polygons['polygon_coords'].keys()}
    else:
        regions = None

    # cycle though each zone
    for zone in zonal_dict.keys():

        if zone == 'all':
            zonestr = 'full spatial domain'
        else:
            zonestr = zone
        logger.info(zonestr)

        newbuoyids = []
        new_label_list = []

        # for each setname
        countdrifterdict = {}

        for setn in zonal_dict[zone].keys():

            new_label_list.append(setn)

            # for each drifter
            for d in zonal_dict[zone][setn]:
                newbuoyids.append(d.obs_buoyid)
                if d.obs_buoyid in countdrifterdict:
                    countdrifterdict[d.obs_buoyid].append(len(d.model_run))
                else:
                    countdrifterdict[d.obs_buoyid] = [len(d.model_run)]

                for modrun in d.model_run.values.tolist():
                    modrunds = d.sel(model_run=modrun)

                    obsin, obsout, obshexout, expdur = count_points_in_region(
                                modrunds, 'obs_lat', 'obs_lon', regions, zone
                                )
                    modin, modout, modhexout, expdur = count_points_in_region(
                                modrunds, 'mod_lat', 'mod_lon', regions, zone
                                )

                    pcdict = {}
                    pcdict['obs_points_inside'] = obsin
                    pcdict['obs_points_outside'] = obsout
                    pcdict['obs_points_outside_during_last_hour'] = obshexout
                    pcdict['mod_points_inside'] = modin
                    pcdict['mod_points_outside'] = modout
                    pcdict['mod_points_outside_during_last_hour'] = modhexout
                    pcdict['total_experiment_duration_in_hours'] = \
                                                            int(round(expdur))

        # find the max number of model releases per drifter (assume
        # that this is the same for each setname)
        for k in countdrifterdict:
            countdrifterdict[k] = np.nanmax(countdrifterdict[k])

        countdict[zone] = countdrifterdict

        label_list_in_zone = list(np.unique(new_label_list))
        buoyids_in_zone = list(np.unique(newbuoyids))
        num_mod_releases_in_zone = sum(countdict[zone].values())
        zonal_savedir = utils.setup_savedir(
                            savedir,
                            subname=zone,
                            overwrite_savedir=overwrite_savedir
                            )

        zonal_metadata[zone] = {
                    'num_buoyids_in_zone': len(buoyids_in_zone),
                    'buoyids_in_zone': buoyids_in_zone,
                    'total_num_mod_runs_in_zone': num_mod_releases_in_zone,
                    'num_mod_runs_by_drifter': countdict[zone],
                    'list_of_labels_in_zone': label_list_in_zone,
                    'zonal_savedir': zonal_savedir,
                    'points_inside_domain': pcdict,
                    }

    return zonal_metadata


def write_zonal_files(
            zonal_dict,
            zonal_metadata,
            plot_durations=None,
            skills_list=None,
            polygons=None
):
    """ Create a yaml file containing statistics as well as a pickled
    file of binned skill score data for each of the given zones. This
    files are saved in the subdirectories corresponding to individual
    sub-regions.

    Parameters
    ----------
    zonal_dict : dict
        a dictionary with setnames as keys and associated datasets as
        values. ie {CIOPSW: [drifter1, drifter2, etc]}
    zonal_metadata : dict
        dictionary containing zone labels as keys and dictionaries of
        metadata as values.
    plot_durations : list
        list of integers representing the plotting durations (ie, [48]
        would produce a plot representing skill score at 48h, etc)
    skills_list : list
        list of strings representing the skill scores to be plotted
    polygons : dict
        dictionary containing zone names as keys and polygon area
        coordinates as values

    """

    logstr = ('\n' + 'creating yaml files for metadata and stats for each '
              + 'zone and pickling the binned skill scores')
    logger.info(logstr)

    statsdict = {}
    for zone in zonal_metadata.keys():

        if zone == 'all':
            zonestr = 'full spatial domain'
        else:
            zonestr = zone

        logstr = '\ncreating stats yaml files for ' + zonestr
        logger.info(logstr)

        zonedict = zonal_metadata[zone]
        if zone == 'all':
            polycoords = None
            polydict = {
                    'lats': None,
                    'lons': None
                    }
        else:
            polycoords = polygons['polygon_coords'][zone]
            polydict = {
                    'lats': [elem[0] for elem in polycoords],
                    'lons': [elem[1] for elem in polycoords]
                    }

        yamldict = {
            'zone_label': zone,
            'num_buoyids_in_zone': int(zonedict['num_buoyids_in_zone']),
            'buoyids_in_zone': [str(z) for z in zonedict['buoyids_in_zone']],
            'total_num_mod_runs_in_zone': int(
                    zonedict['total_num_mod_runs_in_zone']
                    ),
            'num_mod_runs_by_drifter': {
                str(d): int(
                    zonedict['num_mod_runs_by_drifter'][d]
                    )
                for d in zonedict['num_mod_runs_by_drifter'].keys()
                },
            'list_of_labels_in_zone': [
                    str(z) for z in zonedict['list_of_labels_in_zone']
                    ],
            'zone_definition': polydict,
            }

        # find the average skill scores for each plot duration
        persetdict = {}

        for setname in zonal_metadata[zone]['list_of_labels_in_zone']:
            logger.info('binnng data for ' + setname + ' in ' + zonestr)

            perskilldict = {}
            for skill in skills_list:

                all_mean, all_drifters_all_tracks =\
                    putils.bin_skills_all_drifters(
                        zonal_dict[zone][setname],
                        skill,
                        pickledir=zonedict['zonal_savedir']
                    )

                raw_data_dict = {
                    'all_mean': all_mean,
                    'all_drifters_all_tracks': all_drifters_all_tracks
                    }

                avgskilldict = {}
                if plot_durations:
                    for dur in plot_durations:
                        try:
                            avgskilldict[str(dur)] = float(
                                "{:.5f}".format(all_mean.loc[
                                    all_mean['hours since start'] == dur,
                                    skill
                                ].iloc[0])
                            )
                        except IndexError:
                            msg = f"Invalid plot_duration {dur}. " \
                                  "Please check config file to ensure all "\
                                  "plot_durations are within simulation "\
                                  "time range."
                            raise InvalidPlotDuration(msg)

                perskilldict[str(skill)] = avgskilldict

                # pickle the binned skill score data
                picklename = ('binned_skill_scores_' + zone + '_' + setname
                              + '_' + skill + '.pickle')
                picklefolder = utils.setup_savedir(
                    zonal_metadata[zone]['zonal_savedir'],
                    subname='pickled_data',
                    overwrite_savedir=True,
                    clean_folder=False
                    )

                picklepath = os.path.join(
                    picklefolder,
                    picklename
                    )
                with open(picklepath, 'wb') as handle:
                    pickle.dump(
                        raw_data_dict,
                        handle,
                        protocol=pickle.HIGHEST_PROTOCOL
                        )

            # write the data to the stats.yaml file
            persetdict[str(setname)] = perskilldict

        yamldict['average_skill_at_user_given_plot_durations'] = persetdict
        statsdict[zone] = persetdict

        # save out the metadata to a stats file
        statsfilename = zone + '_stats.yaml'
        statsfilepath = os.path.join(zonedict['zonal_savedir'], statsfilename)
        with open(statsfilepath, 'w') as statsfile:
            yaml.safe_dump(yamldict, statsfile)

    # grab the experiment durations and list of skills
    tlist = []
    sklist = []
    for zone in statsdict.keys():
        setname = list(statsdict[zone].keys())
        for sname in setname:
            skills = list(statsdict[zone][sname].keys())
            for sk in skills:
                time = list(statsdict[zone][sname][sk].keys())
                for t in time:
                    tlist.append(t)
                    sklist.append(sk)

    # create the table(s) and save to png and csv
    tablesavedirelems = zonal_metadata['all']['zonal_savedir'].split('/')[0:-1]
    mainsavedir = '/' + '/'.join(tablesavedirelems)
    tablesavedir = utils.setup_savedir(mainsavedir, 'summary_stats_tables')
    for skill in list(set(sklist)):
        for plot_dur in list(set(tlist)):
            create_stats_table(
                statsdict,
                skill, plot_dur,
                tablesavedir
                )


def append_combined_polygons_boundary(polygons):
    '''calculates the outer boundary of a series of polygons

    Parameters
    ----------
    polygons : dict
        dictionary with zones as keys and lists of coordinate pairs
        defining polygon areas as values

    Returns
    -------
    polygons : dict
        as above with a newly added 'Combined_Polygons_Boundary' key
    '''

    from shapely.ops import unary_union
    from shapely.geometry import mapping

    # convert the lists of coordinates to shapely polygons
    regions = {zone: Polygon(polygons[zone])
               for zone in polygons.keys()}

    # get a list of polygons
    polygons_list = []
    for zone_name in list(regions.keys()):
        polygons_list.append(regions[zone_name])

    outer_boundary_polygons = unary_union(polygons_list)

    # this is a work around for the case where the regions polygons are
    # not connected
    if isinstance(outer_boundary_polygons, shapely.geometry.MultiPolygon):
        outer_boundary_polygons = outer_boundary_polygons.convex_hull

    # add the new polygon to the dict containing the rest of the polygons
    coords = mapping(outer_boundary_polygons)['coordinates']

    polygons['Combined_Polygons_Boundary'] = list(map(list, coords[0]))

    return polygons

def get_zonal_data(
        analysis_zones,
        polygons,
        datasets_dict,
        datasets_list,
        savedir,
        overwrite_savedir,
        plot_durations,
        skills_list,
        use_pickles=False
):
    """ Organize the data by region. If a pickle file already exists for the
    data, this will be used. Otherwise, new pickle files will be created 
    during the first run though of the calculations. Caution should be taken
    here since these pickled files are not deleted when the experiment is
    rerun. This means that if the data input source is changed (new files added 
    to datadir for example) without also modifying the experiment name, the 
    previously generated pickle files containing the unmodified data will still
    be loaded. For this reason, the default behavior for use_pickles is set to 
    False.  

    Parameters
    ----------
    use_pickles : boolean
        boolean value specifying whether to incorporate the use of pickle files
        into the calculations when organizing the data by region.
    analysis_zones : list 
        list of strings referencing the names of the zones that will be analyzed
    polygons : dict
        dictionary of the polygon areas that will be analyzed. The keys of this
        dictionary will be the same as the analysis_zones.
    datasets_dict : dict
        dictionary of datasets that will be analyzed (one entry for each
        experiment)
    datasets_list : list
        list of the xarray datasets that will be analyzed
    savedir : string
        path to the experiment directory
    overwrite_savedir : boolean
        boolean value specifying whether the experiment directory will be 
        overwritten
    plot_durations : list
        list of integers with values representing the plot durations that will
        be included in the output (ex: 24, 48)
    skills_list : list
        list of strings with values representing the skills that will be 
        plotted (ex: 'sep', 'molcard')
    """

    # Organize the data by region. If no polygon file is given, default
    # to using one zone called 'all' that contains all the data. If a
    # polygon file is given, sort the data using analysis_zones provided
    # by the user (if they exist) or by keys from the polygon_file (if
    # no zones are specified). The output from this function will create
    # dictionaries with zone names as keys and values representing
    # the previous non-regional datasets_list and datasets_dict

    def create_zonal_data(
        analysis_zones,
        polygons,
        datasets_dict,
        datasets_list,
        savedir,
        overwrite_savedir,
        plot_durations,
        skills_list,
        ):

        # compile the data by region
        zonal_dict, zonal_list_dict, analysis_zones, polygons =\
                compile_data_by_region(analysis_zones,
                                       polygons,
                                       datasets_dict,
                                       datasets_list)

        # compile some metadata for all the regions to be used later
        zonal_metadata = create_regions_metadata(zonal_dict,
                                                 polygons,
                                                 savedir,
                                                 overwrite_savedir)

        # create a yaml file in each zonal subdirectory containing stats
        write_zonal_files(zonal_dict,
                          zonal_metadata,
                          plot_durations,
                          skills_list,
                          polygons)

        returndict = {
            'zonal_dict': zonal_dict, 
            'zonal_list_dict': zonal_list_dict, 
            'analysis_zones': analysis_zones, 
            'polygons': polygons, 
            'zonal_metadata': zonal_metadata
        }
        return returndict


    if not use_pickles:
        # Fall back to the default, which is to not use pickled data
        returndict = create_zonal_data(
            analysis_zones,
            polygons,
            datasets_dict,
            datasets_list,
            savedir,
            overwrite_savedir,
            plot_durations,
            skills_list,
            )
        return returndict
        
    else:
        # if using pickled data
        bname = os.path.basename(savedir)
        dname = os.path.dirname(savedir)

        utils.setup_savedir(
            dname,
            subname='pickle_files',
            overwrite_savedir=True,
            clean_folder=False
        )

        longlabellist = list(['all']+analysis_zones)+list(datasets_dict.keys())
        pickledir = os.path.join(dname, 'pickle_files')
        picklefilename = 'zonal-data_' + bname + '.pickle'
        picklefile = os.path.join(pickledir, picklefilename)

        # if the pickle file exists
        if os.path.isfile(picklefile):

            with open(picklefile, "rb") as infile:
                pickledict=pickle.load(infile)
                zonal_metadata = pickledict['zonal_metadata']
                zonal_dict = pickledict['zonal_dict']
                zonal_list_dict = pickledict['zonal_list_dict']

            logstr = (('-' * 60) + '\n' +  ('-' * 60) + '\nWARNING! '
                        + 'Data is currently being loaded from a pickle file!\n'
                        + ('-' * 60) + '\n' + ('-' * 60) + '\n\nLoading data from '
                        + picklefile + '\n' + 'NOTE: If you would prefer to '
                        + 're-analyze the data, either set use_pickles to '
                        + 'False in the config file or else remove the file:'
                        + picklefile)
            logger.info(logstr)

            # check to see if the sets are approximately equal by looking in 
            # the file and writing out a list to match with the longlabellist.  
            # Comparing the two long strings should give a value of True. This
            # does not necessarily mean that the data is the correct pickle file
            # to use, but will at least weed out some cases where it is not the 
            # right pickle file. If False, the code will remake the pickle file
            # instead, which takes longer than reading a pickle but doesn't 
            # hault on an error if the keys in the file do not match the 
            # requested zone and experiment keys.
            lablist = []
            for k in zonal_metadata.keys():
                lablist.extend(zonal_metadata[k]['list_of_labels_in_zone'])
            longpicklelist = list(np.unique(lablist)) + list(zonal_dict.keys())
            str_match = (set(longlabellist) == set(longpicklelist))

            if str_match:
                # proceed forward with the data from the pickle
                for zone in zonal_dict.keys():
                    utils.setup_savedir(
                        savedir,
                        subname=zone,
                        overwrite_savedir=overwrite_savedir
                    )

                returndict = {
                    'zonal_dict': zonal_dict,
                    'zonal_list_dict': zonal_list_dict,
                    'analysis_zones': analysis_zones,
                    'polygons': polygons,
                    'zonal_metadata': zonal_metadata
                    }
                return returndict

            else:
                # re-create the data from scratch
                returndict = create_zonal_data(
                    analysis_zones,
                    polygons,
                    datasets_dict,
                    datasets_list,
                    savedir,
                    overwrite_savedir,
                    plot_durations,
                    skills_list,
                    )
                return returndict

        else:
            # if the pickle file does not exist, re-create the data.
            returndict = create_zonal_data(
                analysis_zones,
                polygons,
                datasets_dict,
                datasets_list,
                savedir,
                overwrite_savedir,
                plot_durations,
                skills_list,
                )

            # pickle the data for use later
            pickledict = {
                'zonal_metadata': returndict['zonal_metadata'], 
                'zonal_dict': returndict['zonal_dict'], 
                'zonal_list_dict': returndict['zonal_list_dict'], 
                'polygons': polygons,
                'analysis_zones': analysis_zones
                }

            with open(picklefile, "wb") as outfile:
                pickle.dump(pickledict, outfile)

            return returndict
