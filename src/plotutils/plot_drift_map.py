"""
Plot DriftMap workflow
=============================
:Author: Samuel T. Babalola
:Contributors: Nancy Soontiens, Jennifer Holden
:Created: 2018-07-25

This script provides a workflow that runs the various components of the
DriftMap visualization package.
"""

import matplotlib
import os
import sys

from driftutils import utils
from driftutils import cli

from plotutils.drift_map import dm_plot_driftmap_data
from plotutils.drift_map import dm_plot_histogram
from plotutils.drift_map import dm_plot_ratio_histogram
from plotutils.drift_map import dm_plot_percentage_histogram

matplotlib.use('agg')
logger = utils.logger


def plot_drift_map(
    *,
    data_dir,
    bbox,
    etopo_file,
    plot_dir='plots',
    style='default',
    interval=None,
    plot_model_trajectories=True,
    plot_gif=True,
    plot_hist=True,
    plot_ratio_hist=True,
    plot_perc_hist=True,
):
    """ Create plots of DriftMap output.

    Parameters
    ----------
    data_dir : str
        Specifies the directory of ocean output files to be used
        as inputs to plot ocean data.
    bbox : str
        Path to namelist file with  bounding box to be used in plots.
        (lon_min lat_min lon_max lat_max)
    etopo_file : str
        Path to bathymetry file.
    plot_dir : str
        Specifies the directory in which the plots will be created.
    style : str
        string representing the plot style that will be used when creating
        the plot. By default, this parameter is set to use Matplotlibs
        built in default parameters. Alternately, users can provide one of
        Matplotlibs other available styles or else provide a path to a
        custom .mplstyle style sheet created by the user.
    interval : int
        Spacing between model output times for plotting.
        interval=1 means every output time plotted
        interval=2 means every second output time plotted
        interval=None means only last output time plotted
    plot_model_trajectories : boolean
        Value of True creates driftmap trajectory plots at each
        user defined interval. Currently set to choose plot extremes
        based on the data that will be plotted.
    plot_gif : boolean
        Value of True creates a gif from all times included by the
        user defined interval including hour zero. In this case, the
        bbox extents are forced so that the plots will all have the
        same axis.
    plot_hist : boolean
        Value of True creates histogram plots of cummulative distance
        and displacement.
    plot_ratio_hist : boolean
        Value of True creates the histogram plot of the ratio of
        displacement to cummulative distance.
    plot_perc_hist : boolean
        Value of True creates percentage histogram plots of displacement
        and cummulative distance.
    """

    if not os.path.isdir(data_dir):
        loggerstr = ('\n' + data_dir
                     + ' does not exist. Exiting without plotting!\n')
        logger.info(loggerstr)
        sys.exit()

    if not any(filename.endswith('.nc') for filename in os.listdir(data_dir)):
        loggerstr = ('\n' + 'No .nc files found in '
                     + data_dir + '. Exiting without plotting!\n')
        logger.info(loggerstr)
        sys.exit()

    if etopo_file == 'default':
        etopo_file = ('/home/sdfo000/sitestore7/opp_drift_fa3/software/'
                      + 'misc_files/ETOPO1_Bed_g_gmt4.grd')

    os.makedirs(plot_dir, exist_ok=True)
    logger.info('\nplots will be saved in ' + str(plot_dir))

    for dirpath, dirnames, filenames in os.walk(data_dir, followlinks=True):

        for filename in filenames:
            if not filename.endswith('.nc'):
                continue

            run_date = filename.split('_')[2]

            os.makedirs(
                os.path.join(plot_dir, run_date), exist_ok=True
            )

            fname = os.path.join(dirpath, filename)
            outdir = os.path.join(plot_dir, str(run_date))

            if plot_model_trajectories or plot_gif:
                dm_plot_driftmap_data.plot_model_trajectories(
                    file_name=fname,
                    savedir=outdir,
                    bbox=bbox,
                    etopo_file=etopo_file,
                    interval=interval,
                    plot_gif=plot_gif,
                    style=style
                )

            if plot_hist:
                dm_plot_histogram.plot_histogram(
                    filename=fname,
                    savedir=outdir,
                    style=style
                )

            if plot_ratio_hist:
                dm_plot_ratio_histogram.plot_ratio_histogram(
                    filename=fname,
                    savedir=outdir,
                    style=style
                )

            if plot_perc_hist:
                dm_plot_percentage_histogram.plot_percentage_histogram(
                    filename=fname,
                    savedir=outdir,
                    style=style
                )


def main():

    cli.run(plot_drift_map)
    utils.shutdown_logging()


if __name__ == '__main__':
    main()
