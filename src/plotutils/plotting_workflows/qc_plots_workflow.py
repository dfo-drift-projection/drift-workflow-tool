"""
Plot the quality control type plots (persistence, skills_plus_map,
and ratios_plus_map).
==================================================================
:Author: Jennifer Holden
:Created: 2022-02-28
"""

import os
import matplotlib
import matplotlib.pyplot as plt
import cartopy.crs as ccrs

from driftutils import utils
from plotutils import qc_plot_functions as qcfuncs

logger = utils.logger

matplotlib.use('Agg')

####################################################################
# create plot of skills plus map per drifter per experiment
####################################################################


def plot_skills_plus_map_workflow(
    all_drifter_tracks_dict,
    datasets_dict,
    savedir,
    buoyids,
    overwrite_savedir,
    plotcolors,
    skill_label_params,
    landmask_files,
    etopo_file,
    plot_parameters,
):
    """ create plot of skills plus map per drifter per experiment

    Parameters
    ----------
    all_drifter_tracks_dict : dict
        dictionary containing all information for the tracks to be
        plotted. Key values are experiment setnames and values are
        pandas dataframes.
    datasets_dict : dict
        dictionary with setnames as values and lists of xarray DataSets
        as keys. Each Dataset represents a single observed drifter.
    savedir : str
        Path to the main directory where plots will be saved.
        Subdirectories are created within the code for organization.
    plotcolors : dict
        dictionary containing setnames as keys and a color names as
        values.
    skill_label_params : dict
        dictionary containing label information for common variables
        included in the drift tool output
    etopo_file : str
        Path to the etopo file to use when plotting bathymetry.
        Can be None
    landmask_files : dict
        dictionary containing full paths to landmask files. If None or
        missing, default GSHHS landmask is used. If variable names other
        than standard NEMO conventions are use, landmask_files is a nested
        dictionary instead.
    buoyids : list
        list of strings representing the unique buoyids to be included
        in the plots.
    overwrite_savedir : bool
        boolean value set in the user config yaml. A value of True will
        allow a savedir to be cleared out and then overwritten with the
        new data. False will cause an error message to be logged and the
        program exits.
    plot_parameters : dict
        dictionary containing parameters to use when saving plots
        (resolution, filetype, figure size, etc)

    Returns
    -------
    creates plots specified by plot_selection in the
    user_plotting_config.yaml and saves them in savedir.

    """

    logger.info(
        '\ncreating per drifter per experiment plots of maps plus skills'
        )

    qc_tracks_savedir = utils.setup_savedir(
                            savedir,
                            'qc_plots/qc_tracks_plots',
                            overwrite_savedir=overwrite_savedir
                            )

    # cycle though the experiments
    for setname in datasets_dict.keys():

        indv_qc_tracks_savedir =\
            utils.setup_savedir(
                qc_tracks_savedir,
                str(setname),
                overwrite_savedir=overwrite_savedir
                )

        for drifterid in buoyids:

            qcfuncs.do_plot_skills_plus_map(
                setname,
                drifterid,
                datasets_dict,
                plotcolors,
                skill_label_params,
                etopo_file,
                all_drifter_tracks_dict,
                plot_parameters,
                dataproj=ccrs.PlateCarree(),
                proj=ccrs.PlateCarree(),
                )

            skillmapfignamepng =\
                "{}_{}_skills_plus_map.png".format(setname, str(drifterid))
            plt.savefig(
                os.path.join(indv_qc_tracks_savedir, skillmapfignamepng),
                bbox_inches='tight',
                dpi=300
                )
            logger.info(
                'saving ' + os.path.join(
                    indv_qc_tracks_savedir,
                    skillmapfignamepng
                    )
                )
            plt.close()


####################################################################
# create plot of ratios plus map per drifter per experiment
####################################################################

def plot_ratios_plus_map_workflow(
    all_drifter_tracks_dict,
    datasets_dict,
    savedir,
    buoyids,
    overwrite_savedir,
    plotcolors,
    skill_label_params,
    plot_parameters,
):
    """ create plot of obsratio and modratio plus map per drifter per
    experiment

    Parameters
    ----------
    all_drifter_tracks_dict : dict
        dictionary containing all information for the tracks to be
        plotted. Key values are experiment setnames and values are
        pandas dataframes.
    datasets_dict : dict
        dictionary with setnames as values and lists of xarray DataSets
        as keys. Each Dataset represents a single observed drifter.
    savedir : str
        Path to the main directory where plots will be saved.
        Subdirectories are created within the code for organization.
    plotcolors : dict
        dictionary containing setnames as keys and a color names as
        values.
    skill_label_params : dict
        dictionary containing label information for common variables
        included in the drift tool output
    buoyids : list
        list of strings representing the unique buoyids to be included
        in the plots.
    overwrite_savedir : bool
        boolean value set in the user config yaml. A value of True will
        allow a savedir to be cleared out and then overwritten with the
        new data. False will cause an error message to be logged and the
        program exits.
    plot_parameters : dict
        dictionary containing parameters to use when saving plots
        (resolution, filetype, figure size, etc)

    Returns
    -------
    creates plots specified by plot_selection in the
    user_plotting_config.yaml and saves them in savedir.

    """

    logger.info(
        '\ncreating per drifter per experiment plots of maps plus ratios'
        )

    qc_tracks_ratios_savedir = utils.setup_savedir(
                            savedir,
                            'qc_plots/qc_ratio_plots',
                            overwrite_savedir=overwrite_savedir
                            )

    for setname in datasets_dict.keys():

        indv_qc_tracks_ratios_savedir = utils.setup_savedir(
                    qc_tracks_ratios_savedir,
                    str(setname),
                    overwrite_savedir=overwrite_savedir
                    )

        for drifterid in buoyids:

            qcfuncs.do_plot_ratios_plus_map(
                setname,
                drifterid,
                datasets_dict,
                plotcolors,
                skill_label_params,
                all_drifter_tracks_dict,
                plot_parameters,
                dataproj=ccrs.PlateCarree(),
                proj=ccrs.PlateCarree(),
                )

            ratiomapfignamepng =\
                "{}_{}_ratio_plus_map.png".format(setname, str(drifterid))
            plt.savefig(
                os.path.join(
                    indv_qc_tracks_ratios_savedir,
                    ratiomapfignamepng
                    ),
                bbox_inches='tight',
                dpi=300
                )
            logger.info(
                'saving ' + os.path.join(
                    indv_qc_tracks_ratios_savedir,
                    ratiomapfignamepng
                    )
                )
            plt.close()

####################################################################
# Plot the persistence drifter tracks
####################################################################


def plot_persistence_workflow(
    all_drifter_tracks_dict,
    datasets_dict,
    savedir,
    plotcolors,
    etopo_file,
    buoyids,
    overwrite_savedir,
    plot_parameters,
    zone
):

    """ Plot the persistence drifter tracks

    Parameters
    ----------
    all_drifter_tracks_dict : dict
        dictionary containing all information for the tracks to be
        plotted. Key values are experiment setnames and values are
        pandas dataframes.
    datasets_dict : dict
        dictionary with setnames as values and lists of xarray DataSets
        as keys. Each Dataset represents a single observed drifter.
    savedir : str
        Path to the main directory where plots will be saved.
        Subdirectories are created within the code for organization.
    plotcolors : dict
        dictionary containing setnames as keys and a color names as
        values.
    etopo_file : str
        Path to the etopo file to use when plotting bathymetry.
        Can be None
    buoyids : list
        list of strings representing the unique buoyids to be included
        in the plots.
    overwrite_savedir : bool
        boolean value set in the user config yaml. A value of True will
        allow a savedir to be cleared out and then overwritten with the
        new data. False will cause an error message to be logged and the
        program exits.
    plot_parameters : dict
        dictionary containing parameters to use when saving plots
        (resolution, filetype, figure size, etc)
    zone : str
        string representing the label of the user defined polygon area
        in which to plot data. If no sub zones are defined, zone label
        'all' is used and the entire dataset is plotted.

    Returns
    -------
    creates plots specified by plot_selection in the
    user_plotting_config.yaml and saves them in savedir.

    """

    logger.info(
        '\ncreating per drifter per experiment plots of persistence'
        )

    # set up the qc plots savedir
    persistence_tracks_subdir = utils.setup_savedir(
                            savedir,
                            'qc_plots/qc_persistence_plots',
                            overwrite_savedir=overwrite_savedir
                            )

    # cycle though each experiment
    for setname in datasets_dict.keys():

        # set up the per experiment savedir
        persistence_tracks_savedir = utils.setup_savedir(
                    persistence_tracks_subdir,
                    str(setname),
                    overwrite_savedir=overwrite_savedir
                    )

        # plot the tracks individually for each drifter and save them in the
        # previously made subdirectory for each experiment.
        for drifterid in buoyids:
            logger.info(
                '\nplotting persistence map for ' + setname + ' for drifter '
                + str(drifterid) + ' in subdomain ' + str(zone)
            )

            qcfuncs.do_plot_persistence(
                all_drifter_tracks_dict,
                datasets_dict,
                setname,
                etopo_file,
                plotcolors,
                plot_parameters,
                drifterid,
                zone=zone
                )

            # save the figure
            persfname = (str(drifterid) + "_persistence_tracks."
                         + plot_parameters['output_filetype'])

            plt.savefig(
                os.path.join(persistence_tracks_savedir, persfname),
                bbox_inches='tight',
                dpi=plot_parameters['dpi']
            )

            plt.close()
            logger.info(
                'saving ' +
                os.path.join(persistence_tracks_savedir, persfname)
            )
