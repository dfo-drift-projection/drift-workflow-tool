"""
Plot workflow script
================
:Author: Jennifer Holden
:Created: 2020-07-07
"""

import os
import matplotlib
matplotlib.use('Agg')
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from cartopy.mpl.ticker import (LongitudeFormatter, LatitudeFormatter)

from driftutils import utils
from plotutils import plotting_utils as putils
from plotutils import map_functions as mapfuncs

logger = utils.logger

####################################################################
# Plot all observed drifter tracks in set
# This is for plotting the observed data only. If the model tracks
# are required, use 'plot_all_drifter_tracks_with_mod' option
# instead.
####################################################################
def plot_all_drifter_tracks_workflow(
    all_drifter_tracks_dict,
    etopo_file,
    landmask_files,
    landmask_type,
    savedir,
    plotcolors,
    plot_parameters,
    zone='all'
):
    """ Plot all observed drifter tracks from all experiments on the same axis

    Parameters
    ----------
    all_drifter_tracks_dict : dict
        dictionary containing all information for the tracks to be
        plotted. Key values are experiment setnames and values are
        pandas dataframes.
    etopo_file : str
        Path to the etopo file to use when plotting bathymetry.
        Can be None
    landmask_files : dict
        dictionary containing full paths to landmask files. If None or
        missing, default GSHHS landmask is used. If variable names other
        than standard NEMO conventions are use, landmask_files is a nested
        dictionary instead.
    landmask_type : str
        string specifying the type of landmask that will be use (key from
        the landmask_files dictionary or None).
    savedir : str
        Path to the main directory where plots will be saved.
        Subdirectories are created within the code for organization.
    plotcolors : dict
        dictionary containing setnames as keys and a color names as
        values.
    plot_parameters : dict
        dictionary containing parameters to use when saving plots
        (resolution, filetype, figure size, etc)
    zone : str
        string representing the label of the user defined polygon area
        in which to plot data. If no sub zones are defined, zone label
        'all' is used and the entire dataset is plotted.

    Returns
    -------
    creates plots specified by plot_selection parameter in the
    user plotting config yaml file and saves them in savedir
    """

    logger.info('\nplotting all observed drifter tracks')

    alldf = pd.concat([df for df in all_drifter_tracks_dict.values()])
    alldict = {}
    alldict['alltracks'] = alldf
    exp = list(all_drifter_tracks_dict.keys())[0]

    # Create a figure object (right now, it's best to use PlateCarree()
    # for both the data projection and data transform functions. We
    # should revisit this at a later date.
    fig = plt.figure(figsize=(
                        plot_parameters['figure_height'],
                        plot_parameters['figure_width']))
    dataproj = ccrs.PlateCarree()
    proj = ccrs.PlateCarree()

    # Generate axes using Cartopy
    ax = fig.add_subplot(1,1,1, projection=proj)

    ################################
    # calculate and set plot extent
    ################################
    plot_extremes = mapfuncs.finalize_plot_extremes(
            data=alldict,
            proj=proj,
            dataproj=dataproj,
            min_aspect_ratio=plot_parameters['plot_aspect_ratio'],
            lon_buff_percent=None,
            lat_buff_percent=None)
    map_extremes = plot_extremes['padded_extremes']

    # use the calculated extents to create the plot
    ax.set_extent(map_extremes)

    ################################
    # plot the obs drifter tracks
    ################################
    savestr = mapfuncs.plot_drifter_tracks(
        all_drifter_tracks_dict[exp],
        plot_parameters,
        obs_tracks=True,
        mod_tracks=False,
        pers_tracks=False,
        start_dot=True,
        mod_initial_positions=False,
        ax=ax,
        map_extremes=map_extremes,
        obs_color=None,
        mod_color=plotcolors[exp],
        dot_color=None,
        mod_dot_color=None,
        fig=fig,
        add_landmask=True,
        landmask_type=landmask_type,
        landmask_files=landmask_files,
        etopo_file=etopo_file,
        add_gridlines=True
        )

    # add a title
    if zone != 'all':
        ax.set_title(zone)
    fig.tight_layout()

    ################################
    # legend
    ################################
    # Adjust legend to show the dataset name instead of "modelled tracks"
    handles, labels = ax.get_legend_handles_labels()
    newlabels = ['observed tracks']
    mapfuncs.shift_legend(
                    ax,
                    newlabels,
                    handles,
                    plot_parameters['legend_label_fontsize']
                )

    ################################
    # save the figure
    ################################
    savestr = savestr + '_' + zone + '.' + plot_parameters['output_filetype']
    logger.info('saving ' + os.path.join(savedir, savestr))
    plt.savefig(
        os.path.join(savedir, savestr),
        bbox_inches='tight',
        dpi=plot_parameters['dpi'])
    plt.close()


####################################################################
# Plot all observed drifter tracks plus all modelled tracks from all
# experiments on the same axis. If only the observed tracks are
# required, use 'plot_all_drifter_tracks' option instead.
####################################################################

def plot_all_drifter_tracks_with_mod_workflow(
    all_drifter_tracks_dict,
    etopo_file,
    landmask_files,
    landmask_type,
    savedir,
    plotcolors,
    plot_parameters,
    zone='all'
):
    """ Plot all observed drifter tracks plus all modelled tracks from
    all experiments on the same axis

    Parameters
    ----------
    all_drifter_tracks_dict : dict
        dictionary containing all information for the tracks to be
        plotted. Key values are experiment setnames and values are
        pandas dataframes.
    etopo_file : str
        Path to the etopo file to use when plotting bathymetry.
        Can be None
    landmask_files : dict
        dictionary containing full paths to landmask files. If None or
        missing, default GSHHS landmask is used. If variable names other
        than standard NEMO conventions are use, landmask_files is a nested
        dictionary instead.
    landmask_type : str
        string specifying the type of landmask that will be use (key from
        the landmask_files dictionary or None).
    savedir : str
        Path to the main directory where plots will be saved.
        Subdirectories are created within the code for organization.
    plotcolors : dict
        dictionary containing setnames as keys and a color names as
        values.
    plot_parameters : dict
        dictionary containing parameters to use when saving plots
        (resolution, filetype, figure size, etc)
    zone : str
        string representing the label of the user defined polygon area
        in which to plot data. If no sub zones are defined, zone label
        'all' is used and the entire dataset is plotted.

    Returns
    -------
    creates plots specified by plot_selection parameter in the
    user plotting config yaml file and saves them in savedir
    """

    logger.info('\nplotting all observed and modelled drifter tracks')

    alldf = pd.concat([df for df in all_drifter_tracks_dict.values()])
    alldict = {}
    alldict['alltracks'] = alldf

    # Create a figure object (right now, it's best to use PlateCarree()
    # for both the data projection and data transform functions. We
    # should revisit this at a later date.
    fig = plt.figure(figsize=(
                        plot_parameters['figure_height'],
                        plot_parameters['figure_width']))
    dataproj = ccrs.PlateCarree()
    proj = ccrs.PlateCarree()

    # Generate axes using Cartopy
    ax = fig.add_subplot(1,1,1, projection=proj)

    ################################
    # calculate and set plot extent
    ################################
    plot_extremes = mapfuncs.finalize_plot_extremes(
            data=alldict,
            proj=proj,
            dataproj=dataproj,
            min_aspect_ratio=plot_parameters['plot_aspect_ratio'],
            lon_buff_percent=None,
            lat_buff_percent=None)
    map_extremes = plot_extremes['padded_extremes']

    # use the calculated extents to create the plot
    ax.set_extent(map_extremes)

    ################################
    # plot the bathymetry
    ################################
    #add bathymetery if specified
    plot = mapfuncs.plot_bathymetry(
        ax, map_extremes,
        plot_parameters=plot_parameters,
        bathymetry_file=etopo_file,
        dataproj=dataproj,
        add_contourf=True,
        colorbar_visible=True,
        add_contours=False,
        fig=fig
    )
    ################################
    # plot the landmask
    ################################

    mapfuncs.plot_land_mask(
        ax, map_extremes,
        dataproj=dataproj,
        landmask_type=landmask_type,
        landmask_files=landmask_files)

    ################################
    # plot the mod drifter tracks
    ################################
    legend_labels = []
    for exp in list(all_drifter_tracks_dict.keys()):
        savestr = mapfuncs.plot_drifter_tracks(
            all_drifter_tracks_dict[exp],
            plot_parameters,
            obs_tracks=False,
            mod_tracks=True,
            pers_tracks=False,
            start_dot=False,
            mod_initial_positions=False,
            ax=ax,
            map_extremes=map_extremes,
            obs_color=None,
            mod_color=plotcolors[exp],
            dot_color=None,
            mod_dot_color=None,
            fig=fig
        )
        legend_labels.append(exp)

    ################################
    # plot the obs drifter tracks
    ################################
    obs_savestr = mapfuncs.plot_drifter_tracks(
        all_drifter_tracks_dict[legend_labels[0]],
        plot_parameters,
        obs_tracks=True,
        mod_tracks=False,
        pers_tracks=False,
        start_dot=False,
        mod_initial_positions=False,
        ax=ax,
        map_extremes=map_extremes,
        obs_color=None,
        mod_color=plotcolors[exp],
        dot_color=None,
        mod_dot_color=None,
        fig=fig
    )
    savestr = 'drifter-tracks_obs-mod'

    ################################
    # add gridlines and title
    ################################
    ax.gridlines(
        crs=dataproj,
        draw_labels=['bottom', 'left'],
        xformatter=LongitudeFormatter(),
        xlabel_style={'rotation': 45, 'ha': 'center'},
        yformatter=LatitudeFormatter(),
        ylabel_style={'rotation': 45, 'ha': 'center'}
    )

    # add a title and legend, then save the plot
    if zone != 'all':
        ax.set_title(zone)

    fig.tight_layout()

    ################################
    # legend
    ################################
    # Adjust legend to show the dataset name instead of "modelled tracks"
    handles, labels = ax.get_legend_handles_labels()
    newlabels = (legend_labels + ['observed tracks'])
    mapfuncs.shift_legend(
                    ax,
                    newlabels,
                    handles,
                    plot_parameters['legend_label_fontsize']
                )

    ################################
    # save the figure
    ################################
    savestrpng = savestr + '_' + zone + '.' + plot_parameters['output_filetype']
    logger.info('saving ' + os.path.join(savedir, savestr))
    plt.savefig(
        os.path.join(savedir, savestrpng),
        bbox_inches='tight',
        dpi=plot_parameters['dpi'])
    plt.close()


####################################################################
# Plot the drifter tracks (one obs track and the modelled tracks
# from one experiment per plot)
####################################################################
def plot_track_per_drifter_workflow(
    all_drifter_tracks_dict,
    etopo_file,
    landmask_files,
    landmask_type,
    savedir,
    plotcolors,
    plot_parameters,
    overwrite_savedir,
    zone='all'
):
    """ individual track plots for each drifter for each dataset

    Parameters
    ----------
    all_drifter_tracks_dict : dict
        dictionary containing all information for the tracks to be
        plotted. Key values are experiment setnames and values are
        pandas dataframes.
    etopo_file : str
        Path to the etopo file to use when plotting bathymetry.
        Can be None
    landmask_files : dict
        dictionary containing full paths to landmask files. If None or
        missing, default GSHHS landmask is used. If variable names other
        than standard NEMO conventions are use, landmask_files is a nested
        dictionary instead.
    landmask_type : str
        string specifying the type of landmask that will be use (key from
        the landmask_files dictionary or None).
    savedir : str
        Path to the main directory where plots will be saved.
        Subdirectories are created within the code for organization.
    plotcolors : dict
        dictionary containing setnames as keys and a color names as
        values.
    plot_parameters : dict
        dictionary containing parameters to use when saving plots
        (resolution, filetype, figure size, etc)
    overwrite_savedir : boolean
        boolean value set in the user config yaml. A value of True will
        allow a savedir to be cleared out and then overwritten with the
        new data. False will cause an error message to be logged and the
        program exits.
    zone : str
        string representing the label of the user defined polygon area
        in which to plot data. If no sub zones are defined, zone label
        'all' is used and the entire dataset is plotted.

    Returns
    -------
    creates plots specified by plot_selection parameter in the
    user plotting config yaml file and saves them in savedir
    """

    logger.info(
        '\nplot tracks individually for each drifter for each experiment'
        )
    for experiment in all_drifter_tracks_dict.keys():
        indv_tracks_savedir = utils.setup_savedir(
            savedir,
            ('tracks_per_drifter/' + str(experiment)),
            overwrite_savedir = overwrite_savedir
        )

        # set the landmask if there are files given.
        landmask_type = mapfuncs.get_landmask_type(landmask_files, experiment)

        # begin to plot the tracks
        logger.info('\nplotting drifter tracks for' + experiment)

        for drifterid in np.unique(
            all_drifter_tracks_dict[experiment]['buoyid'].values
            ):

            logger.info('......' + drifterid)

            # Create a figure object (right now, it's best to use PlateCarree()
            # for both the data projection and data transform functions. We
            # should revisit this at a later date.
            fig = plt.figure(figsize=(
                                plot_parameters['figure_height'],
                                plot_parameters['figure_width']))
            dataproj = ccrs.PlateCarree()
            proj = ccrs.PlateCarree()

            # Generate axes using Cartopy
            ax = fig.add_subplot(1,1,1, projection=proj)

            ################################
            # calculate and set plot extent
            ################################

            expdf = all_drifter_tracks_dict[experiment]
            datadf = expdf.loc[expdf['buoyid'] == drifterid]

            plot_extremes = mapfuncs.finalize_plot_extremes(
                    data=datadf,
                    proj=proj,
                    dataproj=dataproj,
                    min_aspect_ratio=plot_parameters['plot_aspect_ratio'],
                    lon_buff_percent=None,
                    lat_buff_percent=None)
            map_extremes = plot_extremes['padded_extremes']

            # use the calculated extents to create the plot
            ax.set_extent(map_extremes)

            ################################
            # plot the obs drifter tracks
            ################################
            savestr = mapfuncs.plot_drifter_tracks(
                all_drifter_tracks_dict[experiment],
                plot_parameters,
                obs_tracks=True,
                mod_tracks=True,
                pers_tracks=False,
                start_dot=True,
                mod_initial_positions=True,
                ax=ax,
                map_extremes=map_extremes,
                obs_color=None,
                mod_color=plotcolors[experiment],
                dot_color=None,
                mod_dot_color='orange',
                fig=fig,
                add_landmask=True,
                landmask_type=landmask_type,
                landmask_files=landmask_files,
                etopo_file=etopo_file,
                add_gridlines=True,
                drifterid=drifterid
                )

            # add a title
            titlestr = drifterid + '\n' + experiment
            ax.set_title(titlestr)
            fig.tight_layout()

            ################################
            # save the figure
            ################################
            savestr = savestr + '_' + zone + '.png'
            logger.info(
                'saving ' +
                os.path.join(indv_tracks_savedir, savestr)
            )
            plt.savefig(
                os.path.join(indv_tracks_savedir, savestr),
                bbox_inches='tight',
                dpi=plot_parameters['dpi'])
            plt.close()


####################################################################
# Plot the comparison drifter tracks (one obs track, modelled tracks
# from all sets)
####################################################################
def plot_track_comparison_workflow(
    all_drifter_tracks_dict,
    etopo_file,
    landmask_files,
    landmask_type,
    savedir,
    plotcolors,
    plot_parameters,
    buoyids,
    overwrite_savedir,
    zone='all'
):
    """ track per drifter with modelled tracks for all datasets on each plot

    Parameters
    ----------
    all_drifter_tracks_dict : dict
        dictionary containing all information for the tracks to be
        plotted. Key values are experiment setnames and values are
        pandas dataframes.
    etopo_file : str
        Path to the etopo file to use when plotting bathymetry.
        Can be None
    landmask_files : dict
        dictionary containing full paths to landmask files. If None or
        missing, default GSHHS landmask is used. If variable names other
        than standard NEMO conventions are use, landmask_files is a nested
        dictionary instead.
    landmask_type : str
        string specifying the type of landmask that will be use (key from
        the landmask_files dictionary or None).
    savedir : str
        Path to the main directory where plots will be saved.
        Subdirectories are created within the code for organization.
    plotcolors : dict
        dictionary containing setnames as keys and a color names as
        values.
    plot_parameters : dict
        dictionary containing parameters to use when saving plots
        (resolution, filetype, figure size, etc)
    overwrite_savedir : boolean
        boolean value set in the user config yaml. A value of True will
        allow a savedir to be cleared out and then overwritten with the
        new data. False will cause an error message to be logged and the
        program exits.
    zone : str
        string representing the label of the user defined polygon area
        in which to plot data. If no sub zones are defined, zone label
        'all' is used and the entire dataset is plotted.

    Returns
    -------
    creates plots specified by plot_selection parameter in the
    user plotting config yaml file and saves them in savedir
    """

    # plot the tracks individually for each drifter for each model.
    comparison_tracks_subdir = utils.setup_savedir(
        savedir,
        'comparison_tracks_per_drifter',
        overwrite_savedir = overwrite_savedir
        )

    for drifterid in buoyids:
        drifterdict = {}
        for d in all_drifter_tracks_dict.keys():
            drifterdict[d] = all_drifter_tracks_dict[d][
                all_drifter_tracks_dict[d]['buoyid'] == drifterid
            ]

        # Create a figure object (right now, it's best to use PlateCarree()
        # for both the data projection and data transform functions. We
        # should revisit this at a later date.
        fig = plt.figure(figsize=(
                            plot_parameters['figure_height'],
                            plot_parameters['figure_width']))
        dataproj = ccrs.PlateCarree()
        proj = ccrs.PlateCarree()

        # Generate axes using Cartopy
        ax = fig.add_subplot(1,1,1, projection=proj)

        ################################
        # calculate and set plot extent
        ################################
        plot_extremes = mapfuncs.finalize_plot_extremes(
                data=drifterdict,
                proj=proj,
                dataproj=dataproj,
                min_aspect_ratio=plot_parameters['plot_aspect_ratio'],
                lon_buff_percent=None,
                lat_buff_percent=None)
        map_extremes = plot_extremes['padded_extremes']

        # use the calculated extents to create the plot
        ax.set_extent(map_extremes)

        ################################
        # plot the bathymetry
        ################################
        #add bathymetery if specified
        plot = mapfuncs.plot_bathymetry(
            ax, map_extremes,
            plot_parameters=plot_parameters,
            bathymetry_file=etopo_file,
            dataproj=dataproj,
            add_contourf=True,
            colorbar_visible=True,
            add_contours=False,
            fig=fig
        )
        ################################
        # plot the landmask
        ################################

        mapfuncs.plot_land_mask(
            ax, map_extremes,
            dataproj=dataproj,
            landmask_type=landmask_type,
            landmask_files=landmask_files)

        ################################
        # plot the mod drifter tracks
        ################################
        legend_labels = []
        for experiment in list(drifterdict.keys()):
            logger.info('plotting modelled drifter tracks for' + experiment)
            savestr = mapfuncs.plot_drifter_tracks(
                drifterdict[experiment],
                plot_parameters,
                obs_tracks=False,
                mod_tracks=True,
                pers_tracks=False,
                start_dot=False,
                mod_initial_positions=False,
                ax=ax,
                map_extremes=map_extremes,
                obs_color=None,
                mod_color=plotcolors[experiment],
                dot_color=None,
                mod_dot_color=None,
                fig=fig,
                drifterid=drifterid
            )
            legend_labels.append(experiment)


        ################################
        # plot the obs drifter tracks
        ################################
        # plot the observed track (arbitrarily choosing to do this
        # using whatever is the first dataset)
        logger.info('\n......plotting observed drifter tracks')
        obs_savestr = mapfuncs.plot_drifter_tracks(
            drifterdict[list(drifterdict.keys())[0]],
            plot_parameters,
            obs_tracks=True,
            mod_tracks=False,
            pers_tracks=False,
            start_dot=True,
            mod_initial_positions=False,
            ax=ax,
            map_extremes=map_extremes,
            obs_color=None,
            mod_color=None,
            dot_color=None,
            mod_dot_color=None,
            fig=fig,
            drifterid=drifterid
        )
        savestr = 'drifter-tracks_obs-mod'

        ################################
        # add gridlines and title
        ################################
        ax.gridlines(
            crs=dataproj,
            draw_labels=['bottom', 'left'],
            xformatter=LongitudeFormatter(),
            xlabel_style={'rotation': 45, 'ha': 'center'},
            yformatter=LatitudeFormatter(),
            ylabel_style={'rotation': 45, 'ha': 'center'}
        )

        ################################
        # add a title
        ################################
        ax.set_title(str(drifterid))
        fig.tight_layout()

        ################################
        # legend
        ################################
        # Adjust legend to show the dataset name instead of "modelled tracks"
        handles, labels = ax.get_legend_handles_labels()
        newlabels = (list(drifterdict.keys()) +
            ['observed tracks', 'obs deployment point'])
        mapfuncs.shift_legend(
                    ax,
                    newlabels,
                    handles,
                    plot_parameters['legend_label_fontsize']
                    )

        ################################
        # save the figure
        ################################
        cfigname = "{}_comparison_tracks_{}.{}".format(
                            str(drifterid),
                            zone,
                            plot_parameters['output_filetype']
                            )
        logger.info('saving '+os.path.join(comparison_tracks_subdir,cfigname))
        plt.savefig(
            os.path.join(comparison_tracks_subdir, cfigname),
            bbox_inches='tight',
            dpi=plot_parameters['dpi'])
        plt.close()


####################################################################
# Plot the model drifter start points plus polygon areas
####################################################################
def plot_positions_with_polygons_workflow(
    all_drifter_tracks_dict,
    etopo_file,
    landmask_files,
    landmask_type,
    savedir,
    plotcolors,
    zonecolors,
    plot_parameters,
    polygons,
    zone='all'
):
    """ Plot the model drifter start points plus polygon areas

    Parameters
    ----------
    all_drifter_tracks_dict : dict
        dictionary containing all information for the tracks to be
        plotted. Key values are experiment setnames and values are
        pandas dataframes.
    etopo_file : str
        Path to the etopo file to use when plotting bathymetry.
        Can be None
    landmask_files : dict
        dictionary containing full paths to landmask files. If None or
        missing, default GSHHS landmask is used. If variable names other
        than standard NEMO conventions are use, landmask_files is a nested
        dictionary instead.
    landmask_type : str
        string specifying the type of landmask that will be use (key from
        the landmask_files dictionary or None).
    savedir : str
        Path to the main directory where plots will be saved.
        Subdirectories are created within the code for organization.
    plotcolors : dict
        dictionary containing setnames as keys and a color names as
        values.
    plot_parameters : dict
        dictionary containing parameters to use when saving plots
        (resolution, filetype, figure size, etc)
    polygons : dict
        dictionary containing zone names as keys and polygon area
        coordinates as values
    zonecolors : dict
        dictionary with values of color to use for each zone key.
    zone : str
        string representing the label of the user defined polygon area
        in which to plot data. If no sub zones are defined, zone label
        'all' is used and the entire dataset is plotted.

    Returns
    -------
    creates plots specified by plot_selection parameter in the
    user plotting config yaml file and saves them in savedir
    """

    logger.info('\nplotting modelled start points with polygons')

    drifterdf = all_drifter_tracks_dict[list(all_drifter_tracks_dict.keys())[0]]

    # Create a figure object (right now, it's best to use PlateCarree()
    # for both the data projection and data transform functions. We
    # should revisit this at a later date.
    fig = plt.figure(figsize=(
                    plot_parameters['figure_height'],
                    plot_parameters['figure_width']))
    dataproj = ccrs.PlateCarree()
    proj = ccrs.PlateCarree()

    # Generate axes using Cartopy
    ax = fig.add_subplot(1,1,1, projection=proj)

    ################################
    # calcalate and set plot extent
    ################################

    plot_extremes = mapfuncs.finalize_plot_extremes(
            data=drifterdf,
            proj=proj,
            dataproj=dataproj,
            min_aspect_ratio=plot_parameters['plot_aspect_ratio'],
            lon_buff_percent=None,
            lat_buff_percent=None,
            polygons=polygons,
            zone=zone)
    map_extremes = plot_extremes['padded_extremes']

    # use the calculated extents to create the plot
    ax.set_extent(map_extremes)

    ################################
    # plot the bathymetry
    ################################
    #add bathymetery if specified
    plot = mapfuncs.plot_bathymetry(
        ax, map_extremes,
        plot_parameters=plot_parameters,
        bathymetry_file=etopo_file,
        dataproj=dataproj,
        add_contourf=True,
        colorbar_visible=True,
        add_contours=False,
        fig=fig
        )
    ################################
    # plot the landmask
    ################################
    mapfuncs.plot_land_mask(
        ax, map_extremes,
        dataproj=dataproj,
        landmask_type=landmask_type,
        landmask_files=landmask_files)

    ###############################
    # plot the polygons
    ###############################
    mapfuncs.plot_polygons(
        polygons,
        ax=ax,
        zones = zone,
        zcolors = zonecolors,
        )
    ###############################
    # plot the mod initial positions
    ###############################
    use_exp = list(all_drifter_tracks_dict.keys())[0]
    savestr = mapfuncs.plot_drifter_tracks(
        all_drifter_tracks_dict[use_exp],
        plot_parameters,
        obs_tracks=False,
        mod_tracks=False,
        pers_tracks=False,
        start_dot=False,
        mod_initial_positions=True,
        ax=ax,
        map_extremes=map_extremes,
        obs_color=None,
        mod_color=plotcolors[use_exp],
        dot_color=None,
        mod_dot_color=None,
        fig=fig
        )
    ################################
    # gridlines and title
    ################################
    ax.gridlines(
        crs=dataproj,
        draw_labels=['bottom', 'left'],
        xformatter=LongitudeFormatter(),
        xlabel_style={'rotation': 45, 'ha': 'center'},
        yformatter=LatitudeFormatter(),
        ylabel_style={'rotation': 45, 'ha': 'center'}
    )

    # add a title
    if zone == 'all':
        zone_title_str = "full spatial domain"
    else:
        zone_title_str = zone
    titlestr = 'Distribution of model release points in ' + zone_title_str
    ax.set_title(titlestr)

    fig.tight_layout()

    ################################
    # legend
    ################################
    # adjust legend to show the dataset name instead of "modelled tracks"
    handles, labels = ax.get_legend_handles_labels()
    if zone == 'all':
        if polygons:
            leg_zones = list(polygons['polygon_coords'].keys())
            if len(leg_zones) != 1:
                if 'Combined_Polygons_Boundary' in leg_zones:
                    leg_zones.remove('Combined_Polygons_Boundary')
        else:
            leg_zones = []
    else:
        leg_zones = [zone]
    newlabels = (leg_zones + ['initial model positions'])

    # shift the legend so that it doesn't cover the plot
    mapfuncs.shift_legend(
                    ax,
                    newlabels,
                    handles,
                    plot_parameters['legend_label_fontsize']
                )

    #################################
    # save the plot
    #################################

    savepath = putils.create_savename(
        savedir,
        'mod-release-points_with-polygons',
        drifterid=None,
        zone=zone,
        extension= plot_parameters['output_filetype']
        )

    plt.savefig(savepath, bbox_inches='tight', dpi=plot_parameters['dpi'])
    plt.close()
    logger.info('saving ' + savepath)

####################################################################
# plots side by side versions of each dataset for each drifter
####################################################################

def plot_track_side_by_side_workflow(
    all_drifter_tracks_dict,
    plot_parameters,
    etopo_file,
    landmask_files,
    savedir,
    plotcolors,
    buoyids,
    overwrite_savedir,
    zone='all'
):
    """ side by side subplots of the drifter tracks for all datasets

    Parameters
    ----------
    all_drifter_tracks_dict : dict
        dictionary containing all information for the tracks to be
        plotted. Key values are experiment setnames and values are
        pandas dataframes.
    plot_parameters : dict
        dictionary containing parameters to use when saving plots
        (resolution, filetype, figure size, etc)
    etopo_file : str
        Path to the etopo file to use when plotting bathymetry.
        Can be None
    landmask_files : dict
        dictionary containing full paths to landmask files. If None or
        missing, default GSHHS landmask is used. If variable names other
        than standard NEMO conventions are use, landmask_files is a nested
        dictionary instead.
    savedir : str
        Path to the main directory where plots will be saved.
        Subdirectories are created within the code for organization.
    plotcolors : dict
        dictionary containing setnames as keys and a color names as
        values.
    buoyids : list
        list of strings representing the unique buoyids to be included
        in the plots.
    overwrite_savedir : boolean
        boolean value set in the user config yaml. A value of True will
        allow a savedir to be cleared out and then overwritten with the
        new data. False will cause an error message to be logged and the
        program exits.
    zone : str
        string representing the label of the user defined polygon area
        in which to plot data. If no sub zones are defined, zone label
        'all' is used and the entire dataset is plotted.

    Returns
    -------
    creates plots specified by plot_selection parameter in the
    user plotting config yaml file and saves them in savedir
    """

    sidebyside_tracks_subdir = utils.setup_savedir(
        savedir,
        'side-by-side_comparison_tracks',
        overwrite_savedir = overwrite_savedir
        )
    for drifterid in buoyids:

        drifterdict = {}
        for d in all_drifter_tracks_dict.keys():
            drifterdict[d] = all_drifter_tracks_dict[d][
                all_drifter_tracks_dict[d]['buoyid'] == drifterid
            ]

        # Create a figure object (right now, it's best to use PlateCarree()
        # for both the data projection and data transform functions. We
        # should revisit this at a later date.
        dataproj = ccrs.PlateCarree()
        proj = ccrs.PlateCarree()

        ################################
        # calculate and set plot extent
        ################################
        plot_extremes = mapfuncs.finalize_plot_extremes(
                data=drifterdict,
                proj=proj,
                dataproj=dataproj,
                min_aspect_ratio=plot_parameters['plot_aspect_ratio'],
                lon_buff_percent=None,
                lat_buff_percent=None)
        map_extremes = plot_extremes['padded_extremes']
        map_extremes_tuple = tuple(map_extremes)

        buffered_extremes = plot_extremes['buffered_extremes']
        buffratio = buffered_extremes['buffratio']

        if (buffratio) > 0.8:
            fig, ax = plt.subplots(nrows=1, ncols=len(list(drifterdict.keys())),
                                subplot_kw={'projection': ccrs.PlateCarree()},
                                figsize=(8.5, 11))
        else:
            fig, ax = plt.subplots(nrows=len(list(drifterdict.keys())), ncols=1,
                                subplot_kw={'projection': ccrs.PlateCarree()},
                                figsize=(11, 8.5))

        # loop though the experiments and plot each set on a separate axis.
        for p,drf in enumerate(list(drifterdict.keys())):

            ################################
            # plot the mod drifter tracks
            ################################
            savestr = mapfuncs.plot_drifter_tracks(
                drifterdict[drf],
                plot_parameters,
                obs_tracks=True,
                mod_tracks=True,
                pers_tracks=False,
                start_dot=False,
                mod_initial_positions=False,
                ax=ax[p],
                map_extremes=buffered_extremes['buffered_extremes'],
                obs_color=None,
                mod_color=plotcolors[list(drifterdict.keys())[p]],
                dot_color=None,
                mod_dot_color=None,
                fig=fig,
                drifterid=drifterid,
                etopo_file=etopo_file,
                add_landmask=True,
                landmask_files=landmask_files,
                add_gridlines=True,
                restrict_extent=True
            )
            ax[p].set_title(list(drifterdict.keys())[p])

        fig.tight_layout()
        sbsfigname = "{}_side-by-side_{}.{}".format(
                            str(drifterid),
                            zone,
                            plot_parameters['output_filetype']
                        )
        sbs_savepath = os.path.join(sidebyside_tracks_subdir, sbsfigname)
        logger.info('saving ' + sbs_savepath)
        plt.savefig(
            os.path.join(sidebyside_tracks_subdir, sbsfigname),
            bbox_inches='tight',
            dpi=plot_parameters['dpi']
        )
        plt.close()

