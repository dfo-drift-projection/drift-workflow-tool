"""
crop common comparison set
==========================
:Author: Jennifer Holden
:Contributers: Nancy Soontiens
:Created: 2020-06-15

This module gathers the drift-predict outputs from experiments run with
multiple datasets. It crops the output to a common timeframe and common
observed drifter set, then writes the data to a single common class 4
like file per drifter.
"""

import argparse
import glob
import os
import sys

import numpy as np
import pandas as pd
import xarray as xr

from driftutils import utils

logger = utils.logger


def crop_to_common_comparison_set(datadirs, savedir, overwrite_savedir):
    """crop to common comparison set.

    Parameters
    ----------
    datadirs : dict
        A dictionary with dataset labels as keys and associated data
        folders as values
    savedir : str
        directory where the comparison output files will be stored
    overwrite_savedir : bool
        boolean value to define behavior if user defined savedir exists.
        Default behavior is to exit with error message if folder exists,
        but this can be changed in user config file.

    Returns
    -------
    creates a folder of comparsion output files. The files provide a per
    drifter comparison set that have been cropped to a common timeframe
    and spatial domain.
    """

    # First, find the common buoyids between folders. Only keep buoyids
    # that show up in both.
    buoyids = {}
    for run, dir in datadirs.items():
        buoyids[run] = utils.grab_buoyids_from_attrs(dir)

    commonids = None
    for modelrun in buoyids.keys():
        if commonids is None:
            commonids = set(buoyids[modelrun])
        commonids.intersection_update(buoyids[modelrun])

    # check for input folders and data
    for dir in datadirs.values():
        if not os.path.isdir(dir):
            foldererrstr = 'the input folder does not exist: ' + str(dir)
            sys.exit(foldererrstr)
        else:
            if len(glob.glob(os.path.join(dir, '*.nc'))) == 0:
                sys.exit('There are no files in the input folder: ' + str(dir))

    # Set up the output directory. If the user has not chosen to overwrite
    # the savedir and it exists, provide an error message and exit the
    # program.  Otherwise create a new folder or remove files from an
    # already exisiting folder.
    savedir = utils.setup_savedir(savedir, overwrite_savedir=overwrite_savedir)
    logger.info('\ncropped data will be saved in: ' + savedir + '\n')

    ####################################################################
    # open all the netcdf files and loop though each common drifter
    # drifters with non-common ids are not opened and so no file is
    # written to the cropped subdirectories for them
    ####################################################################

    # for each drifter
    for drifterid in commonids:
        # open the two (or more) datasets belonging to the drifter id from
        # the datadirs. This assumes that each folder only has one file
        # with the unique drifterid.
        ds = []
        for d in list(datadirs.values()):
            files = list(filter(
                lambda f: drifterid in f, glob.glob(os.path.join(d, '*.nc'))
            ))
            if len(files) > 1:
                errstr = ('There are multiple files in ' + str(d)
                          + ' containing ' + str(drifterid) + '. this is '
                          + 'unexpected behavior, so exiting')
                raise ValueError(errstr)
            ds.append(xr.open_dataset(files[0]))
        # find the min and max values that contain times for all of the
        # datasets. What does this do if one observed track is missing
        # all the data in the middle?
        mintime = max([np.nanmin(d.time.values) for d in ds])
        maxtime = min([np.nanmax(d.time.values) for d in ds])

        # find the common model start coordinates
        all_common_starts = [
            list(zip(d.mod_start_lat.values.tolist(),
                     d.mod_start_lon.values.tolist(),
                     d.mod_start_date.values.tolist())) for d in ds]

        common_coordinates = []
        for valpair in all_common_starts[0]:
            match = True
            for acs in all_common_starts[1:]:
                match = match and valpair in acs

            if match:
                common_coordinates.append(valpair)

        # If no matching model tracks (ie, common_coordinates is an empty
        # list) set all the tracks for that buoyid to NaNs
        if not common_coordinates:
            lstr = '.... no matching model tracks - excluding drifter {}'
            logger.info(lstr.format(drifterid))
            continue

        # make a list of variables that do not use timestep as a dimension
        # (ex, mod_start_date)
        model_run_only_vars = []
        for d in ds[0].data_vars:
            if len(ds[0][d].dims) == 1:
                model_run_only_vars.append(d)

        # for each of the files for each buoy, crop the dataset to the
        # common time and common model run ids.
        for i in range(0, len(list(datadirs.values()))):

            # crop to the common set of model runs
            for m in ds[i].model_run.values:

                mod_lats = [ds[i].mod_start_lat[m].values.tolist()]
                mod_lons = [ds[i].mod_start_lon[m].values.tolist()]
                mod_times = [ds[i].mod_start_date[m].values.tolist()]
                mod_start_pair = list(zip(mod_lats, mod_lons, mod_times))[0]

                if mod_start_pair not in common_coordinates:
                    logger.debug('.... excluding some modruns with '
                                 + 'non-matching start positions')
                    for v in ds[i].data_vars:
                        if v in ['time', 'mod_start_lat', 'mod_start_lon',
                                 'mod_start_date', 'original_filename']:
                            continue
                        var = ds[i][v]
                        if len(var.dims) == 2:
                            var[m, :] = np.nan

            # crop to the common timeset.
            ds[i] = ds[i].where(
                (ds[i].time >= mintime) & (ds[i].time <= maxtime)
            )

            ds[i] = ds[i].reset_coords('time')

            # the following covers the case where a model track started before
            # the common time and so it now begins with NaNs but ends with
            # valid data. The entire modelled track is tossed in this case
            # and all values except for time are set to NaN.
            for m in ds[i].model_run.values:
                if np.isnan(ds[i].obs_lat[m, 0].values):
                    # NaN the whole model run except for time
                    for v in ds[i].data_vars:
                        if v in ['time', 'mod_start_lat', 'mod_start_lon',
                                 'mod_start_date', 'original_filename']:
                            continue
                        var = ds[i][v]
                        if len(var.dims) == 2:
                            var[m, :] = np.nan

            # cleanup take timestep 0 from each of the model_run_only_vars
            # and recreate the original one dim format for those variables.
            for v in model_run_only_vars:
                ds[i][v] = ds[i][v].sel(timestep=0).drop('timestep')

            ds[i] = ds[i].set_coords('time')

            datasetsstr = [''.join([str(list(datadirs.keys())[d]), ':',
                                    str(ds[d].attrs['ocean_model']), '_',
                                    str(ds[d].attrs['drift_model'])])
                           for d in range(0, len(datadirs.keys()))]

            # add attributes to identify cropped dataset
            ds[i].attrs["cropped_to_datasets_short"] = "-".join(
                [dkeys.replace("-", "") for dkeys in list(datadirs.keys())]
            )
            ds[i].attrs['cropped_to_datasets_specific'] =\
                ', '.join(datasetsstr)
            ds[i].attrs['cropped_to_time_range_min'] = str(mintime)
            ds[i].attrs['cropped_to_time_range_max'] = str(maxtime)

        # Convert datasets to pandas dataframes
        dfs = []
        prct = 0
        for d in ds:
            if 'DEPTH' in list(d.dims):
                df_all_depths = d.to_dataframe()
                df = df_all_depths[(
                    df_all_depths.index.get_level_values('DEPTH')
                    == d.DEPTH[0].values
                )].copy()

                df.reset_index(level='DEPTH', inplace=True)
                df.drop(labels='DEPTH', axis=1, inplace=True)
            else:
                df = d.to_dataframe()

            # add some extra columns for global attributes that will need
            # to become variables
            df.insert(len(df.keys()), "drift_model", d.drift_model, True)
            df.insert(len(df.keys()), "ocean_model", d.ocean_model, True)
            df.insert(len(df.keys()), "mod_run_name", d.mod_run_name, True)
            rls = list(datadirs.keys())[prct].replace('-', '').replace(' ', '')
            df.insert(len(df.keys()), "setname", rls, True)
            prct += 1  # increment the counter

            # if obs_lat is nan, set all the other variables to nan. This
            # makes it easier to use df.dropna() later
            df.loc[df.obs_lat.isnull(), :] = np.nan
            # maybe not necessary, but for completeness
            df.loc[df.obs_lon.isnull(), :] = np.nan
            df.loc[df.mod_lat.isnull(), :] = np.nan
            df.loc[df.mod_lon.isnull(), :] = np.nan

            # drop the rows with NaNs and reset the model_run index to
            # ignore the runs that were dropped
            df.dropna(inplace=True, how='all')
            df.reset_index(inplace=True)
            model_run = df.model_run.copy()
            for idx, mr in enumerate(sorted(np.unique(df.model_run))):
                model_run.loc[df.model_run == mr] = idx
            df.model_run = model_run
            df.set_index(['setname', 'model_run', 'timestep'], inplace=True)
            dfs.append(df)

        # Since I dropped tracks above that were entirely NaNs, it's possible
        # here that the model tracks do not line up again. I need to drop model
        # tracks that don't exist in all comparison sets again at this point,
        # before the dataframes are combined into a concatenated set.
        # make a list of lists of all the model runs in each set
        modrunlist = []
        for d in range(0, len(dfs)):
            modrunlist.append(
                list(np.unique(
                    dfs[d].index.get_level_values('model_run').values.tolist()
                ))
            )

        # compare each set against the first set to find model runs that do
        # not exist in both sets. The range starts at 1, goes to the length
        # of dfs and compares back to the first set. Add the model runs to
        # exclude to a list of lists.
        for c in range(1, len(dfs)):
            # compare each list to the first list
            comp = np.array_equal(modrunlist[0], modrunlist[c])
            nanset = []
            if not comp:
                nanset.append(
                    list(set(
                        modrunlist[0]
                    ).symmetric_difference(set(modrunlist[c])))
                )

        # flatten the list of lists, then set the rows for those model runs
        # to NaNs to make them easier to drop.
        nanset = [item for sublist in nanset for item in sublist]
        for d in range(0, len(dfs)):
            dfs[d].loc[
                (dfs[d].index.get_level_values('model_run').isin(nanset))
            ] = np.nan
            dfs[d].dropna(inplace=True, how='all')

        # Add all the dataframes in the list to one large dataframe
        megadf = pd.concat(dfs)

        # Ensure that the model_run in each set corresponds to the same start
        # coordinates.
        megadf.reset_index(inplace=True)
        model_run = megadf.model_run.copy()
        for n, coords in enumerate(common_coordinates):
            start_lat, start_lon, start_date = coords
            model_run.loc[(megadf['mod_start_lat'] == start_lat) &
                          (megadf['mod_start_lon'] == start_lon) &
                          (megadf['mod_start_date'] == start_date)] = n
        megadf.model_run = model_run
        megadf.set_index(['setname', 'model_run', 'timestep'], inplace=True)

        # cycle though the model runs to see if the ends need to be cropped
        # off to make the sets match
        for i in np.unique(megadf.index.get_level_values('model_run')):
            modelrundfs = []
            for d in np.unique(megadf.index.get_level_values('setname')):
                df = megadf[
                    (megadf.index.get_level_values('model_run') == i) &
                    (megadf.index.get_level_values('setname') == d)]
                modelrundfs.append(df)

            tsteplen = []
            d_range = len(np.unique(megadf.index.get_level_values('setname')))
            for d in range(0, d_range):
                tsteplen.append(
                    len(modelrundfs[d].index.get_level_values(
                        'timestep'
                    ).values.tolist())
                )

            if len(np.unique(tsteplen)) != 1:
                # find rows with timesteps greater than
                # min(np.unique(tsteplen)) and set all values to NaNs
                tstdrops = megadf.index.get_level_values("timestep")[(
                    ((megadf.index.get_level_values("timestep")
                     >= min(np.unique(tsteplen))))
                    & (megadf.index.get_level_values("model_run") == i)
                )].values.tolist()

                megadf.loc[
                    (megadf.index.get_level_values("timestep").isin(tstdrops))
                    & (megadf.index.get_level_values("model_run") == i)
                ] = np.nan

                # drop the rows with NaNs and reset the model_run index
                # to ignore the runs that were dropped
                megadf.dropna(inplace=True, how='all')

        # do some final checks here to be sure that I haven't missed an
        # edge case. check to be sure that there are the same number of
        # model tracks in all the comparison sets at this point
        modrunlen = []
        for d in np.unique(megadf.index.get_level_values('setname')):
            persetdf = megadf[megadf.index.get_level_values('setname') == d]
            modrunlen.append(
                len(np.unique(
                    persetdf.index.get_level_values(
                        'model_run'
                    ).values.tolist()
                ))
            )
        if len(np.unique(modrunlen)) != 1:
            logstr = 'P2D-' + drifterid + '_aggregated.nc'
            logger.info(logstr)
            errstr = ('The number of model tracks in each comparison set '
                      + 'are not equal: ' + str(modrunlen) + '. this is '
                      + 'unexpected behavior, so exiting')
            raise ValueError(errstr)

        # check that the timesteps match
        for m in np.unique(megadf.index.get_level_values('model_run')):
            timechecklist = []
            perrundf = megadf[megadf.index.get_level_values('model_run') == m]

            for d in np.unique(perrundf.index.get_level_values('setname')):
                timechecklist.append(
                    perrundf['time'][
                        perrundf.index.get_level_values('setname') == d
                    ].values.tolist()
                )
            for ll in range(0, len(timechecklist[0])):
                comparelist = []
                for t in range(0, len(timechecklist)):
                    comparelist.append(timechecklist[t][ll])
                if len(np.unique(comparelist)) != 1:
                    errstr = ('The timestamps in modelrun ' + str(m)
                              + ' do not match accross the comparison sets. '
                              + 'This is unexpected behavior, so exiting')
                    raise ValueError(errstr)

        # convert back to one large dataset here, instead of one
        # large dataframe
        megads = megadf.to_xarray()

        # add the attributes. First add the comparison set specific attribute:
        singledimvar = ['ocean_model', 'drift_model', 'mod_run_name']
        for sdvar in singledimvar:
            megads[sdvar] = megads[sdvar].isel(
                timestep=0, model_run=0, drop=True
            )
        doubledimvar = ['mod_start_date', 'mod_start_lat',
                        'mod_start_lon', 'original_filename']
        for ddvar in doubledimvar:
            megads[ddvar] = megads[ddvar].isel(timestep=0, drop=True)
        setspecificvar = singledimvar + doubledimvar + [
            'cropped_to_time_range_min', 'cropped_to_time_range_max']

        # create a list of the non-comparison set specific attributes
        # from the original datasets and add them to megads
        attlist = []
        for d in ds:
            z = d.attrs
            for item in z.items():
                if item[0] not in setspecificvar:
                    attlist.append((item[0], item[1]))
        # I really should find the unique pairs in attlist here. Right now,
        # this probably loops though attributes and overwrites them
        for item in attlist:
            megads.attrs[item[0]] = str(item[1])

        # add an attribute for the min and max times in the mega file
        newmintime = max([megads.time.values.min()])
        newmaxtime = min([megads.time.values.max()])
        megads.attrs['cropped_to_time_range_min'] = str(newmintime)
        megads.attrs['cropped_to_time_range_max'] = str(newmaxtime)
        megads.attrs['comparison_file_description'] = str(
            'The data in this set has been cropped to represent a '
            'comparison between multiple user defined data sets. '
            'The data represents a common time period and only '
            'contains model tracks that are common to all comparison sets. '
            'In the case where one track ends early due to grounding, '
            'corresponding tracks in all comparison sets have been '
            'truncated as well.')

        # add attributes to the data variables from the original datasets
        for varname, da in megads.data_vars.items():
            if varname in ds[0].data_vars:
                da.attrs = ds[0].data_vars[varname].attrs

        # Drop nans along mmodel_run dim
        megads = megads.dropna(dim='model_run', how='all')

        # add any remaining attributes that aren't covered in the original sets
        megads.data_vars['mod_start_date'].attrs = [
            ('long_name', 'Initial start date of modelled trajectory')]
        megads.data_vars['mod_start_lat'].attrs = [
            ('long_name', 'Latitude of modelled trajectory starting position'),
            ('units', 'degrees_north')
        ]
        megads.data_vars['mod_start_lon'].attrs = [
            ('long_name',
             'Longitude of modelled trajectory starting position'),
            ('units', 'degrees_east')
        ]
        megads.data_vars['original_filename'].attrs = [
            ('long_name', 'Filename of original drift tool output')
        ]
        megads.data_vars['drift_model'].attrs = [
            ('long_name',
             'Drift model used to create modelled trajectory prediction')
        ]
        megads.data_vars['ocean_model'].attrs = [
            ('long_name', 'Ocean model used to predict ocean currents')
        ]
        megads.data_vars['mod_run_name'].attrs = [
            ('long_name', 'Original model run identifier')
        ]
        sec_str = 'seconds since ' + str(megads.time.values[0][0][0])
        megads.data_vars['time'].encoding['units'] = sec_str

        # write out the finished netcdf file containing the comparison dataset.
        savenamestr = str(drifterid) + '_comparison.nc'
        savepath = os.path.join(savedir, savenamestr)
        megads.to_netcdf(savepath)
        logger.info("writing " + savepath)


def main(args=sys.argv[1:]):

    parser = argparse.ArgumentParser()
    configstr = ('path to the cropping config file (ex: ../../examples/'
                 + 'comparisons/user_comparison_crop_config.yaml)')
    parser.add_argument('--config', type=str, help=configstr)

    parser.add_argument('--log_level',
                        default='info',
                        choices=utils.log_level.keys(),
                        help='Set level for log messages')

    args = (parser.parse_args())
    user_config_filename = args.config
    utils.initialize_logging(level=utils.log_level[args.log_level])

    # load the user defined configuation file
    # usercfg = load_yaml_config(user_config_filename)
    usercfg = utils.load_yaml_config(user_config_filename)
    datadirs = usercfg['datadirs']
    savedir = usercfg['savedir']

    # specify whether to overwrite output savedir
    overwrite_savedir = False
    if ('overwrite_savedir' in usercfg and
            usercfg['overwrite_savedir'] != 'None'):
        overwrite_savedir = usercfg['overwrite_savedir']

    # crop the files
    crop_to_common_comparison_set(datadirs, savedir, overwrite_savedir)

    utils.shutdown_logging()


if __name__ == '__main__':
    main()
