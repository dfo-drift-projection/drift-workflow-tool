"""
Assemble Atmosphere or Wave Metadata
=======================
:Author: Clyde Clements, Nancy Soontiens, Jennifer Holden
:Created: 2023-01-31

This module assembles metadata from atmosphere and wave model data files. It
recursively walks through a given directory containing model data
files and assembles the metadata of all netCDF files found. The metadata
is then written to a specified file.

The metadata output is in JSON format; sample output for wave data::

{
  wave_data: {
    '2017-04-01T00:30:00.000000000': {
      xwavevel: data/atmos/20170401/wave_U.nc,
      ywavevel: data/atmos/20170401/wave_V.nc
    },
    '2017-04-01T01:30:00.000000000': {
      xwavevel: data/ocean/20170401/wave_U.nc;
      ywavevel: data/ocean/20170401/wave_V.nc
    },
  },
  updated: '2018-02-15T15:22:20.684280'
}

Sample output for atmospheric data::

{
  atmos_data: {
    '2017-04-01T00:30:00.000000000': {
      xwindvel: data/atmos/20170401/wind_U.nc,
      ywindvel: data/atmos/20170401/wind_V.nc
    },
    '2017-04-01T01:30:00.000000000': {
      xwindvel: data/ocean/20170401/wind_U.nc,
      ywindvel: data/ocean/20170401/wind_V.nc
    },
  },
  updated: '2018-02-15T15:22:20.684280'
}

Requirements/Limitations:

- The specified data directory must contain data from only one wave or atmos
  model configuration and the data files must all be consistent in dimensions
  and variables defined within.
- The directory of model data should not contain netCDF files pertaining to
  other data.
"""

import datetime
import functools
import json
import multiprocessing as mp
import os
import os.path
import sys

import xarray as xr

from driftutils import configargparse
from driftutils import utils


logger = utils.logger

defaults = {'xvel': {'longname': 'x velocity component', 'nc_name': 'u_wind'},
            'yvel': {'longname': 'y velocity component', 'nc_name': 'v_wind'},
            'longitude': {'longname': 'longitude', 'nc_name': 'nav_lon'},
            'latitude': {'longname': 'latitude', 'nc_name': 'nav_lat'},
            'time_var': {'longname': 'time', 'nc_name': 'time_counter'},
            'model_type': {'longname': 'type of model data being assembled',
                           'nc_name': 'atmos'}}


def assemble_atmos_wave_metadata(
    data_dir,
    output_file,
    model_type=defaults['model_type']['nc_name'],
    xvel=defaults['xvel']['nc_name'],
    yvel=defaults['yvel']['nc_name'],
    longitude=defaults['longitude']['nc_name'],
    latitude=defaults['latitude']['nc_name'],
    time_var=defaults['time_var']['nc_name'],
    nproc=1
):
    """"Assemble model metadata.

    Parameters
    ----------
    data_dir : str
        Name of directory containing model data files.
    output_file : str
        Name of output file to create containing the metadata.
    model_type : str
        Type of model being used. At this time, options are "atmos" or "wave"

    Other Parameters
    ----------------
    xvel : str, optional
        Name of variable containing x component of velocity due to Stokes
        Drift if waves or variable containing model eastward wind if atmos,
        ex: 'vsdx' if waves or 'u_wind' if atmos.
    yvel : str, optional
        Name of variable containing y component of velocity due to Stokes
        Drift if waves or variable containing model northward wind if atmos.
        ex: 'vsdy' if waves or 'v_wind' if atmos.
    longitude : str, optional
        Name of longitude variable in netCDF files.
    latitude : str, optional
        Name of latitude variables in netCDF files.
    time_var : str, optional
        Name of time variable in netCDF files.
    nproc : int, optional
        Number of processors to use for parallel scanning.
    """

    logger.info('\nAssembling ' + model_type + ' metadata...')

    data_metadata = {}
    if nproc > 1:
        pool = mp.Pool(processes=nproc)
        iterator = []

    output_dir = os.path.dirname(os.path.abspath(output_file))

    attrdict = {'atmos': {'name_x': 'xwindvel', 'name_y': 'ywindvel'},
                'wave': {'name_x': 'xwavevel', 'name_y': 'ywavevel'}}

    variables = {attrdict[model_type]['name_x']: xvel,
                 attrdict[model_type]['name_y']: yvel}

    for dirpath, dirnames, filenames in os.walk(data_dir, followlinks=True):

        for filename in filenames:

            if not filename.endswith('.nc'):
                continue

            data_filename = os.path.join(dirpath, filename)

            if nproc > 1:
                iterator += [pool.apply_async(
                    scan_metadata_one_file,
                    args=(
                        data_filename,
                        output_dir,
                        variables,
                        time_var))]
            else:
                one_metadata = scan_metadata_one_file(
                    data_filename,
                    output_dir,
                    variables,
                    time_var)
                for key in one_metadata:
                    if key in data_metadata:
                        data_metadata[key].update(one_metadata[key])
                    else:
                        data_metadata[key] = one_metadata[key]

    if nproc > 1:
        for x in iterator:
            one_metadata = x.get()
            for key in one_metadata:
                if key in data_metadata:
                    data_metadata[key].update(one_metadata[key])
                else:
                    data_metadata[key] = one_metadata[key]
        pool.close()
        pool.join()

    # Sort keys in data_metadata
    keys = list(data_metadata.keys())
    keys.sort()
    data_metadata = {key: data_metadata[key] for key in keys}

    now = datetime.datetime.utcnow()
    metadata = {'updated': now.isoformat(),
                (model_type + '_data'): data_metadata}

    logger.info('Dumping ' + model_type + ' metadata to file '
                + output_file + '...')
    with open(output_file, 'w') as f:
        json.dump(metadata, f, indent=1)


def try_and_log(func):
    """ Decorator to log the error in case a function fails.
    """
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            out = func(*args, **kwargs)
        except Exception as e:
            out = None
            logger.error(e, exc_info=True)
        return out
    return wrapper


@try_and_log
def scan_metadata_one_file(
        data_filename,
        output_dir,
        variables,
        time_var):
    """ Reads metadata from one data file

    Parameters
    ----------
    data_filename : str
        Name of data file to be scanned.
    output_dir : str
        Directory of data file.
    variables : list
        List of variables to scan for.
    time_var : str
        Name of time variable in files.

    Returns
    -------
    data_metadata : dict
        Dictionary with metadata from files.
        Metadata includes timestamps and variables names
        contained in each file.
    """

    data_metadata = {}
    logger.debug('Examining data file %s...', data_filename)
    ds = xr.open_dataset(data_filename)

    if not os.path.isabs(data_filename):
        # For the metadata constructed below, we want the path
        # of the data file relative to the output directory.
        data_filename = os.path.relpath(data_filename, output_dir)

    # Determine what variables are in this data file.
    variables_found = {}
    for name, nc_name in variables.items():
        if nc_name and nc_name in ds.variables:
            variables_found[name] = data_filename

    if variables_found:
        time_counter = ds.coords[time_var]
        for t in time_counter.values:
            data_date = str(t)
            if data_date in data_metadata:
                data_metadata[data_date].update(variables_found)
            else:
                data_metadata[data_date] = variables_found.copy()

    return data_metadata


def main(args=sys.argv[1:]):

    arg_parser = configargparse.ArgParser(
        config_file_parser_class=configargparse.YAMLConfigFileParser
    )

    arg_parser.add('-c', '--config', is_config_file=True,
                   help='Name of configuration file')

    arg_parser.add('--log_level', default='info',
                   choices=utils.log_level.keys(),
                   help='Set level for log messages')

    arg_parser.add_argument('--data_dir', type=str, required=True,
                            help='Path to folder containing ocean data files')

    arg_parser.add_argument('-o', '--output', type=str,
                            help='Name of metadata output file to create')

    arg_parser.add_argument('-n', '--nproc', type=int, default=1,
                            help='Number of processors for parallel scanning.')

    for var, info in defaults.items():
        vstr = 'Name of variable containing %s' % info['longname']
        help_str = [info['longname'] if var == 'model_type' else vstr][0]
        arg_parser.add_argument(
            '--%s' % var, type=str, default=info['nc_name'], help=help_str
        )

    config = arg_parser.parse(args)

    utils.initialize_logging(level=utils.log_level[config.log_level])

    assemble_atmos_wave_metadata(
        config.data_dir,
        config.output,
        model_type=config.model_type,
        xvel=config.xvel,
        yvel=config.yvel,
        longitude=config.longitude,
        latitude=config.latitude,
        time_var=config.time_var,
        nproc=config.nproc
    )

    utils.shutdown_logging()


if __name__ == '__main__':
    main()
