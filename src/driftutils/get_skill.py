r"""
Calculate Skill Scores
======================
:Author: Clyde Clements
:Created: 2017-10-18

This module calculates skill scores for synthetic drift trajectories. It
is based on functions for skill score analyis written by Hauke Blanken
at the Institute of Ocean Sciences, Septmeber 2017.

The Liu skill score is calculated from the cumulative Lagrangian
separation normalized by the observed cumulative trajectory length.
To elaborate, first define the non-dimensional index :math:`s`:

.. math::
   :nowrap:

   \begin{equation*}
     s = \frac{ \sum_{i=1}^{N} d_i }{ \sum_{i=1}^{N} l_{oi} }
   \end{equation*}

where :math:`d_i` is the separation distance between the modelled and
observed endpoints of the Lagrangian trajectories at time step
:math:`i`, :math:`l_{oi}` is the length of the observed trajectory at
time step :math:`i`, and :math:`N` is the total number of time steps.
Note that this uses the _cumulative_ trajectory length, not the final
trajectory length. Then the skill score is defined as:

.. math::
   :nowrap:

   \begin{equation*}
     \mathit{ss}
       = \begin{cases}
           1 - \frac{s}{n} & s \le n \\
           0               & s > n
         \end{cases}
   \end{equation*}

where :math:`n` is a non-dimensional, positive number that defines a
threshold for no skill (:math:`\mathit{ss} = 0`). The threshold value
:math:`n` is arbitrarily picked by the person computing the skill score.
The function below calculates the score as a function of time, treating
each point as the end of the trajectory up to that time.

The Molcard skill score is calculated from the instantaneous Lagrangian
separation normalized by the instantaneous displacement along the
observed trajectory. Specifically, the score is calculated from the
formula:

.. math::
   :nowrap:

   \begin{equation*}
     \mathit{ss}
       = \begin{cases}
           1 - \frac{d_i}{D_i} & d_i \le D_i \\
           0                   & d_i > D_i
         \end{cases}
   \end{equation*}

where :math:`d_i` is the separation distance between the modelled and
observed endpoints of the Lagrangian trajectories at time step
:math:`i`, and :math:`D_{oi}` is the displacement of the observed
drifter at time step :math:`i`.
"""

import geopy
from geopy.distance import distance
import numpy as np
import xarray as xr

from . import utils


def get_skill(obs, mod, liu_threshold=1):
    """Calculate cumulative (Liu) and instantaneous (Molcard) skill score.

    Input is a pair of observed-modelled trajectories as a function of
    time. The trajectories should have the same time coordinate. If that
    is not the case, the modelled drifter positions are interpolated to
    times when the observational fixes were recorded.

    Parameters
    ----------
    obs : xarray.Dataset
        Observed trajectory data for the given drifter set. Required
        elements:

        - **time**: DataArray containing timestamps
        - **lon**: DataArray containing longitudes
        - **lat**: DataArray containing latitudes
    mod : xarray.Dataset
        Modelled trajectory data for the given drifter set. It must
        contain the same element types as `obs`.
    liu_threshold : int
        Threshold for computing Liu skill score. When the cumulative
        separation distance between observed and modelled fixes is
        greater than

    Returns
    -------
    skill : xarray.Dataset
        Computed scores. Components include:

        - **liu**: Liu skill score
        - **molcard**: Molcard skill score
        - **sep**: Separation distance between observed and modelled
                    trajectory
        - **obs_dist**: Distance between consecutive observed
                    trajectory points
        - **obs_disp**: Displacement along observed trajectory
        - **mod_dist**: Distance between consecutive modelled
                    trajectory points
        - **mod_disp**: Displacement along modelled trajectory
    """
    if not (obs['time'].size == mod['time'].size and
            np.allclose(obs['time'].astype('float64'),
                        mod['time'].astype('float64'))):
        mod = utils.interpolate_track(obs, mod)

    # Create convenience array for observed track.
    lat = np.ma.masked_invalid(obs['lat'].values)
    lon = np.ma.masked_invalid(obs['lon'].values)
    mask_obs = lat.mask | lon.mask
    ot = np.column_stack([obs['time'].astype('float64'), lat, lon])

    # Initial release position, which will be the reference point
    # for displacement calculations.
    start_time = mod.start_date
    start_lon = mod.start_lon
    start_lat = mod.start_lat

    # Create convenience array for modelled track.
    lat = np.ma.masked_invalid(mod['lat'].values)
    lon = np.ma.masked_invalid(mod['lon'].values)
    mask_mod = lat.mask | lon.mask
    mt = np.column_stack([mod['time'].astype('float64'), lat, lon])

    # calculate the persistance from first two points of the obs drifter
    pers_lat = np.zeros([np.shape(ot)[0], ])
    pers_lon = np.zeros([np.shape(ot)[0], ])

    pers_lat[0] = ot[0, 1]
    pers_lon[0] = ot[0, 2]
    pers_lat[1] = ot[1, 1]
    pers_lon[1] = ot[1, 2]

    # calculate the bearing
    def bearing(lat0, lon0, lat1, lon1):
        lat0_rad = np.radians(lat0)
        lat1_rad = np.radians(lat1)
        diff_rad = np.radians(lon1 - lon0)
        x = np.cos(lat1_rad) * np.sin(diff_rad)
        y = np.cos(lat0_rad) * np.sin(lat1_rad) - np.sin(
            lat0_rad) * np.cos(lat1_rad) * np.cos(diff_rad)
        b = np.arctan2(x, y)
        return np.degrees(b)

    # calculate the distance and time difference between the first two
    # points, then use them to calculate the velocity between the first
    # points and the bearing.
    d_m = distance([ot[0, 1], ot[0, 2]], [ot[1, 1], ot[1, 2]]).m   # in meters
    tdel = obs['time'].values[1] - obs['time'].values[0]
    tdelta = (tdel)/np.timedelta64(1, 's')
    pers_vel = d_m / tdelta
    pers_bearing = bearing(ot[0, 1], ot[0, 2], ot[1, 1], ot[1, 2])

    # for each additional point in the track, use the time difference
    # between the points and the persistence speed to calculate the
    # distance to add to the persistence track
    for i in range(2, len(pers_lat)):
        try:
            t1 = obs['time'].values[i-1]
            t2 = obs['time'].values[i]
            td = (t2-t1)/np.timedelta64(1, 's')
            step_dist = (pers_vel*td)/1000
            p = distance().destination(
                    geopy.Point([pers_lat[i-1], pers_lon[i-1]]),
                    pers_bearing,
                    step_dist
                    )
            pers_lat[i] = p.latitude
            pers_lon[i] = p.longitude
        except ValueError:
            pers_lat[i] = np.nan
            pers_lon[i] = np.nan

    # Calculate the separation between the modelled and observed
    # trajectories at each point, as well as the distance of the
    # drifter from its origin.
    sep = np.zeros([np.shape(ot)[0], ])
    odisp = np.zeros([np.shape(ot)[0], ])
    mdisp = np.zeros([np.shape(mt)[0], ])
    for i in range(len(sep)):
        try:
            if mask_obs[i] or mask_mod[i]:
                sep[i] = np.nan
            else:
                sep[i] = distance(ot[i, 1:], mt[i, 1:]).meters
        except ValueError:
            sep[i] = np.nan
        try:
            if mask_obs[i]:
                odisp[i] = np.nan
            else:
                odisp[i] = distance(ot[i, 1:], [start_lat, start_lon]).meters
        except ValueError:
            odisp[i] = np.nan
        try:
            if mask_mod[i]:
                mdisp[i] = np.nan
            else:
                mdisp[i] = distance(mt[i, 1:], [start_lat, start_lon]).meters
        except ValueError:
            mdisp[i] = np.nan

    # Calculate distance between consecutive trajectory points.
    odist = np.zeros([np.shape(ot)[0], ])
    try:
        odist[0] = distance([start_lat, start_lon], ot[0, 1:]).meters
    except ValueError:
        odist[0] = np.nan
    mdist = np.zeros([np.shape(mt)[0], ])
    try:
        mdist[0] = distance([start_lat, start_lon], mt[0, 1:]).meters
    except ValueError:
        mdist[0] = np.nan
    for i in range(1, len(odist)):
        try:
            odist[i] = distance(ot[i-1, 1:], ot[i, 1:]).meters
        except ValueError:
            odist[i] = np.nan
        try:
            mdist[i] = distance(mt[i-1, 1:], mt[i, 1:]).meters
        except ValueError:
            mdist[i] = np.nan
    # Calculate the length of the observed trajectory at each time.
    olen = np.cumsum(odist)

    # Adjust cumulative trajectory length to avoid division by zero for
    # very small displacements.
    adj_olen = olen.copy()
    adj_olen[adj_olen == 0.] = np.nan

    # Calculate the cumulative observed trajectory length
    oclen = np.cumsum(olen)

    # Calculate the length of the model trajectory at each time.
    mlen = np.cumsum(mdist)
    adj_mlen = mlen.copy()
    adj_mlen[adj_mlen == 0.] = np.nan

    # Calculate Liu skill score as a function of time.
    # Adjust cumulative trajectory length to avoid division by zero for
    # very small displacements.
    adj_oclen = oclen.copy()
    adj_oclen[adj_oclen == 0.] = np.nan
    ss = np.cumsum(sep) / adj_oclen / float(liu_threshold)

    # The model is considered to have no skill if the cumulative sum of
    # the separations betweeen observed and modelled values exceeds the
    # distance travelled by the observed drifter.
    ss[ss > 1.] = 1.
    liu = 1. - ss

    # Calculate Molcard skill score as a function of time, based on work
    # of Molcard et al 2009 with modification to match format of Liu
    # skill score, i.e. high skill score is good (scores above 0.6 are
    # considered to be 'comparable' trajectories). First adjust
    # displacement to avoid division by zero for very small
    # displacements.
    adj_odisp = odisp.copy()
    adj_odisp[adj_odisp == 0.] = np.nan
    molcard = sep / adj_odisp
    molcard[molcard > 1.] = 1.
    molcard = 1. - molcard

    # Calculate the Sutherland Skill Score as a function of time, based
    # on the work of Graig Sutherland.
    delta_t = obs['time'].values[1:] - obs['time'].values[0:-1]
    delta_t = np.insert(delta_t, 0,
                        obs['time'][0] - np.datetime64(start_time))
    delta_t = delta_t / np.timedelta64(1, 's')
    delta_t[delta_t == 0] = np.nan
    v_obs = odist / delta_t
    sutherland_num = np.nancumsum(np.multiply(sep, v_obs))
    sutherland_dem = np.nancumsum(np.multiply(odisp, v_obs))
    sutherland_dem[sutherland_dem == 0] = np.nan
    askill = sutherland_num / sutherland_dem
    askill[askill > 1.] = 1.
    sutherland = 1. - askill

    # Calculate the ratio between the net displacement and total track
    # distance (ie, at each timestep, calculate the displacement from
    # zero divided by the total distance traveled by the drifter). This
    # should be calculated for obs track and mod track separately (1
    # minus a ratio of those could then provide a skill score for
    # evaluation)
    obsratio = odisp/adj_olen
    modratio = mdisp/adj_mlen

    # Track time (ignoring the initial point).
    otime = obs['time'].values

    skill = xr.Dataset(
        {
            'liu': (
                ['time'], liu, {
                    'units': '1', 'long_name': 'Liu skill score'
                }
            ),
            'molcard': (
                ['time'], molcard, {
                    'units': '1', 'long_name': 'Molcard skill score'
                }
            ),
            'sep': (
                ['time'], sep, {
                    'units': 'meters',
                    'long_name': ('Separation distance between observed '
                                  'and modelled trajectory')
                }
            ),
            'obs_dist': (
                ['time'], odist, {
                    'units': 'meters',
                    'long_name': ('Distance between consecutive observed'
                                  ' trajectory points')
                }
            ),
            'obs_disp': (
                ['time'], odisp, {
                    'units': 'meters',
                    'long_name': 'Displacement along observed trajectory'
                }
            ),
            'mod_dist': (
                ['time'], mdist, {
                    'units': 'meters',
                    'long_name': ('Distance between consecutive modelled'
                                  ' trajectory points')
                }
            ),

            'mod_disp': (
                ['time'], mdisp, {
                    'units': 'meters',
                    'long_name': 'Displacement along modelled trajectory'
                }
            ),

            'sutherland': (
                ['time'], sutherland, {
                    'units': '1',
                    'long_name': 'Sutherland Skill Score'
                }
            ),

            'obsratio': (
                ['time'], obsratio, {
                    'units': '1',
                    'long_name': 'Observed displacement/distance ratio'
                }
            ),

            'modratio': (
                ['time'], modratio, {
                    'units': '1',
                    'long_name': 'Model displacement/distance ratio'
                }
            ),


            'pers_lat': (
                ['time'], pers_lat, {
                    'units': 'degrees',
                    'long_name': 'latitude of observed persistence'
                }
            ),

            'pers_lon': (
                ['time'], pers_lon, {
                    'units': 'degrees',
                    'long_name': 'longitude of observed persistence'
                }
            )


        },
        coords={'time': otime}
    )
    skill.attrs['liu_threshold'] = int(liu_threshold)
    return skill


def get_ocean_skill(mod):
    """Calculate drift map skill scores

    Parameters
    ----------
    mod : xarray.Dataset
        Modelled trajectory data for the given drifter set. It must
        contain the same element types as `obs`

    Returns
    -------
    skill : xarray.Dataset
        Computed scores. Components include:

        - **mod_dist**: Distance between consecutive modelled
                        trajectory points
        - **mod_disp**: Displacement along modelled trajectory
    """
    # Create convenience array for modelled track.
    # Create mask for handling nan's
    lat = np.ma.masked_invalid(mod['lat'].values)
    lon = np.ma.masked_invalid(mod['lon'].values)
    mask = lat.mask | lon.mask
    mt = np.column_stack([mod['time'].astype('float64'), lat, lon])

    # Calculate the separation between the modelled and observed
    # trajectories at each point, as well as the distance of the
    # drifter from its origin.
    mdisp = np.zeros([np.shape(mt)[0], ])
    for i in range(len(mdisp)):
        if mask[i]:
            mdisp[i] = np.nan
        else:
            mdisp[i] = distance(mt[i, 1:], mt[0, 1:]).meters
    # Calculate distance between consecutive trajectory points.
    mdist = np.zeros([np.shape(mt)[0], ])
    for i in range(1, len(mdist)):
        if mask[i]:
            mdist[i] = np.nan
        else:
            mdist[i] = distance(mt[i-1, 1:], mt[i, 1:]).meters

    # Track time (ignoring the initial point).
    mtime = mod['time'].values

    # Calculate the cumulative length of the trajectory.
    olen = np.cumsum(mdist)

    # Adjust cumulative trajectory length to avoid division by zero for
    # very small displacements.
    adj_olen = olen.copy()
    adj_olen[adj_olen == 0.] = 1.

    # Calculate ratio (compare displacement to cummulative distance)
    ratio = mdisp/adj_olen

    skill = xr.Dataset(
        {
            'mod_dist': (
                ['time'], mdist, {
                    'units': 'meters',
                    'long_name': ('Distance between consecutive modelled'
                                  ' trajectory points')
                }
            ),
            'mod_disp': (
                ['time'], mdisp, {
                    'units': 'meters',
                    'long_name': 'Displacement along modelled trajectory'
                }
            ),
            'track_dist': (
                ['time'], olen, {
                    'units': 'meters',
                    'long_name': ('Total distance travelled by  '
                                  'modelled trajectory')
                }
            ),
            'ratio': (
                ['time'], ratio, {
                    'units': 'meters',
                    'long_name': ('Ratio od displacement to  '
                                  'cummulative distance')
                }
            )
        },
        coords={'time': mtime}
    )
    return skill
