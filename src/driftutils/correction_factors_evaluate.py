"""
Run Drift Correction Factor Evaluate Workflow
=============================================
:Author: Jennifer Holden, Nancy Soontiens
:Created: 2022-07-04
"""

import os
import glob
import sys
import numpy as np
import xarray as xr

from driftutils import utils
from driftutils import cli
from driftutils import ioutils

logger = utils.logger


def append_speed_and_bearing(ds, types):
    """Add speed and bearing to dataset"""

    if 'ueast_atmos' in ds.variables:
        types.append('atmos')

    for t in types:

        # add the speed
        ds['speed_{}'.format(t)] = magnitude(ds['ueast_{}'.format(t)],
                                             ds['vnorth_{}'.format(t)])

        # calculate and add the bearing
        bearing = calculate_bearing(ds['ueast_{}'.format(t)],
                                    ds['vnorth_{}'.format(t)])
        ds['bearing_{}'.format(t)] = xr.DataArray(bearing, dims=('time'))

    return ds


def append_errors(ds):
    """Append errors for the drifter dataset. The following errors are
    calculated: speed_ocean_error, bearing_ocean_error, ueast_ocean_vdiff,
    vnorth_ocean_vdiff
    """
    for t in ['ocean']:

        # Bearing error
        diff = ds['bearing_{}'.format(t)] - ds['bearing_drifter']
        diff[diff > 180] = diff[diff > 180] - 360
        diff[diff < -180] = diff[diff < -180] + 360
        ds['bearing_{}_error'.format(t)] = diff

        # Speed error
        ds['speed_{}_error'.format(t)] = \
            ds['speed_{}'.format(t)] - ds['speed_drifter']

        # Vector difference
        ds['ueast_{}_vdiff'.format(t)] = \
            ds['ueast_{}'.format(t)] - ds['ueast_drifter']
        ds['vnorth_{}_vdiff'.format(t)] = \
            ds['vnorth_{}'.format(t)] - ds['vnorth_drifter']

    return ds


def calculate_bearing(east, north):
    """Calculate the bearing of a vector"""
    radians = np.arctan2(east, north)
    degrees = radians*180./np.pi
    degrees[degrees < 0] = degrees[degrees < 0] + 360

    return degrees


def magnitude(x, y):
    """Calculate the magnitude of a vector"""
    return np.sqrt(x**2 + y**2)


def check_paths(experiment_dir):
    """ helper function that confirms the validity of the input data
    then returns a list of the netcdf files in experiment_dir """

    # Check to be sure that the data given by the user is exists and
    # is useable
    experiment_dir = os.path.normpath(experiment_dir)

    # Check to be sure that the experiment_dir exists
    if not os.path.isdir(experiment_dir):
        sys.exit('experiment_dir does not exist: ' + str(experiment_dir))

    # Ensure that there are netcdf files present
    files = glob.glob(os.path.join(experiment_dir, '*.nc'))
    if not len(files):
        sys.exit('there are no .nc files to plot in ' + (experiment_dir))
    files.sort()

    return files


def correction_factors_evaluate(*, experiment_dir):
    """ Add calculated variables to drift correction factor output files.

    Parameters
    ----------
    experiment_dir : str
        Full path to directory containing drift correction factor output.
    """

    # Define attributes for the new variables:
    new_attrs = {
        # speeds
        'speed_ocean': {
            'standard_name': 'speed_ocean_model',
            'long_name': 'Ocean Model Speed',
            'units': 'm/s'
        },
        'speed_drifter': {
            'standard_name': 'speed_drifter',
            'long_name': 'Drifter Speed',
            'units': 'm/s'
        },
        'speed_atmos': {
            'standard_name': 'speed_atmos_model',
            'long_name': 'Wind Model Speed',
            'units': 'm/s'
        },
        # bearings
        'bearing_ocean': {
            'standard_name': 'bearing_ocean_model',
            'long_name': 'Direction of Ocean Model Velocity',
            'units': 'degrees clockwise from north'
        },
        'bearing_drifter': {
            'standard_name': 'bearing_drifter',
            'long_name': 'Direction of Drifter Velocity',
            'units': 'degrees clockwise from north'
        },
        'bearing_atmos': {
            'standard_name': 'bearing_atmos_model',
            'long_name': 'Direction of Wind Model Velocity',
            'units': 'degrees clockwise from north'
        },
        # errors (quantity_ocean - quantity_drifter)
        'bearing_ocean_error': {
            'standard_name': 'bearing_ocean_model_error',
            'long_name': 'Error in Ocean Model Direction',
            'units': 'degrees relative to drifter bearing',
            'description': ('Direction of ocean velocity minus '
                            + 'direction of drifter velocity')
        },
        'speed_ocean_error': {
            'standard_name': 'speed_ocean_model_error',
            'long_name': 'Error in Ocean Model Speed',
            'units': 'm/s',
            'description': 'Ocean model speed minus drifter speed'
        },
        # vector differences
        # (ueast_ocean - ueast_drifter)
        'ueast_ocean_vdiff': {
            'standard_name': 'eastward_sea_water_vector_difference',
            'long_name': ('Eastward Component of Ocean Model and '
                          + 'Drifter Vector Difference'),
            'units': 'm/s',
            'description': ('Eastward ocean model velocity minus '
                            + 'eastward drifter velocity')
        },
        # (vnorth_ocean - vnorth_drifter)
        'vnorth_ocean_vdiff': {
            'standard_name': 'northward_sea_water_vector_difference',
            'long_name': ('Northward Component of Ocean Model and '
                          + 'Drifter Vector Difference'),
            'units': 'm/s',
            'description': ('Northward ocean model velocity minus '
                            + 'northward drifter velocity')
        },
        # magnitude of ocean vdiff:
        'speed_ocean_vdiff': {
            'standard_name': 'magnitude_of_sea_water_vector_difference',
            'long_name': ('Magnitude of Ocean Model and Drifter '
                          + 'Vector Difference'),
            'units': 'm/s',
            'description': 'magnitude(ueast_ocean_vdiff, vnorth_ocean_vdiff)'
        },
        # bearing of ocean vdiff:
        'bearing_ocean_vdiff': {
            'standard_name': 'direction_of_sea_water_vector_difference',
            'long_name': ('Direction of Ocean Model and Drifter '
                          + 'Vector Difference'),
            'units': 'degrees clockwise from north',
            'description': 'bearing(ueast_ocean_vdiff, vnorth_ocean_vdiff)'
        },
    }

    # Check to be sure that the experiment_dir exists and then compile a
    # list of the netcdf output files if present.
    files = check_paths(experiment_dir)

    # loop though the files and append the new calculated variables
    for filename in files:

        # load the data separately so that xarray will be willing to
        # overwrite the original netcdf file in a later step.
        ds = ioutils.load_dataset(filename)

        # add 'speed_ocean', 'bearing_ocean', 'speed_drifter',
        # 'bearing_drifter', 'speed_atmos', and 'bearing_atmos'
        append_speed_and_bearing(ds, ['ocean', 'drifter'])

        # Next, add 'bearing_ocean_error', 'speed_ocean_error',
        # 'ueast_ocean_vdiff', and 'vnorth_ocean_vdiff'
        append_errors(ds)

        # finally add 'speed_ocean_vdiff' and 'bearing_ocean_vdiff':
        append_speed_and_bearing(ds, ['ocean_vdiff'])

        # Add the new variable attributes
        for var in new_attrs.keys():
            if var in ds.keys():
                for attr in new_attrs[var].keys():
                    ds[var].attrs[attr] = new_attrs[var][attr]

        # write the data back to the original netcdf file
        ds.to_netcdf(filename, mode='w')


def main():

    cli.run(correction_factors_evaluate)
    utils.shutdown_logging()


if __name__ == '__main__':
    main()
