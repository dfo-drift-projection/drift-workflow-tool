"""
Determine Drifter Location
==========================

:Author: Clyde Clements
:Created: 2017-08-14

This script examines a drifter data file and finds the lat/lon location of the
drifter closest at a specified date and time.
"""

import logging

import numpy as np
from scipy import interpolate

logger = logging.getLogger('drifter')


def get_drifter_location(*, date, dataset, method='linear'):
    """Get drifter location in dataset closest to given date.

    Parameters
    ----------
    date : datetime.datetime
        Date and time at which to determine drifter location.
    dataset : xr.Dataset
        Drifter data set which must contain the following elements:
        * ``time``: DataArray containing timestamps
        * ``lon``: DataArray containing longitudes
        * ``lat``: DataArray containing latitudes
    method : str
        Interpolation method to use to determine drifter location. Valid
        choices are 'nearest', 'linear', 'zero', 'slinear', 'quadratic',
        and 'cubic'.

    Returns
    -------
    (float, float)
        The first element is the longitude and the second is the latitude.
    """
    if method == 'nearest':
        data_subset = dataset.sel(time=date, method=method)
        lon = float(data_subset.variables['lon'].values)
        lat = float(data_subset.variables['lat'].values)
    else:
        atime = np.array([date], dtype='datetime64[s]').astype('float64')
        dtime = np.array(dataset.time, dtype='datetime64[s]').astype('float64')
        f = interpolate.interp1d(
            dtime, dataset.lat.values, kind=method, fill_value='extrapolate')
        lat = float(f(atime[0]))
        f = interpolate.interp1d(
            dtime, dataset.lon.values, kind=method, fill_value='extrapolate')
        lon = float(f(atime[0]))

    #logger.debug('Drifter location on %s is: (lat, lon) = (%r, %r)',
    #            date.strftime('%Y-%m-%d %H:%M:%S'), lat, lon)
    logger.info('Drifter location on %s is: (lat, lon) = (%r, %r)',
                date.strftime('%Y-%m-%d %H:%M:%S'), lat, lon)

    return lon, lat


def main():
    from . import cli
    cli.run(get_drifter_location)


if __name__ == '__main__':
    main()
