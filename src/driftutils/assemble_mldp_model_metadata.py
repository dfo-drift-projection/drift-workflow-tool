"""
Assemble MLDP Model Metadata
============================
:Author: Clyde Clements, Nancy Soontiens
:Created: 2018-05-14

This module assembles metadata from velocity data files. It walks
through a directory containing ocean/atoms velocity data rpn/std
files and assembles the metadata of all files found. The metadata is
then written to a specified file. This works for both atmos and ocean
rpn data.

The metadata output is in JSON format; sample output follows:

{
  ocean_data: {
    '2017-04-01T00:00:00.000000000':
      data/ocean/2017040100_000 ,
    '2017-04-01T01:00:00.000000000':
      data/ocean/2017040100_001
    },
  updated: '2018-02-15T15:22:20.684280'
}

Requirements/Limitations:

- The specified data directory must contain data from only one model
- rpn/std files are assumed to have naming convention YYYYMMDDHH_fhr
  where YYYYMMDDHH represent the year, month, day and hour the model started
  and fhr (three digits, zero padded) represents the hour since model start
  as with forecast data
  OR YYYYMMDDHH_000 where YYYYMMDDHH represents the hour of the output as
  with analysis data.
- The directory of data should be a continuous time series with no
  overlapping timestamps.
- The directory of data should not contain other files with this
  naming convention.
"""

import datetime
import functools
import json
import multiprocessing as mp
import os
import os.path
import re
import sys

from . import configargparse
from . import utils


logger = utils.logger


def assemble_mldp_model_metadata(
        data_dir, output_file, model_type='ocean', nproc=1):
    """"Assemble model metadata from rpn standard files.

    Parameters
    ----------
    data_dir : str
        Name of directory containing ocean-model data files.
    output_file : str
        Name of output file to create containing the metadata.
    model_type : str
        Type of model data - atmos or ocean or preprocess
    nproc : int
        Number of processors for parallel scanning.
    """

    logger.info('Assembling {} metadata...'.format(model_type))
    data_metadata = {}
    if nproc > 1:
        pool = mp.Pool(processes=nproc)
        iterator = []

    output_dir = os.path.dirname(os.path.abspath(output_file))
    matchstr = r'\d{10}_\d{3}'
    for dirpath, dirnames, filenames in os.walk(data_dir,
                                                followlinks=True):
        for filename in filenames:
            # Check if filename format follows convention
            if not re.match(matchstr, filename):
                continue
            data_filename = os.path.join(dirpath, filename)

            if nproc > 1:
                iterator += [pool.apply_async(
                    scan_metadata_one_file,
                    args=(
                        data_filename,
                        output_dir))]
            else:
                one_metadata = scan_metadata_one_file(
                    data_filename,
                    output_dir,)
                for key in one_metadata:
                    if key in data_metadata:
                        data_metadata[key].update(one_metadata[key])
                    else:
                        data_metadata[key] = one_metadata[key]

    if nproc > 1:
        for x in iterator:
            one_metadata = x.get()
            for key in one_metadata:
                if key in data_metadata:
                    data_metadata[key].update(one_metadata[key])

    now = datetime.datetime.utcnow()
    metadata = dict(
        updated=now.isoformat()
    )
    metadata['{}_data'.format(model_type)] = data_metadata
    logger.info('Dumping {} metadata to file {}...'.format(model_type,
                                                           output_file))
    with open(output_file, 'w') as f:
        json.dump(metadata, f, indent=1)


def construct_timestamp(basename):
    """Construct model timestamp based on filename pattern.

    Parameters
    ----------
    basename : str
        Name of file containing ocean model data.
        Format: YYYYMMDDHH_FHR (forecasts) or
                YYYYMMDDHH_000 (analysis)
        For forecasts: YYYYMMDDHH represents year, month, day,
        hour of model start.
        FHR represenets hour since model start (three digits)
        For analysis: YYYYMMDDHH represents the output time.
        Filename represents UTC time.

    Returns
    -------
    t
        A datetime object representing model timestamp in file.
    """
    yy, mm, dd, hh, fhr = \
        int(basename[0:4]), int(basename[4:6]), int(basename[6:8]),\
        int(basename[8:10]), int(basename[11:])
    t = datetime.datetime(yy, mm, dd, hh)
    t = t + datetime.timedelta(hours=fhr)
    return t.isoformat()


def try_and_log(func):
    """ Decorator to log the error in case a function fails.
    """
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            out = func(*args, **kwargs)
        except Exception as e:
            out = None
            logger.error(e, exc_info=True)
        return out
    return wrapper


@try_and_log
def scan_metadata_one_file(data_filename, output_dir):
    """ Reads metadata from one data file

    Parameters
    ----------
    data_filename : str
        Name of data file to be scanned.
    output_dir : str
        Directory of data file.

    Returns
    -------
    data_metadata : dict
        Dictionary with metadata from files.
        Metadata includes timestamps contained in each file.
    """
    data_metadata = {}
    logger.debug('Examining data file %s...', data_filename)
    if not os.path.isabs(data_filename):
        # For the metadata constructed below, we want the path of
        # the data file relative to the output directory.
        data_filename = os.path.relpath(data_filename, output_dir)
    # Construct timestamp from filenames
    basename = os.path.basename(data_filename)
    t = construct_timestamp(basename)
    data_date = str(t)
    data_metadata[data_date] = data_filename
    return data_metadata


def main(args=sys.argv[1:]):
    arg_parser = configargparse.ArgParser(
        config_file_parser_class=configargparse.YAMLConfigFileParser
    )
    arg_parser.add('-c', '--config', is_config_file=True,
                   help='Name of configuration file')
    arg_parser.add('--log_level', default='info',
                   choices=utils.log_level.keys(),
                   help='Set level for log messages')

    arg_parser.add_argument(
        '--data_dir', type=str,
        default=('/home/nso001/data/work2/models/'
                 'MLDP-test-data/ciopse/pseudo-anal/'),
        help='Path to directory containing model data files')
    arg_parser.add_argument(
        '-o', '--output', type=str, default='ocean_data.json',
        help='Name of metadata output file to create')
    arg_parser.add_argument(
        '--model_type', type=str, default='ocean',
        help='Type of model data - atmos or ocean or wave')
    arg_parser.add_argument(
        '-n', '--nproc', type=int, default=1,
        help='Number of processors for parallel scan.')

    config = arg_parser.parse(args)
    utils.initialize_logging(level=utils.log_level[config.log_level])

    assemble_mldp_model_metadata(
        config.data_dir, config.output,
        model_type=config.model_type,
        nproc=config.nproc)

    utils.shutdown_logging()


if __name__ == '__main__':
    main()
