"""
Keep in Domain
======================
:Author: Jennifer Holden
:Created: 2020-01-06

This module checks if modelled tracks exit the ocean domain. If a track
exits the ocean domain, all remaining coordinates are set to NaN.
"""

import numpy as np
import xarray as xr
from matplotlib.path import Path

from driftutils import utils

logger = utils.logger


class IncorrectlyTrimmedTrack(Exception):
    pass


def parse_bbox(bbox):
    """ Helper function that converts the bbox from a string to a list of
    floats and applies utils.wrap_to_180() to the longitude values """

    bbox = [float(i) for i in bbox.strip().split(" ")]

    minlon = np.array([bbox[0]])
    minlon = float(utils.wrap_to_180(minlon)[0])

    maxlon = np.array([bbox[2]])
    maxlon = float(utils.wrap_to_180(maxlon)[0])

    minlon, maxlon = sorted([minlon, maxlon])

    minlat = bbox[1]
    maxlat = bbox[3]

    return [minlon, minlat, maxlon, maxlat]

                  
def find_boundary(ocean_data_file=None,
                  bbox=None,
                  lon_var='nav_lon',
                  lat_var='nav_lat'):
    """ Helper function that returns a path from either a bbox or the
    edges of an ocean data file. Otherwise returns None. """

    none_list = [None, 'None', 'none']

    if ocean_data_file not in none_list:

        # get the edges of the mesh file if it has been given
        with xr.open_dataset(ocean_data_file) as ocean_data:

            lats = ocean_data[lat_var].values.squeeze()
            lons = ocean_data[lon_var].values.squeeze()
            lons = utils.wrap_to_180(lons)

        if lats.ndim == 1:
            # If latitude and longitude are 1D arrays (as is the case with the
            # GIOPS forecast data currently pulled from datamart), then we need to
            # handle this situation in a special manner. If lats is shape m and
            # lons is shape n, we want to reshpe as (m, n) -> use meshgrid
            lons, lats = np.meshgrid(lons, lats)

        edge_lons, edge_lats = find_edges(lons, lats, 1.5)

        # create a path
        coords = [(edge_lons[p], edge_lats[p])
                  for p in range(0, len(edge_lons))]
        p = Path(coords)

    elif bbox not in none_list:

        # define the bbox corners
        bbox = parse_bbox(bbox)
        llbbox = (bbox[0], bbox[1])  # (left, lower)
        lrbbox = (bbox[2], bbox[1])  # (right, lower)
        ulbbox = (bbox[0], bbox[3])  # (left, upper)
        urbbox = (bbox[2], bbox[3])  # (right, upper)

        # create a path
        coords = [urbbox, lrbbox, llbbox, ulbbox, urbbox]
        p = Path(coords)

    else:
        p = None

    return p


def check_if_global(ocean_data_file=None,
                    lon_var=None,
                    lat_var=None,
                    bbox=None):
    """ Helper function that checks if the given ocean model mesh file
    covers a global domain. If global domain, function returns True.
    Otherwise, returns False.

    Currently checks whether the total latitude and longitude surpass
    default values. A more robust test would be useful, since in rare
    cases oddly shaped domains may still unintentionally pass as global
    domains. For example, a long, skinny, sloping rectangular domain
    could theoretically pass this test despite the fact that it does not
    in fact cover the entire global ocean. The default values were chosen
    using the GIOPS domain as a reference, which has extreme values of
    [-180.0, 180.0, -77.010475, 89.94787]

    Parameters
    ----------
    ocean_data_file : str
        path to an ocean mesh file
    lon_var : str
        Name of longitude variable in mesh file
    lat_var : str
        Name of latitude variable in mesh file
    bbox : str
        list of coordinates defining the outer edge of the user provided
        bounding box. [minlon, minlat, maxlon, maxlat]
    """

    if ocean_data_file is None and bbox is None:
        return False

    if ocean_data_file is not None:
        with xr.open_dataset(ocean_data_file) as ds:
            lons = ds[lon_var].values.flatten()
            lons = utils.wrap_to_180(lons)
            lats = ds[lat_var].values.flatten()

        minlon = np.nanmin(lons)
        maxlon = np.nanmax(lons)
        minlat = np.nanmin(lats)
        maxlat = np.nanmax(lats)

    else:
        [minlon, minlat, maxlon, maxlat] = parse_bbox(bbox)

    total_lat = (maxlat - minlat)
    total_lon = (maxlon - minlon)

    # converting longitude 360 with wrap_to_180() returns 0, so a non-global
    # domain should have a total_lon between 0 and 359.
    if total_lat < 161 and total_lon > 0 and total_lon < 359:
        return False
    else:
        return True


def calculate_edges(arr, offset, closed=True):
    """Finds the edges of an array.
    The offset is the number of elements in from the actual
    edge to define the returned edge values. For example, an offset value
    of 2, skips the outer two edge elements and returns the third. The
    closed boolean specifies whether the first point of the list will be
    appended to the end of the final array to create a closed path. """

    arr = np.squeeze(arr)
    Nrows = arr.shape[0]
    Ncols = arr.shape[1]

    for dim, errstr in zip([Nrows, Ncols], ['rows', 'columns']):
        if dim < 2 * offset:
            errstr = ('The given array has dimensions [' + str(Nrows)
                      + ', ' + str(Ncols) + ']. It does not have '
                      + 'enough ' + errstr + ' to find edges using an '
                      + 'offset value of ' + str(offset))
            logger.info(errstr)
            return []

    # top
    top = [arr[0 + offset, 0 + offset:0 - offset]
           if offset > 0 else arr[0, :]][0]

    # bottom
    bot = np.flip([arr[-1 - offset, 0 + offset:0 - offset]
                  if offset > 0 else arr[-1, :]][0])
    bot = [[] if Nrows == 2 * offset + 1 else bot][0]

    # left side
    lft = np.flip(arr[1 + offset:-1 - offset, 0 + offset])
    lft = [[] if Ncols == 2 * offset + 1 else lft][0]

    # right side
    rgt = arr[1 + offset:-1 - offset, -1 - offset]

    edges = np.concatenate([top, rgt, bot, lft])

    # if closed, add the first point to the end of the edges list
    # so that it will be a closed loop
    if closed:
        open_edges = edges
        edges = open_edges.tolist() + [open_edges.tolist()[0]]

    return {'edge': edges, 'top': top, 'rgt': rgt, 'bot': bot, 'lft': lft}


def calculate_weighted_average_edges(inner, outer, coord, offset, closed=True):
    """Find the weighted average between two sets of edges from a mesh.
    The offset is the number of elements in from the actual
    edge to define the returned edge values. For example, an offset value
    of 2, skips the outer two edge elements and returns the third. The
    closed boolean specifies whether the first point of the list will be
    appended to the end of the final array to create a closed path. """

    def calculate_weighted_average(inner, outer, offset):
        frac = offset % 1
        return frac * inner + (1 - frac) * outer

    inner = inner[coord]
    outer = outer[coord]

    # top
    avg_top = calculate_weighted_average(inner['top'],
                                         outer['top'][1: -1],
                                         offset)

    # right side
    inner_rgt_pts = np.concatenate([np.array([inner['top'][-1]]), inner['rgt'],
                                    np.array([inner['bot'][0]])])
    avg_rgt = calculate_weighted_average(inner_rgt_pts, outer['rgt'], offset)

    # bottom
    avg_bot = calculate_weighted_average(inner['bot'],
                                         outer['bot'][1: -1],
                                         offset)

    # left side
    inner_lft_pts = np.concatenate([np.array([inner['bot'][-1]]), inner['lft'],
                                    np.array([inner['top'][0]])])
    avg_lft = calculate_weighted_average(inner_lft_pts, outer['lft'], offset)

    avg_edges = np.concatenate([avg_top, avg_rgt, avg_bot, avg_lft])

    # if closed, add the first point to the end of the edges list
    # so that it will be a closed loop
    if closed:
        open_edges = avg_edges
        avg_edges = open_edges.tolist() + [open_edges.tolist()[0]]

    return {'edge': avg_edges,
            'top': avg_top,
            'rgt': avg_rgt,
            'bot': avg_bot,
            'lft': avg_lft}


def find_edges(lons, lats, offset):
    """Helper function that finds the edges of an array.
    Here the offset is the number of elements in from the actual edge that
    is used to define the returned edge values. For example, an offset value
    of 2, skips the outer two edge elements and returns the third."""

    edges = {}
    offsets = np.unique([int(np.floor(offset)), int(np.ceil(offset))])
    for offs in offsets:
        edges[str(offs)] = {'lons': calculate_edges(lons, offs),
                            'lats': calculate_edges(lats, offs)}

    if len(edges.keys()) > 1:
        edge_keys = [int(k) for k in edges.keys()]
        outer = edges[str(np.min(edge_keys))]
        inner = edges[str(np.max(edge_keys))]
        wlons = calculate_weighted_average_edges(inner, outer, 'lons', offset)
        wlats = calculate_weighted_average_edges(inner, outer, 'lats', offset)
        edges[str(offset)] = {'lons': wlons, 'lats': wlats}

    edge_lons = edges[str(offset)]['lons']['edge']
    edge_lats = edges[str(offset)]['lats']['edge']

    return edge_lons, edge_lats


def get_trimmed_tracks(mod_track,
                       run_name,
                       global_domain=False,
                       boundary=None):
    """ Helper function that checks if a modelled drifter track exists
    the domain, then sets the rest of the track to NaN if it does. If
    using a global domain or if no boundary is provided, function returns
    the unchanged lat and lon values from the original dataset. If the
    tracks are trimmed, the new lats and lons can then be used later to
    replace the values in the original model track dataset """

    mod_track['lon'] = utils.wrap_to_180(mod_track.lon)

    if global_domain:
        trimmed_track_lons = mod_track.lon.values
        trimmed_track_lats = mod_track.lat.values

    elif boundary not in [None, 'None']:
        trimmed_track_lons, trimmed_track_lats = \
            trim_track_to_domain(mod_track, boundary, run_name)

    else:
        logger.info('Not using global ocean domain and no ocean mesh file or '
                    + 'bounding box provided. No trimming will be performed')
        trimmed_track_lons = mod_track.lon.values
        trimmed_track_lats = mod_track.lat.values

    # quick check to be sure that nothing has gone wrong during trimming
    if len(mod_track.lon.values) != len(trimmed_track_lons):
        msg = ('The length of the trimmed track does not equal the number '
               + 'of longitude values in the modelled track.')
        raise IncorrectlyTrimmedTrack(msg)

    return trimmed_track_lons, trimmed_track_lats


def trim_track_to_domain(mod, boundary, run_name):
    """Check if track remains inside domain.

    Parameters
    ----------
    mod : xr.Dataset
        modelled track as a function of time.
    boundary : matplotlib.path.Path
        path created either from the edges of an ocean mesh file or from
        an initial bounding box.
    run_name : str
        identifier for the specific modelled track being trimmed

    Returns
    -------
    lists of modified lats and lons for a modelled drifter track.
    The modified lists have coordinates set to NaN for points occuring
    after a modelled track has left the domain. These lists can be used
    to replace the lats and lons in the output dataset, before skill
    scores are calculated.
    """

    # Create convenience array for modelled track.
    mlat = np.ma.masked_invalid(mod['lat'].values)
    mod['lon'] = utils.wrap_to_180(mod.lon)
    mlon = np.ma.masked_invalid(mod['lon'].values)

    if boundary not in [None, 'None']:
        # if a boundry has been provided (either via an ocean mesh file
        # or bounding box), use it to find track points that are inside
        # the model domain. If the ocean model being used has a global
        # domain, it should not reach this step (ie, trim_tracks_to_domain
        # should not be called in that case)
        mlon, mlat = points_inside_boundary(mlon, mlat, run_name, boundary)

    else:
        logger.info('No bbox or ocean mesh file has been provided '
                    'so data will not be trimmed to a domain.')
        mlon = mlon.flatten()
        mlat = mlat.flatten()

    return mlon, mlat


def points_inside_boundary(mlon, mlat, run_name, boundary):
    """ find the points in a modelled drifter track that are inside a
    given model domain. Set all outside points to np.nan

    Parameters
    ----------
    mlon : numpy.ma.core.MaskedArray
        drifter track longitude values
    mlat : numpy.ma.core.MaskedArray
        drifter track latitude values
    run_name : str
        identifier for the modelled track that is being checked
    boundary : matplotlib.Path
        bounding box of data to use when deciding if points are in or
        out of a model domain. The bounding box is calculated from either
        the edges of the ocean model mask file or from the bbox (usually
        in the case of a MLDP experiment).
    """
    # figure out which points are inside the boundary
    points = np.vstack((mlon.flatten(), mlat.flatten())).T
    grid = boundary.contains_points(points)

    # If the track leaves the domain, find the first index where it does so
    exit_index = np.where(grid.astype(int) == 0)[0]

    # If the track does exit the domain, set all the points
    # to np.nan after the first occurance
    if exit_index.size:
        logger.info('Modelled track ' + str(run_name) + ' has left the '
                    + 'domain. Setting rest of track to NaN ...')
        points[exit_index[0]:] = np.nan

    return points[:, 0], points[:, 1]
