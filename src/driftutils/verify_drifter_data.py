###############################################################################
# Verify drifter data
#
# This script should do some QA on the input data and determine a Go or No-Go
# status for subsequent processing. It should do some basic checks such as
# verifying that drifter data actually exists between the user-specified dates.
#
# Author: Clyde Clements
# Created: 2017-08-28 13:14:21 -0230
###############################################################################

import datetime
import glob
import json
import os
import os.path
import pandas as pd
import re
import sys

import dateutil.parser
import numpy as np
import xarray as xr

from . import configargparse, utils
from .utils import defaults, logger, set_run_name


def verify_drifter_data(
        metadata_file,     # type: str
        first_start_date,  # type: datetime.datetime
        last_start_date,   # type: datetime.datetime
        drifter_depth,     # type: str
        drift_duration     # type: datetime.timedelta
):  # type: (...) -> None
    """Verifies drifter data

    Checks: Time : Checks if time is sorted or accurate
            Displacement: Checks for displace of drifter displacement
            Checks if file opens...
    """
   
    logger.info('\nVerifying drifter data...')
    with open(metadata_file, 'r') as f:
        metadata = json.load(f)
    drifters = metadata['drifters']
    
    for v in drifters:
        metadata_start_date = dateutil.parser.parse(v['start_data_date'])
        metadata_last_date = dateutil.parser.parse(v['last_data_date'])
        end_date = last_start_date + drift_duration

        file_name = v['filename'].split("/")[-1]
        try:
            dataset = xr.open_dataset(v['filename'])
            TIME,LON,LAT = get_variable_name(dataset)

            logger.info('Checking for drifter time accuracy...')

            # Check dataset date accuracy, this determines if time is sorted in drifter data
            if(TIME.values.min() != TIME.values[0] or TIME.values.max() != TIME.values[-1]):
                logger.info('Inaccurate/unsorted drifter time for {}'.format(v['buoyid']))
                v['qc_status'] = 'Unsorted time'

            elif metadata_start_date >= end_date or metadata_last_date <= first_start_date:
                logger.info(('Ignoring drifter %s because it does not contain '
                             'data in the simulation period.'), v['buoyid'])
                v['qc_status'] = ('Drifter does not contain data in '
                                  'user-specified simulation period')
            else:
                logger.info('Drifter time checked for {}...'.format(v['buoyid']))
            
            # Check for drifter depth
            if 'approximate_drogue_depth' not in dataset.attrs:
                logger.info('No approximate_drogue_depth attribute in dataset '
                            'for drifter {}'.format(v['buoyid']))
            else:
                # Don't compare if using model levels vs actual depth
                if (drifter_depth.startswith('L')):
                    continue
                drifter_depth = re.findall('\d+', drifter_depth)[0]
                if(float(drifter_depth) != float(dataset.approximate_drogue_depth)):
                    logger.warn(('WARNING: The model depth specified by the user in drifter_depth'
                                 ' is {} which is different from the approximate_drouge_depth'
                                 ' ({}) for drifter {}. \n'
                                 ' The simulation will continue with the user specified'
                                 ' drifter_depth.'.format(
                                     drifter_depth,
                                     dataset.approximate_drogue_depth,
                                     v['buoyid'])))

        except OSError:
            # Some drifter files failed to open. Probably bad files
            logger.info('Drifter {} failed to open...'.format(file_name))
            v['qc_status'] = 'Drifter file failed to open'

    now = datetime.datetime.utcnow()
    metadata = dict(updated=now.isoformat(), drifters=drifters)
    with open(metadata_file, 'w') as f:
        json.dump(metadata, f, indent=1)


def get_variable_name(ds):
    lon = ['Lon', 'Longitude', 'lon', 'LONGITUDE', 'LON', 'longitude']
    lat  = ['Lat', 'Latitude', 'lat', 'LATITUDE', 'LAT', 'latitude']
    time = ['Time', 'TIME', 'time']

    for v in time:
        if v in ds.coords:
            time_var=ds.coords[v]
        elif v in ds.data_vars:
            time_var=ds.data_vars[v]

    for v in lat:
        if v in ds.coords:
            lat_var=ds.coords[v]
        elif v in ds.data_vars:
            lat_var=ds.data_vars[v]
    for v in lon:
        if v in ds.coords:
            lon_var=ds.coords[v]
        elif v in ds.data_vars:
            lon_var=ds.data_vars[v]
    return time_var, lon_var, lat_var


def main(args=sys.argv[1:]):
    arg_parser = configargparse.ArgParser(
        config_file_parser_class=configargparse.YAMLConfigFileParser
    )
    arg_parser.add('-c', '--config', is_config_file=True,
                   help='Name of configuration file')
    arg_parser.add('--log_level', default='info',
                   choices=utils.log_level.keys(),
                   help='Set level for log messages')
    arg_parser.add('--metadata_file', type=str,
                   help='Path to file containing metadata for drifter data')
    arg_parser.add('--first_start_date', type=str, required=True,
                   help='First date and time for start of drift calculations')
    arg_parser.add('--drifter_depth', type=str, required=True,
                   help='User-specified drifter depth')
    arg_parser.add('--drift_duration', type=int,
                   help='Number of days for drift calculation')
    config = arg_parser.parse(args)

    utils.initialize_logging(level=utils.log_level[config.log_level])

    first_start_date = dateutil.parser.parse(config.first_start_date,
                                             ignoretz=True)
    last_start_date = dateutil.parser.parse(config.last_start_date,
                                            ignoretz=True)
    verify_drifter_data(config.metadata_file, first_start_date,
                        last_start_date, config.num_drift_days)

    utils.shutdown_logging()


if __name__ == '__main__':
    main()
