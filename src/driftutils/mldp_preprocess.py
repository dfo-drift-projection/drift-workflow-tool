
"""
Run MLDP preprocess
=============================
:Author: Samuel T. Babalola, Nancy Soontiens
:Created: 2018-09-19

This module creates and runs the preprocess bash script.
"""

import os
from os.path import abspath, isabs, join as joinpath
import subprocess

from driftutils import utils

logger = utils.logger


def mldp_preprocess(
        *,
        wetfiles,
        metfiles,
        wavefiles,
        preprocess_dir,
        bbox,
        depth=0,
        res=1000,
        make_mask=True,
):
    """Run MLDP preprocessing.

    Parameters
    ----------
    wetfiles : str
        Path to file containing list of ocean files to pre-process.
    metfiles : str
        Path to file containing list of atmos files to pre-process.
    wavefiles : str or None
        Path to file containing list of wave files or None if no wave forcing
    preprocess_dir : str
        Path to store output from pre-preprocessing.
    bbox : str
        Specify coordinates for pre-process.
        min_lon min_lat max_lon max_lat.
    depth : float
        depth of ocean currents in m.
    res : float
        resolution of output grid
    make_mask : boolean
        Produce the mask (True) or not (False).
    """
    cwd = os.getcwd()
    os.chdir(preprocess_dir)
    script_name = 'preprocess_script.sh'
    # Write the executable preprocess script
    call_script = joinpath(preprocess_dir, script_name)
    logger.info('Writing run script to {} '.format(call_script))

    # Reformat bbox in mldpn order - min_lat min_lon max_lat max_lon
    preproc_bbox = bbox.split()
    preproc_bbox = "{} {} {} {}".format(preproc_bbox[1], preproc_bbox[0],
                                        preproc_bbox[3], preproc_bbox[2])

    script = 'EERWetDB.tcl'

    lines = [
        "#!/bin/bash",
        "# Script to call EERWetDB.tcl for processing rpn data for",
        "# use in MLDP.",
        "# Run EERWetDB.tcl --help for explanation of arguments.",
        "",
        "# Run script",
        "%s \\" % script,
        "  -metfiles %s \\" % metfiles,
        "  -wetfiles %s \\" % wetfiles,
        "  -depth %s \\" % depth,
        "  -res %s \\" % res,
        "  -bbox %s \\" % preproc_bbox,
        "  -out %s \\" % preprocess_dir,
        "  -interp \\"
    ]

    if make_mask:
        lines.append("  -mask \\")

    if wavefiles is not None:
        lines.append("  -wavesfiles %s \\" % wavefiles)
    else:
        lines.append("  -waves False \\")

    with open(os.open(call_script, os.O_CREAT | os.O_WRONLY, 0o777), 'w') as f:
        f.write("\n".join(lines))

    logger.info('\nRunning preprocess...')
    preprocess_out = joinpath(preprocess_dir, 'preprocess.out')
    logstr = 'preprocess run information will be written to {}'
    logger.info(logstr.format(preprocess_out))
    with open(preprocess_out, 'w') as out:
        process = subprocess.run([call_script],
                                 cwd=preprocess_dir,
                                 stdout=out,
                                 stderr=subprocess.STDOUT)
    return_status = process.returncode
    if return_status != 0:
        logger.warn('preprocess  with an error status of %s',
                return_status)
    logger.info('Finished running preprocess.')
    os.chdir(cwd)
        

def main():
    from . import cli
    cli.run(mldp_preprocess)

    
if __name__ == '__main__':
    main()
