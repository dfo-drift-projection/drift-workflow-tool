###############################################################################
# Run Ariane trajectory simulation
#
# Author: Clyde Clements
# Created: 2017-08-03 16:15:52 -0230
###############################################################################

import os
import os.path
import shutil
import subprocess
import sys

from . import configargparse
from . import utils
from .utils import logger


def run_ariane(
        run_dir,                 # type: str
        namelist,                # type: str
        initial_position,        # type: List[List]
        ariane_exec,             # type: str
        overwrite_run_dir=False  # type: bool
):  # type: (...) -> int
    run_dir = os.path.abspath(run_dir)
    logger.info('Preparing to run Ariane in directory %s...', run_dir)

    if not os.path.exists(run_dir):
        logger.info('Creating run directory...')
        os.makedirs(run_dir)
    else:
        if not overwrite_run_dir:
            logger.error(('Run directory already exists. Please delete it or '
                          'specify a different directory.'))
            sys.exit(1)

    logger.info('Populating run directory with necessary input files...')
    run_namelist = os.path.join(run_dir, 'namelist')
    if not os.path.abspath(namelist) == os.path.abspath(run_namelist):
        shutil.copy(namelist, run_namelist)
    with open(os.path.join(run_dir, 'initial_positions.txt'), 'w') as f:
        for pos in initial_position:
            f.write('{} {} {} {} 1.0\n'.format(*pos))
    # Only run Ariane if initial positions is populated
    if len(initial_position) == 0:
        logger.warn('No initial positions for run. Not running ariane.')
        return_status='no drifters'
    else:
        logger.info('Running Ariane...')
        process = subprocess.Popen(ariane_exec, cwd=run_dir,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.STDOUT)
        with open(os.path.join(run_dir, 'ariane_output.txt'), 'w') as f:
            while True:
                out = process.stdout.read()
                out = out if isinstance(out, str) else out.decode('utf-8')
                if out == '' and process.poll() is not None:
                    break
                f.write(out)
        return_status = process.poll()
        # Check for coast crash in last line of ariane output
        with open(os.path.join(run_dir, 'ariane_output.txt'), 'r') as f:
            out = f.readlines()
            if 'coast crash' in out[-1]:
                return_status = 'coast crash'

    if return_status != 0:
        logger.warn('Ariane finished with an error status of %s',
                    return_status)
    logger.info('Finished running Ariane.')
    return return_status


def main(args=sys.argv[1:]):
    arg_parser = configargparse.ArgParser(
        config_file_parser_class=configargparse.YAMLConfigFileParser
    )
    arg_parser.add('-c', '--config', is_config_file=True,
                   help='Name of configuration file')
    arg_parser.add('--log_level', default='info',
                   choices=utils.log_level.keys(),
                   help='Set level for log messages')

    arg_parser.add('--ariane_exec', type=str,
                   default='/data/ocean/tools/ariane-2.2.8_04/bin/ariane',
                   help='Path to Ariane executable')
    arg_parser.add('--run_dir', type=str, default='run',
                   help=('Name of directory in which to run Ariane. This is '
                         'where its output files will be generated.'))
    arg_parser.add('--namelist', type=str, default='namelist',
                   help='Name of namelist configuration file')
    arg_parser.add('--initial_position', nargs=4, type=float,
                   action='append', metavar=('x', 'y', 'z', 't'),
                   help='Initial position of particle to track')
    arg_parser.add('--overwrite_run_dir', type=bool, default=True,
                   action='store',
                   help='Overwrite contents of run directory if it exists')
    config = arg_parser.parse(args)

    utils.initialize_logging(level=utils.log_level[config.log_level])

    run_ariane(config.run_dir, config.namelist, config.initial_position,
               config.ariane_exec, overwrite_run_dir=config.overwrite_run_dir)

    utils.shutdown_logging()


if __name__ == '__main__':
    main()
