"""
Command-Line Interface Utilities
================================
:Author: Clyde Clements
:Created: 2017-08-04

This module provides utility routines for command-line interface scripts.
"""
from argparse import RawTextHelpFormatter
import collections
import datetime
import logging
import logging.handlers
from functools import wraps

import dateutil
import json
import xarray as xr

from . import configargparse
from . import ioutils


# Global variables.
# Format of messages to send to the console.
console_fmt = logging.Formatter('%(message)s')
# Format of messages to send to a log file.
file_fmt = logging.Formatter(
    '%(asctime)s %(levelname)s: %(message)s', '%Y-%m-%d %H:%M:%S'
)

first_import = True
logging_initialized = False

# Our logger.
logger = logging.getLogger('drifter')
logger.propagate = False


# Logging information can be sent to two places: a log file and the console
# (more specifically sys.stderr). The log file records all information; that
# is, from the DEBUG level and up. By default, the console records information
# at level INFO and higher, though this can be changed by a command-line
# option.

log_level_dict = dict(
    debug=logging.DEBUG,
    info=logging.INFO,
    warning=logging.WARNING,
    error=logging.ERROR,
    critical=logging.CRITICAL
)

log_level = log_level_dict
default_log_level = logging.INFO

# We need to be able to start logging as soon as possible, even before
# command-line arguments are parsed. But we won't know what level of logging to
# send to the console nor what log file to use until after those arguments have
# been parsed. Therefore, we initially set up a memory handler to buffer all
# log messages. Once the user-specified settings have been checked, we flush
# the in-memory log messages to the log file and the console.
#
# Note that a memory handler requires a specified capacity, flush log level,
# and target handler. The log messages will be flushed to the target handler if
# either the capacity is exceeded or when an event of at least the flush level
# severity is seen.
if first_import:
    # We assume that we are the first ones to call the logging module.
    console_hdlr = logging.StreamHandler()
    console_hdlr.setFormatter(console_fmt)
    console_hdlr.setLevel(logging.DEBUG)
    # Maximum number of log messages bufferred in memory before it is flushed
    # to the console handler.
    mem_hdlr_capacity = 5000
    mem_hdlr = logging.handlers.MemoryHandler(mem_hdlr_capacity,
                                              logging.ERROR, console_hdlr)
    mem_hdlr.setLevel(logging.DEBUG)
    logger.addHandler(mem_hdlr)
    logger.setLevel(logging.DEBUG)
    first_import = False


def initialize_logging(filename=None, level=default_log_level):
    """Initialize logging.

    If ``filename`` is specified, send log messages to that file; otherwise,
    send them to the default log file.
    """
    global mem_hdlr, console_hdlr, file_fmt, logger
    global logging_initialized

    if logging_initialized:
        return

    # Set up logging to the console. The handler for this has already been
    # created; we only need to adjust the log level.
    console_hdlr.setLevel(level)
    logger.addHandler(console_hdlr)
    if filename:
        # Set up logging to a log file.
        logfile_hdlr = logging.FileHandler(filename, 'w')
        logfile_hdlr.setFormatter(file_fmt)
        logfile_hdlr.setLevel(logging.DEBUG)
        logger.addHandler(logfile_hdlr)

    if mem_hdlr is not None:
        # Closing the memory handler will cause its contents to be flushed.
        # We set its target to the root logger, which will then call all of its
        # associated handlers.
        mem_hdlr.setTarget(logging.getLogger())
        logger.removeHandler(mem_hdlr)
        mem_hdlr.close()
        mem_hdlr = None

    logging_initialized = True


def shutdown_logging():
    global mem_hdlr
    if mem_hdlr is not None:
        # This indicates the initialization process has not been performed; do
        # it now.
        initialize_logging()
    logging.shutdown()


def parse_datetime(val):
    date = dateutil.parser.parse(val, ignoretz=True)
    logger.debug('Parsed user-specified date: %s', date)
    return date


def parse_timedelta(val):
    return datetime.timedelta(hours=int(val))


def parse_dataset(val):
    dataset = ioutils.load_dataset(val)
    return dataset

def parse_list(val):    
    list = [val]
    return list

def parse_dict(val):
    return json.loads(val)

def parse_boolean(val):
    if isinstance(val, bool):
        return val
    if val.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif val.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

boolean='boolean'
    
LatLonBoundingBox = collections.namedtuple('LatLonBoundingBox',
                                          ('lat_min', 'lat_max', 'lon_min', 'lon_max'))


def parse_collection(val1, val2, val3, val4):
    return LatLonBoundingBox(lat_min = val1, lat_max = val2, lon_min = val3, lon_max = val4)
    


def _update_func(func):
    @wraps(func)
    def wrapper(*args, config=None, log_level='info', **kwargs):
        initialize_logging(level=log_level_dict[log_level])
        result = func(*args, **kwargs)
        shutdown_logging()
        return result
    return wrapper


def run(func):
    import defopt

    def _create_parser(funcs, *args, **kwargs):
        parsers = kwargs.pop('parsers', None)
        short = kwargs.pop('short', None)
        show_types = kwargs.pop('show_types', False)
        cli_options = kwargs.pop('cli_options', 'kwonly')
        show_defaults = kwargs.pop('show_defaults', True)
        no_negated_flags = kwargs.pop('no_negated_flags', False)
        version = kwargs.pop('version', None)
        argparse_kwargs = kwargs.pop('argparse_kwargs', {})

        if kwargs:
            raise TypeError(
                'unexpected keyword argument: {}'.format(list(kwargs)[0]))
        
        formatter_class = RawTextHelpFormatter
        parser = configargparse.ArgumentParser(
            formatter_class=formatter_class,
            config_file_parser_class=configargparse.YAMLConfigFileParser
        )
        parser.add_argument(
            '-c', '--config', is_config_file=True,
            help='Name of configuration file')
        parser.add_argument(
            '--log-level', default='info', dest='log_level',
            choices=['debug', 'info', 'warning', 'error', 'critical'],
            help='Set level for log messages')
        if callable(funcs):
            defopt._populate_parser(funcs, parser, parsers, short,
                                    cli_options, show_defaults, show_types,
                                    no_negated_flags)
            parser.set_defaults(_func=funcs)
        else:
            subparsers = parser.add_subparsers()
            for func in funcs:
                subparser = subparsers.add_parser(
                    func.__name__.replace('_', '-'),
                    formatter_class=formatter_class,
                    help=defopt._parse_function_docstring(func).first_line)
                defopt._populate_parser(func, subparser, parsers, short,
                                        cli_options, show_defaults, show_types,
                                        no_negated_flags)
                subparser.set_defaults(_func=func)
        return parser

    defopt._create_parser = _create_parser
    defopt.run(
        _update_func(func),
        short={},
        parsers={
            datetime.datetime: parse_datetime,
            datetime.timedelta: parse_timedelta,
            xr.Dataset: parse_dataset,
            dict: parse_dict,
            boolean: parse_boolean
        },
    )
