"""
yaml2bash.py
=============================
:Author: Jennifer Holden
:Created: 2021-02-23
"""

import sys
import argparse
import yaml

def yaml2bash(config):

    with open(config, 'r') as my_yaml:
        try:
            yamldict = yaml.safe_load(my_yaml)
            for key, val in yamldict.items():
                fixed_key = key.replace('-','_')
                if ' ' in str(val):
                    val = '\"' + str(val) + '\"'
                line = str(fixed_key) + '=' + str(val)
                print(line)

        except yaml.YAMLError as exc:
            print(exc)


def main(args=sys.argv[1:]):

    #load the command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', type=str, help='path to yaml config file')
    args = (parser.parse_args())

    #convert the file
    yaml2bash(args.config)

if __name__ == '__main__':
    main()



