###############################################################################
# Verify ocean data
#
# This script should do some QA on the input data and determine a Go or No-Go
# status for subsequent processing. It should do some basic checks such as
# verifying that ocean data actually exists between the user-specified dates.
#
# Author: Clyde Clements
# Created: 2017-08-28
###############################################################################

import sys

import dateutil.parser

from . import configargparse
from . import utils


def verify_ocean_data(
        metadata_file,     # type: str
        first_start_date,  # type: datetime.datetime
        last_start_date,   # type: datetime.datetime
        drift_duration     # type: datetime.timedelta
):  # type: (...) -> None
    """Verifies ocean data

    Raises:
        ValueError: The data is invalid in some way.
    """
    # TODO: Implement checks on data.
    #
    # if ...:
    #     raise ValueError('Invalid ocean data')
    return


def main(args=sys.argv[1:]):
    arg_parser = configargparse.ArgParser(
        config_file_parser_class=configargparse.YAMLConfigFileParser
    )
    arg_parser.add('-c', '--config', is_config_file=True,
                   help='Name of configuration file')
    arg_parser.add('--log_level', default='info',
                   choices=utils.log_level.keys(),
                   help='Set level for log messages')

    arg_parser.add('--metadata_file', type=str,
                   help='Path to file containing metadata for ocean data')
    arg_parser.add('--first_start_date', type=str, required=True,
                   help='First date and time for start of drift calculations')
    arg_parser.add('--last_start_date', type=str, required=True,
                   help='Last date and time for start of drift calculations')
    arg_parser.add('--num_drift_days', type=int,
                   help='Number of days for drift calculation')
    config = arg_parser.parse(args)

    utils.initialize_logging(level=utils.log_level[config.log_level])

    first_start_date = dateutil.parser.parse(config.first_start_date,
                                             ignoretz=True)
    last_start_date = dateutil.parser.parse(config.last_start_date,
                                            ignoretz=True)
    verify_ocean_data(config.metadata_file, first_start_date,
                      last_start_date, config.num_drift_days)

    utils.shutdown_logging()


if __name__ == '__main__':
    main()
