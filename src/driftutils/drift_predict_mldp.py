"""
Run Drift Prediction Workflow
=============================
:Author: Clyde Clements
:Contributors: Nancy Soontiens
:Created: 2017-08-15
"""

import datetime
import glob
import json
import os
from os.path import abspath, isabs, join as joinpath
import sys

import dateutil.rrule
import numpy as np

from .adjust_bbox import adjust_bbox
from .add_traj_data_date import add_traj_data_date_to_datafile
from .add_traj_label import add_traj_label_to_datafile
from .assemble_drift_predictions import assemble_drift_predictions
from .assemble_drifter_data import (assemble_drifter_data,
                                    NoDrifterDataForTimePeriod)
from .assemble_drifter_metadata import assemble_drifter_metadata
from .assemble_mldp_model_metadata import assemble_mldp_model_metadata
from .assemble_mldp_model_data import (assemble_preprocessed_data,
                                       assemble_raw_data)
from .make_mldp_namelist import make_mldp_namelist
from .mldp_preprocess import mldp_preprocess
from .mldp_pre_eval import convert_drift_predictions
from .run_mldp import run_mldp
from .set_initial_positions import set_initial_positions_mldp
from .utils import (defaults, logger, set_run_name, setup_savedir, debuginfo)
from .verify_drifter_data import verify_drifter_data
from .verify_ocean_data import verify_ocean_data


def drift_predict_mldp(*,
        experiment_dir,
        drifter_data_dir,
        ocean_data_dir,
        ocean_model_name,
        first_start_date,
        last_start_date,
        drift_duration,
        atmos_data_dir,
        atmos_model_name,
        wave_data_dir='None',
        wave_model_name='None',
        drifter_depth=0,
        drift_model_name='MLDP',
        alpha_wind=3,
        current_uncertainty=0,
        drifter_id_attr='buoyid',
        drifter_meta_variables=None,
        start_date_frequency='daily',
        start_date_interval=1,
        overwrite_savedir=False,
        bbox=None,
        MLDP_executable='MLDPn',
        run_preprocessing=True,
        preprocess_dir=None,
        res=1000,
        make_mask=True,
        mldp_dt=60,
        output_dt=3600,
):
        
    """Run drift prediction workflow.

    Parameters
    ----------
    experiment_dir : str
        Name of directory in which to run the experiment. This is where the
        output files will be generated.
    drifter_data_dir : str
        Path to directory containing drifter data files.
    ocean_data_dir : str
        Path to directory containing ocean-model data files.
    ocean_model_name : str
        Name of ocean model (e.g. GIOPS or Salish Sea).
    first_start_date : datetime.datetime
        Start date for first drift simulation.
    last_start_date : datetime.datetime
        Start date for last drift simulation.
    drift_duration : datetime.timedelta
        Duration of drift.
    atmos_data_dir : str
        Path to directory containing atmos-model data files.
    atmos_model_name : str
        Name of atmos model (e.g. HRDPS).
    wave_data_dir : str
        Path to directory containing wave model data files. Can be 'None' if
        no Stokes drift forcing is desired.
    wave_model_name : str
        Name of wave model (e.g. SMOC). Can be 'None' if no Stokes drift
        forcing is desired.
    drifter_depth : float
        Specifies the depth in metres for drift trajectory simulations.
    drift_model_name : str
        Name of drift model (one of 'Ariane' or 'OpenDrift' or 'MLDP').
    alpha_wind : float
        Percentage of wind velocity to be applied to drifter.
    current_uncertainty : float
        Root-mean-square value for current uncertainty in m/s which is used
        to add a random walk to particle motion.
        Relates to horizontal diffusivity, K as
        K = 0.5*current_uncertainty**2*mldp_dt
    drifter_id_attr : str
        Name of attribute in drifter data files containing buoy id.
    drifter_meta_variables : str
        Names of variables in drifter data files that contain metadata.
    start_date_frequency : str
        Identifies the type of recurrence rule for start dates. The default
        value is "daily", to specify repeating events based on an interval of
        a day or more.
    start_date_interval : int
        A positive integer representing how often the recurrence rule for start
        dates repeats. The default value is 1, meaning every day for a daily
        rule. A value of 2 would mean every two days for a daily rule and so
        on. 
    overwrite_savedir : boolean
        boolean value specifying whether to overwrite the user defined
        savedir if it already exists.
    bbox : str
        outer corners of bounding box domain in which to perform evaluations
        e.g. "min_lon min_lat max_lon max_lat"
        Note: preprocessed files must correspond to this domain
    MLDP_executable : str
        path to MLDP executable
    run_preprocessing : boolean
        Run pre-processing of rpn files.
    preprocess_dir : str
       Path to pre-processed rpn files. Only needed if
       run_prerocessing=False, otherwise set to None and a
       new directory will be created.
    res : float
        Resolution of output grid for pre-processed rpn files
    make_mask : boolean
        Create the mask file (True) or not (False).
    mldp_dt : float
        Integration time step in seconds.
    output_dt : float
        Output frequency of simluation in seconds.
    """
    # Modify None settings
    if preprocess_dir == 'None':
        preprocess_dir = None

    # Track details about each run such as run directory and the workflow start
    # and finish times.
    run_details = {
        'drift_model': drift_model_name,
        'ocean_model': ocean_model_name,
        'atmos_model': atmos_model_name,
        'wave_model': wave_model_name,
        'alpha_wind': alpha_wind,
        'runs': {},
        'workflow_start_time': datetime.datetime.utcnow().isoformat()
    }

    # Check that wave settings make sense
    wave_forcing = True
    if wave_data_dir == 'None':
        wave_forcing = False
    if wave_forcing and (wave_model_name == 'None'):
        logger.warn(
            ("WARNING: wave_data_dir is provided but wave_model_name "
             "set to 'None'. To avoid this warning, set "
             "wave_model_name when providing wave data.")
        )
        wave_model_name = 'Unknown'
        run_details['wave_model'] = wave_model_name

    run_details_file = abspath(joinpath(experiment_dir, 'runs.json'))

    drifter_metadata_file = abspath(joinpath(experiment_dir, 'drifters.json'))
    ocean_metadata_file = abspath(joinpath(experiment_dir, 'ocean_data.json'))
    atmos_metadata_file = abspath(joinpath(experiment_dir, 'atmos_data.json'))

    logger.info('\nCreating workflow directory...')

    # Set up the output directory. If the user has not chosen to overwrite
    # the savedir and it exists, provide an error message and exit the
    # program.  Otherwise create a new folder or remove files from an
    # already exisiting folder. 
    setup_savedir(experiment_dir, overwrite_savedir=overwrite_savedir)


    # Assemble model metadata
    assemble_mldp_model_metadata(
        ocean_data_dir,
        output_file=ocean_metadata_file,
        model_type='ocean'
    )

    assemble_mldp_model_metadata(
        atmos_data_dir,
        output_file=atmos_metadata_file,
        model_type='atmos'
    )

    if wave_forcing:
        waves_metadata_file = abspath(joinpath(experiment_dir,
                                               'waves_data.json'))
        assemble_mldp_model_metadata(
            wave_data_dir,
            output_file=waves_metadata_file,
            model_type='waves'
        )

    # Expand bbox
    mldpn_bbox = adjust_bbox(bbox, scale_factor=0.5)
    # Run pre-processing if needed
    if run_preprocessing:
        preprocess_dir = joinpath(experiment_dir, 'preprocessing_output')
        os.makedirs(preprocess_dir)
        bbox_list = mldpn_bbox.split()
        if make_mask:
            # Mask generation is required which can be slow over large domains.
            # Check size of bbox and warn user if it is very large.
            # Too large is 0.2deg in longitude or latitude.
            diff_lon = np.abs(float(bbox_list[0]) - float(bbox_list[2]))
            diff_lat = np.abs(float(bbox_list[1]) - float(bbox_list[3]))
            if diff_lon > 0.2 or diff_lat > 0.2:
                logger.warn("\nWARNING: Requested bbox {} is very large."\
                            " Pre-processing may take a long time to"\
                            " complete.".format(bbox))
        else:
            logger.warn("\nWARNING: Running with make_mask={} may lead to"\
                        " inaccurate results near coastlines.".format(make_mask))
        # Look up files required for pre-processing
        wetfiles = joinpath(preprocess_dir, 'wetfiles')
        assemble_raw_data(
            first_start_date,
            last_start_date + drift_duration,
            ocean_metadata_file,
            wetfiles,
            model_type='ocean'
        )
        metfiles = joinpath(preprocess_dir, 'metfiles')
        assemble_raw_data(
            first_start_date,
            last_start_date + drift_duration,
            atmos_metadata_file,
            metfiles,
            model_type='atmos'
        )

        if wave_forcing:
            wavefiles = joinpath(preprocess_dir, 'wavefiles')
            assemble_raw_data(
                first_start_date,
                last_start_date + drift_duration,
                waves_metadata_file,
                wavefiles,
                model_type='waves'
            )
        else:
            wavefiles = None
        
        mldp_preprocess(
            wetfiles=wetfiles,
            metfiles=metfiles,
            wavefiles=wavefiles,
            preprocess_dir=preprocess_dir,
            bbox=mldpn_bbox,
            depth=drifter_depth,
            res=res,
            make_mask=make_mask,
        )
    else:
        if preprocess_dir is None:
            msg="preprocess_dir is not defined in config file. "\
                 "Please define preprocess_dir or set "\
                 "run_preprocessing=True in the config file."
            raise RuntimeError(msg)
        if not os.path.exists(preprocess_dir):
            msg="Cannot find preprocessed input data because "\
                 "preprocess_dir {} does not exist. "\
                 "Please check preprocess_dir in config file or set "\
                 "run_preprocessing=True.".format(preprocess_dir)
            raise RuntimeError(msg)
        if os.path.exists(preprocess_dir) and os.path.isdir(preprocess_dir):
            if not os.listdir(preprocess_dir):
                msg="Cannot find preprocessed input data. Please set "\
                     "run_preprocessing=True in the config file, "\
                     "or copy the preprocessed data to {}".format(preprocess_dir)
                raise RuntimeError(msg)
            
    # Compile metadata on pre-processed data
    preprocess_metadata_file = abspath(joinpath(experiment_dir,
                                                'preprocess_data.json'))
    assemble_mldp_model_metadata(
        preprocess_dir,
        output_file=preprocess_metadata_file,
        model_type='preprocess'
    )

    assemble_drifter_metadata(
        drifter_data_dir, drifter_metadata_file,
        buoy_id_attr_name=drifter_id_attr,
        meta_variables=drifter_meta_variables)

    verify_drifter_data(
        drifter_metadata_file, first_start_date, last_start_date,
        str(drifter_depth),
        drift_duration)

    verify_ocean_data(
        ocean_metadata_file, first_start_date, last_start_date, drift_duration)     

    # Interpret current_uncertainty
    if current_uncertainty != 0:
        logger.info("Trajectories will be computed with a horizontal random "
                    "walk with rms ocean velocity {} m/s and equivalent diffusivity "
                    "K={} m^2/s".format(current_uncertainty,
                                        0.5*current_uncertainty**2*mldp_dt))
 
    rrule_freq_dict = {
        'yearly': dateutil.rrule.YEARLY, 'monthly': dateutil.rrule.MONTHLY,
        'weekly': dateutil.rrule.WEEKLY, 'daily': dateutil.rrule.DAILY,
        'hourly': dateutil.rrule.HOURLY, 'minutely': dateutil.rrule.MINUTELY,
        'secondly': dateutil.rrule.SECONDLY
    }
    rrule = dateutil.rrule.rrule(
        freq=rrule_freq_dict[start_date_frequency],
        interval=start_date_interval, dtstart=first_start_date,
        until=last_start_date)

    if rrule.count() > 1:
        rrule_increment = rrule[1] - rrule[0]
    else:
        rrule_increment = datetime.timedelta(0, 0)
    logger.info(
        ('\nTrajectories will be calculated with starting dates running from %s '
         'to %s of duration %s with an increment of %s between dates'),
        first_start_date.strftime('%Y-%m-%d %H:%M:%S'),
        last_start_date.strftime('%Y-%m-%d %H:%M:%S'),
        str(drift_duration), str(rrule_increment))

        
    found_valid_drifter = False
    for start_date in rrule:
        logger.info(
            '\nBeginning trajectory calculations for start date %s...',
            start_date.strftime('%Y-%m-%d %H:%M:%S')
        )
        end_date = start_date + drift_duration
        run_name = set_run_name(ocean_model_name, drift_model_name,
                                start_date, drift_duration)
        run_base_dir = joinpath('runs', run_name)
        exp_run_dir = joinpath(experiment_dir, run_base_dir)
        os.makedirs(exp_run_dir)
        cwd = os.getcwd()
        os.chdir(exp_run_dir)

        data_assembly_dir = 'data'
        try:
            drifter_data = assemble_drifter_data(
                drifter_metadata_file, start_date, drift_duration,
                data_assembly_dir)
        except NoDrifterDataForTimePeriod as e:
            logger.info('no drifter data for this date')
            with open('NOTE', 'w') as f:
                f.write('Drift simulation not performed: {}'.format(e))
            run_details['runs'][run_name] = {
                'drift_start_date': start_date.isoformat(),
                'drift_end_date': end_date.isoformat(),
                'num_drifters': 0,
                'drift_calculation_status': 'no_drifters',
                'run_dir': run_base_dir,
                'updated': datetime.datetime.utcnow().isoformat()
            }
            with open(run_details_file, 'w') as f:
                json.dump(run_details, f, indent=1)
            os.chdir(cwd)
            continue

        drifter_positions = set_initial_positions_mldp(
            start_date=start_date,
            drifter_data_dir=drifter_data['drifter_data_dir'],
            bbox=bbox,
            drifter_depth=drifter_depth,
            output_file='drifter_positions.json',
        )

        # Sort positions by buoy ID (which is the key in the positions
        # dictionary).
        initial_positions = []
        for key in sorted(drifter_positions.keys()):
            initial_positions.append(drifter_positions[key])

        # Check if there are no drifters in domain
        if not initial_positions:
            with open('NOTE', 'w') as f:
                f.write('Drift simulation not performed: No drifter in domain')
            run_details['runs'][run_name] = {
                'drift_start_date': start_date.isoformat(),
                'drift_end_date': end_date.isoformat(),
                'num_drifters': 0,
                'drift_calculation_status': 'no_drifters',
                'run_dir': run_base_dir,
                'updated': datetime.datetime.utcnow().isoformat()
            }
            with open(run_details_file, 'w') as f:
                json.dump(run_details, f, indent=1)
            os.chdir(cwd)
            continue

        # Assemble preprocessed data
        meteo_dir = joinpath(exp_run_dir, data_assembly_dir, 'meteo')
        meteo_metadata = assemble_preprocessed_data(
                start_date,
                end_date,
                preprocess_metadata_file,
                meteo_dir
        )
        if len(meteo_metadata) <= 2:
            logger.info(
                "{} does not have data for simulation".format(start_date)
            )
            with open('NOTE', 'w') as f:
                f.write('{} does not contain enough preprocessed data'\
                        ' for entire simulation duration'.format(meteo_dir))
            continue

        # Information about this run.
        run_info = {
            'drift_start_date': start_date.isoformat(),
            'drift_end_date': end_date.isoformat(),
            'meteo_data_files': meteo_metadata,
            'num_drifters': drifter_data['num_drifters'],
            'drifter_data_files': drifter_data['drifter_data_files'],
            'initial_drifter_grid_positions': drifter_positions,
            'drift_calculation_method': drift_model_name,
            'drift_calculation_status': 'in_progress',
            'run_dir': run_base_dir,
            'updated': datetime.datetime.utcnow().isoformat()
        }

        run_details['runs'][run_name] = run_info

        # Write out initial run information *before* calling MLDP. Since the
        # MLDP process may be long running, this allows the user to see some
        # details pertaining to the run while waiting for MLDP to finish.
        # Once MLDP is done, the run information will be updated and written
        # again.
        # NOTE: If this script is ever parallelized, then the next step will
        # need to be adapted to ensure only a single process writes to the
        # output file at any given time.
        with open(run_details_file, 'w') as f:
            json.dump(run_details, f, indent=1)


        # Starting MLDP run
        start_time = datetime.datetime.utcnow()
        namelist = os.path.join(exp_run_dir, 'MLDP.in')

        st_pos = {}
        for d in drifter_positions:
            st_pos[d] = [drifter_positions[d][0], drifter_positions[d][1]]

        meteo_files = glob.glob(joinpath(meteo_dir, '*'))
        meteo_files.sort()
        make_mldp_namelist(
            meteo_files,
            preprocess_dir,
            ocean_model_name,
            exp_run_dir,
            start_date,
            st_pos,
            drift_duration,
            alpha_wind,
            current_uncertainty,
            mldp_dt,
            namelist,
            output_dt=output_dt
        )

        results = run_mldp(
            mldp_exec=MLDP_executable,
            namelist=namelist,
            run_dir=exp_run_dir,
            overwrite_run_dir=False
        )

        convert_drift_predictions(
            results,
            drift_duration,
            alpha_wind,
            exp_run_dir
        )
        status = 0

        finish_time = datetime.datetime.utcnow()
        run_info.update({
            'drift_calculation_status': 'finished' if status == 0 else status,
            'drift_calculation_start_time': start_time.isoformat(),
            'drift_calculation_finish_time': finish_time.isoformat(),
            'run_status': status,
            'updated': datetime.datetime.utcnow().isoformat()
        })
        run_details['runs'][run_name] = run_info

        run_details['updated'] = datetime.datetime.utcnow().isoformat()
        # Update run information file. This happens on *every* loop which means
        # the output file is overwritten in every loop with increasingly more
        # detail, rather than writing it once at the end of all of the runs.
        # This method is inefficient and could be improved, but the suspected
        # small overhead is outweighed by the advantage of having the file
        # contain *some* information part way through a long run.
        with open(run_details_file, 'w') as f:
            json.dump(run_details, f, indent=1)
        os.chdir(cwd)

        found_valid_drifter = True

    now = datetime.datetime.utcnow().isoformat()
    run_details['workflow_finish_time'] = now
    run_details['updated'] = now
    with open(run_details_file, 'w') as f:
        json.dump(run_details, f, indent=1)

    if found_valid_drifter:
        assemble_drift_predictions(
            ocean_data_file=None,
            lon_var=None,
            lat_var=None,
            experiment_dir=experiment_dir,
            bbox=mldpn_bbox)
    else:
        logger.info(' ')
        logger.info('No valid drifters for the entire experiment period. Exiting.')

        
def main():
    from . import cli
    cli.run(drift_predict_mldp)


if __name__ == '__main__':
    main()
