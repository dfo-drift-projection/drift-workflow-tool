"""
Run Drift Evaluation Workflow
=============================
:Author: Clyde Clements
:Created: 2017-10-27
"""

import os
from os.path import join as joinpath

from .get_skill import get_skill
from . import ioutils
from . import utils

logger = utils.logger


def drift_evaluate(*, data_dir, liu_threshold=1):
    """Run drift evaluation workflow.

    Parameters
    ----------
    data_dir : str
        Path to directory containing drifter class-4 type
        netCDF files.
    liu_threshold : float
        The tolerance threshold for the Liu skill score.
        See Liu and Weisberg (2011).
    """
    for dirpath, dirnames, filenames in os.walk(data_dir, followlinks=True):
        for filename in filenames:
            if not filename.endswith('.nc'):
                continue
            ds_filename = joinpath(dirpath, filename)
            obs_track = ioutils.load_drifter_track(ds_filename,
                                                   name_prefix='obs_')
            mod_track = ioutils.load_drifter_track(ds_filename,
                                                   name_prefix='mod_')
            drifter_id = obs_track.buoyid

            skill = get_skill(obs_track, mod_track,
                              liu_threshold=liu_threshold)
            liu_score = skill['liu']
            molcard_score = skill['molcard']
            logger.debug('Drifter %s:', drifter_id)
            if liu_score.size > 1:
                logger.debug('  liu: [%s, %s]',
                             liu_score[1:].min().values,
                             liu_score[1:].max().values)
            else:
                logger.warn('  liu size <= 1')
            if molcard_score.size > 1:
                logger.debug('  molcard: [%s, %s]',
                             molcard_score[1:].min().values,
                             molcard_score[1:].max().values)
            else:
                logger.warn('  molcard size <= 1')
            ds = ioutils.load_dataset(ds_filename)
            for v in skill.data_vars:
                ds[v] = skill[v]
            for a in skill.attrs:
                ds.attrs[a] = skill.attrs[a]
            ds.to_netcdf(ds_filename, mode='w')


def main():
    from . import cli
    cli.run(drift_evaluate)


if __name__ == '__main__':
    main()
