# Written October 20th, 2017 by Stephanne Taylor, based on script from Li Zhai, adapted for python2.7 by Hauke Blanken, Dec 2018
# Rotates u/v to true east/north

import numpy as np
import netCDF4 as nc
import pickle as pickle
import os
from os.path import exists
from scipy import interpolate

from . import utils

logger = utils.logger

def rot_rep_2017_p(pxin, pyin, cd_type, cdtodo, coeffs):
    gsint = coeffs[0]; gcost = coeffs[1]
    gsinu = coeffs[2]; gcosu = coeffs[3]
    gsinv = coeffs[4]; gcosv = coeffs[5]
    gsinf = coeffs[6]; gcosf = coeffs[7]

    if cdtodo == 'en->i':
        if cd_type == 'T':
            prot = pxin * gcost + pyin * gsint
        elif cd_type == 'U':
            prot = pxin * gcosu + pyin * gsinu
        elif cd_type == 'V':
            prot = pxin * gcosv + pyin * gsinv
        elif cd_type == 'F':
            prot = pxin * gcosf + pyin * gsinf
    elif cdtodo == 'en->j':
        if cd_type == 'T':
            prot = pyin * gcost - pxin * gsint
        elif cd_type == 'U':
            prot = pyin * gcosu - pxin * gsinu
        elif cd_type == 'V':
            prot = pyin * gcosv - pxin * gsinv
        elif cd_type == 'F':
            prot = pyin * gcosf - pxin * gsinf
    elif cdtodo == 'ij->e':
        if cd_type == 'T':
            prot = pxin * gcost - pyin * gsint
        elif cd_type == 'U':
            prot = pxin * gcosu - pyin * gsinu
        elif cd_type == 'V':
            prot = pxin * gcosv - pyin * gsinv
        elif cd_type == 'F':
            prot = pxin * gcosf - pyin * gsinf
    elif cdtodo == 'ij->n':
        if cd_type == 'T':
            prot = pyin * gcost + pxin * gsint
        elif cd_type == 'U':
            prot = pyin * gcosu + pxin * gsinu
        elif cd_type == 'V':
            prot = pyin * gcosv + pxin * gsinv
        elif cd_type == 'F':
            prot = pyin * gcosf + pxin * gsinf

    return prot

def opa_angle_2016_p(gridfile, orca_grid=False):
    with nc.Dataset(gridfile,'r') as grid:
        glamt = grid['glamt'][:]
        glamu = grid['glamu'][:]
        glamv = grid['glamv'][:]
        glamf = grid['glamf'][:]

        gphit = grid['gphit'][:]
        gphiu = grid['gphiu'][:]
        gphiv = grid['gphiv'][:]
        gphif = grid['gphif'][:]
    ######################
    # important !!!!!!!!!!!!!!!!
    # to be consistent with old nc library
    ######################
    if glamt.ndim > 2:
        glamt = glamt[0,:,:]
        glamu = glamu[0,:,:]
        glamv = glamv[0,:,:]
        glamf = glamf[0,:,:]

        gphit = gphit[0,:,:]
        gphiu = gphiu[0,:,:]
        gphiv = gphiv[0,:,:]
        gphif = gphif[0,:,:]

    gcost = np.zeros_like(glamt)
    gcosu = np.zeros_like(glamt)
    gcosv = np.zeros_like(glamt)
    gcosf = np.zeros_like(glamt)
    gsint = np.zeros_like(glamt)
    gsinu = np.zeros_like(glamt)
    gsinv = np.zeros_like(glamt)
    gsinf = np.zeros_like(glamt)

    jpj,jpi = np.shape(glamt)

    rpi = np.pi
    rad = np.pi/180.

    for jj in range(1,jpj-1):
        for ji in range(1,jpi):
            # ! north pole direction & modulous (at t-point)
            zlam = glamt[jj,ji]
            zphi = gphit[jj,ji]
            zxnpt = 0. - 2. * np.cos( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )
            zynpt = 0. - 2. * np.sin( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )
            znnpt = zxnpt*zxnpt + zynpt*zynpt

            # ! north pole direction & modulous (at u-point)
            zlam = glamu[jj,ji]
            zphi = gphiu[jj,ji]
            zxnpu = 0. - 2. * np.cos( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )
            zynpu = 0. - 2. * np.sin( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )
            znnpu = zxnpu*zxnpu + zynpu*zynpu

            # ! north pole direction & modulous (at v-point)
            zlam = glamv[jj,ji]
            zphi = gphiv[jj,ji]
            zxnpv = 0. - 2. * np.cos( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )
            zynpv = 0. - 2. * np.sin( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )
            znnpv = zxnpv*zxnpv + zynpv*zynpv

            # ! north pole direction & modulous (at f-point)
            zlam = glamf[jj,ji]
            zphi = gphif[jj,ji]
            zxnpf = 0. - 2. * np.cos( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )
            zynpf = 0. - 2. * np.sin( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )
            znnpf = zxnpf*zxnpf + zynpf*zynpf

            # ! j-direction: v-point segment direction (around t-point)
            zlam = glamv[jj,  ji]
            zphi = gphiv[jj,  ji]
            zlan = glamv[jj-1,ji]
            zphh = gphiv[jj-1,ji]
            zxvvt =  2. * np.cos( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. ) - 2. * np.cos( rad*zlan ) * np.tan( rpi/4. - rad*zphh/2. )
            zyvvt =  2. * np.sin( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. ) - 2. * np.sin( rad*zlan ) * np.tan( rpi/4. - rad*zphh/2. )
            znvvt = np.sqrt( znnpt * ( zxvvt*zxvvt + zyvvt*zyvvt )  )
            znvvt = np.array([znvvt, 1.e-14]).max()

            # ! j-direction: f-point segment direction (around u-point)
            zlam = glamf[jj  ,ji]
            zphi = gphif[jj  ,ji]
            zlan = glamf[jj-1,ji]
            zphh = gphif[jj-1,ji]
            zxffu =  2. * np.cos( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )  -  2. * np.cos( rad*zlan ) * np.tan( rpi/4. - rad*zphh/2. )
            zyffu =  2. * np.sin( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )  -  2. * np.sin( rad*zlan ) * np.tan( rpi/4. - rad*zphh/2. )
            znffu = np.sqrt( znnpu * ( zxffu*zxffu + zyffu*zyffu )  )
            znffu = np.max(np.array([znffu, 1.e-14]))

            # ! i-direction: f-point segment direction (around v-point)
            zlam = glamf[jj,ji  ]
            zphi = gphif[jj,ji  ]
            zlan = glamf[jj,ji-1]
            zphh = gphif[jj,ji-1]
            zxffv =  2. * np.cos( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )  -  2. * np.cos( rad*zlan ) * np.tan( rpi/4. - rad*zphh/2. )
            zyffv =  2. * np.sin( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )  -  2. * np.sin( rad*zlan ) * np.tan( rpi/4. - rad*zphh/2. )
            znffv = np.sqrt( znnpv * ( zxffv*zxffv + zyffv*zyffv )  )
            znffv = np.max(np.array([znffv, 1.e-14]))

            # ! j-direction: u-point segment direction (around f-point)
            zlam = glamu[jj+1,ji]
            zphi = gphiu[jj+1,ji]
            zlan = glamu[jj  ,ji]
            zphh = gphiu[jj  ,ji]
            zxuuf =  2. * np.cos( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )  -  2. * np.cos( rad*zlan ) * np.tan( rpi/4. - rad*zphh/2. )
            zyuuf =  2. * np.sin( rad*zlam ) * np.tan( rpi/4. - rad*zphi/2. )  -  2. * np.sin( rad*zlan ) * np.tan( rpi/4. - rad*zphh/2. )
            znuuf = np.sqrt( znnpf * ( zxuuf*zxuuf + zyuuf*zyuuf )  )
            znuuf = np.max(np.array([znuuf, 1.e-14]))

            # ! cosinus and sinus using scalar and vectorial products
            gsint[jj,ji] = ( zxnpt*zyvvt - zynpt*zxvvt ) / znvvt
            gcost[jj,ji] = ( zxnpt*zxvvt + zynpt*zyvvt ) / znvvt

            gsinu[jj,ji] = ( zxnpu*zyffu - zynpu*zxffu ) / znffu
            gcosu[jj,ji] = ( zxnpu*zxffu + zynpu*zyffu ) / znffu

            gsinf[jj,ji] = ( zxnpf*zyuuf - zynpf*zxuuf ) / znuuf
            gcosf[jj,ji] = ( zxnpf*zxuuf + zynpf*zyuuf ) / znuuf

            # ! (caution, rotation of 90 degres)
            gsinv[jj,ji] = ( zxnpv*zxffv + zynpv*zyffv ) / znffv
            gcosv[jj,ji] =-( zxnpv*zyffv - zynpv*zxffv ) / znffv

    if orca_grid:
        # Treatment of northfold for orca grids
        sign=-1
        gsint = orca_bc(gsint, sign, 'T')
        gcost = orca_bc(gcost, sign, 'T')
        gsinu = orca_bc(gsinu, sign, 'U')
        gcosu = orca_bc(gcosu, sign, 'U')
        gsinv = orca_bc(gsinv, sign, 'V')
        gcosv = orca_bc(gcosv, sign, 'V')
        gsinf = orca_bc(gsinf, sign, 'F')
        gcosf = orca_bc(gcosf, sign, 'F')
    else: 
        # 1:  first row is copied from second row
        gsint[:,0] = gsint[:,1]
        gcost[:,0] = gcost[:,1]
        gsinu[:,0] = gsinu[:,1]
        gcosu[:,0] = gcosu[:,1]
        gsinv[:,0] = gsinv[:,1]
        gcosv[:,0] = gcosv[:,1]
        gsinf[:,0] = gsinf[:,1]
        gcosf[:,0] = gcosf[:,1]

        # 2: last row copied from second last row
        gsint[:,-1] = gsint[:,-2]
        gcost[:,-1] = gcost[:,-2]
        gsinu[:,-1] = gsinu[:,-2]
        gcosu[:,-1] = gcosu[:,-2]
        gsinv[:,-1] = gsinv[:,-2]
        gcosv[:,-1] = gcosv[:,-2]
        gsinf[:,-1] = gsinf[:,-2]
        gcosf[:,-1] = gcosf[:,-2]

        # first column copied from second column
        gsint[0,:] = gsint[1,:]
        gcost[0,:] = gcost[1,:]
        gsinu[0,:] = gsinu[1,:]
        gcosu[0,:] = gcosu[1,:]
        gsinv[0,:] = gsinv[1,:]
        gcosv[0,:] = gcosv[1,:]
        gsinf[0,:] = gsinf[1,:]
        gcosf[0,:] = gcosf[1,:]

        # 4: last column copied from second last column
        gsint[-1,:] = gsint[-2,:]
        gcost[-1,:] = gcost[-2,:]
        gsinu[-1,:] = gsinu[-2,:]
        gcosu[-1,:] = gcosu[-2,:]
        gsinv[-1,:] = gsinv[-2,:]
        gcosv[-1,:] = gcosv[-2,:]
        gsinf[-1,:] = gsinf[-2,:]
        gcosf[-1,:] = gcosf[-2,:]

    return gsint,gcost,gsinu,gcosu,gsinv,gcosv,gsinf,gcosf

    
def orca_bc(var, sign, case, fill_value=1.0e20):
    """Replicate orca north-fold bc for T-pivot
       Also, east/west cyclic"""
    varT = np.copy(var.T)
    Ny, Nx = var.shape
    # East/West cyclic first
    varT[0, :] = varT[Nx-2, :]
    varT[Nx-1, :] = varT[1, :]
    # South closed
    varT[:, 0] = fill_value
    # North fold
    jpiglo, jpjglo = Nx, Ny
    ipr2dj = 0
    ijpj = Ny
    ijpjm1 = Ny-1
    jpi = Nx
    jpj = Ny
    if case == 'T':
        for jl in  range(ipr2dj+1):
            for ji in range(2, jpiglo+1):
                ijt = jpiglo-ji+2
                varT[ji-1, ijpj+jl-1] = sign * varT[ijt-1, ijpj-2-jl-1]
        varT[0, ijpj-1] = sign * varT[2, ijpj-3]
        for ji in range(int(jpiglo/2)+1, jpiglo+1):
            ijt = jpiglo-ji+2
            varT[ji-1, ijpj-2] = sign * varT[ijt-1, ijpj-2]
    elif case =='U':
        for jl in range(ipr2dj+1):
            for ji in range (1, jpiglo):
                iju = jpiglo-ji+1
                varT[ji-1, ijpj+jl-1] = sign * varT[iju-1, ijpj-2-jl -1]
        varT[0       , ijpj-1] = sign * varT[    1   , ijpj-3]
        varT[jpiglo-1, ijpj-1] = sign * varT[jpiglo-2, ijpj-3]
        varT[0       , ijpj-2] = sign * varT[jpiglo-1, ijpj-2]
        for ji in range( int(jpiglo/2), jpiglo):
            iju = jpiglo-ji+1
            varT[ji-1, ijpjm1-1] = sign * varT[iju-1, ijpjm1-1]
    elif case == 'V':
        for jl in range( -1, ipr2dj +1):
            for ji in range( 2, jpiglo +1):
                ijt = jpiglo-ji+2
                varT[ji-1, ijpj+jl-1] = sign* varT[ijt -1, ijpj-3-jl-1]
        varT[0, ijpj-1]   = sign * varT[ 2 ,ijpj-3]
    elif case == 'F':
        for jl in range( -1, ipr2dj+1):
            for ji in range(1, jpiglo):
                iju = jpiglo-ji+1
                varT[ji-1, ijpj+jl-1] = sign * varT[iju-1, ijpj-3-jl-1]
        varT[   0    , ijpj-1] = sign * varT[    1   , ijpj-4]
        varT[jpiglo-1, ijpj-1] = sign * varT[jpiglo-2, ijpj-4]
        varT[jpiglo-1, ijpj-2] = sign * varT[jpiglo-2, ijpj-3]
        varT[   0    , ijpj-2] = sign * varT[    1   , ijpj-2]
    else:
        print("Case {} is invalid".format(case))
        exit
    return varT.T


def unstagger(ugrid, vgrid):
    u = np.add(ugrid[..., :-1], ugrid[..., 1:]) / 2.
    v = np.add(vgrid[..., :-1, :], vgrid[..., 1:, :]) / 2.
    return u[..., 1:, :], v[..., 1:]


def rotate_fields(u_input_filename, v_input_filename,
                  u_output_filename, v_output_filename,
                  rotation_coefficient_file, mesh_file, u_invar,
                  v_invar, timeVar, drifter_depth='1c',
                  grid_type='cgrid',
                  orca_grid=False):
    npairs   = 1
        
    u_outvar = 'x_sea_water_velocity'; v_outvar = 'y_sea_water_velocity'
    if exists(rotation_coefficient_file):
        with open(rotation_coefficient_file,'rb') as f:
            coeffs = pickle.load(f)
    else:
        coeffs = opa_angle_2016_p(mesh_file, orca_grid=orca_grid)
        with open(rotation_coefficient_file,'wb',-1) as f:
            pickle.dump(coeffs,f)

    # Open the file, grab the data, rotate, output

    with nc.Dataset(u_input_filename,'r') as data:
        uu = data[u_invar][:]
        u_missing_value = data[u_invar].missing_value
        time = data[timeVar]
        time_data = time[:]
        time_units = time.units

    with nc.Dataset(v_input_filename,'r') as data: 
        vv = data[v_invar][:]
        v_missing_value = data[v_invar].missing_value

    with nc.Dataset(mesh_file,'r') as grid:
        glamu = grid['glamu'][:][0]
        glamv = grid['glamv'][:][0]
        glamt = grid['glamt'][:][0]

        gphiu = grid['gphiu'][:][0]
        gphiv = grid['gphiv'][:][0]
        gphit = grid['gphit'][:][0]

        gdept_1d = grid['gdept_1d'][:][0]
        tmask = grid['tmask'][:][0]
        umask = grid['umask'][:][0]
        vmask = grid['vmask'][:][0]

    ndimensions = len(uu.shape)
    nt = uu.shape[0]
    # rotate u/v to true north/east
    if grid_type == 'cgrid':
        ruo = rot_rep_2017_p(uu, vv, 'U','ij->e',coeffs)
        rvo = rot_rep_2017_p(uu, vv, 'V','ij->n',coeffs)
    elif grid_type == 'agrid':
        ruo = rot_rep_2017_p(uu, vv, 'T','ij->e',coeffs)
        rvo = rot_rep_2017_p(uu, vv, 'T','ij->n',coeffs)
    elif grid_type == 'user_grid':
        raise ValueError(
            ("Velocity rotation is not supported for grid_type={}. "
             "Review choices for grid_type and rotation_data_file.".format(grid_type))
        )
                
    # Parse drifter_depth option
    if drifter_depth.startswith('L'):
        depth_option = drifter_depth[1:]
        depth_in_layer = True
    else:
        depth_option = drifter_depth
        depth_in_layer = False
    if drifter_depth.endswith('c'):
        depth_option = depth_option[:-1]
    else:
        logger.warn(('Variable drifter_depth %s is not available '
                     'when rotation is needed. Drifters will '
                     'be simulated with constant depth. To avoid this '
                     'warning, use "c" suffix on drifter_depth when '
                     'rotation is required.', drifter_depth))

    # Identify depth layer or interpolate if needed
    if ndimensions == 4:
        if depth_in_layer:
            depth_option = int(depth_option) - 1 # python indexing
            ud = ruo[:,depth_option,:,:]
            ud = np.ma.masked_array(ud,
                                    mask=np.ones(ud.shape)-umask[depth_option,:,:])
            vd = rvo[:,depth_option,:,:]
            vd = np.ma.masked_array(vd,
                                    mask=np.ones(vd.shape)-vmask[depth_option,:,:])
            tmask = tmask[depth_option,:,:]
        else:
            logger.info("Interpolate rotated velocities to %s m",
                        depth_option)
            # Ensure ruo/rvo are masked before interpolating vertically
            ruo = np.ma.masked_array(ruo, mask=np.ones(ruo.shape)-umask)
            rvo = np.ma.masked_array(rvo, mask=np.ones(rvo.shape)-vmask)
            fu = interpolate.interp1d(gdept_1d, ruo, axis=1,
                                      fill_value='extrapolate',
                                      assume_sorted=True)
            ud = fu(float(depth_option))
            fv = interpolate.interp1d(gdept_1d, rvo, axis=1,
                                      fill_value='extrapolate',
                                      assume_sorted=True)
            vd = fv(float(depth_option))
            # find nearest depth level to determine mask
            iz = np.argmin(np.abs(gdept_1d-float(depth_option)))
            ud = np.ma.masked_array(
                ud, mask=np.ones(ud.shape)-umask[iz,:,:])
            vd = np.ma.masked_array(
                vd, mask=np.ones(vd.shape)-vmask[iz,:,:])
            tmask = tmask[iz,:,:]
    else:
        # 2D fields, use depth_option level or first veritcal level
        # for mask
        iz = depth_option if depth_in_layer else 0
        iz = int(iz)
        ud = np.ma.masked_array(ruo,
                                mask=np.ones(ruo.shape)-umask[iz,:,:]) 
        vd = np.ma.masked_array(rvo,
                                mask=np.ones(rvo.shape)-vmask[iz,:,:])
        tmask = tmask[iz, ...]


    # Save the rotated U velocity
    with nc.Dataset(u_output_filename, 'w', format='NETCDF4') as out:
        flat = out.createDimension('flat',len(gphiu.flatten()))
        t = out.createDimension('time_counter', None)

        lat = out.createVariable('latitude','f8',('flat'))
        lat[:] = gphiu.flatten()
        lat.units = 'degrees north'
        lat.standard_name = 'projection_y_coordinate'
        lat.long_name = 'latitude u grid points'

        lon = out.createVariable('longitude','f8',('flat'))
        lon[:] = glamu.flatten()
        lon.units = 'degrees east'
        lon.standard_name = 'projection_x_coordinate'
        lon.long_name = 'longitude of u grid points'

        Time = out.createVariable(timeVar,'f8',('time_counter'))
        Time[:] = time_data.flatten()
        Time.units = time_units
        Time.standard_name = 'time'
        Time.long_name = 'model time stamp'

        U = out.createVariable(u_outvar,'f8',('time_counter','flat'),
                               fill_value=u_missing_value)

        u1 = ud.reshape(nt, ud.shape[-1]*ud.shape[-2])

        U[:] = u1
        U.units = 'm/s'
        U.standard_name = 'x_sea_water_velocity'
        U.long_name = 'ocean current along true east'
        U.missing_value = u_missing_value

    with nc.Dataset(v_output_filename, 'w', format='NETCDF4') as out:
        flat = out.createDimension('flat',len(gphiv.flatten()))
        t = out.createDimension('time_counter', None)

        lat = out.createVariable('latitude','f8',('flat'))
        lat[:] = gphiv.flatten()
        lat.units = 'degrees north'
        lat.standard_name = 'projection_y_coordinate'
        lat.long_name = 'latitude v grid points'

        lon = out.createVariable('longitude','f8',('flat'))
        lon[:] = glamv.flatten()
        lon.units = 'degrees east'
        lon.standard_name = 'projection_x_coordinate'
        lon.long_name = 'longitude of v grid points'

        Time = out.createVariable(timeVar,'f8',('time_counter'))
        Time[:] = time_data.flatten()
        Time.units = time_units
        Time.standard_name = 'time'
        Time.long_name = 'model time stamp'

        V = out.createVariable(v_outvar,'f8',('time_counter','flat'),
                               fill_value=v_missing_value)

        v1 = vd.reshape(nt, vd.shape[-1]*vd.shape[-2])

        V[:] = v1
        V.units = 'm/s'
        V.standard_name = 'y_sea_water_velocity'
        V.long_name = 'ocean current along true north'
        V.missing_value = v_missing_value

    # create tmask
    # The tmask is needed for the land mask and needs to follow
    # naming conventions for Open Drift. This file is used later to create
    # a land_binary_mask.
    # I don't like creating the file with the name hardcoded
    # as tmask_flattened.nc because we need to use that name in
    # daily_drift_map.py and drift_predict.py
    # This section of the code and the creation of the land binary mask be
    # improved
    dirname = os.path.dirname(os.path.dirname(v_output_filename))
    tmask_file = os.path.join(dirname, 'tmask_flattened.nc')
    if not os.path.exists(tmask_file):
        with nc.Dataset(tmask_file, 'w', format='NETCDF4') as out:
            flat = out.createDimension('flat',len(tmask.flatten()))

            lat = out.createVariable('latitude', 'f8', ('flat'))
            lat[:] = gphit.flatten()
            lat.units = 'degrees north'
            lat.standard_name = 'projection_y_coordinate'
            lat.long_name = 'latitude t grid points'

            lon = out.createVariable('longitude','f8',('flat'))
            lon[:] = glamt.flatten()
            lon.units = 'degrees east'
            lon.standard_name = 'projection_x_coordinate'
            lon.long_name = 'longitude of t grid points'

            Tmask = out.createVariable('tmask','f8',('flat'))
            Tmask[:] = tmask.flatten()
            Tmask.long_name = 'mask on t grid'
