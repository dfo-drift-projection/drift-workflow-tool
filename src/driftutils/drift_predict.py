"""
Run Drift Prediction Workflow
=============================
:Author: Clyde Clements
:Contributors: Nancy Soontiens
:Created: 2017-08-15
"""

import datetime
import os
from os.path import abspath, isabs, join as joinpath
import glob
import json
import dateutil.rrule

from driftutils.add_traj_data_date import add_traj_data_date_to_datafile
from driftutils.add_traj_label import add_traj_label_to_datafile
from driftutils.assemble_atmos_wave_data import assemble_atmos_wave_data
from driftutils.assemble_atmos_wave_metadata import assemble_atmos_wave_metadata
from driftutils.assemble_drift_predictions import assemble_drift_predictions
from driftutils.assemble_drifter_data import (assemble_drifter_data,
                                              NoDrifterDataForTimePeriod)
from driftutils.assemble_drifter_metadata import assemble_drifter_metadata
from driftutils.assemble_ocean_data import assemble_ocean_data
from driftutils.assemble_ocean_metadata import assemble_ocean_metadata
from driftutils.make_ariane_namelist import make_ariane_namelist
from driftutils.run_ariane import run_ariane
from driftutils.set_initial_positions import set_initial_positions
from driftutils.utils import (defaults, logger, set_run_name, setup_savedir)
from driftutils.verify_drifter_data import verify_drifter_data
from driftutils.verify_mesh_mask import verify_mesh_mask
from driftutils.verify_ocean_data import verify_ocean_data


def drift_predict(
    *,
    experiment_dir,
    drifter_data_dir,
    ocean_data_dir,
    ocean_model_name,
    first_start_date,
    last_start_date,
    drift_duration,
    ocean_mesh_file='None',
    atmos_data_dir='None',
    atmos_model_name='None',
    wave_data_dir='None',
    wave_model_name='None',
    drifter_depth='1c',
    drift_model_name='OpenDrift',
    ariane_config_file='ariane.yaml',
    ariane_exec='ariane',
    rotation_data_file='None',
    grid_type='cgrid',
    orca_grid=False,
    land_mask_type='GSHHS',
    alpha_wind=0.,
    opendrift_dt=60.,
    current_uncertainty=0,
    lon_var='nav_lon',
    lat_var='nav_lat',
    ulon_var='glamu',
    ulat_var='gphiu',
    vlon_var='glamv',
    vlat_var='gphiv',
    dep_var='gdepw_1d',
    tmask_var='tmask',
    interp_method='ariane',
    drifter_id_attr='buoyid',
    drifter_meta_variables=None,
    start_date_frequency='daily', start_date_interval=1,
    xwatervel=defaults['xwatervel']['nc_name'],
    ywatervel=defaults['ywatervel']['nc_name'],
    zwatervel=None,
    temperature=None,
    salinity=None,
    density=None,
    model_time_ocean='time_centered',
    xwindvel=defaults['xwindvel']['nc_name'],
    ywindvel=defaults['ywindvel']['nc_name'],
    model_time_atmos='time_counter',
    lon_var_atmos='nav_lon',
    lat_var_atmos='nav_lat',
    xwavevel=defaults['xwavevel']['nc_name'],
    ywavevel=defaults['ywavevel']['nc_name'],
    time_var_wave='time',
    lon_var_wave='longitude',
    lat_var_wave='latitude',
    overwrite_savedir=False,
    output_dt=3600,
):

    """Run drift prediction workflow.

    Parameters
    ----------
    experiment_dir : str
        Name of directory in which to run the experiment. This is where the
        output files will be generated.
    drifter_data_dir : str
        Path to directory containing drifter data files.
    ocean_data_dir : str
        Path to directory containing ocean-model data files.
    ocean_mesh_file : str
        Path to mesh file for ocean model. 'None' if no mesh.
    ocean_model_name : str
        Name of ocean model (e.g. GIOPS or Salish Sea).
    first_start_date : datetime.datetime
        Start date for first drift simulation.
    last_start_date : datetime.datetime
        Start date for last drift simulation.
    drift_duration : datetime.timedelta
        Duration of drift.
    atmos_data_dir : str
        Path to directory containing atmos-model data files. Can be 'None' if
        no atmospheric forcing is desired.
    atmos_model_name : str
        Name of atmos model (e.g. HRDPS). Can be 'None' if no atmospheric
        forcing is desired.
    wave_data_dir : str
        Path to directory containing wave-model data files. Can be 'None' if
        no wave forcing is desired.
    wave_model_name : str
        Name of wave model. Can be 'None' if no wave forcing is desired.
    drifter_depth : str
        Specifies the depth for drift trajectory simulations. If the string
        begins with 'L', it indicates the corresponding depth layer for the
        W (vertical) velocity gridpoints; otherwise, it indicates the depth in
        meters. If the string ends with 'c', it indicates a constant-layer
        (or fixed depth) trajectory calculation; otherwise, the particle is
        free to move in the vertical direction.
    drift_model_name : str
        Name of drift model (one of 'Ariane' or 'OpenDrift').
    ariane_config_file : str
        Path to configuration file specifying further parameters for Ariane.
    ariane_exec : str
        Path to Ariane executable.
    rotation_data_file : str
        Path to data file containing coefficients for rotating velocity
        components on desired grid to true east/north. This file will be
        created if it does not exists, i.e. if a grid is being run for
        the first time. If set to  'None' no rotation is performed.
    grid_type : str
        Indicates the type of ocean model grid. Options are 'cgrid',
        'agrid', or 'user_grid'.
    orca_grid : boolean
        Indicates an orca grid which requires specials treatment for
        rotation of velocity fields for OpenDrift. True means the grid is an
        orca grid.
    land_mask_type : str
        Option for how OpenDrift determines land/ocean points. Choices are:
        'ocean_model' - a land mask is created from the ocean model data
        'GSHHS' - a land mask is created from a GSHHS coastline dataset
        (Opendrift's default)
    alpha_wind : float
        Percentage of wind velocity to be applied to drifter.
    opendrift_dt : float
        Integration timestep for OpenDrift in seconds.
    current_uncertainty : float
        Root-mean-square value for current uncertainty in m/s which is used
        to add a random walk to particle motion.
        Relates to horizontal diffusivity, K as
        K = 0.5*current_uncertainty**2*opendrift_dt
    lon_var : str
        Name of longitude variable in mesh file.
    lat_var : str
        Name of latitude variable in mesh file.
    ulon_var : str
        Name of U longitude variable in mesh file.
    ulat_var : str
        Name of U latitude variable in mesh file.
    vlon_var : str
        Name of V longitude variable in mesh file.
    vlat_var : str
        Name of V latitude variable in mesh file.
    dep_var : str
        Name of depth (1D) variable. Must be w depth for ariane
    tmask_var : str
        Name of variable containing land/ocean mask for T grid points in mesh
        file.
    interp_method : str
        Interpolation method to use to find grid point closest to drifter
        location.
    drifter_id_attr : str
        Name of attribute in drifter data files containing buoy id.
    drifter_meta_variables : str
        Names of variables in drifter data files that contain metadata.
    start_date_frequency : str
        Identifies the type of recurrence rule for start dates. The default
        value is "daily", to specify repeating events based on an interval of
        a day or more.
    start_date_interval : int
        A positive integer representing how often the recurrence rule for start
        dates repeats. The default value is 1, meaning every day for a daily
        rule. A value of 2 would mean every two days for a daily rule and so
        on.
    xwatervel : str
        Name of variable containing X water velocity
    ywatervel : str
        Name of variable containing Y water velocity
    zwatervel : str
        Name of variable containing Z water velocity
    temperature : str
        Name of variable containing temperature
    salinity : str
        Name of variable containing salinity
    density : str
        Name of variable containing density
    model_time_ocean : str
        Name of variable containing timestamp of ocean model output
    xwindvel : str
        Name of variable containing eastward wind
    ywindvel : str
        Name of variable containing northward wind
    lon_var_atmos : str
        Name of longitude variable in atmos model.
    lat_var_atmos : str
        Name of latitude variable in atmos model.
    model_time_atmos: str
        Name of variable containing timestamp of atmos model output
    xwavevel : str
        Name of variable containing eastward wind
    ywavevel : str
        Name of variable containing northward wind
    lon_var_wave : str
        Name of longitude variable in wave model.
    lat_var_wave : str
        Name of latitude variable in wave model.
    time_var_wave: str
        Name of variable containing timestamp of wave model output
    overwrite_savedir : boolean
        boolean value specifying whether to overwrite the user defined
        savedir if it already exists.
    output_dt : float
        Output frequency of simluation in seconds.
    """

    # Track details about each run such as run directory and the workflow start
    # and finish times.
    run_details = {
        'drift_model': drift_model_name,
        'ocean_model': ocean_model_name,
        'atmos_model': atmos_model_name,
        'wave_model': wave_model_name,
        'alpha_wind': alpha_wind,
        'runs': {},
        'workflow_start_time': datetime.datetime.utcnow().isoformat()
    }
    run_details_file = abspath(joinpath(experiment_dir, 'runs.json'))

    drifter_metadata_file = abspath(joinpath(experiment_dir, 'drifters.json'))
    ocean_metadata_file = abspath(joinpath(experiment_dir, 'ocean_data.json'))
    if not isabs(ariane_config_file):
        ariane_config_file = abspath(ariane_config_file)

    logger.info('\nCreating workflow directory...')

    # Set up the output directory. If the user has not chosen to overwrite
    # the savedir and it exists, provide an error message and exit the
    # program.  Otherwise create a new folder or remove files from an
    # already exisiting folder.
    setup_savedir(experiment_dir, overwrite_savedir=overwrite_savedir)

    assemble_drifter_metadata(
        drifter_data_dir, drifter_metadata_file,
        buoy_id_attr_name=drifter_id_attr,
        meta_variables=drifter_meta_variables)

    assemble_ocean_metadata(
        ocean_data_dir, ocean_mesh_file, ocean_metadata_file,
        xwatervel=xwatervel, ywatervel=ywatervel,
        zwatervel=zwatervel, temperature=temperature, salinity=salinity,
        density=density, longitude=lon_var, latitude=lat_var, depth=dep_var,
        time_var=model_time_ocean)

    verify_drifter_data(drifter_metadata_file, first_start_date,
                        last_start_date, drifter_depth, drift_duration)

    verify_ocean_data(
        ocean_metadata_file, first_start_date, last_start_date, drift_duration)

    if ocean_mesh_file != 'None':
        verify_mesh_mask(ocean_mesh_file)

    # Check that the atmospheric settings make sense
    atmos_forcing = True
    if atmos_data_dir == 'None':
        atmos_forcing = False
        atmos_metadata_file = None

    if not atmos_forcing and (alpha_wind != 0):
        logger.warn(
            ("Atmospheric forcing data directory set to 'None' but "
             "alpha_wind=%s. \n"
             "The effect of winds will not be included in the simulation.\n"
             "To avoid this warning, set alpha_wind=0 if atmospheric data is "
             "not provided by atmos_data_dir."), alpha_wind)
    if atmos_forcing and (alpha_wind == 0):
        logger.warn(
            ("Atmospheric forcing directory is not 'None' but "
             "alpha_win=%s.\n"
             "The effect of winds will not be included in the simualation.\n"
             "To avoid this warning, set atmos_data_dir='None' and "
             "alpha_wind=0 if no windage is desired."), alpha_wind)

    if atmos_forcing:
        atmos_metadata_file = abspath(joinpath(experiment_dir,
                                               'atmos_data.json'))
        assemble_atmos_wave_metadata(
            atmos_data_dir,
            atmos_metadata_file,
            model_type='atmos',
            xvel=xwindvel,
            yvel=ywindvel,
            longitude=lon_var_atmos,
            latitude=lat_var_atmos,
            time_var=model_time_atmos
        )
        # Should probably add verify_atmos_data here.

    wave_forcing = True
    if wave_data_dir == 'None':
        wave_forcing = False
        wave_metadata_file = None

    if (wave_data_dir == 'None') and (wave_model_name != 'None'):
        logger.warn(
            ("wave_model_name=%s but no wave data provided: "
             "wave_data_dir=%s \n. Setting wave_model_name='None'"),
            wave_model_name, wave_data_dir)
        wave_model_name = 'None'

    if wave_forcing:
        wave_metadata_file = abspath(joinpath(experiment_dir,
                                              'wave_data.json'))
        assemble_atmos_wave_metadata(wave_data_dir,
                                     wave_metadata_file,
                                     model_type='wave',
                                     xvel=xwavevel,
                                     yvel=ywavevel,
                                     longitude=lon_var_wave,
                                     latitude=lat_var_wave,
                                     time_var=time_var_wave)

    # Interpret current_uncertainty
    if current_uncertainty != 0:
        logstr = ("Trajectories will be computed with a horizontal random "
                  "walk with rms ocean velocity {} m/s and equivalent "
                  "diffusivity K={} m^2/s")
        logger.info(logstr.format(current_uncertainty,
                                  0.5*current_uncertainty**2*opendrift_dt))

    rrule_freq_dict = {
        'yearly': dateutil.rrule.YEARLY, 'monthly': dateutil.rrule.MONTHLY,
        'weekly': dateutil.rrule.WEEKLY, 'daily': dateutil.rrule.DAILY,
        'hourly': dateutil.rrule.HOURLY, 'minutely': dateutil.rrule.MINUTELY,
        'secondly': dateutil.rrule.SECONDLY
    }
    rrule = dateutil.rrule.rrule(
        freq=rrule_freq_dict[start_date_frequency],
        interval=start_date_interval, dtstart=first_start_date,
        until=last_start_date)

    if rrule.count() > 1:
        rrule_increment = rrule[1] - rrule[0]
    else:
        rrule_increment = datetime.timedelta(0, 0)
    logger.info(
        ('\nTrajectories will be calculated with starting dates running from '
         '%s to %s of duration %s with an increment of %s between dates'),
        first_start_date.strftime('%Y-%m-%d %H:%M:%S'),
        last_start_date.strftime('%Y-%m-%d %H:%M:%S'),
        str(drift_duration), str(rrule_increment))

    if rotation_data_file == 'None':
        rotation_data_file = None
    if ocean_mesh_file == 'None':
        has_mesh = False
    else:
        has_mesh = True

    # Check that rotation and grid settings make sense
    if (rotation_data_file is not None) and (grid_type == 'user_grid'):
        raise ValueError(
            ("Velocity rotation is not supported for grid_type={}. "
             "Review choices for grid_type and "
             "rotation_data_file.".format(grid_type))
        )

    if (rotation_data_file is not None) and (not has_mesh):
        raise ValueError(
            ("Velocity rotation requires an ocean_mesh_file. "
             "Review choices for ocean_mesh_file and rotation_data_file.")
        )

    found_valid_drifter = False
    for start_date in rrule:
        logger.info(
            '\nBeginning trajectory calculations for start date %s...',
            start_date.strftime('%Y-%m-%d %H:%M:%S')
        )
        end_date = start_date + drift_duration
        run_name = set_run_name(ocean_model_name, drift_model_name,
                                start_date, drift_duration)
        run_base_dir = joinpath('runs', run_name)
        exp_run_dir = joinpath(experiment_dir, run_base_dir)
        os.makedirs(exp_run_dir)
        cwd = os.getcwd()
        os.chdir(exp_run_dir)

        data_assembly_dir = 'data'
        try:
            drifter_data = assemble_drifter_data(
                drifter_metadata_file, start_date, drift_duration,
                data_assembly_dir)
        except NoDrifterDataForTimePeriod as e:
            logger.info('no drifter data for this date')
            with open('NOTE', 'w') as f:
                f.write('Drift simulation not performed: {}'.format(e))
            run_details['runs'][run_name] = {
                'drift_start_date': start_date.isoformat(),
                'drift_end_date': end_date.isoformat(),
                'num_drifters': 0,
                'drift_calculation_status': 'no_drifters',
                'run_dir': run_base_dir,
                'updated': datetime.datetime.utcnow().isoformat()
            }
            with open(run_details_file, 'w') as f:
                json.dump(run_details, f, indent=1)
            os.chdir(cwd)
            continue

        ocean_data = assemble_ocean_data(
            ocean_metadata_file, start_date, drift_duration,
            data_assembly_dir,
            model_time=model_time_ocean)

        if has_mesh:
            ocean_data_file = ocean_data['ocean_mesh_file']
        else:
            ocean_data_files = glob.glob(
                os.path.join(ocean_data['ocean_data_dir'], '*.nc'))
            ocean_data_files.sort()
            ocean_data_file = ocean_data_files[0]

        if atmos_forcing:
            atmos_data = assemble_atmos_wave_data(
                atmos_metadata_file,
                start_date,
                drift_duration,
                data_assembly_dir,
                model_type='atmos',
                xvel=xwindvel,
                yvel=ywindvel,
                model_time=model_time_atmos
            )

        if wave_forcing:
            wave_data = assemble_atmos_wave_data(wave_metadata_file,
                                                 start_date,
                                                 drift_duration,
                                                 data_assembly_dir,
                                                 model_type='wave',
                                                 xvel=xwavevel,
                                                 yvel=ywavevel,
                                                 model_time=time_var_wave)

        drifter_positions = set_initial_positions(
            start_date=start_date,
            drifter_data_dir=drifter_data['drifter_data_dir'],
            ocean_data_file=ocean_data_file,
            ocean_metadata_file=ocean_metadata_file,
            run_ocean_data_dates=ocean_data['run_ocean_data_dates'],
            drift_model_name=drift_model_name,
            drifter_depth=drifter_depth,
            lon_var=lon_var, lat_var=lat_var,
            ulon_var=ulon_var, ulat_var=ulat_var,
            vlon_var=vlon_var, vlat_var=vlat_var,
            dep_var=dep_var, tmask_var=tmask_var,
            interp_method=interp_method,
            output_file='drifter_positions.json',
            has_mesh=has_mesh)

        # Sort positions by buoy ID (which is the key in the positions
        # dictionary).
        initial_positions = []
        for key in sorted(drifter_positions.keys()):
            initial_positions.append(drifter_positions[key])

        # Check if there are no drifters in domain
        if not initial_positions:
            with open('NOTE', 'w') as f:
                f.write('Drift simulation not performed: No drifter in domain')
            run_details['runs'][run_name] = {
                'drift_start_date': start_date.isoformat(),
                'drift_end_date': end_date.isoformat(),
                'num_drifters': 0,
                'drift_calculation_status': 'no_drifters',
                'run_dir': run_base_dir,
                'updated': datetime.datetime.utcnow().isoformat()
            }
            with open(run_details_file, 'w') as f:
                json.dump(run_details, f, indent=1)
            os.chdir(cwd)
            continue

        # Information about this run.
        run_info = {
            'drift_start_date': start_date.isoformat(),
            'drift_end_date': end_date.isoformat(),
            'ocean_mesh_file': ocean_data['ocean_mesh_file'],
            'ocean_data_files': ocean_data['ocean_data_files'],
            'num_drifters': drifter_data['num_drifters'],
            'drifter_data_files': drifter_data['drifter_data_files'],
            'initial_drifter_grid_positions': drifter_positions,
            'drift_calculation_method': drift_model_name,
            'drift_calculation_status': 'in_progress',
            'run_dir': run_base_dir,
            'updated': datetime.datetime.utcnow().isoformat()
        }
        if atmos_forcing:
            run_info['atmos_data_files'] = atmos_data['atmos_data_files']

        if wave_forcing:
            run_info['wave_data_files'] = wave_data['wave_data_files']

        run_details['runs'][run_name] = run_info

        # Write out initial run information *before* calling Ariane. Since the
        # Ariane process may be long running, this allows the user to see some
        # details pertaining to the run while waiting for Ariane to finish.
        # Once Ariane is done, the run information will be updated and written
        # again.
        # NOTE: If this script is ever parallelized, then the next step will
        # need to be adapted to ensure only a single process writes to the
        # output file at any given time.
        with open(run_details_file, 'w') as f:
            json.dump(run_details, f, indent=1)

        start_time = datetime.datetime.utcnow()

        ################################################################
        # if using Ariane
        ################################################################
        if drift_model_name == 'Ariane':
            if atmos_forcing:
                raise ValueError(
                    ("Atmospheric forcing with Ariane is not available. "
                     "Please set atmos_data_dir='None' with an Ariane "
                     "simulation."
                     ))
            if wave_forcing:
                raise ValueError(
                    ("Wave forcing with Ariane is not available. "
                     "Please set wave_data_dir='None' with an Ariane "
                     "simulation."
                     ))
            ariane_namelist_file = 'namelist'
            make_ariane_namelist(
                ariane_config_file, os.getcwd(), ocean_data['ocean_data_dir'],
                ocean_data['run_ocean_data_dates'],
                ocean_data['ocean_data_variables'],
                ocean_data['ocean_mesh_file'], drifter_data['num_drifters'],
                drift_duration, ariane_namelist_file, output_dt=output_dt)
            status = run_ariane(os.getcwd(), ariane_namelist_file,
                                initial_positions, ariane_exec,
                                overwrite_run_dir=True)

            ariane_traj_file = 'ariane_trajectories_qualitative.nc'
            if not os.path.exists(ariane_traj_file):
                continue
            add_traj_data_date_to_datafile(ariane_traj_file, start_date)
            labels = sorted(drifter_positions.keys())
            add_traj_label_to_datafile(ariane_traj_file, labels,
                                       drift_model_name=drift_model_name)

        ################################################################
        # If using OpenDrift
        ################################################################
        elif drift_model_name == 'OpenDrift':
            os.environ['TMPDIR'] = os.path.abspath(experiment_dir)
            from driftutils.run_opendrift import run_opendrift

            status = run_opendrift(
                run_dir=exp_run_dir,
                data_assembly_dir=joinpath(exp_run_dir, data_assembly_dir),
                drifter_positions_file=joinpath(exp_run_dir,
                                                'drifter_positions.json'),
                start_date=start_date,
                drift_duration=drift_duration,
                ocean_mesh_file=ocean_mesh_file,
                ocean_metadata_file=ocean_metadata_file,
                xwatervel=xwatervel,
                ywatervel=ywatervel,
                zwatervel=zwatervel,
                model_time_ocean=model_time_ocean,
                lon_var_ocean=lon_var,
                lat_var_ocean=lat_var,
                alpha_wind=alpha_wind,
                atmos_forcing=atmos_forcing,
                atmos_metadata_file=atmos_metadata_file,
                xwindvel=xwindvel,
                ywindvel=ywindvel,
                model_time_atmos=model_time_atmos,
                lon_var_atmos=lon_var_atmos,
                lat_var_atmos=lat_var_atmos,
                rotation_data_file=rotation_data_file,
                wave_forcing=wave_forcing,
                wave_metadata_file=wave_metadata_file,
                xwavevel=xwavevel,
                ywavevel=ywavevel,
                model_time_wave=time_var_wave,
                lon_var_wave=lon_var_wave,
                lat_var_wave=lat_var_wave,
                opendrift_dt=opendrift_dt,
                current_uncertainty=current_uncertainty,
                land_mask_type=land_mask_type,
                grid_type=grid_type,
                orca_grid=orca_grid,
                output_dt=output_dt)

        ################################################################
        # back to common code
        ################################################################

        finish_time = datetime.datetime.utcnow()
        run_info.update({
            'drift_calculation_status': 'finished' if status == 0 else status,
            'drift_calculation_start_time': start_time.isoformat(),
            'drift_calculation_finish_time': finish_time.isoformat(),
            'run_status': status,
            'updated': datetime.datetime.utcnow().isoformat()
        })
        run_details['runs'][run_name] = run_info

        run_details['updated'] = datetime.datetime.utcnow().isoformat()
        # Update run information file. This happens on *every* loop which means
        # the output file is overwritten in every loop with increasingly more
        # detail, rather than writing it once at the end of all of the runs.
        # This method is inefficient and could be improved, but the suspected
        # small overhead is outweighed by the advantage of having the file
        # contain *some* information part way through a long run.
        with open(run_details_file, 'w') as f:
            json.dump(run_details, f, indent=1)
        os.chdir(cwd)

        found_valid_drifter = True

    now = datetime.datetime.utcnow().isoformat()
    run_details['workflow_finish_time'] = now
    run_details['updated'] = now
    with open(run_details_file, 'w') as f:
        json.dump(run_details, f, indent=1)

    if found_valid_drifter:
        assemble_drift_predictions(
            ocean_data_file=ocean_data_file,
            lon_var=lon_var,
            lat_var=lat_var,
            experiment_dir=experiment_dir)
    else:
        logger.info('\nNo valid drifters for the entire experiment period. '
                    + 'Exiting.')


def main():
    from . import cli
    cli.run(drift_predict)


if __name__ == '__main__':
    main()
