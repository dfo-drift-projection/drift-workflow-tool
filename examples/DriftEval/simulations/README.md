# DriftEval Simulations and basic analysis

A DriftEval simulation creates modelled drifter trajectories using starting positons derived from observed drifter trajectories. These simulated results are then compared to the observed drifter tracks to evaluate the accuracy of the modelled drifters using a common set of verification metrics. 

The examples/DriftEval/simulations/ directory includes sample configuation files and runscripts that can be used for producing DriftEval simulations with OpenDrift, Ariane and MLDP.

# Running DriftEval

The easiest way to launch a DriftEval simulation is by creating a run script. This run script will read experiment settings from a yaml configuration file and then call three methods:

1. `drift_predict` - computes modelled drift trajectories based on a set of observational drifter data.
2. `drift_evaluate` -  the output from `drift_predict` is used to calculate skill scores for each modelled drifter. Output files containing track information for the observed and modelled tracks as well as the associated skill scores are created.
3. `combine_track_segments` -  the output files from `drift_evaluate` are combined together to create class4-like per drifter output files.

For a description of the arguments of these methods type: `drift_predict --help`, `drift_evaluate --help` and `combine_track_segments --help`. 

## DriftEval simulations using OpenDrift or Ariane

A sample script for running a simulation with OpenDrift or Ariane can be executed by running:

`run_drifteval-sims.sh -c <required yaml configuation file> -o <experiment output directory>`  

This script requires the addition of a yaml configuration file, which contains information such as the drift methodology, location of the ocean model data and observed drifter data, and other experimental parameters like the model drifter release frequency and duration. Sample config files have been included in the examples/DriftEval/simulations/ directory, which cover a number of common use cases. Please see these files for a further description of the parameters that are required:

- `config_de-sims_nemo_cgrid_ariane.yaml` - sample config file to use for an Ariane simuation with NEMO c-grid data
- `config_de-sims_nemo_cgrid_opendrift.yaml` - sample config file to use for an OpenDrift simulation with NEMO c-grid data
- `config_de-sims_user_grid_opendrift.yaml` - sample config file to use for an OpenDrift simulation with model data on the user grid.
- `config_de-sims_nemo_cgrid_opendrift_waves.yaml` - sample config file to use for an OpenDrift simulation with NEMO c-grid data and wave model forcing


## DriftEval simulations using MLDP

Likewise, a sample script for running a simulation with MLDP can be executed by running:

`run_drifteval-sims_mldp.sh -c <required yaml configuration file> -o <output directory>`

In addition, sample configuration files have been provided as examples for running simulations using MLDP:

- `config_de-sims_mldp.yaml` - sample config file for use when preprocessing of the ocean and atmospheric RPN files is required
- `config_de-sims_mldp_no_preprocess.yaml` - sample config file to use in the case where preprocessing of the ocean and atmospheric RPN files has already been completed elsewhere. In this case, it is assumed that the user will provide a path to previously processed merged input files.
- `config_de-sims_mldp_waves.yaml` - sample config file to use in the case where waves data is included.