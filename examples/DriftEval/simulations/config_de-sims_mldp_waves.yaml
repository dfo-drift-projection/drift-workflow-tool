########################################################################
# Sample config file to use when running a MLDP DriftEval experiment 
# using CIOPS-E ocean outputs, HRDPS atmosphere outputs, and GDWPS wave 
# forcing. All model outputs must be in RPN format. It serves as an 
# example of how to create similar similations with MLDP and RPN ocean, 
# atmosphere and wave outputs. In this example, the ocean/atmosphere/wave 
# outputs will be run through a pre-processing phase to prepare the data 
# for MLDP.
#
# Each entry below represents an argument for the main DriftEval program.
# For a description of each argument use "drift_predict --help"
# Hint: you will need to have the drift-tool installed in your python
# environment to execute the help command. Or run in the repos
# src/driftutils directory.
#
# In this example, virtual drifters are compared with observed drifter 
# tracks. Virtual drifters are released at the ocean surface every 6 hours 
# from 2023-06-24 to 2023-06-25. The observed trajectory is used to determine 
# the initial release position of each virtual drifter. A subset of the 
# CIOPS-E domain is considered and defined by bbox.
#
# This config file should be called using 
# run_drifteval-sims_mldp.sh -c $CONFIG -m $MINICONDA_PATH -o $EXPERIMENT_DIR
# where:
#  - $CONFIG is this file
#  - $MINICONDA_PATH is the optional path to your miniconda install 
#    (otherwise the default install on sdfo000 is used)
#  - $EXPERIMENT_DIR path to the main experiment output directory
########################################################################

# define a directory in which to store output. The full path to this
# directory must either be defined here or passed as an argument (-o) to
# run_drifteval_mldpn.sh, for example:
# experiment-dir: "/your/output/path/can/optionally/go/here/"

# Pre-processing parameters
# choose to run the MLDP preprocessing steps by setting
# run-preprocessing True or False
# If run-preprocessing is False, it is expected that preprocessed wet/met
# RPN data will already exist in preprocess-dir
run-preprocessing: True
preprocess-dir: None
# If running preprocessing, you can optionally define the grid resolution
# and whether or not the mask file should be generated::
make-mask: True
res: 100

# Define paths to original RPN data
ocean-data-dir: /home/sdfo000/sitestore7/opp_drift_fa3/share_drift/sample_waves_experiment_input/mldp/currents/
atmos-data-dir: /home/sdfo000/sitestore7/opp_drift_fa3/share_drift/sample_waves_experiment_input/mldp/winds/
wave-data-dir: /home/sdfo000/sitestore7/opp_drift_fa3/share_drift/sample_waves_experiment_input/mldp/waves/

# experiment specifics
first-start-date: 2023-06-24
last-start-date: 2023-06-25

drift-duration: 12
bbox: "-46.0 50.0 -45.0 51"
start-date-frequency:  "hourly"
start-date-interval: 6 
ocean-model-name:  "CIOPS-E"
alpha-wind: 0

# drifter settings
drifter-data-dir: /home/sdfo000/sitestore7/opp_drift_fa3/share_drift/sample_waves_experiment_input/mldp/drifter/
drifter-id-attr:  buoyid

# Virtual drifter settings
drifter-depth:  0

# Run settings
drift-model-name: MLDP
atmos-model-name: HRDPS
wave-model-name: GDWPS
