# Drift-tool analysis workflow

The scripts in examples/DriftEval/analysis/ provide a sample workflow for plotting drift-tool output. These scripts can be used to analyze either a single experimental output or to create a comparison between multiple drift-tool outputs depending on the information provided by the user in separate configuration files. The analysis creates plots for the full dataset by default, but it is also possible to analyze the data over sub-regions by including polygon definitions that describe the sub-domains in the yaml config file.

The analysis is performed in a similar manner to running the simulations portion of a DriftEval experiment. First, the drift tool method responsible for generating the plots (`plotting_workflow.py`) is called. This is done by executing a run script at the command line with an associated config file as a command line argument. The runscript will be the same regardless of whether the user chooses to run an analysis for a single experiment, for a comparison set or for an analysis that is performed across multiple sub-regions. The choice of config file parameters will differ however based on these various requirements. Also, additional processing is required prior to running the analysis in the case of a comparison between experiments.

A sample run script that can be used for creating an analysis of drift-tool output has been included in the examples/DriftEval/analysis directory. It can be executed from the command line and requires an argument specifying a yaml configuration file: 

`examples/DriftEval/analysis/run_drift-tool_analysis.sh –c <user config file>`

yaml configuration files are described in more detail below and the sample files provided in examples/DriftEval/analysis/ can be explored for more details on specific parameters. The wiki pages [Creating a configuation file](https://gitlab.com/dfo-drift-projection/drift-workflow-tool/-/wikis/creating-a-configuration-file) and [yaml config file parameters formats](https://gitlab.com/dfo-drift-projection/drift-workflow-tool/-/wikis/yaml-config-file-parameter-formats) contain very detailed information about creating config files and are meant to act as a reference guide for developers as opposed to a guide for new users. 
 
## Running a single experiment analysis

A sample yaml configuation file to use in the case of the analysis for a single experiment is provided here:

`examples/DriftEval/analysis/config_de-ana_user_single-output_plotting.yaml`

This sample configuation file can be used as the input for `run_drift-tool_analysis.sh` to provide an example of an analysis using a single experiment.

## Comparing multiple datasets

### Creating a comparison set of cropped experimental outputs 

As stated above, if a comparison between multiple experiments is required, the user must first create a common set of data between the experiments before plotting the results. After creating multiple individual experiment outputs by running multiple drift tool experiments, users must crop the comparison output to a common temporal and spatial dataset using the following script:

`examples/DriftEval/analysis/run_comparison_cropping.sh –c <yaml cropping config file>`

This run script creates a set of comparison outputs and per drifter output files that contain the observed drifter track, the modelled drifter tracks from each experiment and the skill scores related to each experiment. It does this by calling:

`crop_to_common_comparison_set.py` 

The datasets to be cropped are defined in a yaml cropping configuration file. The input required to run this script is the per-drifter output files stored in the output_per_drifter/ directory. Please see this sample yaml file for a description of the various arguments:

`examples/DriftEval/analysis/config_de-ana_user_comparison_crop.yaml`

### Plotting comparison output 

Once cropped into a common set, the data can then be plotted as normal using:

`examples/DriftEval/analysis/run_drift-tool_analysis.sh –c <user comparison config file>`

In this case a sample yaml configuation file that can be used when plotting a comparison between experiments can be found here: 

`examples/DriftEval/analysis/config_de-ana_user_comparison_plotting.yaml`

## Regional analysis

Finally, if the user would like to analyze the data over a set of geographical regions, they can do so by including some regional analysis specific parameters in the config file that is passed to `run_drift-tool_analysis.sh`. These include the names of the areas that will be used in the analysis as well as the path to a second yaml file that contains a set of polygon definitions defining the areas to be used in the analysis. 

An example of a config file that can be used to perform a regional analysis is given here:

`examples/DriftEval/analysis/config_de-ana_user_regional_analysis.yaml`

and an example of the second config file containing the polygon definitions that are needed for a regional analysis is given here:

`examples/DriftEval/analysis/CTD_analysis_domain_config_template-SalishSea.yaml`
