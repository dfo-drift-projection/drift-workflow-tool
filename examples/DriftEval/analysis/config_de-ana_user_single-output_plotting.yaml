##############################################################################
# user config file for plotting
##############################################################################

# Directory containing the dataset(s) that will be plotted

##sample individual input
datadir: "/home/sdfo000/sitestore7/opp_drift_fa3/share_drift\
        /sample_analysis_input/single_experiments/ciopsw_alpha0.01/"

# Main output directory for the plots. 
# Plotting scripts create subfolders for organization.
savedir: "/path/to/folder/where/output/will/be/written/"

# Allow tool to overwrite an existing output directory if it already
# exists. Default behavior (False) causes the program to exit if savedir
# already exists.
overwrite_savedir: False

# Path to etopo file. If missing or None, no bathymetry is plotted. 
etopo_file: "/home/sdfo000/sitestore7/opp_drift_fa3/software/misc_files\
                /ETOPO1_Bed_g_gmt4.grd"

# Dictionary containing paths to landmask files if required. If None or 
# missing, default GSHHS landmask will be plotted. Landmask files 
# should be in NetCDF format and contain lon, lat and tmask variables 
# (ex, NEMO mesh mask files). If using default NEMO variable names 
# (nav_lon, nav_lat, tmask), each dictionary value can simply be a 
# string representing the full path to the file.  If using non-default 
# variable names, each dictionary value should contain a nested 
# dictionary with keys 'path', 'lon_var', 'lat_var', and 'tmask_var'.
# If providing multiple landmask files, it is acceptable to include
# variable names for some files but not for others. 
# The dictionary keys must match the setname variables if using a 
# comparison set or the ocean_model attribute if using normal 
# drift-tool output.
# Sample directory on the GPSC currently contains files for CIOPS-W,
# RIOPS, and SalishSeaCast: "/home/sdfo000\
# /sitestore7/opp_drift_fa3/software/misc_files" 

landmask_files:
    ciopsw_alpha0.01:
        path: "/home/sdfo000/sitestore7/opp_drift_fa3/software/misc_files\
                /CIOPSW_landmask.nc"

# Dictionary of colors to use for plotting. if lcolors: None or missing, 
# colors will be assigned randomly during plotting. Dictionary keys must 
# match the setname variables if using a comparison set or the 
# ocean_model attribute if using normal drift-tool output.

lcolors:
    ciopsw_alpha0.01: blue

# skills_list can include sep, molcard, liu, sutherland, obsratio, 
# modratio, logobsratio, logmodratio, obs_dist, obs_disp, mod_dist, 
# and mod_disp. If none or missing, defaults of sep and molcard are used.
skills_list:
    - sep
    - molcard

# filled_calculation_method can include IQR, boot95, 1std, and extremes. If
# None or missing, default is set to IQR.
filled_calculation_method:
    - IQR

# Plot durations to use with hexbin plots. Value can be a single integer 
# or a list of integers. For example, a value of 48 will produce a plot 
# representing skill score at 48h.
plot_durations:
    #- 24
    - 48

########################################################################
# Choose which plots to create (plot descriptions are provided below
# selections). Options set to False or missing will not execute.
########################################################################

plot_selection:
    #drifter track related plots
    plot_all_drifter_tracks: True 
    plot_all_drifter_tracks_with_mod: True
    #skillscore related lineplots
    plot_average_of_mean_skills: False
    plot_skills_subplots_with_filled: True
    #hexbin plots
    plot_hexbin_skillscores: True
    #per drifter plots
    plot_track_per_drifter: True
    plot_drifter_skillscores: True
    plot_skills_subplots_with_indv: False
    plot_persistence: False
    #qc plots
    plot_skills_plus_map: True
    plot_ratios_plus_map: False

########################################################################
# Plot descriptions
########################################################################
#
# drifter track related plots
# --------------------------
#
# plot_all_drifter_tracks:
#   plot all observed tracks in the comparison set
#
# plot_all_drifter_tracks_with_mod:
#   plot all observed tracks in the comparison set as well as all
#   modelled tracks on the same axis.
#
# skill score related lineplots
# -----------------------------
#
# plot_average_of_mean_skills:
#   per-experiment (or per-area) plot of average of skillscores
#   across all included drifters and modeled tracks
#
# plot_skills_subplots_with_filled:
#   per-experiment (or per-area) plot of average of skillscores
#   across all included drifters including all modeled tracks. Plot
#   includes a filled range defined by choosing filled_range_type above.
#
# hexbin plots
#-------------
#
# plot_hexbin_skillscores:
#   hexbin plot of the average skillscore at a time chosen by the
#   plot_durations argument above.
#
# per drifter plots
# -----------------
# Warning: the following options are very slow to run depending on the
# input data provided! The plots are created for each individual drifter,
# for each individual experiment and for each individual sub-region. In a
# comparison between many drifters over multiple experiments using
# multiple sub-regions, these can be very lengthy comparisons to create.
# Plots containing drifter tracks plotted on a map will be particularly
# time intensive to run.
#
# plot_track_per_drifter:
#   individual track plots for each drifter for each dataset
#
# plot_drifter_skillscores:
#   per-drifter plot of skillscore averaged across all modeled tracks
#
# plot_skills_subplots_with_indv:
#   per-experiment (or per-area) plot of average of skillscores
#   across all included drifters including all modeled tracks. Plot also
#   includes individual lines showing average skill per drifter.
#
# plot_persistence:
#   plot all observed tracks in the comparison set as well as all
#   modelled tracks on the same axis. Also includes persistence tracks
#   calculated using the initial velocity between the first two
#   measurements of the observed drifter at each model release point. A
#   separate plot is created for each individual drifter, for each
#   individual experiment and for each individual sub-domain.
#
# quality control type plots
# --------------------------
# Warning: the following options are very slow to run depending on the
# input data provided! The plots are created for each individual drifter,
# for each individual experiment and for each individual sub-region. In a
# comparison between many drifters over multiple experiments using
# multiple sub-regions, these can be very lengthy comparisons to create.
# These options would not typically be chosen for a routine analysis and
# would only be used if the user would like to explore the particular
# behavior of individual drifters in more detail.
#
# plot_skills_plus_map:
#   Plot that include subplots of average skill score for 'sep',
#   'molcard' and 'liu', as well as the observed and modelled drifter
#   tracks for each individual drifter. Plots are created separately for
#   each individual experiment and each individual sub-domain.
#
# plot_ratios_plus_map:
#   Plot that includes a subplot of average skill score for 'obsratio'
#   and 'modratio', as well as a subplot of the observed and modelled
#   drifter tracks. Plots are created separately for each individual
#   experiment and each individual sub-domain.


