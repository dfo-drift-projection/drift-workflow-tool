# DriftMap

A DriftMap simulation is used to characterize drift in an area of interest. The resulting analysis and plots are desgined to describe general drift charactersitics and trends. These simulations do not compare results with observations.

The examples/DriftMap/ directory includes sample configuation files and runscripts that can be used for producing drift simulations with OpenDrift, with Ariane and with MLDP.

# Running DriftMap

To launch and analyze a DriftMap simulation, two methods should be called:

- drift_map - produces a set of predicted drift trajectories saved in output files
- plot_drift_map - generates plots of the output

For a description of the arguments of these methods type: drift_map --help and plot_drift_map --help.

An easy way to accomplish this is to create a yaml configuration file for your experiment, then call the required methods using a workflow script. As an example, we have provided sample runscripts and yaml configuation files in the examples/DriftMap/ directory. 

To run DriftMap for OpenDrift or Ariane simulations, the following example run script can be used: 
- `run_driftmap-sims.sh` - sample runscipt that calls the required methods
- usage: `run_driftmap-sims.sh -c user_defined_config_file.yaml -o user_defined_output_directory -m optional/full/path/to/miniconda/install`

Sample yaml configuation files to use when running this script are also provided:
- `config_dm-sims_nemo_cgrid_ariane.yaml` - sample config file to use for an Ariane simuation with NEMO c-grid data 
- `config_dm-sims_nemo_cgrid_opendrift.yaml` - sample config file to use for an OpenDrift simulation with NEMO c-grid data
- `config_dm-sims_user_grid_opendrift.yaml` - sample config file to use for an OpenDrift simulation with model data on the user grid.
- `config_dm-sims_nemo_cgrid_opendrift_waves.yaml` - sample config file to use for an OpenDrift simulation with NEMO c-grid data and wave model forcing

A demonstration for running and plotting DriftMap using MLDP is included in the call script:
- `run_driftmap-sims_mldp.sh` 
- usage: `run_driftmap-sims_mldp.sh -c user_defined_config_file.yaml -o user_defined_output_directory -m optional/full/path/to/miniconda/install`

Sample yaml configuation files to use when running MLDP are provided by:
- `config_dm-sims_mldp.yaml` - sample config file to use when preprocessing of the ocean and atmospheric RPN files is required
- `config_dm-sims_mldp_no_preprocess.yaml` - sample config file to use in the case where preprocessing of the ocean and atmospheric RPN files has already been completed elsewhere. In this case, it is assumed that the user will provide a path to previously processed merged input files.
- `config_dm-sims_mldp_waves.yaml` - sample config file to use in the case where waves data is included.
