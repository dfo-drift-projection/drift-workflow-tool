#!/bin/bash

########################################################################
# This script provides an example of how to run a DriftMap simualtion
# using a run configuation file and then plot the results. The run
# configuration file contains all of the run parameters needed for the
# simulation. Several example configuration files are provided in
# examples/runs/DriftMap.

# To run a new experiment, users should provide the following variables
# via the command line:
# MINICONDA_PATH (optional)
# CONFIG_FILE
#
# Note: if running on the gpsc, it is recommended that this script is
# submitted to the job queues.
#
# Usage: run_driftmap.sh -c <yaml config file> -m <optional. Full path
# to drift-tool python environment if other than default provided below>
########################################################################

# On GPSC, load ariane executable as follows:
. ssmuse-sh -x /fs/ssm/dfo/dpnm/exp/ariane_2.3.0_gnu/
# Otherwise, specify the path to the ariane executable with the ariane-exec
# entry in the yaml config file. 

# MINICONDA_PATH: path to drift-tool python environment. For example, the path
# to the master branch environment of the drift-tool is:
MINICONDA_PATH='/home/sdfo000/sitestore7/opp_drift_fa3/software/drift-tool-environment/envs/v6.2/'

# give usage information
usage() {
    u="Usage: run_driftmap.sh" 
    u="$u -c <yaml config file>"
    u="$u -m <Optional. Full path to drift-tool python environment if other than default>"
    u="$u -o <Optional. Full path to directory in which to save experiment output."
    u="$u Alternately, this path can be added to the config file as experiment-dir>"
    echo $u
    exit 1
}

# check for arguments being passed in at the command line. Users are
# required to provide a CONFIG file and are optionally able to define a
# path to a miniconda install if they do not wish to use the default
# provided on the GPSC
while getopts "h?c:m:o:" args; do
case $args in
    h|\?)
        usage;
        exit 1;;
    c) CONFIG=${OPTARG};;
    m) MINICONDA_PATH=${OPTARG};;
    o) EXPERIMENT_DIR=${OPTARG};;
    : )
        echo "Missing option argument for -$OPTARG"; exit 1;;
    *  )
        echo "Unimplemented option: -$OPTARG"; exit 1;;
  esac
done

# warn if using default miniconda path
if [ "$MINICONDA_PATH" == "/home/sdfo000/sitestore7/opp_drift_fa3/software/drift-tool-environment/envs/v6.2/" ]; then
    n="NOTE: MINICONDA_PATH is set to the default environment for the master branch."
    n="$n This may cause issues if trying to run code from the development branch instead."
    echo $n
fi

# Set environment
# ---------------
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH
export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin

# if no config is provided, exit.
if [ -z "$CONFIG" ]; then
    usage
fi

if [ -n $EXPERIMENT_DIR ]; then
    experiment_dir=$EXPERIMENT_DIR
fi

# Source the config file and load the ECCC tools. The config file needs
# to be converted to a form more easily readable by Bash using the
# following python script.
eval $(yaml2bash --config $CONFIG)

# confirm existance of experiment_dir
if [ -z "$experiment_dir" ]; then
    echo "$CONFIG must contain experiment-dir" 
    exit 1;
fi


# Run drift tool
# --------------
drift_map \
    -c $CONFIG \
    --experiment-dir $experiment_dir \

# Plot the results. Plots will be available in $experiment_dir/plot
# ------------------------------------------------------------------
plot_drift_map \
  --data-dir $experiment_dir/output \
  --bbox $experiment_dir/namelist_bbox \
  --etopo-file '/home/sdfo000/sitestore7/opp_drift_fa3/software/misc_files/ETOPO1_Bed_g_gmt4.grd' \
  --plot-dir $experiment_dir/plots \
  --style 'seaborn-v0_8-paper' \
  --interval 24
