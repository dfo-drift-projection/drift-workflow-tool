# Examples

Example bash run scripts and yaml configuration files for producing drift simulations and processing, analyzing and plotting the results.

## Subdirectories
- ariane-config-files - example ariane configuration files for several commonly used NEMO models. An ariane configuration file is needed to produce drift simulations with the Ariane drift methodology.
- DriftEval - example run scripts and configuration files for producing and analyzing DriftEval simulations. A DriftEval simulation and analysis compares modelled drifter trajectories to observed drifter trajectories. The resulting analysis and plots are designed to evaluate the accuracy of the modelled drifters using a common set of verification metrics.
- DriftMap - example run scripts and configuration files for producing and analyzing DriftMap simulations. A DriftMap simulation is used to characterize drift in an area of interest. The resulting analysis and plots are desgined to describe general drift charactersitics and trends in the area of interest. These simulations do not compare results with observations.
- DriftCorrectionFactor - example call scripts and yaml configuration files for producing DriftCorrectionFactor calculations. This function compares observed drifter velocities to modelled ocean and atmospheric velocities to calculate the correction factor needed to produce a perfect drift prediction.

