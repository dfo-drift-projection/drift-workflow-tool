====================
Installing OpenDrift
====================

:Author: Clyde Clements
:Created: 2018-03-09
:Date: 2018-03-12

Please note that the latest release of the drift-tool along with a
drift-tool compatible version of OpenDrift have already been installed
in the shared sdfo000 group account here::

    MINICONDA_PATH=/home/sdfo000/sitestore7/opp_drift_fa3/software/

Users planning to run exclusively with the latest release of the drift-tool
may choose to simply use these shared installations in their runscripts.
If however you have chosen to install a copy of the drifter-tool yourself,
you should also install OpenDrift in the same python environment where you
have successfully installed the drift-tool.

First, change to the directory where you have installed the drift-tool::

    cd $HOME/drifters/drift-tool

Next define the following parameters::

    OPENDRIFT_PATH=$HOME/drifters/opendrift #path to where you would like to download the OpenDrift repository
    MINICONDA_PATH=/gpfs/fs4/dfo/dpnm/dfo_dpnm/$USER/miniconda #path to the python environment where you have installed the drift-tool
    RUN_TESTS=true #choose to run OpenDrift's built in tests to ensure that the installation was successful.

Finally, install OpenDrift::

    ./opendrift_install.sh -o $OPENDRIFT_PATH -p $MINICONDA_PATH -t $RUN_TESTS

Check the installation output in $OPENDRIFT_PATH/install.txt and
$OPENDRIFT_PATH/test_out.txt for errors.
