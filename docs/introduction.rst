Introduction
============

:Author: Clyde Clements
:Created: 2018-02-05
:Date: 2018-02-05


There are two primary tasks to the drift verification; the first is the
calculation of synthetic drift trajectories meant to simulate the tracks of the
drifter buoys and the second is the analysis and comparison of the modelled
trajectories with the observed ones.

The first task is performed by the script ``src/drift_predict.py``.

.. figure:: drift_verify_process.png
   :alt: drift verification process
   :scale: 50%

   Process for drift verification.
