Data Inputs
===========

:Author: Clyde Clements
:Created: 2018-02-08
:Date: 2018-02-08


Drifter Data
------------

- Data should be available as individual files, one file per drifter.
- NetCDF format.
- Required global (file) attributes:

  - ``buoyid``: identifier for buoy that must be unique across all drifters
    considered.

- Required variables:

  - longitude
  - latitude
  - time

  The names of these variables are (should be) configurable.

  Variables must be of a single dimension?

