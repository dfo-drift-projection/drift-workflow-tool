==============================================
Installing the drift workflow tool on the gpsc
==============================================

:Author: Clyde Clements
:Contributors: Nancy Soontiens, Jennifer Holden
:Created: 2018-03-09
:Date: 2018-03-12

Please note that the latest release of the drift-tool has already been
installed in the shared sdfo000 group account here:

``MINICONDA_PATH=/home/sdfo000/sitestore7/opp_drift_fa3/software/drift-tool-environment/envs/6.2``

Users planning to run exclusively with the latest release of the drift-tool
may choose to simply use this shared python installation in their runscripts.
If however a user would like to develop with the drift tool, the following
setup instructions may be used instead.

**Clone the repository**

On the GPSC, make a directory in $HOME called drifters/. This directory
will be needed later when installing other required packages.::

    cd $HOME
    mkdir drifters

Next, clone drift tool repository into this folder::

    cd drifters
    git clone https://gitlab.com/dfo-drift-projection/drift-workflow-tool drift-tool

You will be asked for your gitlab username and password. Alternatively,
you could set up ssh keys which doesn't require you to type your password
(https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair ) and
clone like this::

    git clone git@gitlab.com:dfo-drift-projection/drift-workflow-tool.git drift-tool

**Install drift tool**

Note: Regardless of whether you choose to install using version 1 or 2 below,
it is a good idea to install with ``pip install -e .``.  That way, executables
will be updated if you make changes to the code at a later date. First,
navigate to the drift-tool directory::

    cd drift-tool

An installation script called ``install_pyenv.sh`` is provided that sets up a
minimal Python environment using the Miniconda distribution and then installs
all required dependencies for the drift workflow tool [#f1]_. This script
can be used following these instructions:

To create a new Miniconda install, we recommend choosing a path in your working directory. This is because space in $HOME is limited (10 GB limit per user) and a Miniconda installation is relatively large (2.1GB). Choose an environment name in which you will install the drift-tool::

    MINICONDA_PATH=/gpfs/fs7/dfo/dpnm/$USER/miniconda
    ENV_NAME=drift-tool
    ./install_pyenv.sh -d $MINICONDA_PATH -e $ENV_NAME

If you already have a miniconda installation at ``MINICONDA_PATH`` then this script will simply create a new environmet called ``ENV_NAME``. Otherwise, a new miniconda base is installed at ``MINICONDA_PATH`` and the new environment is created.

After the above installation is finished, modify the ``PATH`` environment to
include the ``bin`` directory of the new Python environment; if your login
shell is bash or zsh, this can be accomplished with the command::

    export PATH="$MINICONDA_PATH/bin:$PATH"

Next, acivate the new environment::

    source activate $ENV_NAME

Next, run the following command from the top-level code directory::

    pip install -e .

Likewise, to uninstall the package, use this command::

    pip uninstall driftutils

If you would like to develop with the drift tool (i.e. modify source code),
it's a good idea to install on the develop branch or your own branch. That is::

    cd $HOME/drifters/drift-tool
    git checkout develop
    pip uninstall driftutils
    pip install -e .

You can always switch between branches with::

   git checkout branch-name
   pip uninstall driftutils
   pip install -e .


.. rubric:: Footnotes

.. [#f1] Initially the workflow tool was developed and tested using version
   4.30 of the Anaconda Python 3 distribution with some additional packages
   installed from the `conda-forge` channel of `conda` and others installed via
   `pip`. This software environment of dependencies was installed manually as
   and when third-party packages were first used. The provided installation
   script aims to create a minimal working environment that includes just
   third-party dependencies at the versions used during initial development.

.. [#f2] These instructions do not describe how to install Ariane, MLDP or OpenDrift,
   which are third party software dependencies for the drift tool. Please see
   [set up instructions](https://gitlab.com/dfo-drift-projection/drift-workflow-tool/-/wikis/setup-on-the-gpsc) for instructions on installing
   those dependencies.
