# MLDPn development outline

This document is an outline for adding MLDPn to the drift tool. Work is in progress on the mldp branch.

Contact for MLDPn questions: Guillaume Marcotte guillaume.marcotte@canada.ca

Documentation: [MLDPn documentation](http://eer.cmc.ec.gc.ca/people/Guillaume/a8gyh1Gjh87fAous9hqbu/DFO/CMOE-MLDPn%20-%20Wiki.htm)

## Sketch of workflow modules - inspired by current `drift_predict`

```bash
   mldp_predict  
   assemble_drifter_metadata (same as current version or very similar)  
   assemble_ocean_metadata
   assemble_atm_metadata
   for each drift start date
       assemble_drifter_data (same as current version or very similar)
       assemble_ocean_atm_data (will need to run mldp pre-processing)
       write_mldp_namelist
       run_mldp
       post_process_mldp
   assemble_drift_predictions
```
After `assemble_drift_predictions` we should end up with a set of netcdf files that can be injested by `drift_evaluate`.

#### Note
It may not be necessary to run each drift start date separately in mldp. 
So long has each drifter has a unique id, all drifters can be launched from one namelist, even if start and end times are different.
This approach could simplify/reduce the amount of preprocessing. 
However, it complicates recording of metadata associated with each drift run date.

### Sketch of mldp_predict
Arguments:
mldp_predict
Arguments:
```bash
    experiment_dir,
    drifter_data_dir,
    ocean_data_dir,
    atm_data_dir,
    ocean_model_name,
    first_start_date,
    last_start_date,
    drift_duration,
    drifter_depth=0.0 [m],
    drift_model_name =’MLDP’,
    mldp_exec=’MLDP’,
    mldp_config_file=’mldp.yaml’,
    drifter_id_attr='buoyid',
    drifter_meta_variables=None,
    start_date_frequency='daily',
    start_date_interval=1,
    fcst_hrs=[00, 06, 12, 18]
```

### Notes on working with ECCC RPN files

*  Will assume (without checking) that rpn files contain the required variables. Checks could be incorporated with calls to the rmn python library (but where to get documentatio and is it compatible with python 3?)
* Will assume that each rpn file contains one time stamp and valid time can be easily decoded from filename. Again, could be verified with rmn python library
* Assuming ocean data directory contains no output from other models.

#### Forecasts
It is challenging to work with the forecast data for the following reasons
* Forecast data does not make a continuous time series. For example, a RIOPS forecast is launched every 6 hours for a period of 48 hours. How does one identify which model forecast to use for a given drifter run? -> pass an argument list
* Perhaps any forecast which covers the drift window should be used.
* Is it worthwhile to try to compose a “continuous” run with “best” results from each forecast?

