## Purpose of merge request


## Why did you take this approach?


## Anything in particular that should be highlighted?


## Screenshot(s)


## Checks
### Run Examples
I've tested the relevant changes from a user POV by running all examples in the following sets and confirmed the examples completed without error:
- [ ] DriftEval
- [ ] DriftMap
- [ ] DriftCorrectionFactor

If some examples or sets were omitted, list those here and explain why:

### Code Formatting
- [ ] I've applied code formatting by running flake8 with default settings

### Code Quality
I've considered to following coding principles in this merge request:
- [ ] Documentation (e.g. docstrings, comments)
- [ ] Easy to read
- [ ] Testable
- [ ] Easy to change
- [ ] Modular
- [ ] Fit-for-purpose
