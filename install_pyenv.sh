#!/bin/bash
#
###############################################################################
# Author: Clyde Clements
# Contributors: Nancy Soontiens
# Created: 2018-03-12
#
# Usage:
#   ./install_pyenv.sh -d dir_name -e env_name [-f env_file]
#       -d dir_name: directory that contains (or will contain) the base
#                    Miniconda installation
#       -e env_name: the name of the python environment that will be created
#       -f env_file: the name of the file which contains the list of packages 
#                    to install. By default, a package list for a 
#                    Linux-64 distribution is used.
#
# This script creates a Python environment containing all dependencies
# required for the drift utilities. The environment will be created
# from a base Miniconda installation given by the -d flag or a new
# base Miniconda will be installed if one doesn't already exist.
# Finally, you can optionally set the name of the file which contains the 
# list of packages to install. By default, a package list for a Linux-64 
# bit distribution is used.
#
# For example, if using Linux:
#
# Install a fresh Miniconda environment then add a separate Python environment: 
#   ./install_pyenv.sh -d <path to a non-existant directory> -e <environment name>
#
# Install as an environment in an already existing Miniconda installation:
#   ./install_pyenv.sh -d <path to pre-existing Miniconda dir> -e <environment name>
#
# If installing on a non-linux-64 machine, -f environment.yaml should be
# included in the argument list for the above examples.
###############################################################################

# give usage information
usage(){
    echo "Usage: install_pyenv.sh -d dir_name -e env_name [-f env_file]"
    u="\t-d dir_name: directory that contains (or will contain) the base Miniconda installation.\n"
    u="$u \t-e env_name: the name of the python environment that will be created.\n"
    u="$u \t-f env_file: the name of the file which contains the list of "
    u="$u packages to install. By default, a package list for a Linux-64 "
    u="$u distribution is used."
    echo -e $u
    exit 1;
}

# check for arguments being passed in at the command line.
while getopts "h?d:e:f:" args; do
case $args in
    h|\?)
        usage;
        exit 1;;
    d) dir_name=${OPTARG};;
    e) pyenv=${OPTARG};;
    f) env_file=${OPTARG};;
    : )
        echo "Missing option argument for -$OPTARG"; exit 1;;
    *  )
        echo "Unimplemented option: -$OPTARG"; exit 1;;
  esac
done

# If an env_file isn't given, default to the linux version
if [ -z $env_file ]; then
    env_file="packages-linux64.txt"
fi
echo "Using env_file ${env_file}"

# reset the Python path
if [[ ! -z "$PYTHONPATH" ]]; then
  echo "unsetting PYTHONPATH"
  unset PYTHONPATH
fi

# Check if the user has specified a directory for the Miniconda install
if [[ ! -z $dir_name ]]; then
    # If the Miniconda install already exists there, create the env in the base 
    # Miniconda env (ie, Miniconda already installed in $dir_name)
    if [[ -f $dir_name/bin/conda ]]; then
        echo "Miniconda installation found in $dir_name, not reinstalling"
    else
        # Otherwise, do a fresh Miniconda install. 
        if [[ ! -f Miniconda3-latest-Linux-x86_64.sh ]]; then
            curl -L -O https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
        fi
        /bin/bash ./Miniconda3-latest-Linux-x86_64.sh -b -p $dir_name
    fi
    export PATH=${dir_name}/bin:$PATH
fi

# install the required packages
conda update --yes --name base conda
conda env create --name $pyenv --file $env_file
source activate $pyenv
conda config --append channels conda-forge --env

echo -e "\n\ninstalling additional packages via pip"
pip install defopt==6.2.0
pip install motuclient
pip install pytest-sphinx
