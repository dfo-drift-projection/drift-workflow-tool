#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  6 10:06:44 2023

Script to generate drift-tool files from netcdf files generated for upload to 
Water Properties. This script calls several functions to perform the following 
tasks:
    1. Load all files with matching IDs (i.e. same drifter)
    2. Split tracks by both groundings and the specified time gap
    3. Remove atSea = 1 points
    4. Interpolate to the specified time interval
    5. Remove bad speeds (shouldn't be any since they are removed for Water Properties)
    6. Remove tracks shorter than the specified duration
    7. Assign metadata (including unique buoyid for each track)
    8. Plot original and final tracks
    9. Save a netcdf file for each track                      

All inputs and options for the script are included above the function definitions. 
The script is stand-alone in that it doesn't depend on any external functions
other than those in the imported packages that are all publicly available. 

For a help message and description of arguments run
python write_DriftTool_files.py --help

@author: jmm000, nso001
"""

import argparse
import xarray as xr
import numpy as np
import pandas as pd
import glob
from datetime import timedelta,datetime
import matplotlib.pyplot as plt
import cartopy
import pathlib
import os
import math
import sys
from geopy.distance import distance
             
#%% Functions

def calculate_duration(track):
    """
    Calculate the total number of days in a track for atSea = 1 points.

    Parameters
    ----------
    track : DataFrame
        Data fro a single track. Must have ATSEA column and time as index

    Returns
    -------
    duration_days : float
        Duraton of a track in days

    """
    ind = np.where(track.ATSEA == 1)[0]
    dt = track.index[ind[-1]] - track.index[ind[0]]
    duration_days = dt.total_seconds()/3600/24
    
    return duration_days

def calculate_distance(track): 
    """
    Calculate the total distance in km of a track for atSea = 1 points.

    Parameters
    ----------
    track : DataFrame
        Data fro a single track. Must have ATSEA, LATITUDE AND LONGITUDE columns

    Returns
    -------
    distance_km : float
        Total cumulative distance of a track in kilometers

    """
    ind = np.where(track.ATSEA == 1)[0]
    dist = 0
    for i in range(len(track.LONGITUDE[ind])-1):
        dist += distance((track.LATITUDE[ind][i], track.LONGITUDE[ind][i]),
                         (track.LATITUDE[ind][i+1], track.LONGITUDE[ind][i+1])).meters
    distance_km = dist/1000
     
    return distance_km


def split_track(dfTrack,maxAtSea,timeGap):
    """
    Split one track (dfTrack) into several tracks based on groundings and/or 
    time gaps

    Parameters
    ----------
    dfTracks : dataframe
        Dataframe with data from one drifter track
    maxAtSea : int
        atSea value to split tracks at. Typically 2 or 3
    timeGap : float
        time gap [hours] where to split tracks

    Returns
    -------
    tracks : list
        list of dataframes of subdivided tracks

    """
    
    print('Splitting tracks')
    # atSea gaps (typically to split at groundings)
    atSea = dfTrack.ATSEA
    indG = np.where(atSea>maxAtSea)[0]
    if atSea[-1] == 1:
        indG = np.append(indG, len(atSea)-1)
    indGGaps = indG[np.where(np.diff(indG)>1)[0]]

    # time gaps
    time = dfTrack.index
    tGapMin = np.int64(timeGap*60)
    if np.float64(tGapMin/60.)!= timeGap:
        raise Exception('Computation of timeGap in minutes failed')
    indTGaps = np.where(np.diff(time)>np.timedelta64(tGapMin,'m'))[0]

    # all gaps
    indGaps = sorted(np.unique(np.append(indGGaps,indTGaps)))

    tracksOut = []
    indS = 0
    for ii in range(0,len(indGaps)+1):
        if ii<len(indGaps):
            indE = indGaps[ii]
        else:
            indE = len(time)
     
        tracksOut.append(dfTrack.iloc[indS:indE+1])
        indS = indE+1

    # Check lengths
    tot = 0
    for t in tracksOut:
        tot += len(t)
    if tot!= len(dfTrack):
        raise Exception("Length of subtracks do not add to length of initial track")

    print(f'  Separated into {len(tracksOut)} tracks')

    return tracksOut

def get_buoyid(ID, trackNumber, totalTracks):
    """
    Get a unique buoyid for a track based on the drifter ID, track number and 
    total number of tracks. The buoyid consist of the ID (with spaces, dashes
    and underscores removed) plus -- if there is more than one track -- one or 
    more letters that increase incrementally. If there are fewer than 26 tracks,
    the letters go from a, b, c, etc to z. If there are more than 26 tracks,
    the letters go from aa, ac, ac, etc to zz. 
    
    Parameters
    ----------
    ID : str
        The ID of the drifter (unique for a drifter)
    trackNumber : int
        Integer number of the track
    totalTracks : int
        Total number of tracks for the drifter ID

    Returns
    -------
    buoyid : str
        Unique ID for the drifter and the track.

    """
    
    #Short ID
    idShort = ID
    idShort = idShort.replace(' ','')
    idShort = idShort.replace('_','')
    idShort = idShort.replace('-','')

    # Unique letters
    if totalTracks == 1:
        letter1 = ''
        letter2 = ''
    elif (totalTracks>1) & (totalTracks <=26):
        letter1 = chr(ord('a') + trackNumber)
        letter2 =''
    elif (totalTracks>26) & (totalTracks <=676):
        if np.mod(trackNumber+1,26) == 0:
            Ncyc = np.int64(np.floor((trackNumber+1)/26))
        else:
            Ncyc = np.int64(np.floor((trackNumber+1)/26)+1)
        letter1 = chr(ord('a') + Ncyc-1)
        letter2 = chr(ord('a') +trackNumber-(Ncyc-1)*26);
    else:
        raise Exception('Too many tracks to assign buoy id. Need to have less than 677 for two letters')
    letters = letter1 + letter2
    
    # Combine
    buoyid = idShort+letters
    
    return buoyid

def resample_tracks(tracks,resampleIntHours,resampleStartMethod):
    """
    Resample and interpolate the tracks to a different time base, specified as 
    an input in hours. The time base can either start from the first recorded 
    time in a track or the nearest round number of the specified resample 
    interval. The resampling method currently uses numpy because directly using
    dataframe methods did not yield accurate results (see debug section of 
    function).

    Parameters
    ----------
    tracks : list of DataFrames
        list of one or more dataframes, each corresponding to a track
    resampleIntHours : float
        the resample interval in hours (e.g. 10.0/60.0 for 10 minute resampling)
    resampleStartMethod : str
        either 'Original' or 'Rounded'
        If 'Original', the first time stamp in the interpolated track will be 
        the same as in the original track and the timestamps will defined from 
        that value. If 'Rounded', the first time stamp will be the next round 
        number time after the first time stamp in the original track. For example, 
        if the first time stamp is 12:02:02 and the resampleIntHours is 10 minutes
        (0.1667 hours), then:
            'Original' would yield times of 12:02:02, 12:12:02, 12:22:02, etc
             'Rounded' would yield times of 12:10:00, 12:20:00, 12:30:00, etc

    Returns
    -------
    tracksOut : list of DataFrames
        list of one or more dataFrames which contain the interpolated data

    """
    
    print(f"Resampling tracks to {resampleIntHours:3.2f} hour ({resampleIntHours*60} min) intervals")
    tracksOut = []

    resampleIntSec = resampleIntHours*3600
    for tt in range(0,len(tracks)):
        track = tracks[tt]
        
        # Get time vector (in seconds)
        t = []
        for pp in range(0,len(track)):
            t.append(track.index[pp].timestamp())
        
        # Timestamps to interpolate to
        if resampleStartMethod == 'Original': # to start on first timestamp in original data
            tS = t[0] 
        elif resampleStartMethod == 'Rounded': # To start on even multiples of resampleIntHours
            tS = int(math.ceil(t[0] / (resampleIntSec))) * resampleIntSec
        tE = t[-1]
        timeI = np.arange(tS,tE,resampleIntSec)
        
        # Get dates
        dateI = pd.to_datetime(timeI*1e9)
        
        # Create interpolated data frame
        trackI = pd.DataFrame({'TIME':dateI})
        trackI = trackI.set_index('TIME')
        
        # Add columns to dataframe
        for col in track.columns.tolist():
            trackI[col] = np.interp(timeI,t,track[col])
            
        # Add to existing and show output
        if len(trackI)>2:
            tracksOut.append(trackI)
            print(f"  track {tt+1} -- {len(trackI)} points from {len(track)} points")
        else:
            print(f"  track {tt+1} -- {len(trackI)} points from {len(track)} points - DROPPING")
        
        # for dubugging (Compare interpolation methods)
        if False: 
            # Direct method using dataframe resampling (doesn't quite give same result)   
            resampleTimeStr = str(resampleIntHours) + 'H'
            trackI2 = track.resample(resampleTimeStr).mean().interpolate(method = 'linear') 
            print("Original")
            print(track[0:10])
            print("Numpy")
            print(trackI[0:10])
            print("Dataframe")
            print(trackI2[0:10])
            
            
            plt.figure()
            plt.subplot(2,1,1)
            plt.plot(track.LONGITUDE,track.LATITUDE,marker = 'o',label='original')
            plt.plot(trackI.LONGITUDE,trackI.LATITUDE,marker = '.',label='numpy')
            plt.plot(trackI2.LONGITUDE,trackI2.LATITUDE,marker = '.',label='dataframe')
            plt.legend()
            
            plt.subplot(2,1,2)
            plt.plot(track.index,track.LATITUDE,marker = 'o',label='original')
            plt.plot(trackI.index,trackI.LATITUDE,marker = '.',label='numpy')
            plt.plot(trackI2.index,trackI2.LATITUDE,marker = '.',label='dataframe')
            plt.legend()
        
    return tracksOut

def remove_high_atSea(tracks, maxAtSea):
    """
    Remove points where the ATSEA value exceeds the specified maximum

    Parameters
    ----------
    tracks : list of DataFrames
        list of one or more dataframes, each corresponding to a track
    maxAtSea : int, typically 1
        The maximum ATSEA value to include in the returned tracks.

    Returns
    -------
    tracksOut : list of DataFrames
        list of one or more dataframes, each corresponding to a track with any 
        bad ATSEA points have been removed

    """
    print(f"Dropping points where atSea > {maxAtSea}:")
    
    tracksOut = []
    for tt in range(0,len(tracks)):
        track = tracks[tt]
        indHigh = track[track.ATSEA>maxAtSea].index
        
        trackTmp = track.drop(indHigh)
        
        # Ensure track length is greater than 2 points
        if len(trackTmp)<=2:
            print(f"  track {tt+1} -- Dropping track because fewer than 2 points in track")
        else:
            tracksOut.append(trackTmp)
            print(f"  track {tt+1} -- {len(indHigh)} points dropped ")
    return tracksOut
    
def remove_bad_speeds(tracks,speedMax):
    """
    Remove points where the speed equals zero or exceeds the specified maximum

    Parameters
    ----------
    tracks : list of DataFrames
        list of one or more dataframes, each corresponding to a track
    speedMax : TYPE
        The maximum speed [m/s] to include in the returned dataframe

    Returns
    -------
    tracksOut : list of DataFrames
        list of one or more dataframes, each corresponding to a track with any 
        bad speed points have been removed

    """
    print("Checking speed") 

    tracksOut = []
    for tt in range(0,len(tracks)):
        track = tracks[tt]
        dist = np.zeros(len(track.LONGITUDE)-1)
        for i in range(len(dist)):
            dist[i] = distance((track.LATITUDE[i+1], track.LONGITUDE[i+1]),
                               (track.LATITUDE[i], track.LONGITUDE[i])).meters
        dtime = np.diff(track.index) / np.timedelta64(1, 's')
        speed = dist / dtime
        speed = np.append([np.nan], speed)
        
        ind = np.where((speed==0) | (speed > speedMax))[0]
        
        tracksOut.append(track.drop(track.index[ind]))

        if len(ind)>0:
            print(f"  track {tt+1} -- Dropping {len(ind)} bad speed points")
        else:
            print(f"  track {tt+1} -- All good")
    return tracksOut

def remove_short_tracks(tracks,minTrackLength):
    """
    Removes any tracks with durations shorter than the specified length from 
    the returned list of DataFrames

    Parameters
    ----------
    tracks : list of DataFrames
        list of one or more dataframes, each corresponding to a track
    minTrackLength : float
        minimum duration, in hours, of a track. If the total duration is 
        shorter than this value the entire track will be dropped

    Returns
    -------
    tracksOut : list of DataFrames
        list of one or more dataframes, which all have durations that exceed the
        minTrackLength

    """
    print(f"Remove tracks that are shorter than {minTrackLength} hours")
    
    minLengthMin = np.int64(minTrackLength*60)
    
    tracksOut = []
    for tt in range(0,len(tracks)):
        dtime = tracks[tt].index[-1] - tracks[tt].index[0]
        
        text = f"  track {tt+1} -- duration is {np.round(dtime.total_seconds()/3600):.0f} hours"
        if dtime < np.timedelta64(minLengthMin,'m'):
            print(f"{text} (Dropping)")
        else:
            print(f"{text} (Keeping)")
            tracksOut.append(tracks[tt])
    totalTracks = len(tracksOut)
    print(f"  Final number of tracks: {totalTracks}")
    return tracksOut
    
def assign_metadata(tracks,dfMetaIn,fieldsIn, description):
    """
    Creates a dataframe with each row corresponding to the metadata for a track.
    Some of the fields are copied from the input metadata (e.g. deployment 
    admin info), and others computed directly from each of the tracks (e.g. 
    distance, duration, variable maximums and minimums)

    Parameters
    ----------
    tracks : list of DataFrames
        list of one or more dataframes, each corresponding to a track
    dfMetaIn : DataFrame
        metadata for the original track
    fieldsIn : list of strings
        List of strings for the fields to include in the metadata
    description : str
        Description for metadata

    Returns
    -------
    dfMetaOut : DataFrame
        DataFrame where each row corresponds to a track. Columns represent 
        metadata fields. 

    """
    print("Assigning metadata")  

    fields = fieldsIn.copy()
    if 'SST' not in list(tracks[0].columns):
        fields.remove('sst_min')
        fields.remove('sst_max')
    if 'BP' not in list(tracks[0].columns):
        fields.remove('bp_min')
        fields.remove('bp_max')
    
    metadata = {}
    dfMetaOut = pd.DataFrame()
    for tt in range(0,len(tracks)):
        track = tracks[tt]
        for key in fields:
            if key == 'buoyid':
                metadata[key] = get_buoyid(dfMetaIn.loc['ID'].values[0], tt, len(tracks))
            elif key == 'track_number':
                metadata[key] = float(tt+1)
            elif key == 'launch_date':
                metadata[key] = pd.to_datetime(track.index[0]).strftime('%Y-%m-%d')
            elif key == 'duration_days':
                metadata[key] = calculate_duration(track)
            elif key == 'distance_km':
                metadata[key] = calculate_distance(track) 
            elif key == 'first_date_observation':
                metadata[key] = track.index[0].strftime('%Y-%m-%d %H:%M:%S')
            elif key == 'last_date_observation':
                metadata[key] = track.index[-1].strftime('%Y-%m-%d %H:%M:%S')
            elif key == 'first_latitude_observation':
                metadata[key] = track.LATITUDE[0]
            elif key == 'last_latitude_observation':
                metadata[key] = track.LATITUDE[-1]
            elif key == 'first_longitude_observation':
                metadata[key] = track.LONGITUDE[0]
            elif key == 'last_longitude_observation':
                metadata[key] = track.LONGITUDE[-1]
            elif key == 'lat_min':
                metadata[key] = np.min(track.LATITUDE)
            elif key == 'lat_max':
                metadata[key] = np.max(track.LATITUDE)
            elif key == 'lon_min':
                metadata[key] = np.min(track.LONGITUDE)
            elif key == 'lon_max':
                metadata[key] = np.max(track.LONGITUDE)
            elif key == 'atSea_min':
                metadata[key] = np.min(track.ATSEA)
            elif key == 'atSea_max':
                metadata[key] = np.max(track.ATSEA)
            elif key == 'sst_min':
                metadata[key] = np.min(track.SST)
            elif key == 'sst_max':
                metadata[key] = np.max(track.SST)
            elif key == 'bp_min':
                metadata[key] = np.min(track.BP)
            elif key == 'bp_max':
                metadata[key] = np.max(track.BP)
            elif key == 'data_description':
                metadata[key] = description
            else:
                metadata[key] = dfMetaIn.loc[key].values[0]

        dfRow = pd.DataFrame.from_dict(metadata,orient='index').T
        dfMetaOut = pd.concat([dfMetaOut,dfRow],ignore_index=True)
        
    indent = " " * 2
    dfStr = indent + dfMetaOut[['track_number','buoyid','duration_days','distance_km','first_date_observation']].to_string(index=False).replace("\n", "\n" + indent)
    print(dfStr)

    return dfMetaOut

def save_netcdf(tracks,dataDirOut, dfMetaOut):
    """
    Saves a netcdf file for each track

    Parameters
    ----------
    tracks : list of DataFrames
        list of one or more dataframes, each corresponding to a track
    dataDirOut : str
        Path to the output directory
    dfMetaOut : dict
        Dictionary containing metadata

    Returns
    -------
    None.

    """
    print("Saving netcdfs")
    
    
    for tt in range(0, len(tracks)):
        dsOut = tracks[tt].to_xarray()
        dsOut = dsOut.assign_attrs(dfMetaOut.iloc[tt])

        dateS = pd.to_datetime(np.min(dsOut.TIME.values)).strftime('%Y%m%d')
        dateE = pd.to_datetime(np.max(dsOut.TIME.values)).strftime('%Y%m%d')

        fileName = f"{dsOut.buoyid}_{dateS}_{dateE}.nc"

        print(f"  track {tt+1}: Saving to {fileName}")
        dsOut.attrs['netcdf_data_file'] = fileName
        dsOut.to_netcdf(dataDirOut+'/'+fileName)
    
    return

def plot_tracks(tracksOrig,tracks,dfMeta):
    """
    Plot original and divided tracks. Used to confirm tracks output tracks 
    accurately represent original tracks. 

    Parameters
    ----------
    tracksOrig : list
        list of one or more dataframes, each corresponding to an original track
    tracks : list
        list of one or more dataframes, each corresponding to a final track.
    dfMeta : pd.DataFrame
        DataFrame of metadata (used to label the tracks in the plots)

    Returns
    -------
    None.

    """
  
    plt.figure(figsize=(11,8.5))
    ax0 = plt.subplot2grid((2, 1), (0, 0), colspan=1, rowspan=1, projection=cartopy.crs.PlateCarree())
    ax1 = plt.subplot2grid((2, 1), (1, 0), colspan=1, rowspan=1)

    # map
    ax0.add_feature(cartopy.feature.LAND,color='0.6')
    gl = ax0.gridlines(draw_labels=True, linewidth=0.5, color='gray', alpha=0.5, linestyle='--')
    gl.top_labels = False
    gl.right_labels = False
    for tt in range(0,len(tracksOrig)):
        ax0.plot(tracksOrig[tt].LONGITUDE,tracksOrig[tt].LATITUDE,marker = 'o',color='k',label=f'original (track {tt+1})')
        ax0.plot(tracksOrig[tt].LONGITUDE[0],tracksOrig[tt].LATITUDE[0],marker = 'X',color='r')
    for tt in range(0,len(tracks)):
        ax0.plot(tracks[tt].LONGITUDE,tracks[tt].LATITUDE,marker = '.',label=f"{dfMeta['buoyid'].iloc[tt]}")
    ax0.legend()
    try:
        ax0.set_title(dfMeta['ID'].iloc[0])
    except:
        ax0.set_title(dfMeta.loc['ID'][0])
    
    # timeseries
    for tt in range(0,len(tracksOrig)):
        ax1.plot(tracksOrig[tt].index,tracksOrig[tt].LATITUDE,marker='o',color='k',label=f'original (track {tt+1})')
        ax1.plot(tracksOrig[tt].index[0],tracksOrig[tt].LATITUDE[0],marker = 'X',color='r')
    for tt in range(0,len(tracks)):
        ax1.plot(tracks[tt].index,tracks[tt].LATITUDE,marker='.',label=f"{dfMeta['buoyid'].iloc[tt]}")
    ax1.legend()
    ax1.grid()
    ax1.set_ylabel('LATITUDE')
    ax1.set_xlabel('DATE')
    
    return


#%% START OF MAIN SCRIPT
def write_DriftTool_files(*,
    dataDirOut, dataDir, idStr, figDir,
    maxAtSea=1, splitTimeGap=6.5,
    minTrackLength=6, interpTimeStart='Rounded',
    interpTimeInt=10./60., speedMax=5):
    """ Main function for writing drift tool files.

    Parameters
    ----------
    dataDirOut : str
        Parent directory to write files to. NetCDF files are written to
        dataDirOut/idStr.
    dataDir : str
        Parent directory with input files. NetCDF files in 
        dataDir/idStr are used.
    idStr : str
        Sub-directory for input drifter files
    figDir : str
        Parent directory to write plots to. Plots are written in
        figDir/idStr.
    maxAtSea : int
        The maximum ATSEA value to include in the returned tracks.
        Typically 1.
    splitTimeGap : float
        Time gap [hours] where to split tracks
    minTrackLength : float
        Mininum track duration [hours]
    interpTimeStart : str
        'Rounded' or 'Original'.
         start interpolatation at original time or rounded time, e.g. 01:20 for 10 minute intervals
    interpTimeInt : float
        Time interval for interpolated tracks in hours
    speedMax : float
        Maximum expected speed of drifters in m/s.
        Reject data with larger speeds.
    """
    #%% Parameters
    options = dict()
    options['maxAtSea'] = maxAtSea
    options['splitTimeGap'] = splitTimeGap #[hours]
    options['minTrackLength'] = minTrackLength #[hours]
    options['interpTimeStart'] = interpTimeStart # 'Original' or 'Rounded' (start interpolatation at original time or rounded time, e.g. 01:20 for 10 minute intervals
    options['interpTimeInt'] = interpTimeInt #[hours]
    options['speedMax'] = speedMax #[m/s]

    description = (f"Drifter data in netCDF file format created by Fisheries and Oceans Canada for use with the drift-tool. "
                   f"Tracks split at groundings and when time gap exceeds {options['splitTimeGap']:3.2f} hours. "
                   f"Data interpolated to {options['interpTimeInt']:3.2f} hour time interval. ")

    fieldsMeta = ['ID','buoyid','track_number','IMEI','wmoid','type','approximate_drogue_depth_meters','region',
                  'launch_date','duration_days','distance_km',
                  'first_date_observation','last_date_observation',
                  'first_latitude_observation','last_latitude_observation',
                  'first_longitude_observation','last_longitude_observation',
                  'lat_min', 'lat_max','lon_min','lon_max','atSea_min','atSea_max',
                  'sst_min','sst_max','bp_min','bp_max',
                  'admin_mission', 'admin_agency', 'admin_country', 'admin_project' , 'admin_scientist', 'admin_platform',
                  'raw_data_file','data_description','qc_comment']


    #%% Set up output directory
    dataDirOut = os.path.join(dataDirOut, idStr)
    dataDir = os.path.join(dataDir, idStr)
    figDir = os.path.join(figDir, idStr)
    print(f"WRITING DRIFT-TOOL FILES FOR {idStr}")
    if not os.path.exists(dataDirOut):
        print(f"  Creating directory: {dataDirOut}")
        os.makedirs(dataDirOut)
    else:
        print(f"  Clearing directory: {dataDirOut}")
        files = glob.glob(f"{dataDirOut}/*")
        for f in files:
            os.remove(f)

    #%% Get unique IDs
    allFiles = glob.glob(dataDir + '/*')
    ids = []
    files = []
    for ii,file in enumerate(allFiles):

        # Open netcdf file        
        ds =  xr.open_dataset(file)
    
        # Get ID and file
        ids.append(ds.attrs['ID'])
        files.append(os.path.basename(file))
 
    
    dfFiles = pd.DataFrame({'ID': ids, 'fileName':files}).sort_values(by='fileName')

    idsU = sorted(set(dfFiles['ID']))

    #%% Process data for each unique ID
    for idNow in idsU:
        print('========================================')
        print('ID: '+ idNow)
        print('========================================')    
    
        filesID = dfFiles[dfFiles.ID == idNow].fileName.values
    
        tracksOrig = []    
        tracks = []
    
        #%% load files with matching IDs
        for ff,file in enumerate(filesID):
            print(f"Loading file {ff+1} of {len(filesID)}: {file}")
        
            # Open netcdf file        
            ds =  xr.open_dataset(dataDir+'/'+file)
        
            # Put data and metadata into dataframes
            dfTrack = ds.to_dataframe()
            if ff == 0:
                dfMetaIn = pd.DataFrame.from_dict(ds.attrs,orient='index')
                filemeta = file
        
            # Split tracks by groundings and time gaps
            tracksSpl = split_track(dfTrack,options['maxAtSea'],options['splitTimeGap'])
        
            # concatenate
            tracks = tracks + tracksSpl
            tracksOrig.append(dfTrack)

        print('--------------------------------------')    
        print(f"TOTAL: {len(tracks)} tracks for this ID")
        print('--------------------------------------')
        #%% Remove atSea>1 (and drop empty tracks)
        tracks = remove_high_atSea(tracks,options['maxAtSea'])
    
        #%% Interpolate to time base
        tracks = resample_tracks(tracks,options['interpTimeInt'],options['interpTimeStart'])
      
        #%% Check for impossible speeds (should already be removed in water properties files)
        tracks = remove_bad_speeds(tracks,options['speedMax'])

        #%% Remove tracks that are too short
        tracks = remove_short_tracks(tracks,options['minTrackLength'])

        #%% Assign metadata (including buoyid)
        if len(tracks)>0:
            dfMetaOut = assign_metadata(tracks,dfMetaIn,fieldsMeta, description)
        else:
            dfMetaOut = dfMetaIn

        #%% plot
        plot_tracks(tracksOrig, tracks,dfMetaOut)
        if not os.path.exists(figDir):
            print(f"  Creating directory: {figDir}")
            os.makedirs(figDir)
        fileFig = figDir +'/'+ idNow.replace(' ','_') + '.png'
        print('Saving figure to: '+fileFig)
        plt.savefig(fileFig)

        #%% Save netcdf    
        if len(tracks)>0:
            save_netcdf(tracks,dataDirOut, dfMetaOut)

def main(args=sys.argv[1:]):
    arg_parser = argparse.ArgumentParser()

    arg_parser.add_argument(
        '--dataDir', type=str, required=True,
        help='Parent directory to write files to. NetCDF files are written to dataDirOut/idStr.')
    arg_parser.add_argument(
        '--dataDirOut', type=str, required=True,
        help='Parent directory with input files. NetCDF files in dataDir/idStr are used.')
    arg_parser.add_argument(
        '--idStr', type=str, required=True,
        help='Sub-directory for input drifter files.')
    arg_parser.add_argument(
        '--figDir', type=str, required=True,
        help='Parent directory to write plots to. Plots are written in figDir/idStr.')
    arg_parser.add_argument(
        '--maxAtSea', type=int, default=1,
        help='The maximum ATSEA value to include in the returned tracks. Typically 1.')
    arg_parser.add_argument(
        '--splitTimeGap', type=float, default=6.5,
        help='Time gap [hours] where to split tracks')
    arg_parser.add_argument(
        '--minTrackLength', type=float, default=6,
        help='Mininum track duration [hours].')
    arg_parser.add_argument(
        '--interpTimeStart', type=str, default='Rounded',
        help='Rounded or Original. Start interpolatation at original time or rounded time, e.g. 01:20 for 10 minute intervals.')
    arg_parser.add_argument(
        '--interpTimeInt', type=float, default=10./60.,
        help='Time interval for interpolated tracks in hours.')
    arg_parser.add_argument(
        '--speedMax', type=float, default=5,
        help='Maximum expected speed of drifters in m/s. Reject data with larger speeds.')

    config = arg_parser.parse_args()

    write_DriftTool_files(
        dataDir=config.dataDir,
        dataDirOut=config.dataDirOut,
        idStr=config.idStr,
        figDir=config.figDir,
        maxAtSea=config.maxAtSea,
        splitTimeGap=config.splitTimeGap,
        minTrackLength=config.minTrackLength,
        interpTimeStart=config.interpTimeStart,
        interpTimeInt=config.interpTimeInt,
        speedMax=config.speedMax
    )
    

            
if __name__=='__main__':
    main()
