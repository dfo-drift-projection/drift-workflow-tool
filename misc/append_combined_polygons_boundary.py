import os
import argparse
import yaml
import shapely
from shapely.geometry.polygon import Polygon
from shapely.ops import  unary_union
from shapely.geometry import mapping

########################################################################
# append_combined_polygons_boundary.py
# 
# Helper script to pull outer boundary of all polygons definitions in a
# polygons_file. The output is written back out to a modified version of 
# the polygons_file. The new zone, called 'Combined_Polygons_Boundary', 
# is appended for use with the drift-tool.
#
# should be run as:
# python append_combined_polygons_boundary.py --polygons_file "/path/to/file/polygon_definitions.yaml"
#
# Sample polygons file:
# polygons_file="/fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/
#                   drift-tool/miscellaneous/
#                   sample_polygons_definitions_file.yaml"
#
########################################################################

# grab the path to the polygons_file then open the file
parser = argparse.ArgumentParser()
parser.add_argument(
    '--polygons_file',
    type=str,
    help='path to the file containing polygon areas (ex: ../../examples'
    '/plotting/polygon_coordinates.yaml)'
    )
args = (parser.parse_args())

# create a filename for the output file
polygons_file = args.polygons_file
bname = os.path.basename(polygons_file).split('.')[0]
pathname = os.path.dirname(polygons_file)
outfile = bname + '_modified' + '.yaml'
outpath = os.path.join(pathname, outfile)
print('\nsaving data to:')
print(outpath)

# load the polygons
with open(polygons_file, 'r') as f:
    polygons = yaml.load(f,Loader=yaml.FullLoader)

# create a function to format the polygon information
def format_polygon_definitions(polygons, region_name):

    #convert the polygon to a more useable format for printing
    coords = mapping(polygons)['coordinates']

    with open(outpath, 'a') as file:
        pstr1 = '\n    ' + region_name + ':'
        file.write(pstr1)
        for pair in coords[0]:
            pstr = '\n        - [' + str(pair[0]) + ',' + str(pair[1]) + ']'
            file.write(pstr)

# convert the lists of coordinates to shapely polygons
regions = {
            zone: Polygon(polygons['polygon_coords'][zone])
                for zone in polygons['polygon_coords'].keys()
            }

# get a list of polygons
polygons_list = []
for zone_name in list(regions.keys()):
    polygons_list.append(regions[zone_name])

##loop though the polygon areas and the outer boundary

#add a header for the list
with open(outpath, 'w') as file:
    file.write('polygon_coords:')

# print the original zones
for zone_name in list(regions.keys()):
    format_polygon_definitions(regions[zone_name], zone_name)
    
# find the outer boundary of all the polygons combined
outer_boundary_polygons = unary_union(polygons_list)
format_polygon_definitions(outer_boundary_polygons, 'Combined_Polygons_Boundary')

